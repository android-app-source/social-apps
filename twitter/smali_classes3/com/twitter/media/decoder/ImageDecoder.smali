.class public abstract Lcom/twitter/media/decoder/ImageDecoder;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/media/decoder/ImageDecoder$a;,
        Lcom/twitter/media/decoder/ImageDecoder$ScaleType;
    }
.end annotation


# static fields
.field private static final m:Lcom/twitter/media/decoder/ImageDecoder$a;

.field private static n:Lcom/twitter/media/decoder/ImageDecoder$a;


# instance fields
.field protected a:Lcom/twitter/media/decoder/d;

.field protected b:Landroid/graphics/Bitmap$Config;

.field protected c:I

.field protected d:Z

.field protected e:Landroid/net/Uri;

.field protected f:Landroid/content/Context;

.field protected g:Ljava/io/InputStream;

.field protected h:Ljava/io/File;

.field protected i:Z

.field protected j:Ljava/lang/String;

.field protected k:I

.field protected l:Lcom/twitter/media/util/ImageOrientation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/media/decoder/ImageDecoder$1;

    invoke-direct {v0}, Lcom/twitter/media/decoder/ImageDecoder$1;-><init>()V

    sput-object v0, Lcom/twitter/media/decoder/ImageDecoder;->m:Lcom/twitter/media/decoder/ImageDecoder$a;

    .line 49
    sget-object v0, Lcom/twitter/media/decoder/ImageDecoder;->m:Lcom/twitter/media/decoder/ImageDecoder$a;

    sput-object v0, Lcom/twitter/media/decoder/ImageDecoder;->n:Lcom/twitter/media/decoder/ImageDecoder$a;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Lcom/twitter/media/decoder/d;

    invoke-direct {v0}, Lcom/twitter/media/decoder/d;-><init>()V

    iput-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->a:Lcom/twitter/media/decoder/d;

    .line 58
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->b:Landroid/graphics/Bitmap$Config;

    .line 68
    sget-object v0, Lcom/twitter/media/util/ImageOrientation;->a:Lcom/twitter/media/util/ImageOrientation;

    iput-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->l:Lcom/twitter/media/util/ImageOrientation;

    .line 71
    return-void
.end method

.method public static a(Lcom/twitter/media/model/MediaFile;)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 2

    .prologue
    .line 112
    sget-object v0, Lcom/twitter/media/decoder/ImageDecoder;->n:Lcom/twitter/media/decoder/ImageDecoder$a;

    iget-object v1, p0, Lcom/twitter/media/model/MediaFile;->g:Lcom/twitter/media/model/MediaType;

    invoke-interface {v0, v1}, Lcom/twitter/media/decoder/ImageDecoder$a;->a(Lcom/twitter/media/model/MediaType;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/media/model/MediaFile;->f:Lcom/twitter/util/math/Size;

    .line 113
    invoke-virtual {v0, v1}, Lcom/twitter/media/decoder/ImageDecoder;->e(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    .line 114
    invoke-direct {v0, v1}, Lcom/twitter/media/decoder/ImageDecoder;->b(Ljava/io/File;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    .line 112
    return-object v0
.end method

.method private a(Lcom/twitter/media/util/ImageOrientation;)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->a:Lcom/twitter/media/decoder/d;

    invoke-virtual {v0, p1}, Lcom/twitter/media/decoder/d;->a(Lcom/twitter/media/util/ImageOrientation;)Lcom/twitter/media/decoder/d;

    .line 144
    return-object p0
.end method

.method public static a(Ljava/io/File;)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 2

    .prologue
    .line 107
    sget-object v0, Lcom/twitter/media/decoder/ImageDecoder;->n:Lcom/twitter/media/decoder/ImageDecoder$a;

    sget-object v1, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    invoke-interface {v0, v1}, Lcom/twitter/media/decoder/ImageDecoder$a;->a(Lcom/twitter/media/model/MediaType;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    invoke-direct {v0, p0}, Lcom/twitter/media/decoder/ImageDecoder;->b(Ljava/io/File;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/net/Uri;Z)Lcom/twitter/media/decoder/b;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 365
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->l:Lcom/twitter/media/util/ImageOrientation;

    .line 366
    sget-object v2, Lcom/twitter/media/util/ImageOrientation;->a:Lcom/twitter/media/util/ImageOrientation;

    if-ne v0, v2, :cond_0

    iget-boolean v2, p0, Lcom/twitter/media/decoder/ImageDecoder;->d:Z

    if-nez v2, :cond_0

    .line 367
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->f:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/util/ac;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    .line 368
    invoke-static {v0}, Lcom/twitter/media/util/f;->a(Ljava/io/File;)Lcom/twitter/media/util/ImageOrientation;

    move-result-object v0

    .line 370
    :cond_0
    iget v2, p0, Lcom/twitter/media/decoder/ImageDecoder;->k:I

    invoke-virtual {v0, v2}, Lcom/twitter/media/util/ImageOrientation;->c(I)Lcom/twitter/media/util/ImageOrientation;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/media/decoder/ImageDecoder;->a(Lcom/twitter/media/util/ImageOrientation;)Lcom/twitter/media/decoder/ImageDecoder;

    .line 373
    :try_start_0
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 374
    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 375
    if-eqz v2, :cond_1

    :try_start_1
    invoke-direct {p0, v2, p2}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/InputStream;Z)Lcom/twitter/media/decoder/b;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 379
    :goto_0
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 381
    :goto_1
    return-object v0

    :cond_1
    move-object v0, v1

    .line 375
    goto :goto_0

    .line 376
    :catch_0
    move-exception v0

    move-object v2, v1

    .line 377
    :goto_2
    :try_start_2
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 379
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    move-object v0, v1

    .line 381
    goto :goto_1

    .line 379
    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    .line 376
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private a(Ljava/io/File;Z)Lcom/twitter/media/decoder/b;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 404
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->l:Lcom/twitter/media/util/ImageOrientation;

    .line 405
    sget-object v2, Lcom/twitter/media/util/ImageOrientation;->a:Lcom/twitter/media/util/ImageOrientation;

    if-ne v0, v2, :cond_0

    iget-boolean v2, p0, Lcom/twitter/media/decoder/ImageDecoder;->d:Z

    if-nez v2, :cond_0

    .line 406
    invoke-static {p1}, Lcom/twitter/media/util/f;->a(Ljava/io/File;)Lcom/twitter/media/util/ImageOrientation;

    move-result-object v0

    .line 408
    :cond_0
    iget v2, p0, Lcom/twitter/media/decoder/ImageDecoder;->k:I

    invoke-virtual {v0, v2}, Lcom/twitter/media/util/ImageOrientation;->c(I)Lcom/twitter/media/util/ImageOrientation;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/media/decoder/ImageDecoder;->a(Lcom/twitter/media/util/ImageOrientation;)Lcom/twitter/media/decoder/ImageDecoder;

    .line 411
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    :try_start_1
    invoke-direct {p0, v2, p2}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/FileInputStream;Z)Lcom/twitter/media/decoder/b;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 416
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 418
    :goto_0
    return-object v0

    .line 413
    :catch_0
    move-exception v0

    move-object v2, v1

    .line 414
    :goto_1
    :try_start_2
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 416
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    move-object v0, v1

    .line 418
    goto :goto_0

    .line 416
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_2
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 413
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private a(Ljava/io/FileInputStream;Z)Lcom/twitter/media/decoder/b;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 424
    if-eqz p2, :cond_1

    .line 425
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/FileInputStream;)Lcom/twitter/util/math/Size;

    move-result-object v0

    .line 426
    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->e()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/media/decoder/ImageDecoder;->a:Lcom/twitter/media/decoder/d;

    .line 427
    invoke-virtual {v2, v0}, Lcom/twitter/media/decoder/d;->h(Lcom/twitter/util/math/Size;)Lcom/twitter/util/math/Size;

    move-result-object v0

    move-object v2, v0

    .line 428
    :goto_0
    new-instance v0, Lcom/twitter/media/decoder/b;

    invoke-direct {v0, v2}, Lcom/twitter/media/decoder/b;-><init>(Lcom/twitter/util/math/Size;)V

    .line 438
    :goto_1
    return-object v0

    .line 427
    :cond_0
    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    move-object v2, v0

    goto :goto_0

    .line 430
    :cond_1
    invoke-virtual {p0, p1}, Lcom/twitter/media/decoder/ImageDecoder;->b(Ljava/io/FileInputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 431
    if-eqz v2, :cond_2

    new-instance v0, Lcom/twitter/media/decoder/b;

    invoke-direct {v0, v2}, Lcom/twitter/media/decoder/b;-><init>(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 433
    :catch_0
    move-exception v0

    .line 434
    invoke-static {v0}, Lcpg;->a(Ljava/lang/OutOfMemoryError;)V

    :goto_2
    move-object v0, v1

    .line 438
    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 431
    goto :goto_1

    .line 435
    :catch_1
    move-exception v0

    .line 436
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private a(Ljava/io/InputStream;Z)Lcom/twitter/media/decoder/b;
    .locals 2

    .prologue
    .line 386
    instance-of v0, p1, Ljava/io/FileInputStream;

    if-eqz v0, :cond_0

    .line 387
    check-cast p1, Ljava/io/FileInputStream;

    invoke-direct {p0, p1, p2}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/FileInputStream;Z)Lcom/twitter/media/decoder/b;

    move-result-object v0

    .line 399
    :goto_0
    return-object v0

    .line 389
    :cond_0
    sget-object v0, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    iget-object v0, v0, Lcom/twitter/media/model/MediaType;->extension:Ljava/lang/String;

    invoke-static {p1, v0}, Lcqc;->a(Ljava/io/InputStream;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 390
    if-eqz v1, :cond_1

    .line 392
    :try_start_0
    invoke-direct {p0, v1, p2}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/File;Z)Lcom/twitter/media/decoder/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 395
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    throw v0

    .line 399
    :cond_1
    new-instance v1, Lcom/twitter/media/decoder/b;

    const/4 v0, 0x0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v1, v0}, Lcom/twitter/media/decoder/b;-><init>(Landroid/graphics/Bitmap;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private b(Ljava/io/File;)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/twitter/media/decoder/ImageDecoder;->h:Ljava/io/File;

    .line 138
    return-object p0
.end method


# virtual methods
.method public a()Lcom/twitter/media/decoder/ImageDecoder;
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->d:Z

    .line 257
    return-object p0
.end method

.method public a(I)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 1

    .prologue
    .line 215
    invoke-static {p1}, Lcom/twitter/util/math/Size;->a(I)Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/media/decoder/ImageDecoder;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/graphics/Bitmap$Config;)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->b:Landroid/graphics/Bitmap$Config;

    if-eq v0, p1, :cond_0

    .line 241
    iput-object p1, p0, Lcom/twitter/media/decoder/ImageDecoder;->b:Landroid/graphics/Bitmap$Config;

    .line 242
    iget v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->c:I

    if-eqz v0, :cond_0

    .line 244
    iget v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->c:I

    invoke-virtual {p0, v0}, Lcom/twitter/media/decoder/ImageDecoder;->d(I)Lcom/twitter/media/decoder/ImageDecoder;

    .line 247
    :cond_0
    return-object p0
.end method

.method public a(Lcom/twitter/media/decoder/ImageDecoder$ScaleType;)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 262
    iget-object v3, p0, Lcom/twitter/media/decoder/ImageDecoder;->a:Lcom/twitter/media/decoder/d;

    sget-object v0, Lcom/twitter/media/decoder/ImageDecoder$ScaleType;->b:Lcom/twitter/media/decoder/ImageDecoder$ScaleType;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/twitter/media/decoder/ImageDecoder$ScaleType;->c:Lcom/twitter/media/decoder/ImageDecoder$ScaleType;

    if-ne p1, v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-virtual {v3, v0}, Lcom/twitter/media/decoder/d;->a(Z)Lcom/twitter/media/decoder/d;

    .line 263
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->a:Lcom/twitter/media/decoder/d;

    sget-object v3, Lcom/twitter/media/decoder/ImageDecoder$ScaleType;->c:Lcom/twitter/media/decoder/ImageDecoder$ScaleType;

    if-ne p1, v3, :cond_2

    :goto_1
    invoke-virtual {v0, v2}, Lcom/twitter/media/decoder/d;->b(Z)Lcom/twitter/media/decoder/d;

    .line 264
    return-object p0

    :cond_1
    move v0, v1

    .line 262
    goto :goto_0

    :cond_2
    move v2, v1

    .line 263
    goto :goto_1
.end method

.method public a(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->a:Lcom/twitter/media/decoder/d;

    invoke-virtual {v0, p1}, Lcom/twitter/media/decoder/d;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/d;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/decoder/c;->a:Lcom/twitter/media/decoder/c;

    .line 170
    invoke-virtual {v0, v1}, Lcom/twitter/media/decoder/d;->a(Lcom/twitter/media/decoder/c;)Lcom/twitter/media/decoder/d;

    .line 171
    return-object p0
.end method

.method public a(Lcom/twitter/util/math/Size;Lcom/twitter/media/decoder/c;)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->a:Lcom/twitter/media/decoder/d;

    invoke-virtual {v0, p1}, Lcom/twitter/media/decoder/d;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/d;

    move-result-object v0

    .line 184
    invoke-virtual {v0, p2}, Lcom/twitter/media/decoder/d;->a(Lcom/twitter/media/decoder/c;)Lcom/twitter/media/decoder/d;

    .line 185
    return-object p0
.end method

.method public a(Lcom/twitter/util/math/c;)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->a:Lcom/twitter/media/decoder/d;

    invoke-virtual {v0, p1}, Lcom/twitter/media/decoder/d;->a(Lcom/twitter/util/math/c;)Lcom/twitter/media/decoder/d;

    .line 270
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/twitter/media/decoder/ImageDecoder;->j:Ljava/lang/String;

    .line 321
    return-object p0
.end method

.method public a(Z)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->a:Lcom/twitter/media/decoder/d;

    invoke-virtual {v0, p1}, Lcom/twitter/media/decoder/d;->c(Z)Lcom/twitter/media/decoder/d;

    .line 302
    return-object p0
.end method

.method protected abstract a(Ljava/io/FileInputStream;)Lcom/twitter/util/math/Size;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public b()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 329
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/media/decoder/ImageDecoder;->c(Z)Lcom/twitter/media/decoder/b;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/media/decoder/b;->a:Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract b(Ljava/io/FileInputStream;)Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public b(I)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 1

    .prologue
    .line 223
    invoke-static {p1}, Lcom/twitter/util/math/Size;->a(I)Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/media/decoder/ImageDecoder;->b(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->a:Lcom/twitter/media/decoder/d;

    invoke-virtual {v0, p1}, Lcom/twitter/media/decoder/d;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/d;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/decoder/c;->b:Lcom/twitter/media/decoder/c;

    .line 195
    invoke-virtual {v0, v1}, Lcom/twitter/media/decoder/d;->a(Lcom/twitter/media/decoder/c;)Lcom/twitter/media/decoder/d;

    .line 196
    return-object p0
.end method

.method public b(Z)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 0

    .prologue
    .line 311
    iput-boolean p1, p0, Lcom/twitter/media/decoder/ImageDecoder;->i:Z

    .line 312
    return-object p0
.end method

.method public c(I)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 1

    .prologue
    .line 231
    invoke-static {p1}, Lcom/twitter/util/math/Size;->a(I)Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/media/decoder/ImageDecoder;->c(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    return-object v0
.end method

.method public c(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->a:Lcom/twitter/media/decoder/d;

    invoke-virtual {v0, p1}, Lcom/twitter/media/decoder/d;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/d;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/decoder/c;->c:Lcom/twitter/media/decoder/c;

    .line 206
    invoke-virtual {v0, v1}, Lcom/twitter/media/decoder/d;->a(Lcom/twitter/media/decoder/c;)Lcom/twitter/media/decoder/d;

    .line 207
    return-object p0
.end method

.method c(Z)Lcom/twitter/media/decoder/b;
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->e:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->e:Landroid/net/Uri;

    invoke-direct {p0, v0, p1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Landroid/net/Uri;Z)Lcom/twitter/media/decoder/b;

    move-result-object v0

    .line 357
    :goto_0
    return-object v0

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->g:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 355
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->g:Ljava/io/InputStream;

    invoke-direct {p0, v0, p1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/InputStream;Z)Lcom/twitter/media/decoder/b;

    move-result-object v0

    goto :goto_0

    .line 356
    :cond_1
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->h:Ljava/io/File;

    if-eqz v0, :cond_2

    .line 357
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->h:Ljava/io/File;

    invoke-direct {p0, v0, p1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/File;Z)Lcom/twitter/media/decoder/b;

    move-result-object v0

    goto :goto_0

    .line 359
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "No bitmap source specified."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c()Lcom/twitter/util/math/Size;
    .locals 1

    .prologue
    .line 338
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/media/decoder/ImageDecoder;->c(Z)Lcom/twitter/media/decoder/b;

    move-result-object v0

    .line 339
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/media/decoder/b;->b:Lcom/twitter/util/math/Size;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    goto :goto_0
.end method

.method public d(I)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->b:Landroid/graphics/Bitmap$Config;

    invoke-static {v0}, Lcom/twitter/media/util/a;->a(Landroid/graphics/Bitmap$Config;)I

    move-result v0

    .line 288
    iget-object v1, p0, Lcom/twitter/media/decoder/ImageDecoder;->a:Lcom/twitter/media/decoder/d;

    div-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/twitter/media/decoder/d;->a(I)Lcom/twitter/media/decoder/d;

    .line 289
    iput p1, p0, Lcom/twitter/media/decoder/ImageDecoder;->c:I

    .line 290
    return-object p0
.end method

.method public d(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->a:Lcom/twitter/media/decoder/d;

    invoke-virtual {v0, p1}, Lcom/twitter/media/decoder/d;->b(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/d;

    .line 276
    return-object p0
.end method

.method public e(I)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 0

    .prologue
    .line 295
    iput p1, p0, Lcom/twitter/media/decoder/ImageDecoder;->k:I

    .line 296
    return-object p0
.end method

.method public e(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/ImageDecoder;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/twitter/media/decoder/ImageDecoder;->a:Lcom/twitter/media/decoder/d;

    invoke-virtual {v0, p1}, Lcom/twitter/media/decoder/d;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/d;

    .line 282
    return-object p0
.end method
