.class public Lcom/twitter/media/decoder/c;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcom/twitter/media/decoder/c;

.field public static final b:Lcom/twitter/media/decoder/c;

.field public static final c:Lcom/twitter/media/decoder/c;


# instance fields
.field public final d:F

.field public final e:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 6
    invoke-static {v1, v1}, Lcom/twitter/media/decoder/c;->a(FF)Lcom/twitter/media/decoder/c;

    move-result-object v0

    sput-object v0, Lcom/twitter/media/decoder/c;->a:Lcom/twitter/media/decoder/c;

    .line 7
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-static {v0, v1}, Lcom/twitter/media/decoder/c;->a(FF)Lcom/twitter/media/decoder/c;

    move-result-object v0

    sput-object v0, Lcom/twitter/media/decoder/c;->b:Lcom/twitter/media/decoder/c;

    .line 8
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v1, v0}, Lcom/twitter/media/decoder/c;->a(FF)Lcom/twitter/media/decoder/c;

    move-result-object v0

    sput-object v0, Lcom/twitter/media/decoder/c;->c:Lcom/twitter/media/decoder/c;

    return-void
.end method

.method private constructor <init>(FF)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p1, p0, Lcom/twitter/media/decoder/c;->d:F

    .line 20
    iput p2, p0, Lcom/twitter/media/decoder/c;->e:F

    .line 21
    return-void
.end method

.method public static a(FF)Lcom/twitter/media/decoder/c;
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/twitter/media/decoder/c;

    invoke-direct {v0, p0, p1}, Lcom/twitter/media/decoder/c;-><init>(FF)V

    return-object v0
.end method
