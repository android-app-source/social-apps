.class public Lcom/twitter/media/filters/c;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/media/filters/a;


# direct methods
.method private static a()Lcom/twitter/media/filters/a;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/twitter/media/filters/c;->a:Lcom/twitter/media/filters/a;

    if-nez v0, :cond_0

    .line 136
    new-instance v0, Lcom/twitter/media/filters/a;

    invoke-direct {v0}, Lcom/twitter/media/filters/a;-><init>()V

    sput-object v0, Lcom/twitter/media/filters/c;->a:Lcom/twitter/media/filters/a;

    .line 137
    sget-object v0, Lcom/twitter/media/filters/c;->a:Lcom/twitter/media/filters/a;

    invoke-virtual {v0}, Lcom/twitter/media/filters/a;->a()Z

    .line 139
    :cond_0
    sget-object v0, Lcom/twitter/media/filters/c;->a:Lcom/twitter/media/filters/a;

    return-object v0
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 41
    if-nez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 19
    const-class v1, Lcom/twitter/media/filters/c;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/twitter/media/NativeInit;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_0

    .line 21
    invoke-static {p0}, Lcom/twitter/media/filters/Filters;->a(Landroid/content/Context;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 19
    :goto_0
    monitor-exit v1

    return v0

    .line 21
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 19
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Ljava/io/File;IIIZFF)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 104
    if-gez p5, :cond_0

    .line 120
    :goto_0
    return v6

    .line 107
    :cond_0
    new-instance v0, Lcom/twitter/media/filters/Filters;

    invoke-static {}, Lcom/twitter/media/filters/c;->a()Lcom/twitter/media/filters/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/media/filters/Filters;-><init>(Lcom/twitter/media/filters/a;)V

    .line 109
    const/16 v1, 0x8

    if-le p5, v1, :cond_1

    const/4 v1, 0x1

    .line 112
    :goto_1
    invoke-virtual {v0, p0, v1}, Lcom/twitter/media/filters/Filters;->a(Landroid/content/Context;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 113
    invoke-virtual {v0, p1, p3, p4, p6}, Lcom/twitter/media/filters/Filters;->a(Landroid/net/Uri;IIZ)I

    move-result v2

    .line 114
    if-lez v2, :cond_2

    move v1, p5

    move-object v3, p2

    move v4, p7

    move v5, p8

    .line 115
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/media/filters/Filters;->a(IILjava/io/File;FF)Z

    move-result v1

    .line 118
    :goto_2
    invoke-virtual {v0, v6}, Lcom/twitter/media/filters/Filters;->a(Z)V

    move v6, v1

    .line 120
    goto :goto_0

    :cond_1
    move v1, v6

    .line 109
    goto :goto_1

    :cond_2
    move v1, v6

    goto :goto_2
.end method
