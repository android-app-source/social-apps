.class public Lcom/twitter/media/service/tasks/ResizeTask;
.super Lcom/twitter/media/service/core/MediaServiceTask;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/media/service/tasks/ResizeTask;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/io/File;

.field private b:Ljava/io/File;

.field private c:I

.field private d:I

.field private e:Z

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/media/service/tasks/ResizeTask$1;

    invoke-direct {v0}, Lcom/twitter/media/service/tasks/ResizeTask$1;-><init>()V

    sput-object v0, Lcom/twitter/media/service/tasks/ResizeTask;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/twitter/media/service/core/MediaServiceTask;-><init>()V

    .line 49
    invoke-virtual {p0, p1}, Lcom/twitter/media/service/tasks/ResizeTask;->a(Landroid/os/Parcel;)V

    .line 50
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/twitter/media/service/tasks/ResizeTask$1;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/twitter/media/service/tasks/ResizeTask;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/io/File;IIZ)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/twitter/media/service/core/MediaServiceTask;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/twitter/media/service/tasks/ResizeTask;->a:Ljava/io/File;

    .line 55
    iput-object p2, p0, Lcom/twitter/media/service/tasks/ResizeTask;->b:Ljava/io/File;

    .line 56
    iput p3, p0, Lcom/twitter/media/service/tasks/ResizeTask;->c:I

    .line 57
    iput p4, p0, Lcom/twitter/media/service/tasks/ResizeTask;->d:I

    .line 58
    const-string/jumbo v0, "media_service_native_resize_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->e:Z

    .line 59
    iput-boolean p5, p0, Lcom/twitter/media/service/tasks/ResizeTask;->f:Z

    .line 60
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->f:Z

    if-eqz v0, :cond_2

    .line 162
    iget-object v1, p0, Lcom/twitter/media/service/tasks/ResizeTask;->a:Ljava/io/File;

    iget-object v2, p0, Lcom/twitter/media/service/tasks/ResizeTask;->b:Ljava/io/File;

    if-eqz p1, :cond_1

    sget-object v0, Lcom/twitter/media/util/ImageOrientation;->a:Lcom/twitter/media/util/ImageOrientation;

    :goto_0
    invoke-static {v1, v2, v0}, Lcom/twitter/media/util/f;->a(Ljava/io/File;Ljava/io/File;Lcom/twitter/media/util/ImageOrientation;)Z

    .line 171
    :cond_0
    :goto_1
    return-void

    .line 162
    :cond_1
    sget-object v0, Lcom/twitter/media/util/ImageOrientation;->b:Lcom/twitter/media/util/ImageOrientation;

    goto :goto_0

    .line 164
    :cond_2
    if-eqz p1, :cond_0

    .line 166
    iget-object v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->a:Ljava/io/File;

    invoke-static {v0}, Lcom/twitter/media/util/f;->a(Ljava/io/File;)Lcom/twitter/media/util/ImageOrientation;

    move-result-object v0

    .line 167
    sget-object v1, Lcom/twitter/media/util/ImageOrientation;->a:Lcom/twitter/media/util/ImageOrientation;

    if-eq v0, v1, :cond_0

    .line 168
    iget-object v1, p0, Lcom/twitter/media/service/tasks/ResizeTask;->b:Ljava/io/File;

    invoke-static {v1, v0}, Lcom/twitter/media/util/f;->a(Ljava/io/File;Lcom/twitter/media/util/ImageOrientation;)Z

    goto :goto_1
.end method

.method private c()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 100
    iget-boolean v1, p0, Lcom/twitter/media/service/tasks/ResizeTask;->e:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/twitter/media/MediaUtils;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/twitter/media/service/tasks/ResizeTask;->a:Ljava/io/File;

    invoke-static {v1}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/File;)Lcom/twitter/media/ImageInfo;

    move-result-object v1

    .line 102
    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/twitter/media/service/tasks/ResizeTask;->a:Ljava/io/File;

    iget-object v2, p0, Lcom/twitter/media/service/tasks/ResizeTask;->b:Ljava/io/File;

    iget v3, p0, Lcom/twitter/media/service/tasks/ResizeTask;->c:I

    iget v4, p0, Lcom/twitter/media/service/tasks/ResizeTask;->d:I

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/File;Ljava/io/File;II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    invoke-direct {p0, v0}, Lcom/twitter/media/service/tasks/ResizeTask;->a(Z)V

    .line 116
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 120
    iget-object v1, p0, Lcom/twitter/media/service/tasks/ResizeTask;->a:Ljava/io/File;

    invoke-static {v1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/File;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/media/decoder/ImageDecoder;->c()Lcom/twitter/util/math/Size;

    move-result-object v1

    .line 121
    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 156
    :cond_0
    :goto_0
    return v0

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/twitter/media/service/tasks/ResizeTask;->a:Ljava/io/File;

    invoke-static {v1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/File;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/media/service/tasks/ResizeTask;->c:I

    .line 129
    invoke-virtual {v1, v2}, Lcom/twitter/media/decoder/ImageDecoder;->a(I)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/media/decoder/ImageDecoder;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 133
    if-nez v1, :cond_3

    .line 134
    iget-object v1, p0, Lcom/twitter/media/service/tasks/ResizeTask;->a:Ljava/io/File;

    invoke-static {v1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/File;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/media/service/tasks/ResizeTask;->c:I

    .line 135
    invoke-virtual {v1, v2}, Lcom/twitter/media/decoder/ImageDecoder;->c(I)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/media/decoder/ImageDecoder;->a()Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/media/decoder/ImageDecoder;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 136
    const/4 v2, 0x1

    .line 140
    :goto_1
    if-nez v1, :cond_2

    .line 141
    iget-object v1, p0, Lcom/twitter/media/service/tasks/ResizeTask;->a:Ljava/io/File;

    invoke-static {v1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/File;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    iget v3, p0, Lcom/twitter/media/service/tasks/ResizeTask;->c:I

    .line 142
    invoke-virtual {v1, v3}, Lcom/twitter/media/decoder/ImageDecoder;->b(I)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/media/decoder/ImageDecoder;->a()Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/media/decoder/ImageDecoder;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 145
    :cond_2
    if-eqz v1, :cond_0

    .line 150
    iget-object v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->b:Ljava/io/File;

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    iget v4, p0, Lcom/twitter/media/service/tasks/ResizeTask;->d:I

    invoke-static {v1, v0, v3, v4}, Lcom/twitter/media/util/a;->a(Landroid/graphics/Bitmap;Ljava/io/File;Landroid/graphics/Bitmap$CompressFormat;I)Z

    move-result v0

    .line 153
    invoke-direct {p0, v2}, Lcom/twitter/media/service/tasks/ResizeTask;->a(Z)V

    .line 155
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    invoke-super {p0, p1}, Lcom/twitter/media/service/core/MediaServiceTask;->a(Landroid/os/Parcel;)V

    .line 65
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->a:Ljava/io/File;

    .line 66
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->b:Ljava/io/File;

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->c:I

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->d:I

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->e:Z

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/twitter/media/service/tasks/ResizeTask;->f:Z

    .line 71
    return-void

    :cond_0
    move v0, v2

    .line 69
    goto :goto_0

    :cond_1
    move v1, v2

    .line 70
    goto :goto_1
.end method

.method protected b(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/twitter/media/service/tasks/ResizeTask;->c()Z

    move-result v0

    .line 87
    if-nez v0, :cond_0

    .line 88
    invoke-direct {p0}, Lcom/twitter/media/service/tasks/ResizeTask;->d()Z

    move-result v0

    .line 90
    :cond_0
    return v0
.end method

.method protected c(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/twitter/media/service/tasks/ResizeTask;->d()Z

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    invoke-super {p0, p1, p2}, Lcom/twitter/media/service/core/MediaServiceTask;->writeToParcel(Landroid/os/Parcel;I)V

    .line 76
    iget-object v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78
    iget v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 79
    iget v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 80
    iget-boolean v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->e:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 81
    iget-boolean v0, p0, Lcom/twitter/media/service/tasks/ResizeTask;->f:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    return-void

    :cond_0
    move v0, v2

    .line 80
    goto :goto_0

    :cond_1
    move v1, v2

    .line 81
    goto :goto_1
.end method
