.class public Lcom/twitter/media/service/tasks/CropTask;
.super Lcom/twitter/media/service/core/MediaServiceTask;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/media/service/tasks/CropTask;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/io/File;

.field public c:Lcom/twitter/util/math/c;

.field public d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/media/service/tasks/CropTask$1;

    invoke-direct {v0}, Lcom/twitter/media/service/tasks/CropTask$1;-><init>()V

    sput-object v0, Lcom/twitter/media/service/tasks/CropTask;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/twitter/media/service/core/MediaServiceTask;-><init>()V

    .line 44
    invoke-virtual {p0, p1}, Lcom/twitter/media/service/tasks/CropTask;->a(Landroid/os/Parcel;)V

    .line 45
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/twitter/media/service/tasks/CropTask$1;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/twitter/media/service/tasks/CropTask;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/File;Lcom/twitter/util/math/c;I)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/twitter/media/service/core/MediaServiceTask;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/twitter/media/service/tasks/CropTask;->a:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/twitter/media/service/tasks/CropTask;->b:Ljava/io/File;

    .line 51
    iput-object p3, p0, Lcom/twitter/media/service/tasks/CropTask;->c:Lcom/twitter/util/math/c;

    .line 52
    iput p4, p0, Lcom/twitter/media/service/tasks/CropTask;->d:I

    .line 53
    return-void
.end method

.method private a(Lcom/twitter/util/math/Size;)Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/media/service/tasks/CropTask;->c:Lcom/twitter/util/math/c;

    invoke-virtual {v0, p1}, Lcom/twitter/util/math/c;->a(Lcom/twitter/util/math/Size;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/util/math/Size;->h()Landroid/graphics/Rect;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/math/b;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method private c()Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 91
    iget-object v0, p0, Lcom/twitter/media/service/tasks/CropTask;->a:Ljava/lang/String;

    invoke-static {v0}, Lcqc;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 92
    new-instance v4, Ljava/io/File;

    iget-object v0, p0, Lcom/twitter/media/service/tasks/CropTask;->a:Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 93
    invoke-static {v4}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/File;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/decoder/ImageDecoder;->b()Landroid/graphics/Bitmap;

    move-result-object v5

    .line 95
    if-nez v5, :cond_1

    .line 130
    :cond_0
    :goto_0
    return v2

    .line 100
    :cond_1
    invoke-static {v5}, Lcom/twitter/util/math/Size;->a(Landroid/graphics/Bitmap;)Lcom/twitter/util/math/Size;

    move-result-object v6

    .line 101
    invoke-direct {p0, v6}, Lcom/twitter/media/service/tasks/CropTask;->a(Lcom/twitter/util/math/Size;)Landroid/graphics/Rect;

    move-result-object v7

    .line 102
    invoke-virtual {v6}, Lcom/twitter/util/math/Size;->e()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {v7}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 103
    :goto_1
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v6}, Lcom/twitter/util/math/Size;->a()I

    move-result v9

    if-ne v8, v9, :cond_2

    .line 104
    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v8

    invoke-virtual {v6}, Lcom/twitter/util/math/Size;->b()I

    move-result v6

    if-eq v8, v6, :cond_6

    .line 106
    :cond_2
    :goto_2
    if-eqz v0, :cond_8

    if-eqz v1, :cond_8

    .line 107
    const/4 v0, 0x0

    invoke-static {v5, v7, v0, v2}, Lcom/twitter/media/util/a;->a(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 108
    if-eqz v1, :cond_3

    .line 109
    const-string/jumbo v0, "image/png"

    .line 110
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    .line 112
    :goto_3
    iget-object v2, p0, Lcom/twitter/media/service/tasks/CropTask;->b:Ljava/io/File;

    const/16 v3, 0x5f

    invoke-static {v1, v2, v0, v3}, Lcom/twitter/media/util/a;->a(Landroid/graphics/Bitmap;Ljava/io/File;Landroid/graphics/Bitmap$CompressFormat;I)Z

    move-result v2

    .line 113
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 119
    :cond_3
    :goto_4
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 121
    if-eqz v2, :cond_4

    iget v0, p0, Lcom/twitter/media/service/tasks/CropTask;->d:I

    if-eqz v0, :cond_4

    .line 122
    iget-object v0, p0, Lcom/twitter/media/service/tasks/CropTask;->b:Ljava/io/File;

    iget v1, p0, Lcom/twitter/media/service/tasks/CropTask;->d:I

    invoke-static {v0, v1}, Lcom/twitter/media/util/f;->a(Ljava/io/File;I)Z

    .line 125
    :cond_4
    if-nez v2, :cond_0

    .line 127
    iget-object v0, p0, Lcom/twitter/media/service/tasks/CropTask;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    :cond_5
    move v0, v2

    .line 102
    goto :goto_1

    :cond_6
    move v1, v2

    .line 104
    goto :goto_2

    .line 110
    :cond_7
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    goto :goto_3

    .line 116
    :cond_8
    iget-object v0, p0, Lcom/twitter/media/service/tasks/CropTask;->b:Ljava/io/File;

    invoke-static {v4, v0}, Lcqc;->b(Ljava/io/File;Ljava/io/File;)Z

    move-result v2

    goto :goto_4
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/twitter/media/service/core/MediaServiceTask;->a(Landroid/os/Parcel;)V

    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/service/tasks/CropTask;->a:Ljava/lang/String;

    .line 59
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/media/service/tasks/CropTask;->b:Ljava/io/File;

    .line 60
    sget-object v0, Lcom/twitter/util/math/c;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/math/c;

    iput-object v0, p0, Lcom/twitter/media/service/tasks/CropTask;->c:Lcom/twitter/util/math/c;

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/media/service/tasks/CropTask;->d:I

    .line 62
    return-void
.end method

.method protected b(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/twitter/media/service/tasks/CropTask;->c()Z

    move-result v0

    return v0
.end method

.method protected c(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/twitter/media/service/tasks/CropTask;->b(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, Lcom/twitter/media/service/core/MediaServiceTask;->writeToParcel(Landroid/os/Parcel;I)V

    .line 67
    iget-object v0, p0, Lcom/twitter/media/service/tasks/CropTask;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/twitter/media/service/tasks/CropTask;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/twitter/media/service/tasks/CropTask;->c:Lcom/twitter/util/math/c;

    sget-object v1, Lcom/twitter/util/math/c;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 70
    iget v0, p0, Lcom/twitter/media/service/tasks/CropTask;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 71
    return-void
.end method
