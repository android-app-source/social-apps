.class Lcom/twitter/media/service/core/MediaServiceClient$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/media/service/core/MediaServiceClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/media/service/core/MediaServiceClient;

.field private b:Landroid/os/Messenger;

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/twitter/media/service/core/MediaServiceClient$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/twitter/media/service/core/MediaServiceClient;)V
    .locals 1

    .prologue
    .line 88
    iput-object p1, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->c:Landroid/util/SparseArray;

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/media/service/core/MediaServiceClient;Lcom/twitter/media/service/core/MediaServiceClient$1;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/twitter/media/service/core/MediaServiceClient$a;-><init>(Lcom/twitter/media/service/core/MediaServiceClient;)V

    return-void
.end method

.method private c(Lcom/twitter/media/service/core/MediaServiceClient$c;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 145
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-static {v0}, Lcom/twitter/media/service/core/MediaServiceClient;->c(Lcom/twitter/media/service/core/MediaServiceClient;)Ljava/util/Map;

    move-result-object v1

    monitor-enter v1

    .line 146
    :try_start_0
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-static {v0}, Lcom/twitter/media/service/core/MediaServiceClient;->c(Lcom/twitter/media/service/core/MediaServiceClient;)Ljava/util/Map;

    move-result-object v0

    iget v2, p1, Lcom/twitter/media/service/core/MediaServiceClient$c;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    iget v0, p1, Lcom/twitter/media/service/core/MediaServiceClient$c;->b:I

    iget v1, p1, Lcom/twitter/media/service/core/MediaServiceClient$c;->a:I

    const/4 v2, 0x0

    .line 149
    invoke-static {v3, v0, v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 150
    iget-object v1, p1, Lcom/twitter/media/service/core/MediaServiceClient$c;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 151
    iget-object v1, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-static {v1}, Lcom/twitter/media/service/core/MediaServiceClient;->d(Lcom/twitter/media/service/core/MediaServiceClient;)Landroid/os/Messenger;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 153
    :try_start_1
    iget-object v1, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->b:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 160
    :goto_0
    return-void

    .line 147
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 154
    :catch_0
    move-exception v0

    .line 155
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-static {v0}, Lcom/twitter/media/service/core/MediaServiceClient;->c(Lcom/twitter/media/service/core/MediaServiceClient;)Ljava/util/Map;

    move-result-object v1

    monitor-enter v1

    .line 156
    :try_start_3
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-static {v0}, Lcom/twitter/media/service/core/MediaServiceClient;->c(Lcom/twitter/media/service/core/MediaServiceClient;)Ljava/util/Map;

    move-result-object v0

    iget v2, p1, Lcom/twitter/media/service/core/MediaServiceClient$c;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 158
    invoke-static {p1, v3}, Lcom/twitter/media/service/core/MediaServiceClient;->a(Lcom/twitter/media/service/core/MediaServiceClient$c;Landroid/os/Bundle;)V

    goto :goto_0

    .line 157
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-static {v0}, Lcom/twitter/media/service/core/MediaServiceClient;->a(Lcom/twitter/media/service/core/MediaServiceClient;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-static {v2}, Lcom/twitter/media/service/core/MediaServiceClient;->a(Lcom/twitter/media/service/core/MediaServiceClient;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/twitter/media/service/core/MediaService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 94
    return-void
.end method

.method public declared-synchronized a(Lcom/twitter/media/service/core/MediaServiceClient$c;)V
    .locals 2

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->b:Landroid/os/Messenger;

    if-eqz v0, :cond_0

    .line 165
    invoke-direct {p0, p1}, Lcom/twitter/media/service/core/MediaServiceClient$a;->c(Lcom/twitter/media/service/core/MediaServiceClient$c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :goto_0
    monitor-exit p0

    return-void

    .line 168
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->c:Landroid/util/SparseArray;

    iget v1, p1, Lcom/twitter/media/service/core/MediaServiceClient$c;->a:I

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lcom/twitter/media/service/core/MediaServiceClient$c;)V
    .locals 2

    .prologue
    .line 173
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->c:Landroid/util/SparseArray;

    iget v1, p1, Lcom/twitter/media/service/core/MediaServiceClient$c;->a:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    monitor-exit p0

    return-void

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    iput-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->b:Landroid/os/Messenger;

    .line 105
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 106
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/service/core/MediaServiceClient$c;

    invoke-direct {p0, v0}, Lcom/twitter/media/service/core/MediaServiceClient$a;->c(Lcom/twitter/media/service/core/MediaServiceClient$c;)V

    .line 106
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-static {v0}, Lcom/twitter/media/service/core/MediaServiceClient;->b(Lcom/twitter/media/service/core/MediaServiceClient;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    monitor-exit p0

    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 117
    .line 120
    monitor-enter p0

    .line 121
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->b:Landroid/os/Messenger;

    .line 125
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-static {v0}, Lcom/twitter/media/service/core/MediaServiceClient;->c(Lcom/twitter/media/service/core/MediaServiceClient;)Ljava/util/Map;

    move-result-object v2

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 126
    :try_start_1
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-static {v0}, Lcom/twitter/media/service/core/MediaServiceClient;->c(Lcom/twitter/media/service/core/MediaServiceClient;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-static {v3}, Lcom/twitter/media/service/core/MediaServiceClient;->c(Lcom/twitter/media/service/core/MediaServiceClient;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 128
    iget-object v3, p0, Lcom/twitter/media/service/core/MediaServiceClient$a;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-static {v3}, Lcom/twitter/media/service/core/MediaServiceClient;->c(Lcom/twitter/media/service/core/MediaServiceClient;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 130
    :goto_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 131
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 132
    if-eqz v0, :cond_0

    .line 134
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/service/core/MediaServiceClient$c;

    .line 135
    invoke-static {v0, v1}, Lcom/twitter/media/service/core/MediaServiceClient;->a(Lcom/twitter/media/service/core/MediaServiceClient$c;Landroid/os/Bundle;)V

    goto :goto_1

    .line 130
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 131
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 139
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/media/service/core/MediaServiceClient$a;->a()V

    .line 140
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
