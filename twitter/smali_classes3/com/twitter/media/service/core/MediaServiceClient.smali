.class public Lcom/twitter/media/service/core/MediaServiceClient;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/media/service/core/MediaServiceClient$b;,
        Lcom/twitter/media/service/core/MediaServiceClient$NativeCrashException;,
        Lcom/twitter/media/service/core/MediaServiceClient$a;,
        Lcom/twitter/media/service/core/MediaServiceClient$d;,
        Lcom/twitter/media/service/core/MediaServiceClient$c;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/media/service/core/MediaServiceClient;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/twitter/media/service/core/MediaServiceClient$c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/media/service/core/MediaServiceClient$a;

.field private final e:Landroid/os/HandlerThread;

.field private f:Landroid/os/Messenger;

.field private g:Landroid/os/Handler;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient;->c:Ljava/util/Map;

    .line 38
    new-instance v0, Lcom/twitter/media/service/core/MediaServiceClient$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/media/service/core/MediaServiceClient$a;-><init>(Lcom/twitter/media/service/core/MediaServiceClient;Lcom/twitter/media/service/core/MediaServiceClient$1;)V

    iput-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient;->d:Lcom/twitter/media/service/core/MediaServiceClient$a;

    .line 82
    iput-object p1, p0, Lcom/twitter/media/service/core/MediaServiceClient;->b:Landroid/content/Context;

    .line 83
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "MediaServiceHandler"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient;->e:Landroid/os/HandlerThread;

    .line 84
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 85
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient;->d:Lcom/twitter/media/service/core/MediaServiceClient$a;

    invoke-virtual {v0}, Lcom/twitter/media/service/core/MediaServiceClient$a;->a()V

    .line 86
    return-void
.end method

.method static synthetic a(Lcom/twitter/media/service/core/MediaServiceClient;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient;->b:Landroid/content/Context;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/media/service/core/MediaServiceClient;
    .locals 3

    .prologue
    .line 46
    const-class v1, Lcom/twitter/media/service/core/MediaServiceClient;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/media/service/core/MediaServiceClient;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/media/service/core/MediaServiceClient;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/media/service/core/MediaServiceClient;->a:Lcom/twitter/media/service/core/MediaServiceClient;

    .line 48
    const-class v0, Lcom/twitter/media/service/core/MediaServiceClient;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 50
    :cond_0
    sget-object v0, Lcom/twitter/media/service/core/MediaServiceClient;->a:Lcom/twitter/media/service/core/MediaServiceClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 184
    const/4 v0, 0x2

    const/4 v1, 0x0

    new-instance v2, Lcom/twitter/media/service/core/MediaServiceClient$1;

    invoke-direct {v2, p0}, Lcom/twitter/media/service/core/MediaServiceClient$1;-><init>(Lcom/twitter/media/service/core/MediaServiceClient;)V

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/media/service/core/MediaServiceClient;->a(ILandroid/os/Bundle;Lcom/twitter/media/service/core/MediaServiceClient$d;)I

    .line 195
    return-void
.end method

.method static synthetic a(Lcom/twitter/media/service/core/MediaServiceClient$c;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 30
    invoke-static {p0, p1}, Lcom/twitter/media/service/core/MediaServiceClient;->b(Lcom/twitter/media/service/core/MediaServiceClient$c;Landroid/os/Bundle;)V

    return-void
.end method

.method private declared-synchronized b()Landroid/os/Handler;
    .locals 3

    .prologue
    .line 199
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient;->g:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 201
    new-instance v0, Lcom/twitter/media/service/core/MediaServiceClient$b;

    iget-object v1, p0, Lcom/twitter/media/service/core/MediaServiceClient;->e:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/media/service/core/MediaServiceClient;->c:Ljava/util/Map;

    invoke-direct {v0, v1, v2}, Lcom/twitter/media/service/core/MediaServiceClient$b;-><init>(Landroid/os/Looper;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient;->g:Landroid/os/Handler;

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient;->g:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(Lcom/twitter/media/service/core/MediaServiceClient$c;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 239
    monitor-enter p0

    .line 240
    :try_start_0
    iput-object p1, p0, Lcom/twitter/media/service/core/MediaServiceClient$c;->g:Landroid/os/Bundle;

    .line 241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$c;->f:Z

    .line 243
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 244
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$c;->d:Lcom/twitter/media/service/core/MediaServiceClient$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$c;->e:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient$c;->e:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/media/service/core/MediaServiceClient$2;

    invoke-direct {v1, p0}, Lcom/twitter/media/service/core/MediaServiceClient$2;-><init>(Lcom/twitter/media/service/core/MediaServiceClient$c;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 255
    :cond_0
    return-void

    .line 244
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/twitter/media/service/core/MediaServiceClient;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/media/service/core/MediaServiceClient;->a()V

    return-void
.end method

.method private declared-synchronized c()Landroid/os/Messenger;
    .locals 2

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient;->f:Landroid/os/Messenger;

    if-nez v0, :cond_0

    .line 209
    new-instance v0, Landroid/os/Messenger;

    invoke-direct {p0}, Lcom/twitter/media/service/core/MediaServiceClient;->b()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient;->f:Landroid/os/Messenger;

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient;->f:Landroid/os/Messenger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/twitter/media/service/core/MediaServiceClient;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient;->c:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/media/service/core/MediaServiceClient;)Landroid/os/Messenger;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/media/service/core/MediaServiceClient;->c()Landroid/os/Messenger;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Lcom/twitter/media/service/core/MediaServiceClient$d;)I
    .locals 2

    .prologue
    .line 258
    new-instance v0, Lcom/twitter/media/service/core/MediaServiceClient$c;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/twitter/media/service/core/MediaServiceClient$c;-><init>(ILandroid/os/Bundle;Lcom/twitter/media/service/core/MediaServiceClient$1;)V

    .line 259
    if-eqz p3, :cond_0

    .line 260
    iput-object p3, v0, Lcom/twitter/media/service/core/MediaServiceClient$c;->d:Lcom/twitter/media/service/core/MediaServiceClient$d;

    .line 261
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 262
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, v0, Lcom/twitter/media/service/core/MediaServiceClient$c;->e:Landroid/os/Handler;

    .line 267
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/twitter/media/service/core/MediaServiceClient;->d:Lcom/twitter/media/service/core/MediaServiceClient$a;

    invoke-virtual {v1, v0}, Lcom/twitter/media/service/core/MediaServiceClient$a;->a(Lcom/twitter/media/service/core/MediaServiceClient$c;)V

    .line 268
    iget v0, v0, Lcom/twitter/media/service/core/MediaServiceClient$c;->a:I

    return v0

    .line 264
    :cond_1
    invoke-direct {p0}, Lcom/twitter/media/service/core/MediaServiceClient;->b()Landroid/os/Handler;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/media/service/core/MediaServiceClient$c;->e:Landroid/os/Handler;

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 273
    const v0, 0xea60

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/media/service/core/MediaServiceClient;->a(ILandroid/os/Bundle;I)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public a(ILandroid/os/Bundle;I)Landroid/os/Bundle;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 278
    new-instance v1, Lcom/twitter/media/service/core/MediaServiceClient$c;

    invoke-direct {v1, p1, p2, v0}, Lcom/twitter/media/service/core/MediaServiceClient$c;-><init>(ILandroid/os/Bundle;Lcom/twitter/media/service/core/MediaServiceClient$1;)V

    .line 279
    iget-object v2, p0, Lcom/twitter/media/service/core/MediaServiceClient;->d:Lcom/twitter/media/service/core/MediaServiceClient$a;

    invoke-virtual {v2, v1}, Lcom/twitter/media/service/core/MediaServiceClient$a;->a(Lcom/twitter/media/service/core/MediaServiceClient$c;)V

    .line 283
    monitor-enter v1

    .line 284
    :try_start_0
    iget-boolean v2, v1, Lcom/twitter/media/service/core/MediaServiceClient$c;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 286
    int-to-long v2, p3

    :try_start_1
    invoke-virtual {v1, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 289
    :goto_0
    :try_start_2
    iget-boolean v0, v1, Lcom/twitter/media/service/core/MediaServiceClient$c;->f:Z

    if-nez v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/twitter/media/service/core/MediaServiceClient;->d:Lcom/twitter/media/service/core/MediaServiceClient$a;

    invoke-virtual {v0, v1}, Lcom/twitter/media/service/core/MediaServiceClient$a;->b(Lcom/twitter/media/service/core/MediaServiceClient$c;)V

    .line 292
    :cond_0
    iget-object v0, v1, Lcom/twitter/media/service/core/MediaServiceClient$c;->g:Landroid/os/Bundle;

    .line 294
    :cond_1
    monitor-exit v1

    .line 296
    return-object v0

    .line 294
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 287
    :catch_0
    move-exception v0

    goto :goto_0
.end method
