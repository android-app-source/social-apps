.class public Lcom/twitter/media/manager/UserImageRequest;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/media/manager/UserImageRequest$AvatarSize;
    }
.end annotation


# static fields
.field private static a:I

.field private static b:I

.field private static c:I

.field private static d:I

.field private static final e:Lcom/twitter/media/request/a$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/16 v0, 0x20

    sput v0, Lcom/twitter/media/manager/UserImageRequest;->a:I

    .line 30
    const/16 v0, 0x24

    sput v0, Lcom/twitter/media/manager/UserImageRequest;->b:I

    .line 31
    const/16 v0, 0x36

    sput v0, Lcom/twitter/media/manager/UserImageRequest;->c:I

    .line 32
    const/16 v0, 0x54

    sput v0, Lcom/twitter/media/manager/UserImageRequest;->d:I

    .line 52
    new-instance v0, Lcom/twitter/media/manager/UserImageRequest$1;

    invoke-direct {v0}, Lcom/twitter/media/manager/UserImageRequest$1;-><init>()V

    sput-object v0, Lcom/twitter/media/manager/UserImageRequest;->e:Lcom/twitter/media/request/a$c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    return-void
.end method

.method public static a(I)I
    .locals 0

    .prologue
    .line 127
    packed-switch p0, :pswitch_data_0

    .line 141
    :goto_0
    return p0

    .line 129
    :pswitch_0
    sget p0, Lcom/twitter/media/manager/UserImageRequest;->a:I

    goto :goto_0

    .line 132
    :pswitch_1
    sget p0, Lcom/twitter/media/manager/UserImageRequest;->b:I

    goto :goto_0

    .line 135
    :pswitch_2
    sget p0, Lcom/twitter/media/manager/UserImageRequest;->c:I

    goto :goto_0

    .line 138
    :pswitch_3
    sget p0, Lcom/twitter/media/manager/UserImageRequest;->d:I

    goto :goto_0

    .line 127
    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    invoke-static {p0, v0}, Lcom/twitter/media/manager/UserImageRequest;->a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;I)Lcom/twitter/media/request/a$a;
    .locals 1

    .prologue
    .line 96
    invoke-static {p1}, Lcom/twitter/media/manager/UserImageRequest;->a(I)I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/math/Size;->a(I)Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/media/manager/UserImageRequest;->a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;
    .locals 2

    .prologue
    .line 101
    invoke-static {p0, p1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/manager/UserImageRequest;->e:Lcom/twitter/media/request/a$c;

    .line 102
    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/a$c;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    const-string/jumbo v1, "user"

    .line 103
    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a$a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 101
    return-object v0
.end method

.method public static a(IIII)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 107
    const-class v0, Lcom/twitter/media/manager/UserImageRequest;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 108
    if-lez p0, :cond_0

    move v0, v1

    :goto_0
    const-string/jumbo v3, "MINI size must be a positive number"

    invoke-static {v0, v3}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 109
    sput p0, Lcom/twitter/media/manager/UserImageRequest;->a:I

    .line 110
    if-lez p1, :cond_1

    move v0, v1

    :goto_1
    const-string/jumbo v3, "SMALL size must be a positive number"

    invoke-static {v0, v3}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 111
    sput p1, Lcom/twitter/media/manager/UserImageRequest;->b:I

    .line 112
    if-lez p2, :cond_2

    move v0, v1

    :goto_2
    const-string/jumbo v3, "NORMAL size must be a positive number"

    invoke-static {v0, v3}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 113
    sput p2, Lcom/twitter/media/manager/UserImageRequest;->c:I

    .line 114
    if-lez p3, :cond_3

    :goto_3
    const-string/jumbo v0, "LARGE size must be a positive number"

    invoke-static {v1, v0}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 115
    sput p3, Lcom/twitter/media/manager/UserImageRequest;->d:I

    .line 116
    return-void

    :cond_0
    move v0, v2

    .line 108
    goto :goto_0

    :cond_1
    move v0, v2

    .line 110
    goto :goto_1

    :cond_2
    move v0, v2

    .line 112
    goto :goto_2

    :cond_3
    move v1, v2

    .line 114
    goto :goto_3
.end method
