.class public final Lcom/twitter/media/request/a;
.super Lcom/twitter/media/request/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/media/request/a$a;,
        Lcom/twitter/media/request/a$c;,
        Lcom/twitter/media/request/a$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/media/request/b",
        "<",
        "Lcom/twitter/media/request/ImageResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/media/model/MediaFile;

.field private final d:Z

.field private final e:Lcom/twitter/util/math/Size;

.field private final f:Lcom/twitter/media/decoder/ImageDecoder$ScaleType;

.field private final g:Lcom/twitter/util/math/c;

.field private final h:Z

.field private final i:Ljava/lang/String;

.field private final j:Lbzi;

.field private final k:I

.field private final l:Ljava/lang/String;

.field private final m:Landroid/graphics/Bitmap$Config;

.field private final n:I

.field private final o:Z

.field private final p:Lcom/twitter/media/model/MediaType;

.field private final q:Lcom/twitter/media/request/process/a;


# direct methods
.method protected constructor <init>(Lcom/twitter/media/request/a$a;)V
    .locals 3

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/twitter/media/request/b;-><init>(Lcom/twitter/media/request/b$a;)V

    .line 76
    iget-object v0, p1, Lcom/twitter/media/request/a$a;->c:Lcom/twitter/util/math/Size;

    iput-object v0, p0, Lcom/twitter/media/request/a;->e:Lcom/twitter/util/math/Size;

    .line 77
    iget-boolean v0, p1, Lcom/twitter/media/request/a$a;->d:Z

    iput-boolean v0, p0, Lcom/twitter/media/request/a;->d:Z

    .line 79
    iget-object v0, p1, Lcom/twitter/media/request/a$a;->b:Lcom/twitter/media/request/a$c;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p1, Lcom/twitter/media/request/a$a;->b:Lcom/twitter/media/request/a$c;

    iget-object v1, p1, Lcom/twitter/media/request/a$a;->p:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/media/request/a;->e:Lcom/twitter/util/math/Size;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/media/request/a$c;->a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/request/a;->b:Ljava/util/List;

    .line 81
    iget-object v0, p0, Lcom/twitter/media/request/a;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/media/request/a;->a:Ljava/lang/String;

    .line 86
    :goto_0
    iget-object v0, p1, Lcom/twitter/media/request/a$a;->a:Lcom/twitter/media/model/MediaFile;

    iput-object v0, p0, Lcom/twitter/media/request/a;->c:Lcom/twitter/media/model/MediaFile;

    .line 87
    iget-object v0, p1, Lcom/twitter/media/request/a$a;->e:Lcom/twitter/media/decoder/ImageDecoder$ScaleType;

    iput-object v0, p0, Lcom/twitter/media/request/a;->f:Lcom/twitter/media/decoder/ImageDecoder$ScaleType;

    .line 88
    iget-object v0, p1, Lcom/twitter/media/request/a$a;->f:Lcom/twitter/util/math/c;

    iput-object v0, p0, Lcom/twitter/media/request/a;->g:Lcom/twitter/util/math/c;

    .line 89
    iget-boolean v0, p1, Lcom/twitter/media/request/a$a;->h:Z

    iput-boolean v0, p0, Lcom/twitter/media/request/a;->h:Z

    .line 90
    iget-object v0, p1, Lcom/twitter/media/request/a$a;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/media/request/a;->i:Ljava/lang/String;

    .line 91
    iget-object v0, p1, Lcom/twitter/media/request/a$a;->i:Lbzi;

    iput-object v0, p0, Lcom/twitter/media/request/a;->j:Lbzi;

    .line 92
    iget v0, p1, Lcom/twitter/media/request/a$a;->g:I

    iput v0, p0, Lcom/twitter/media/request/a;->k:I

    .line 93
    iget-object v0, p1, Lcom/twitter/media/request/a$a;->j:Landroid/graphics/Bitmap$Config;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/twitter/media/request/a$a;->j:Landroid/graphics/Bitmap$Config;

    :goto_1
    iput-object v0, p0, Lcom/twitter/media/request/a;->m:Landroid/graphics/Bitmap$Config;

    .line 94
    iget v0, p1, Lcom/twitter/media/request/a$a;->m:I

    iput v0, p0, Lcom/twitter/media/request/a;->n:I

    .line 95
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/media/request/a;->a(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/request/a;->l:Ljava/lang/String;

    .line 96
    iget-object v0, p1, Lcom/twitter/media/request/a$a;->l:Lcom/twitter/media/model/MediaType;

    iput-object v0, p0, Lcom/twitter/media/request/a;->p:Lcom/twitter/media/model/MediaType;

    .line 97
    iget-boolean v0, p1, Lcom/twitter/media/request/a$a;->n:Z

    iput-boolean v0, p0, Lcom/twitter/media/request/a;->o:Z

    .line 98
    iget-object v0, p1, Lcom/twitter/media/request/a$a;->o:Lcom/twitter/media/request/process/a;

    iput-object v0, p0, Lcom/twitter/media/request/a;->q:Lcom/twitter/media/request/process/a;

    .line 99
    return-void

    .line 83
    :cond_0
    iget-object v0, p1, Lcom/twitter/media/request/a$a;->p:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/request/a;->b:Ljava/util/List;

    .line 84
    iget-object v0, p1, Lcom/twitter/media/request/a$a;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/media/request/a;->a:Ljava/lang/String;

    goto :goto_0

    .line 93
    :cond_1
    invoke-direct {p0}, Lcom/twitter/media/request/a;->D()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    goto :goto_1
.end method

.method private D()Landroid/graphics/Bitmap$Config;
    .locals 2

    .prologue
    .line 108
    const-string/jumbo v0, "android_photo_consumption_bitmap_config_degradation_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 109
    invoke-static {}, Lcob;->a()Lcob;

    move-result-object v0

    invoke-virtual {v0}, Lcob;->b()I

    move-result v0

    const/16 v1, 0x7dd

    if-ge v0, v1, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/twitter/media/request/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/media/model/ImageFormat;->a(Ljava/lang/String;)Lcom/twitter/media/model/ImageFormat;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/model/ImageFormat;->a:Lcom/twitter/media/model/ImageFormat;

    if-eq v0, v1, :cond_1

    .line 111
    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 116
    :goto_0
    return-object v0

    .line 113
    :cond_1
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    goto :goto_0

    .line 116
    :cond_2
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    goto :goto_0
.end method

.method public static a(Lcom/twitter/media/model/MediaFile;)Lcom/twitter/media/request/a$a;
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lcom/twitter/media/request/a$a;

    invoke-direct {v0, p0}, Lcom/twitter/media/request/a$a;-><init>(Lcom/twitter/media/model/MediaFile;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    invoke-static {p0, v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;II)Lcom/twitter/media/request/a$a;
    .locals 1

    .prologue
    .line 61
    invoke-static {p1, p2}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/twitter/media/request/a$a;

    invoke-direct {v0, p0}, Lcom/twitter/media/request/a$a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    return-object v0
.end method

.method private a(Z)Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x24

    const/16 v6, 0x5f

    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 232
    invoke-super {p0}, Lcom/twitter/media/request/b;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 234
    iget-object v1, p0, Lcom/twitter/media/request/a;->e:Lcom/twitter/util/math/Size;

    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->a()I

    move-result v1

    invoke-static {v1, v7}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 236
    iget-object v1, p0, Lcom/twitter/media/request/a;->e:Lcom/twitter/util/math/Size;

    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->b()I

    move-result v1

    invoke-static {v1, v7}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    iget-object v1, p0, Lcom/twitter/media/request/a;->f:Lcom/twitter/media/decoder/ImageDecoder$ScaleType;

    sget-object v2, Lcom/twitter/media/decoder/ImageDecoder$ScaleType;->a:Lcom/twitter/media/decoder/ImageDecoder$ScaleType;

    if-eq v1, v2, :cond_0

    .line 238
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 239
    iget-object v1, p0, Lcom/twitter/media/request/a;->f:Lcom/twitter/media/decoder/ImageDecoder$ScaleType;

    invoke-virtual {v1}, Lcom/twitter/media/decoder/ImageDecoder$ScaleType;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/twitter/media/request/a;->g:Lcom/twitter/util/math/c;

    .line 242
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/twitter/util/math/c;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 243
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 244
    const-string/jumbo v2, "[%s,%s,%s,%s]"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, v1, Lcom/twitter/util/math/c;->d:F

    .line 245
    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    invoke-static {v5, v7}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, v1, Lcom/twitter/util/math/c;->e:F

    .line 246
    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    invoke-static {v5, v7}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget v5, v1, Lcom/twitter/util/math/c;->f:F

    .line 247
    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    invoke-static {v5, v7}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget v1, v1, Lcom/twitter/util/math/c;->g:F

    .line 248
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    invoke-static {v1, v7}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    .line 244
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 249
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    :cond_1
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/twitter/media/request/a;->j:Lbzi;

    if-eqz v1, :cond_2

    .line 252
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 253
    iget-object v1, p0, Lcom/twitter/media/request/a;->j:Lbzi;

    invoke-interface {v1}, Lbzi;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    :cond_2
    iget v1, p0, Lcom/twitter/media/request/a;->k:I

    if-eqz v1, :cond_3

    .line 256
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 257
    iget v1, p0, Lcom/twitter/media/request/a;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 259
    :cond_3
    iget-object v1, p0, Lcom/twitter/media/request/a;->m:Landroid/graphics/Bitmap$Config;

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v1, v2, :cond_4

    .line 260
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 261
    iget-object v1, p0, Lcom/twitter/media/request/a;->m:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v1}, Landroid/graphics/Bitmap$Config;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 263
    :cond_4
    iget v1, p0, Lcom/twitter/media/request/a;->n:I

    if-lez v1, :cond_5

    .line 264
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 265
    iget v1, p0, Lcom/twitter/media/request/a;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 267
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/io/File;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/twitter/media/request/a;->c:Lcom/twitter/media/model/MediaFile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/media/request/a;->c:Lcom/twitter/media/model/MediaFile;

    iget-object v0, v0, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/media/request/b;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/twitter/media/request/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcom/twitter/media/request/b;)Z
    .locals 2

    .prologue
    .line 225
    if-eqz p1, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/media/request/b;->a(Lcom/twitter/media/request/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/media/request/a;->q:Lcom/twitter/media/request/process/a;

    check-cast p1, Lcom/twitter/media/request/a;

    iget-object v1, p1, Lcom/twitter/media/request/a;->q:Lcom/twitter/media/request/process/a;

    .line 226
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 225
    :goto_0
    return v0

    .line 226
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/media/request/a;->b:Ljava/util/List;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    invoke-super {p0}, Lcom/twitter/media/request/b;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/twitter/media/model/MediaFile;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/media/request/a;->c:Lcom/twitter/media/model/MediaFile;

    return-object v0
.end method

.method public e()Lcom/twitter/util/math/Size;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/twitter/media/request/a;->e:Lcom/twitter/util/math/Size;

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/twitter/media/request/a;->d:Z

    return v0
.end method

.method public g()Lcom/twitter/media/decoder/ImageDecoder$ScaleType;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/twitter/media/request/a;->f:Lcom/twitter/media/decoder/ImageDecoder$ScaleType;

    return-object v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/twitter/media/request/a;->k:I

    return v0
.end method

.method public i()Lcom/twitter/util/math/c;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/twitter/media/request/a;->g:Lcom/twitter/util/math/c;

    return-object v0
.end method

.method public j()Lbzi;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/twitter/media/request/a;->j:Lbzi;

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/twitter/media/request/a;->h:Z

    return v0
.end method

.method public l()Landroid/graphics/Bitmap$Config;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/twitter/media/request/a;->m:Landroid/graphics/Bitmap$Config;

    return-object v0
.end method

.method public m()Lcom/twitter/media/model/MediaType;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/twitter/media/request/a;->p:Lcom/twitter/media/model/MediaType;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/twitter/media/request/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/twitter/media/request/a;->n:I

    return v0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 203
    iget-boolean v0, p0, Lcom/twitter/media/request/a;->o:Z

    return v0
.end method

.method public q()Lcom/twitter/media/request/process/a;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/twitter/media/request/a;->q:Lcom/twitter/media/request/process/a;

    return-object v0
.end method

.method public r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/twitter/media/request/a;->l:Ljava/lang/String;

    return-object v0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/media/request/a;->a(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
