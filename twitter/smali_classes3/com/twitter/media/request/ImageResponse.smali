.class public Lcom/twitter/media/request/ImageResponse;
.super Lcom/twitter/media/request/ResourceResponse;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/media/request/ImageResponse$a;,
        Lcom/twitter/media/request/ImageResponse$Error;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/media/request/ResourceResponse",
        "<",
        "Lcom/twitter/media/request/a;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/media/model/MediaFile;

.field private final b:Lcom/twitter/media/request/ImageResponse$Error;

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/twitter/media/request/ImageResponse$a;)V
    .locals 3

    .prologue
    .line 23
    invoke-static {p1}, Lcom/twitter/media/request/ImageResponse$a;->a(Lcom/twitter/media/request/ImageResponse$a;)Lcom/twitter/media/request/a;

    move-result-object v0

    invoke-static {p1}, Lcom/twitter/media/request/ImageResponse$a;->b(Lcom/twitter/media/request/ImageResponse$a;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {p1}, Lcom/twitter/media/request/ImageResponse$a;->c(Lcom/twitter/media/request/ImageResponse$a;)Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/media/request/ResourceResponse;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)V

    .line 24
    invoke-static {p1}, Lcom/twitter/media/request/ImageResponse$a;->d(Lcom/twitter/media/request/ImageResponse$a;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/request/ImageResponse;->a:Lcom/twitter/media/model/MediaFile;

    .line 25
    invoke-static {p1}, Lcom/twitter/media/request/ImageResponse$a;->e(Lcom/twitter/media/request/ImageResponse$a;)Lcom/twitter/media/request/ImageResponse$Error;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/request/ImageResponse;->b:Lcom/twitter/media/request/ImageResponse$Error;

    .line 26
    invoke-static {p1}, Lcom/twitter/media/request/ImageResponse$a;->f(Lcom/twitter/media/request/ImageResponse$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/media/request/ImageResponse;->c:Z

    .line 27
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/media/model/MediaFile;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/media/request/ImageResponse;->a:Lcom/twitter/media/model/MediaFile;

    return-object v0
.end method

.method public b()Lcom/twitter/media/request/ImageResponse$Error;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/media/request/ImageResponse;->b:Lcom/twitter/media/request/ImageResponse$Error;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/twitter/media/request/ImageResponse;->c:Z

    return v0
.end method
