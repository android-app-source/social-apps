.class public Lcom/twitter/media/NativeInit;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field static a:Z

.field static b:Z

.field public static c:Ljava/lang/String;

.field private static final d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    const-string/jumbo v0, "filters"

    sput-object v0, Lcom/twitter/media/NativeInit;->c:Ljava/lang/String;

    .line 14
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/twitter/media/NativeInit;->d:Ljava/util/HashSet;

    .line 16
    sget-object v0, Lcom/twitter/media/NativeInit;->d:Ljava/util/HashSet;

    const-string/jumbo v1, "armeabi-v7a"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 17
    sget-object v0, Lcom/twitter/media/NativeInit;->d:Ljava/util/HashSet;

    const-string/jumbo v1, "arm64-v8a"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 19
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    .line 20
    sget-object v0, Lcom/twitter/media/NativeInit;->d:Ljava/util/HashSet;

    const-string/jumbo v1, "x86"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 22
    :cond_0
    sget-object v0, Lcom/twitter/media/NativeInit;->d:Ljava/util/HashSet;

    const-string/jumbo v1, "x86_64"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 23
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()V
    .locals 4

    .prologue
    .line 26
    const-class v1, Lcom/twitter/media/NativeInit;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/twitter/media/NativeInit;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 28
    :try_start_1
    invoke-static {}, Lcom/twitter/media/NativeInit;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    const-string/jumbo v0, "twittermedia"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 30
    invoke-static {}, Lcom/twitter/media/NativeInit;->nativeInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x1

    sput-boolean v0, Lcom/twitter/media/NativeInit;->a:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 37
    :cond_0
    const/4 v0, 0x1

    :try_start_2
    sput-boolean v0, Lcom/twitter/media/NativeInit;->b:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 40
    :cond_1
    :goto_0
    monitor-exit v1

    return-void

    .line 34
    :catch_0
    move-exception v0

    .line 35
    :try_start_3
    sget-object v2, Lcom/twitter/media/NativeInit;->c:Ljava/lang/String;

    const-string/jumbo v3, "Failed to init() twittermedia"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 37
    const/4 v0, 0x1

    :try_start_4
    sput-boolean v0, Lcom/twitter/media/NativeInit;->b:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 37
    :catchall_1
    move-exception v0

    const/4 v2, 0x1

    :try_start_5
    sput-boolean v2, Lcom/twitter/media/NativeInit;->b:Z

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lcom/twitter/media/NativeInit;->a()V

    .line 44
    sget-boolean v0, Lcom/twitter/media/NativeInit;->a:Z

    return v0
.end method

.method private static c()Z
    .locals 2

    .prologue
    .line 48
    invoke-static {}, Lcom/twitter/media/filters/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/twitter/media/NativeInit;->d:Ljava/util/HashSet;

    sget-object v1, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static native nativeInit()Z
.end method
