.class public Lcom/twitter/media/util/l$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/media/util/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:J

.field private b:J

.field private c:I

.field private d:J

.field private e:J

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 45
    iput p1, p0, Lcom/twitter/media/util/l$a;->c:I

    .line 46
    iget-wide v0, p0, Lcom/twitter/media/util/l$a;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 47
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/media/util/l$a;->d:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/media/util/l$a;->e:J

    .line 48
    iget-wide v0, p0, Lcom/twitter/media/util/l$a;->e:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 49
    iget-wide v0, p0, Lcom/twitter/media/util/l$a;->a:J

    iget-wide v2, p0, Lcom/twitter/media/util/l$a;->e:J

    div-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/media/util/l$a;->b:J

    .line 52
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 34
    iput-object p1, p0, Lcom/twitter/media/util/l$a;->f:Ljava/lang/String;

    .line 35
    iput-wide p2, p0, Lcom/twitter/media/util/l$a;->a:J

    .line 36
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/media/util/l$a;->d:J

    .line 37
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    const-string/jumbo v1, "operation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    iget-object v1, p0, Lcom/twitter/media/util/l$a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    const-string/jumbo v1, ",status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    iget v1, p0, Lcom/twitter/media/util/l$a;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 71
    iget-wide v2, p0, Lcom/twitter/media/util/l$a;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 72
    const-string/jumbo v1, ",size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    iget-wide v2, p0, Lcom/twitter/media/util/l$a;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 74
    const-string/jumbo v1, ",timeMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    iget-wide v2, p0, Lcom/twitter/media/util/l$a;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 76
    const-string/jumbo v1, ",rateBps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    iget-wide v2, p0, Lcom/twitter/media/util/l$a;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 79
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
