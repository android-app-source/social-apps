.class public Lcom/twitter/media/util/q;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static b:Lcom/twitter/media/util/q;


# instance fields
.field a:Ljava/util/Set;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Lcom/twitter/media/util/q$1;

    invoke-direct {v0, p0}, Lcom/twitter/media/util/q$1;-><init>(Lcom/twitter/media/util/q;)V

    invoke-static {v0}, Lcoj;->a(Lcoj$a;)V

    .line 64
    return-void
.end method

.method public static a()Lcom/twitter/media/util/q;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/twitter/media/util/q;->b:Lcom/twitter/media/util/q;

    if-nez v0, :cond_0

    .line 38
    const-class v0, Lcom/twitter/media/util/q;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 39
    new-instance v0, Lcom/twitter/media/util/q;

    invoke-direct {v0}, Lcom/twitter/media/util/q;-><init>()V

    sput-object v0, Lcom/twitter/media/util/q;->b:Lcom/twitter/media/util/q;

    .line 42
    :cond_0
    sget-object v0, Lcom/twitter/media/util/q;->b:Lcom/twitter/media/util/q;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/media/util/q;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/twitter/media/util/q;->b()V

    return-void
.end method

.method public static a(Lcom/twitter/model/core/MediaEntity;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 114
    if-eqz p0, :cond_0

    const-string/jumbo v0, "ad_formats_snapreel_enabled"

    .line 115
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-static {p0}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    invoke-static {}, Lcom/twitter/media/util/q;->a()Lcom/twitter/media/util/q;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/media/util/q;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 114
    :goto_0
    return v0

    .line 117
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;)Z
    .locals 2

    .prologue
    .line 100
    if-eqz p0, :cond_0

    const-string/jumbo v0, "ad_formats_snapreel_enabled"

    .line 101
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-static {}, Lcom/twitter/media/util/q;->a()Lcom/twitter/media/util/q;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/core/Tweet;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/media/util/q;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 100
    :goto_0
    return v0

    .line 103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 86
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/media/util/q;->a:Ljava/util/Set;

    .line 87
    const-string/jumbo v0, "ad_formats_snapreel_sources"

    invoke-static {v0}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 88
    iget-object v2, p0, Lcom/twitter/media/util/q;->a:Ljava/util/Set;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 90
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 82
    :goto_0
    return v0

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/twitter/media/util/q;->a:Ljava/util/Set;

    if-nez v0, :cond_1

    .line 74
    invoke-direct {p0}, Lcom/twitter/media/util/q;->b()V

    .line 76
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 77
    iget-object v0, p0, Lcom/twitter/media/util/q;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 78
    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 82
    goto :goto_0
.end method
