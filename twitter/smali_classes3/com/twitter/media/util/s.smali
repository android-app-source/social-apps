.class public Lcom/twitter/media/util/s;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/media/util/s$a;
    }
.end annotation


# static fields
.field public static final a:J

.field private static b:Lcom/twitter/media/util/s;


# instance fields
.field private final c:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lcom/twitter/media/util/s$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 29
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/twitter/media/util/s;->a:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/media/util/s;->c:Landroid/support/v4/util/LongSparseArray;

    return-void
.end method

.method public static a()Lcom/twitter/media/util/s;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/twitter/media/util/s;->b:Lcom/twitter/media/util/s;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/twitter/media/util/s;

    invoke-direct {v0}, Lcom/twitter/media/util/s;-><init>()V

    sput-object v0, Lcom/twitter/media/util/s;->b:Lcom/twitter/media/util/s;

    .line 39
    const-class v0, Lcom/twitter/media/util/s;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 41
    :cond_0
    sget-object v0, Lcom/twitter/media/util/s;->b:Lcom/twitter/media/util/s;

    return-object v0
.end method

.method public static a(JLjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    invoke-static {}, Lcom/twitter/media/util/s;->a()Lcom/twitter/media/util/s;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/twitter/media/util/s;->a(J)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/media/model/MediaFile;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    :cond_0
    return-object p2
.end method


# virtual methods
.method public a(J)Lcom/twitter/media/model/MediaFile;
    .locals 7

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/media/util/s;->c:Landroid/support/v4/util/LongSparseArray;

    .line 73
    invoke-virtual {v0, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/util/s$a;

    .line 74
    if-eqz v0, :cond_1

    .line 75
    iget-wide v2, v0, Lcom/twitter/media/util/s$a;->b:J

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 76
    iget-object v0, v0, Lcom/twitter/media/util/s$a;->a:Lcom/twitter/media/model/MediaFile;

    .line 81
    :goto_0
    return-object v0

    .line 78
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/twitter/media/util/s;->b(J)V

    .line 81
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(JLcom/twitter/media/model/MediaFile;)V
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/media/util/s;->c:Landroid/support/v4/util/LongSparseArray;

    new-instance v1, Lcom/twitter/media/util/s$a;

    invoke-direct {v1, p3}, Lcom/twitter/media/util/s$a;-><init>(Lcom/twitter/media/model/MediaFile;)V

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 64
    return-void
.end method

.method public b(J)V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/media/util/s;->c:Landroid/support/v4/util/LongSparseArray;

    .line 94
    invoke-virtual {v0, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/util/s$a;

    .line 95
    if-eqz v0, :cond_0

    .line 96
    iget-object v0, v0, Lcom/twitter/media/util/s$a;->a:Lcom/twitter/media/model/MediaFile;

    .line 97
    iget-object v1, p0, Lcom/twitter/media/util/s;->c:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/support/v4/util/LongSparseArray;->remove(J)V

    .line 98
    invoke-virtual {v0}, Lcom/twitter/media/model/MediaFile;->c()Lrx/g;

    .line 100
    :cond_0
    return-void
.end method
