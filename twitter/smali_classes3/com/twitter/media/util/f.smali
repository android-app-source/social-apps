.class public Lcom/twitter/media/util/f;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/twitter/util/SynchronizedDateFormat;

    const-string/jumbo v1, "yyyy:MM:dd HH:mm:ss"

    invoke-direct {v0, v1}, Lcom/twitter/util/SynchronizedDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/twitter/media/util/f;->a:Ljava/text/SimpleDateFormat;

    .line 27
    sget-object v0, Lcom/twitter/media/util/f;->a:Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 28
    return-void
.end method

.method public static a(Ljava/io/File;)Lcom/twitter/media/util/ImageOrientation;
    .locals 1

    .prologue
    .line 82
    if-nez p0, :cond_0

    .line 83
    sget-object v0, Lcom/twitter/media/util/ImageOrientation;->a:Lcom/twitter/media/util/ImageOrientation;

    .line 86
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/media/util/f$1;

    invoke-direct {v0, p0}, Lcom/twitter/media/util/f$1;-><init>(Ljava/io/File;)V

    invoke-static {v0}, Lcom/twitter/util/x;->a(Lcom/twitter/util/concurrent/i;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/util/ImageOrientation;

    goto :goto_0
.end method

.method public static a(Ljava/io/File;Z)Lcom/twitter/media/util/i;
    .locals 2

    .prologue
    .line 39
    :try_start_0
    new-instance v0, Lcom/twitter/media/util/i;

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/media/util/i;-><init>(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    return-object v0

    .line 40
    :catch_0
    move-exception v0

    .line 43
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/io/File;I)Z
    .locals 1

    .prologue
    .line 71
    rem-int/lit16 v0, p1, 0x168

    if-nez v0, :cond_0

    .line 72
    const/4 v0, 0x1

    .line 77
    :goto_0
    return v0

    .line 73
    :cond_0
    rem-int/lit8 v0, p1, 0x5a

    if-eqz v0, :cond_1

    .line 74
    const/4 v0, 0x0

    goto :goto_0

    .line 76
    :cond_1
    invoke-static {p0}, Lcom/twitter/media/util/f;->a(Ljava/io/File;)Lcom/twitter/media/util/ImageOrientation;

    move-result-object v0

    .line 77
    invoke-virtual {v0, p1}, Lcom/twitter/media/util/ImageOrientation;->c(I)Lcom/twitter/media/util/ImageOrientation;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/media/util/f;->a(Ljava/io/File;Lcom/twitter/media/util/ImageOrientation;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/io/File;Lcom/twitter/media/util/ImageOrientation;)Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/twitter/media/util/f;->a(Ljava/io/File;Lcom/twitter/media/util/ImageOrientation;Z)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/io/File;Lcom/twitter/media/util/ImageOrientation;Z)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 111
    const/4 v1, 0x1

    :try_start_0
    invoke-static {p0, v1}, Lcom/twitter/media/util/f;->a(Ljava/io/File;Z)Lcom/twitter/media/util/i;

    move-result-object v1

    .line 112
    if-eqz v1, :cond_1

    .line 113
    invoke-virtual {v1, p1}, Lcom/twitter/media/util/i;->a(Lcom/twitter/media/util/ImageOrientation;)V

    .line 114
    if-eqz p2, :cond_0

    .line 115
    const-string/jumbo v2, "DateTime"

    sget-object v3, Lcom/twitter/media/util/f;->a:Ljava/text/SimpleDateFormat;

    new-instance v4, Ljava/util/Date;

    .line 116
    invoke-virtual {p0}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 115
    invoke-virtual {v1, v2, v3}, Lcom/twitter/media/util/i;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/media/util/i;->saveAttributes()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :goto_0
    return v0

    .line 121
    :catch_0
    move-exception v0

    .line 124
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/io/File;Ljava/io/File;Lcom/twitter/media/util/ImageOrientation;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 49
    const/4 v2, 0x1

    :try_start_0
    invoke-static {p0, v2}, Lcom/twitter/media/util/f;->a(Ljava/io/File;Z)Lcom/twitter/media/util/i;

    move-result-object v2

    .line 50
    if-eqz v2, :cond_0

    .line 51
    const/4 v3, 0x0

    invoke-static {p1, v3}, Lcom/twitter/media/util/f;->a(Ljava/io/File;Z)Lcom/twitter/media/util/i;

    move-result-object v3

    .line 52
    if-eqz v3, :cond_0

    .line 53
    invoke-virtual {v3, v2}, Lcom/twitter/media/util/i;->a(Lcom/twitter/media/util/i;)V

    .line 54
    invoke-virtual {v3, p2}, Lcom/twitter/media/util/i;->a(Lcom/twitter/media/util/ImageOrientation;)V

    .line 55
    invoke-virtual {v3}, Lcom/twitter/media/util/i;->saveAttributes()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    return v0

    .line 59
    :catch_0
    move-exception v0

    :cond_0
    move v0, v1

    .line 62
    goto :goto_0
.end method
