.class public Lcom/twitter/media/util/n;
.super Lcom/twitter/util/android/d;
.source "Twttr"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "_data NOT NULL AND _data != ? AND _size > 0 AND (mime_type != "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "image/gif"

    .line 26
    invoke-static {v1}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/media/util/n;->a:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZ)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 31
    const-string/jumbo v0, "external"

    .line 32
    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/media/model/b;->a:[Ljava/lang/String;

    .line 34
    invoke-static {p2, p3}, Lcom/twitter/media/util/n;->a(ZZ)Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const-string/jumbo v0, ""

    aput-object v0, v5, v7

    const-string/jumbo v6, "date_added DESC"

    move-object v0, p0

    move-object v1, p1

    .line 31
    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    invoke-virtual {p0, v7}, Lcom/twitter/media/util/n;->a(Z)Lcom/twitter/util/android/d;

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZI)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 41
    const-string/jumbo v0, "external"

    .line 42
    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/media/model/b;->a:[Ljava/lang/String;

    .line 44
    invoke-static {p2, p3}, Lcom/twitter/media/util/n;->a(ZZ)Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const-string/jumbo v0, ""

    aput-object v0, v5, v7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "date_added DESC LIMIT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    .line 41
    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0, v7}, Lcom/twitter/media/util/n;->a(Z)Lcom/twitter/util/android/d;

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZLcom/twitter/media/model/MediaStoreBucket;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 52
    const-string/jumbo v0, "external"

    .line 53
    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/media/model/b;->a:[Ljava/lang/String;

    .line 55
    invoke-static {p2, p3}, Lcom/twitter/media/util/n;->b(ZZ)Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/String;

    const-string/jumbo v0, ""

    aput-object v0, v5, v7

    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    invoke-virtual {p4}, Lcom/twitter/media/model/MediaStoreBucket;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v6, "%"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    const-string/jumbo v6, "date_added DESC"

    move-object v0, p0

    move-object v1, p1

    .line 52
    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0, v7}, Lcom/twitter/media/util/n;->a(Z)Lcom/twitter/util/android/d;

    .line 59
    return-void
.end method

.method private static a(ZZ)Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    if-eqz p0, :cond_0

    const-string/jumbo v0, "_data NOT NULL AND _data != ? AND _size > 0"

    .line 66
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_1

    const-string/jumbo v0, "media_type = 1 OR media_type = 3)"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 63
    :cond_0
    sget-object v0, Lcom/twitter/media/util/n;->a:Ljava/lang/String;

    goto :goto_0

    .line 66
    :cond_1
    const-string/jumbo v0, "media_type = 1)"

    goto :goto_1
.end method

.method private static b(ZZ)Ljava/lang/String;
    .locals 2

    .prologue
    .line 78
    invoke-static {p0, p1}, Lcom/twitter/media/util/n;->a(ZZ)Ljava/lang/String;

    move-result-object v0

    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " AND _data LIKE ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 80
    return-object v0
.end method
