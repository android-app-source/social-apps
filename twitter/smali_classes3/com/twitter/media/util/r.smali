.class public Lcom/twitter/media/util/r;
.super Lcom/twitter/media/request/a$c;
.source "Twttr"


# instance fields
.field private final a:Lcea;


# direct methods
.method public constructor <init>(Lcea;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/twitter/media/request/a$c;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/twitter/media/util/r;->a:Lcea;

    .line 18
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/twitter/util/math/Size;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/twitter/media/util/r;->a:Lcea;

    iget-object v1, v1, Lcea;->d:Lcdw;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/media/util/r;->a:Lcea;

    iget-object v1, v1, Lcea;->d:Lcdw;

    iget-object v1, v1, Lcdw;->b:Lcom/twitter/util/math/Size;

    invoke-virtual {v1, p2}, Lcom/twitter/util/math/Size;->b(Lcom/twitter/util/math/Size;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25
    iget-object v1, p0, Lcom/twitter/media/util/r;->a:Lcea;

    iget-object v1, v1, Lcea;->d:Lcdw;

    iget-object v1, v1, Lcdw;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 27
    :cond_0
    iget-object v1, p0, Lcom/twitter/media/util/r;->a:Lcea;

    iget-object v1, v1, Lcea;->e:Lcdw;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/media/util/r;->a:Lcea;

    iget-object v1, v1, Lcea;->e:Lcdw;

    iget-object v1, v1, Lcdw;->b:Lcom/twitter/util/math/Size;

    invoke-virtual {v1, p2}, Lcom/twitter/util/math/Size;->b(Lcom/twitter/util/math/Size;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 28
    iget-object v1, p0, Lcom/twitter/media/util/r;->a:Lcea;

    iget-object v1, v1, Lcea;->e:Lcdw;

    iget-object v1, v1, Lcdw;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 30
    :cond_1
    iget-object v1, p0, Lcom/twitter/media/util/r;->a:Lcea;

    iget-object v1, v1, Lcea;->f:Lcdw;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/media/util/r;->a:Lcea;

    iget-object v1, v1, Lcea;->f:Lcdw;

    iget-object v1, v1, Lcdw;->b:Lcom/twitter/util/math/Size;

    invoke-virtual {v1, p2}, Lcom/twitter/util/math/Size;->b(Lcom/twitter/util/math/Size;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 31
    iget-object v1, p0, Lcom/twitter/media/util/r;->a:Lcea;

    iget-object v1, v1, Lcea;->f:Lcdw;

    iget-object v1, v1, Lcdw;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 33
    :cond_2
    iget-object v1, p0, Lcom/twitter/media/util/r;->a:Lcea;

    iget-object v1, v1, Lcea;->g:Lcdw;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/media/util/r;->a:Lcea;

    iget-object v1, v1, Lcea;->g:Lcdw;

    iget-object v1, v1, Lcdw;->b:Lcom/twitter/util/math/Size;

    invoke-virtual {v1, p2}, Lcom/twitter/util/math/Size;->b(Lcom/twitter/util/math/Size;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 34
    iget-object v1, p0, Lcom/twitter/media/util/r;->a:Lcea;

    iget-object v1, v1, Lcea;->g:Lcdw;

    iget-object v1, v1, Lcdw;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 36
    :cond_3
    iget-object v1, p0, Lcom/twitter/media/util/r;->a:Lcea;

    iget-object v1, v1, Lcea;->c:Lcdw;

    iget-object v1, v1, Lcdw;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 37
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method
