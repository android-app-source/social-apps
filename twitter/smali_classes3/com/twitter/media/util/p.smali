.class public Lcom/twitter/media/util/p;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Landroid/renderscript/RenderScript;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 43
    const-class v4, Lcom/twitter/media/util/p;

    monitor-enter v4

    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 44
    sget-object v2, Lcom/twitter/media/util/p;->a:Landroid/renderscript/RenderScript;

    if-nez v2, :cond_0

    .line 45
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/renderscript/RenderScript;->create(Landroid/content/Context;)Landroid/renderscript/RenderScript;

    move-result-object v2

    sput-object v2, Lcom/twitter/media/util/p;->a:Landroid/renderscript/RenderScript;

    .line 46
    const-class v2, Lcom/twitter/media/util/p;

    invoke-static {v2}, Lcru;->a(Ljava/lang/Class;)V

    .line 48
    :cond_0
    sget-object v5, Lcom/twitter/media/util/p;->a:Landroid/renderscript/RenderScript;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 53
    :try_start_1
    invoke-static {v5, p1}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 54
    :try_start_2
    invoke-static {v5, v0}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v2

    .line 55
    :try_start_3
    invoke-static {v5}, Landroid/renderscript/Element;->U8_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/renderscript/ScriptIntrinsicBlur;->create(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)Landroid/renderscript/ScriptIntrinsicBlur;

    move-result-object v1

    .line 56
    invoke-virtual {v1, v3}, Landroid/renderscript/ScriptIntrinsicBlur;->setInput(Landroid/renderscript/Allocation;)V

    .line 57
    invoke-virtual {v1, p2}, Landroid/renderscript/ScriptIntrinsicBlur;->setRadius(F)V

    .line 58
    invoke-virtual {v1, v2}, Landroid/renderscript/ScriptIntrinsicBlur;->forEach(Landroid/renderscript/Allocation;)V

    .line 59
    invoke-virtual {v2, v0}, Landroid/renderscript/Allocation;->copyTo(Landroid/graphics/Bitmap;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 62
    if-eqz v3, :cond_1

    .line 63
    :try_start_4
    invoke-virtual {v3}, Landroid/renderscript/Allocation;->destroy()V

    .line 65
    :cond_1
    if-eqz v2, :cond_2

    .line 66
    invoke-virtual {v2}, Landroid/renderscript/Allocation;->destroy()V

    .line 68
    :cond_2
    if-eqz v1, :cond_3

    .line 69
    invoke-virtual {v1}, Landroid/renderscript/ScriptIntrinsicBlur;->destroy()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 60
    :cond_3
    monitor-exit v4

    return-object v0

    .line 62
    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_0
    if-eqz v3, :cond_4

    .line 63
    :try_start_5
    invoke-virtual {v3}, Landroid/renderscript/Allocation;->destroy()V

    .line 65
    :cond_4
    if-eqz v2, :cond_5

    .line 66
    invoke-virtual {v2}, Landroid/renderscript/Allocation;->destroy()V

    .line 68
    :cond_5
    if-eqz v1, :cond_6

    .line 69
    invoke-virtual {v1}, Landroid/renderscript/ScriptIntrinsicBlur;->destroy()V

    :cond_6
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 43
    :catchall_1
    move-exception v0

    monitor-exit v4

    throw v0

    .line 62
    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_0

    :catchall_3
    move-exception v0

    goto :goto_0
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 29
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 30
    invoke-static {}, Lcom/twitter/util/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 29
    :goto_0
    return v0

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
