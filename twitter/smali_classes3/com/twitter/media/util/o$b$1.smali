.class Lcom/twitter/media/util/o$b$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/media/util/o$b;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/media/util/o$b;


# direct methods
.method constructor <init>(Lcom/twitter/media/util/o$b;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/twitter/media/util/o$b$1;->a:Lcom/twitter/media/util/o$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMediaScannerConnected()V
    .locals 6

    .prologue
    .line 266
    iget-object v0, p0, Lcom/twitter/media/util/o$b$1;->a:Lcom/twitter/media/util/o$b;

    iget-object v1, v0, Lcom/twitter/media/util/o$b;->b:Ljava/util/List;

    monitor-enter v1

    .line 267
    :try_start_0
    iget-object v0, p0, Lcom/twitter/media/util/o$b$1;->a:Lcom/twitter/media/util/o$b;

    iget-object v2, v0, Lcom/twitter/media/util/o$b;->b:Ljava/util/List;

    .line 268
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/twitter/media/util/o$b$a;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/media/util/o$b$a;

    .line 269
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 270
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 272
    iget-object v4, p0, Lcom/twitter/media/util/o$b$1;->a:Lcom/twitter/media/util/o$b;

    iget-object v4, v4, Lcom/twitter/media/util/o$b;->a:Landroid/media/MediaScannerConnection;

    iget-object v5, v3, Lcom/twitter/media/util/o$b$a;->a:Ljava/lang/String;

    iget-object v3, v3, Lcom/twitter/media/util/o$b$a;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v3}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 270
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 274
    :cond_0
    return-void
.end method

.method public onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 279
    iget-object v0, p0, Lcom/twitter/media/util/o$b$1;->a:Lcom/twitter/media/util/o$b;

    iget-object v1, v0, Lcom/twitter/media/util/o$b;->e:Landroid/support/v4/util/SimpleArrayMap;

    monitor-enter v1

    .line 280
    :try_start_0
    iget-object v0, p0, Lcom/twitter/media/util/o$b$1;->a:Lcom/twitter/media/util/o$b;

    iget-object v0, v0, Lcom/twitter/media/util/o$b;->e:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/SimpleArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/util/o$c;

    .line 281
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282
    if-eqz v0, :cond_0

    .line 284
    iget-object v1, p0, Lcom/twitter/media/util/o$b$1;->a:Lcom/twitter/media/util/o$b;

    iget-object v1, v1, Lcom/twitter/media/util/o$b;->d:Landroid/os/Handler;

    new-instance v2, Lcom/twitter/media/util/o$b$1$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/twitter/media/util/o$b$1$1;-><init>(Lcom/twitter/media/util/o$b$1;Lcom/twitter/media/util/o$c;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 291
    :cond_0
    return-void

    .line 281
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
