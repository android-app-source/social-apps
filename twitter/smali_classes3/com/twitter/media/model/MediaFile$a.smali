.class Lcom/twitter/media/model/MediaFile$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/media/model/MediaFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/media/model/MediaFile;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/media/model/MediaFile$1;)V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/twitter/media/model/MediaFile$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/media/model/MediaFile;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 267
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/media/model/MediaType;->a(I)Lcom/twitter/media/model/MediaType;

    move-result-object v0

    .line 268
    sget-object v1, Lcom/twitter/media/model/MediaFile$5;->a:[I

    invoke-virtual {v0}, Lcom/twitter/media/model/MediaType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 285
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unknown media type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 270
    :pswitch_0
    sget-object v0, Lcom/twitter/media/model/AnimatedGifFile;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    .line 282
    :goto_0
    return-object v0

    .line 273
    :pswitch_1
    sget-object v0, Lcom/twitter/media/model/ImageFile;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    goto :goto_0

    .line 276
    :pswitch_2
    sget-object v0, Lcom/twitter/media/model/SegmentedVideoFile;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    goto :goto_0

    .line 279
    :pswitch_3
    sget-object v0, Lcom/twitter/media/model/VideoFile;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    goto :goto_0

    .line 282
    :pswitch_4
    sget-object v0, Lcom/twitter/media/model/SvgFile;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    goto :goto_0

    .line 268
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/media/model/MediaFile;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 247
    iget-object v0, p2, Lcom/twitter/media/model/MediaFile;->g:Lcom/twitter/media/model/MediaType;

    iget v0, v0, Lcom/twitter/media/model/MediaType;->typeId:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 248
    instance-of v0, p2, Lcom/twitter/media/model/AnimatedGifFile;

    if-eqz v0, :cond_0

    .line 249
    sget-object v0, Lcom/twitter/media/model/AnimatedGifFile;->a:Lcom/twitter/util/serialization/l;

    check-cast p2, Lcom/twitter/media/model/AnimatedGifFile;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 261
    :goto_0
    return-void

    .line 250
    :cond_0
    instance-of v0, p2, Lcom/twitter/media/model/ImageFile;

    if-eqz v0, :cond_1

    .line 251
    sget-object v0, Lcom/twitter/media/model/ImageFile;->a:Lcom/twitter/util/serialization/l;

    check-cast p2, Lcom/twitter/media/model/ImageFile;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    goto :goto_0

    .line 252
    :cond_1
    instance-of v0, p2, Lcom/twitter/media/model/SegmentedVideoFile;

    if-eqz v0, :cond_2

    .line 253
    sget-object v0, Lcom/twitter/media/model/SegmentedVideoFile;->a:Lcom/twitter/util/serialization/l;

    check-cast p2, Lcom/twitter/media/model/SegmentedVideoFile;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    goto :goto_0

    .line 254
    :cond_2
    instance-of v0, p2, Lcom/twitter/media/model/VideoFile;

    if-eqz v0, :cond_3

    .line 255
    sget-object v0, Lcom/twitter/media/model/VideoFile;->a:Lcom/twitter/util/serialization/l;

    check-cast p2, Lcom/twitter/media/model/VideoFile;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    goto :goto_0

    .line 256
    :cond_3
    instance-of v0, p2, Lcom/twitter/media/model/SvgFile;

    if-eqz v0, :cond_4

    .line 257
    sget-object v0, Lcom/twitter/media/model/SvgFile;->a:Lcom/twitter/util/serialization/l;

    check-cast p2, Lcom/twitter/media/model/SvgFile;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    goto :goto_0

    .line 259
    :cond_4
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid media type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 243
    check-cast p2, Lcom/twitter/media/model/MediaFile;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/media/model/MediaFile$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/media/model/MediaFile;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 243
    invoke-virtual {p0, p1, p2}, Lcom/twitter/media/model/MediaFile$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    return-object v0
.end method
