.class public Lcom/twitter/media/model/MediaStoreBucket;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/media/model/MediaStoreBucket;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:[Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:J

.field private final d:J

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "bucket_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "bucket_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "datetaken"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/media/model/MediaStoreBucket;->a:[Ljava/lang/String;

    .line 29
    new-instance v0, Lcom/twitter/media/model/MediaStoreBucket$1;

    invoke-direct {v0}, Lcom/twitter/media/model/MediaStoreBucket$1;-><init>()V

    sput-object v0, Lcom/twitter/media/model/MediaStoreBucket;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/model/MediaStoreBucket;->b:Ljava/lang/String;

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/media/model/MediaStoreBucket;->c:J

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/media/model/MediaStoreBucket;->d:J

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/model/MediaStoreBucket;->e:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JJLjava/lang/String;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/twitter/media/model/MediaStoreBucket;->b:Ljava/lang/String;

    .line 57
    iput-wide p2, p0, Lcom/twitter/media/model/MediaStoreBucket;->c:J

    .line 58
    iput-wide p4, p0, Lcom/twitter/media/model/MediaStoreBucket;->d:J

    .line 59
    iput-object p6, p0, Lcom/twitter/media/model/MediaStoreBucket;->e:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public static a(Landroid/content/res/Resources;)Lcom/twitter/media/model/MediaStoreBucket;
    .locals 7

    .prologue
    .line 110
    sget v0, Lbzl$a;->gallery:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 111
    new-instance v0, Lcom/twitter/media/model/MediaStoreBucket;

    const-wide/16 v2, 0x0

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    const-string/jumbo v6, ""

    invoke-direct/range {v0 .. v6}, Lcom/twitter/media/model/MediaStoreBucket;-><init>(Ljava/lang/String;JJLjava/lang/String;)V

    return-object v0
.end method

.method public static a(Landroid/database/Cursor;)Lcom/twitter/media/model/MediaStoreBucket;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 90
    const/4 v1, 0x1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 91
    const/4 v2, 0x0

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 92
    const/4 v4, 0x2

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 94
    const/4 v6, 0x3

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 96
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-object v0

    .line 99
    :cond_1
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 100
    invoke-virtual {v7}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v6

    .line 102
    if-eqz v6, :cond_0

    .line 105
    new-instance v0, Lcom/twitter/media/model/MediaStoreBucket;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/media/model/MediaStoreBucket;-><init>(Ljava/lang/String;JJLjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/media/model/MediaStoreBucket;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/twitter/media/model/MediaStoreBucket;->c:J

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/media/model/MediaStoreBucket;->e:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 123
    if-ne p0, p1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return v0

    .line 126
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 127
    goto :goto_0

    .line 130
    :cond_3
    check-cast p1, Lcom/twitter/media/model/MediaStoreBucket;

    .line 132
    iget-wide v2, p0, Lcom/twitter/media/model/MediaStoreBucket;->c:J

    iget-wide v4, p1, Lcom/twitter/media/model/MediaStoreBucket;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/twitter/media/model/MediaStoreBucket;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/media/model/MediaStoreBucket;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/media/model/MediaStoreBucket;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v0

    .line 117
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/media/model/MediaStoreBucket;->c:J

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/twitter/media/model/MediaStoreBucket;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 143
    iget-wide v0, p0, Lcom/twitter/media/model/MediaStoreBucket;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 144
    iget-wide v0, p0, Lcom/twitter/media/model/MediaStoreBucket;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 145
    iget-object v0, p0, Lcom/twitter/media/model/MediaStoreBucket;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 146
    return-void
.end method
