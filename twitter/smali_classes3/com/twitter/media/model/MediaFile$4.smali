.class final Lcom/twitter/media/model/MediaFile$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/media/model/MediaFile;->b(Landroid/content/Context;Landroid/net/Uri;Lcom/twitter/media/model/MediaType;)Lrx/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Landroid/net/Uri;

.field final synthetic c:Lcom/twitter/media/model/MediaType;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/net/Uri;Lcom/twitter/media/model/MediaType;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/twitter/media/model/MediaFile$4;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/media/model/MediaFile$4;->b:Landroid/net/Uri;

    iput-object p3, p0, Lcom/twitter/media/model/MediaFile$4;->c:Lcom/twitter/media/model/MediaType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/media/model/MediaFile;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lcom/twitter/media/model/MediaFile$4;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/media/model/MediaFile$4;->b:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/media/model/MediaFile$4;->c:Lcom/twitter/media/model/MediaType;

    invoke-static {v0, v1, v2}, Lcom/twitter/media/model/MediaFile;->a(Landroid/content/Context;Landroid/net/Uri;Lcom/twitter/media/model/MediaType;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/twitter/media/model/MediaFile$4;->a()Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    return-object v0
.end method
