.class public Lcom/twitter/media/ui/fresco/a$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/media/ui/fresco/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/twitter/media/request/a;

.field private final b:Lbzb;

.field private c:Lcom/twitter/media/request/b$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/media/request/b$b",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lbzd;

.field private e:Lbzd;

.field private f:Lbzd;


# direct methods
.method public constructor <init>(Lcom/twitter/media/request/a;Lbzb;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p1, p0, Lcom/twitter/media/ui/fresco/a$a;->a:Lcom/twitter/media/request/a;

    .line 110
    iput-object p2, p0, Lcom/twitter/media/ui/fresco/a$a;->b:Lbzb;

    .line 111
    return-void
.end method

.method static synthetic a(Lcom/twitter/media/ui/fresco/a$a;)Lcom/twitter/media/request/a;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a$a;->a:Lcom/twitter/media/request/a;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/media/ui/fresco/a$a;)Lbzb;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a$a;->b:Lbzb;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/media/ui/fresco/a$a;)Lcom/twitter/media/request/b$b;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a$a;->c:Lcom/twitter/media/request/b$b;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/media/ui/fresco/a$a;)Lbzd;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a$a;->d:Lbzd;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/media/ui/fresco/a$a;)Lbzd;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a$a;->e:Lbzd;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/media/ui/fresco/a$a;)Lbzd;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a$a;->f:Lbzd;

    return-object v0
.end method


# virtual methods
.method public a(Lbzd;)Lcom/twitter/media/ui/fresco/a$a;
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/twitter/media/ui/fresco/a$a;->d:Lbzd;

    .line 122
    return-object p0
.end method

.method public a(Lcom/twitter/media/request/b$b;)Lcom/twitter/media/ui/fresco/a$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/media/request/b$b",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;)",
            "Lcom/twitter/media/ui/fresco/a$a;"
        }
    .end annotation

    .prologue
    .line 115
    iput-object p1, p0, Lcom/twitter/media/ui/fresco/a$a;->c:Lcom/twitter/media/request/b$b;

    .line 116
    return-object p0
.end method

.method public a()Lcom/twitter/media/ui/fresco/a;
    .locals 2

    .prologue
    .line 139
    new-instance v0, Lcom/twitter/media/ui/fresco/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/media/ui/fresco/a;-><init>(Lcom/twitter/media/ui/fresco/a$a;Lcom/twitter/media/ui/fresco/a$1;)V

    return-object v0
.end method

.method public b(Lbzd;)Lcom/twitter/media/ui/fresco/a$a;
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/twitter/media/ui/fresco/a$a;->e:Lbzd;

    .line 128
    return-object p0
.end method

.method public c(Lbzd;)Lcom/twitter/media/ui/fresco/a$a;
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/twitter/media/ui/fresco/a$a;->f:Lbzd;

    .line 134
    return-object p0
.end method
