.class Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/request/ImageRequester;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/media/ui/fresco/FrescoMediaImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final b:Lbzd;

.field private final c:Lbzd;

.field private final d:Lbzd;

.field private e:Lcom/facebook/drawee/view/GenericDraweeView;

.field private f:Lcom/twitter/media/request/a;

.field private g:Lcom/facebook/imagepipeline/request/ImageRequest;

.field private h:Lcom/twitter/media/request/b$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/media/request/b$b",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268
    const-string/jumbo v0, "photo_wait_time_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    new-instance v0, Lbzd;

    invoke-direct {v0}, Lbzd;-><init>()V

    iput-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->b:Lbzd;

    .line 270
    new-instance v0, Lbzd;

    invoke-direct {v0}, Lbzd;-><init>()V

    iput-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->c:Lbzd;

    .line 271
    new-instance v0, Lbzd;

    invoke-direct {v0}, Lbzd;-><init>()V

    iput-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->d:Lbzd;

    .line 277
    :goto_0
    return-void

    .line 273
    :cond_0
    iput-object v1, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->b:Lbzd;

    .line 274
    iput-object v1, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->c:Lbzd;

    .line 275
    iput-object v1, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->d:Lbzd;

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;)Lcom/facebook/imagepipeline/request/ImageRequest;
    .locals 1

    .prologue
    .line 249
    invoke-direct {p0}, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->d()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;Z)Z
    .locals 0

    .prologue
    .line 249
    iput-boolean p1, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;)Lcom/twitter/media/request/b$b;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->h:Lcom/twitter/media/request/b$b;

    return-object v0
.end method

.method private d()Lcom/facebook/imagepipeline/request/ImageRequest;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->g:Lcom/facebook/imagepipeline/request/ImageRequest;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/media/request/a;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->f:Lcom/twitter/media/request/a;

    return-object v0
.end method

.method public a(Lcom/facebook/drawee/view/GenericDraweeView;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->e:Lcom/facebook/drawee/view/GenericDraweeView;

    .line 281
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->b:Lbzd;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->b:Lbzd;

    invoke-virtual {v0, p1}, Lbzd;->a(Ljava/lang/String;)V

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->c:Lbzd;

    if-eqz v0, :cond_1

    .line 385
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->c:Lbzd;

    invoke-virtual {v0, p1}, Lbzd;->a(Ljava/lang/String;)V

    .line 387
    :cond_1
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->d:Lbzd;

    if-eqz v0, :cond_2

    .line 388
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->d:Lbzd;

    invoke-virtual {v0, p1}, Lbzd;->a(Ljava/lang/String;)V

    .line 390
    :cond_2
    return-void
.end method

.method public a(Z)V
    .locals 5

    .prologue
    .line 313
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->f:Lcom/twitter/media/request/a;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 314
    new-instance v1, Lbzb;

    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->f:Lcom/twitter/media/request/a;

    invoke-direct {v1, v0}, Lbzb;-><init>(Lcom/twitter/media/request/a;)V

    .line 316
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->d:Lbzd;

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->d:Lbzd;

    invoke-virtual {v0}, Lbzd;->a()V

    .line 318
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->b:Lbzd;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->b:Lbzd;

    invoke-virtual {v0}, Lbzd;->a()V

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->c:Lbzd;

    if-eqz v0, :cond_1

    .line 322
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->c:Lbzd;

    invoke-virtual {v0}, Lbzd;->a()V

    .line 326
    :cond_1
    new-instance v0, Lcom/twitter/media/ui/fresco/a$a;

    iget-object v2, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->f:Lcom/twitter/media/request/a;

    invoke-direct {v0, v2, v1}, Lcom/twitter/media/ui/fresco/a$a;-><init>(Lcom/twitter/media/request/a;Lbzb;)V

    new-instance v2, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a$1;

    invoke-direct {v2, p0}, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a$1;-><init>(Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;)V

    .line 328
    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/fresco/a$a;->a(Lcom/twitter/media/request/b$b;)Lcom/twitter/media/ui/fresco/a$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->b:Lbzd;

    .line 337
    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/fresco/a$a;->a(Lbzd;)Lcom/twitter/media/ui/fresco/a$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->c:Lbzd;

    .line 338
    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/fresco/a$a;->b(Lbzd;)Lcom/twitter/media/ui/fresco/a$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->d:Lbzd;

    .line 339
    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/fresco/a$a;->c(Lbzd;)Lcom/twitter/media/ui/fresco/a$a;

    move-result-object v0

    .line 340
    invoke-virtual {v0}, Lcom/twitter/media/ui/fresco/a$a;->a()Lcom/twitter/media/ui/fresco/a;

    move-result-object v2

    .line 342
    invoke-static {}, Lbq;->a()Lbs;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->g:Lcom/facebook/imagepipeline/request/ImageRequest;

    .line 343
    invoke-virtual {v0, v3}, Lbs;->b(Ljava/lang/Object;)Lbw;

    move-result-object v0

    check-cast v0, Lbs;

    .line 344
    invoke-virtual {v0, v1}, Lbs;->a(Ljava/lang/Object;)Lbw;

    move-result-object v0

    check-cast v0, Lbs;

    .line 345
    invoke-virtual {v0, v2}, Lbs;->a(Lby;)Lbw;

    move-result-object v0

    check-cast v0, Lbs;

    .line 346
    invoke-virtual {v0}, Lbs;->h()Lbv;

    move-result-object v1

    .line 348
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->f:Lcom/twitter/media/request/a;

    invoke-virtual {v0}, Lcom/twitter/media/request/a;->i()Lcom/twitter/util/math/c;

    move-result-object v2

    .line 349
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/twitter/util/math/c;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 350
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->e:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/GenericDraweeView;->getHierarchy()Lcc;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/generic/a;

    .line 351
    sget-object v3, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->h:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/generic/a;->a(Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)V

    .line 352
    new-instance v3, Landroid/graphics/PointF;

    invoke-virtual {v2}, Lcom/twitter/util/math/c;->e()F

    move-result v4

    invoke-virtual {v2}, Lcom/twitter/util/math/c;->f()F

    move-result v2

    invoke-direct {v3, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/generic/a;->a(Landroid/graphics/PointF;)V

    .line 355
    :cond_2
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->e:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/GenericDraweeView;->setController(Lcb;)V

    .line 356
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->i:Z

    .line 358
    :cond_3
    return-void
.end method

.method public a(Lcom/twitter/media/request/a;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 285
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->f:Lcom/twitter/media/request/a;

    .line 286
    invoke-static {v0, p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 287
    iput-object p1, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->f:Lcom/twitter/media/request/a;

    .line 288
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->C()Lcom/twitter/media/request/b$b;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->h:Lcom/twitter/media/request/b$b;

    .line 289
    if-nez p1, :cond_1

    .line 293
    :goto_1
    iput-object v1, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->g:Lcom/facebook/imagepipeline/request/ImageRequest;

    .line 294
    invoke-virtual {p0}, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->c()Z

    move v0, v2

    .line 297
    :goto_2
    return v0

    :cond_0
    move-object v0, v1

    .line 288
    goto :goto_0

    .line 289
    :cond_1
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->f:Lcom/twitter/media/request/a;

    .line 291
    invoke-virtual {v0}, Lcom/twitter/media/request/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->a(Landroid/net/Uri;)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;

    move-result-object v0

    .line 292
    invoke-virtual {v0, v2}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->a(Z)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;

    move-result-object v0

    .line 293
    invoke-virtual {v0}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->l()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v1

    goto :goto_1

    .line 297
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 362
    iget-boolean v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->i:Z

    return v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 367
    iput-boolean v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->i:Z

    .line 368
    iget-object v1, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->e:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/GenericDraweeView;->getController()Lcb;

    move-result-object v1

    .line 369
    if-eqz v1, :cond_1

    .line 370
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->d:Lbzd;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$a;->d:Lbzd;

    invoke-virtual {v0}, Lbzd;->b()V

    .line 373
    :cond_0
    invoke-interface {v1}, Lcb;->h()V

    .line 374
    const/4 v0, 0x1

    .line 376
    :cond_1
    return v0
.end method
