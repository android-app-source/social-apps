.class public Lcom/twitter/media/ui/fresco/a;
.super Lbx;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/media/ui/fresco/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbx",
        "<",
        "Ldt;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/media/request/a;

.field private final b:Lbzb;

.field private final c:Lcom/twitter/media/request/b$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/media/request/b$b",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lbzd;

.field private final e:Lbzd;

.field private final f:Lbzd;


# direct methods
.method private constructor <init>(Lcom/twitter/media/ui/fresco/a$a;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lbx;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/twitter/media/ui/fresco/a$a;->a(Lcom/twitter/media/ui/fresco/a$a;)Lcom/twitter/media/request/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/ui/fresco/a;->a:Lcom/twitter/media/request/a;

    .line 31
    invoke-static {p1}, Lcom/twitter/media/ui/fresco/a$a;->b(Lcom/twitter/media/ui/fresco/a$a;)Lbzb;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/ui/fresco/a;->b:Lbzb;

    .line 32
    invoke-static {p1}, Lcom/twitter/media/ui/fresco/a$a;->c(Lcom/twitter/media/ui/fresco/a$a;)Lcom/twitter/media/request/b$b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/ui/fresco/a;->c:Lcom/twitter/media/request/b$b;

    .line 33
    invoke-static {p1}, Lcom/twitter/media/ui/fresco/a$a;->d(Lcom/twitter/media/ui/fresco/a$a;)Lbzd;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/ui/fresco/a;->d:Lbzd;

    .line 34
    invoke-static {p1}, Lcom/twitter/media/ui/fresco/a$a;->e(Lcom/twitter/media/ui/fresco/a$a;)Lbzd;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/ui/fresco/a;->e:Lbzd;

    .line 35
    invoke-static {p1}, Lcom/twitter/media/ui/fresco/a$a;->f(Lcom/twitter/media/ui/fresco/a$a;)Lbzd;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/ui/fresco/a;->f:Lbzd;

    .line 36
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/media/ui/fresco/a$a;Lcom/twitter/media/ui/fresco/a$1;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/media/ui/fresco/a;-><init>(Lcom/twitter/media/ui/fresco/a$a;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ldt;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 62
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a;->d:Lbzd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a;->d:Lbzd;

    invoke-virtual {v0}, Lbzd;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a;->d:Lbzd;

    const-string/jumbo v1, "first"

    invoke-virtual {v0, v1}, Lbzd;->b(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a;->d:Lbzd;

    invoke-virtual {v0}, Lbzd;->b()V

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a;->e:Lbzd;

    if-eqz v0, :cond_1

    .line 68
    if-nez p2, :cond_2

    .line 69
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a;->e:Lbzd;

    invoke-virtual {v0}, Lbzd;->c()V

    .line 79
    :cond_1
    :goto_0
    return-void

    .line 70
    :cond_2
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a;->e:Lbzd;

    invoke-virtual {v0}, Lbzd;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    invoke-interface {p2}, Ldt;->g()Ldv;

    move-result-object v0

    invoke-interface {v0}, Ldv;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    new-instance v0, Landroid/graphics/Rect;

    .line 73
    invoke-interface {p2}, Ldt;->a()I

    move-result v1

    invoke-interface {p2}, Ldt;->b()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 74
    iget-object v1, p0, Lcom/twitter/media/ui/fresco/a;->e:Lbzd;

    const-string/jumbo v2, "intermediate"

    invoke-virtual {v1, v2}, Lbzd;->b(Ljava/lang/String;)V

    .line 75
    iget-object v1, p0, Lcom/twitter/media/ui/fresco/a;->e:Lbzd;

    iget-object v2, p0, Lcom/twitter/media/ui/fresco/a;->b:Lbzb;

    invoke-virtual {v2}, Lbzb;->a()Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lbzd;->a(Lcom/twitter/media/request/ResourceResponse$ResourceSource;Landroid/graphics/Rect;Ljava/lang/Long;)V

    .line 76
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a;->e:Lbzd;

    invoke-virtual {v0}, Lbzd;->b()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ldt;Landroid/graphics/drawable/Animatable;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 40
    iget-object v2, p0, Lcom/twitter/media/ui/fresco/a;->b:Lbzb;

    .line 41
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a;->f:Lbzd;

    if-eqz v0, :cond_0

    .line 42
    if-eqz p2, :cond_2

    new-instance v0, Landroid/graphics/Rect;

    .line 43
    invoke-interface {p2}, Ldt;->a()I

    move-result v3

    invoke-interface {p2}, Ldt;->b()I

    move-result v4

    invoke-direct {v0, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 46
    :goto_0
    iget-object v3, p0, Lcom/twitter/media/ui/fresco/a;->f:Lbzd;

    const-string/jumbo v4, "success"

    invoke-virtual {v3, v4}, Lbzd;->b(Ljava/lang/String;)V

    .line 47
    iget-object v3, p0, Lcom/twitter/media/ui/fresco/a;->f:Lbzd;

    invoke-virtual {v2}, Lbzb;->a()Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    move-result-object v4

    invoke-virtual {v3, v4, v0, v1}, Lbzd;->a(Lcom/twitter/media/request/ResourceResponse$ResourceSource;Landroid/graphics/Rect;Ljava/lang/Long;)V

    .line 48
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a;->f:Lbzd;

    invoke-virtual {v0}, Lbzd;->b()V

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/a;->c:Lcom/twitter/media/request/b$b;

    if-eqz v0, :cond_1

    .line 52
    new-instance v0, Lcom/twitter/media/request/ImageResponse$a;

    iget-object v1, p0, Lcom/twitter/media/ui/fresco/a;->a:Lcom/twitter/media/request/a;

    invoke-direct {v0, v1}, Lcom/twitter/media/request/ImageResponse$a;-><init>(Lcom/twitter/media/request/a;)V

    .line 53
    invoke-virtual {v2}, Lbzb;->a()Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/request/ImageResponse$a;->a(Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/media/request/ImageResponse$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 54
    invoke-virtual {v0, v1}, Lcom/twitter/media/request/ImageResponse$a;->a(Z)Lcom/twitter/media/request/ImageResponse$a;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/twitter/media/request/ImageResponse$a;->a()Lcom/twitter/media/request/ImageResponse;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/twitter/media/ui/fresco/a;->c:Lcom/twitter/media/request/b$b;

    invoke-interface {v1, v0}, Lcom/twitter/media/request/b$b;->a(Lcom/twitter/media/request/ResourceResponse;)V

    .line 58
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    .line 43
    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 0

    .prologue
    .line 20
    check-cast p2, Ldt;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/media/ui/fresco/a;->a(Ljava/lang/String;Ldt;Landroid/graphics/drawable/Animatable;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p2, Ldt;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/media/ui/fresco/a;->a(Ljava/lang/String;Ldt;)V

    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 83
    sget-object v0, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->a:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    .line 84
    iget-object v1, p0, Lcom/twitter/media/ui/fresco/a;->f:Lbzd;

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/twitter/media/ui/fresco/a;->f:Lbzd;

    const-string/jumbo v2, "failure"

    invoke-virtual {v1, v2}, Lbzd;->b(Ljava/lang/String;)V

    .line 86
    iget-object v1, p0, Lcom/twitter/media/ui/fresco/a;->f:Lbzd;

    invoke-virtual {v1, v0, v3, v3}, Lbzd;->a(Lcom/twitter/media/request/ResourceResponse$ResourceSource;Landroid/graphics/Rect;Ljava/lang/Long;)V

    .line 87
    iget-object v1, p0, Lcom/twitter/media/ui/fresco/a;->f:Lbzd;

    invoke-virtual {v1}, Lbzd;->b()V

    .line 90
    :cond_0
    iget-object v1, p0, Lcom/twitter/media/ui/fresco/a;->c:Lcom/twitter/media/request/b$b;

    if-eqz v1, :cond_1

    .line 91
    new-instance v1, Lcom/twitter/media/request/ImageResponse$a;

    iget-object v2, p0, Lcom/twitter/media/ui/fresco/a;->a:Lcom/twitter/media/request/a;

    invoke-direct {v1, v2}, Lcom/twitter/media/request/ImageResponse$a;-><init>(Lcom/twitter/media/request/a;)V

    .line 92
    invoke-virtual {v1, v0}, Lcom/twitter/media/request/ImageResponse$a;->a(Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/media/request/ImageResponse$a;

    move-result-object v0

    const/4 v1, 0x0

    .line 93
    invoke-virtual {v0, v1}, Lcom/twitter/media/request/ImageResponse$a;->a(Z)Lcom/twitter/media/request/ImageResponse$a;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lcom/twitter/media/request/ImageResponse$a;->a()Lcom/twitter/media/request/ImageResponse;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/twitter/media/ui/fresco/a;->c:Lcom/twitter/media/request/b$b;

    invoke-interface {v1, v0}, Lcom/twitter/media/request/b$b;->a(Lcom/twitter/media/request/ResourceResponse;)V

    .line 97
    :cond_1
    return-void
.end method
