.class public Lcom/twitter/media/ui/fresco/TwitterDraweeView;
.super Lcom/facebook/drawee/view/GenericDraweeView;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/ui/image/config/a;
.implements Lcom/twitter/media/ui/image/config/e;


# instance fields
.field private a:Lcom/twitter/media/ui/image/config/f;

.field private b:Lcom/twitter/media/ui/image/config/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/twitter/media/ui/a$a;->twitterDraweeViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/view/GenericDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    sget-object v0, Lcom/twitter/media/ui/image/config/f;->a:Lcom/twitter/media/ui/image/config/f;

    iput-object v0, p0, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->a:Lcom/twitter/media/ui/image/config/f;

    .line 22
    sget-object v0, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->a:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    iput-object v0, p0, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->b:Lcom/twitter/media/ui/image/config/g;

    .line 34
    sget-object v0, Lcom/twitter/media/ui/a$c;->TwitterDraweeView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 35
    invoke-virtual {p0}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->getHierarchy()Lcc;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/generic/a;

    sget v2, Lcom/twitter/media/ui/a$c;->TwitterDraweeView_defaultDrawable:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/generic/a;->b(Landroid/graphics/drawable/Drawable;)V

    .line 36
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 37
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->b:Lcom/twitter/media/ui/image/config/g;

    iget-object v1, p0, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->a:Lcom/twitter/media/ui/image/config/f;

    invoke-interface {v0, v1}, Lcom/twitter/media/ui/image/config/g;->b(Lcom/twitter/media/ui/image/config/f;)F

    move-result v1

    .line 41
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->b:Lcom/twitter/media/ui/image/config/g;

    iget-object v2, p0, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->a:Lcom/twitter/media/ui/image/config/f;

    invoke-interface {v0, v2}, Lcom/twitter/media/ui/image/config/g;->c(Lcom/twitter/media/ui/image/config/f;)F

    move-result v2

    .line 42
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->b:Lcom/twitter/media/ui/image/config/g;

    iget-object v3, p0, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->a:Lcom/twitter/media/ui/image/config/f;

    invoke-interface {v0, v3}, Lcom/twitter/media/ui/image/config/g;->d(Lcom/twitter/media/ui/image/config/f;)F

    move-result v3

    .line 43
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->b:Lcom/twitter/media/ui/image/config/g;

    iget-object v4, p0, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->a:Lcom/twitter/media/ui/image/config/f;

    invoke-interface {v0, v4}, Lcom/twitter/media/ui/image/config/g;->e(Lcom/twitter/media/ui/image/config/f;)F

    move-result v4

    .line 45
    invoke-virtual {p0}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->getHierarchy()Lcc;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/generic/a;

    invoke-virtual {v0}, Lcom/facebook/drawee/generic/a;->c()Lcom/facebook/drawee/generic/RoundingParams;

    move-result-object v0

    .line 46
    if-nez v0, :cond_0

    .line 47
    invoke-static {v1, v2, v3, v4}, Lcom/facebook/drawee/generic/RoundingParams;->b(FFFF)Lcom/facebook/drawee/generic/RoundingParams;

    move-result-object v0

    move-object v1, v0

    .line 51
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->getHierarchy()Lcc;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/generic/a;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/generic/a;->a(Lcom/facebook/drawee/generic/RoundingParams;)V

    .line 52
    return-void

    .line 49
    :cond_0
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/drawee/generic/RoundingParams;->a(FFFF)Lcom/facebook/drawee/generic/RoundingParams;

    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method public a(IF)V
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->getHierarchy()Lcc;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/generic/a;

    invoke-virtual {v0}, Lcom/facebook/drawee/generic/a;->c()Lcom/facebook/drawee/generic/RoundingParams;

    move-result-object v0

    .line 57
    if-nez v0, :cond_0

    .line 58
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/facebook/drawee/generic/RoundingParams;->b(F)Lcom/facebook/drawee/generic/RoundingParams;

    move-result-object v0

    move-object v1, v0

    .line 60
    :goto_0
    invoke-virtual {v1, p1, p2}, Lcom/facebook/drawee/generic/RoundingParams;->a(IF)Lcom/facebook/drawee/generic/RoundingParams;

    .line 61
    invoke-virtual {p0}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->getHierarchy()Lcc;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/generic/a;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/generic/a;->a(Lcom/facebook/drawee/generic/RoundingParams;)V

    .line 62
    return-void

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method public getCornerRadii()[F
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->getHierarchy()Lcc;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/generic/a;

    invoke-virtual {v0}, Lcom/facebook/drawee/generic/a;->c()Lcom/facebook/drawee/generic/RoundingParams;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/drawee/generic/RoundingParams;->b()[F

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRoundingConfig(Lcom/twitter/media/ui/image/config/f;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->a:Lcom/twitter/media/ui/image/config/f;

    .line 86
    invoke-direct {p0}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->a()V

    .line 87
    return-void
.end method

.method public setRoundingStrategy(Lcom/twitter/media/ui/image/config/g;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->b:Lcom/twitter/media/ui/image/config/g;

    .line 80
    invoke-direct {p0}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->a()V

    .line 81
    return-void
.end method
