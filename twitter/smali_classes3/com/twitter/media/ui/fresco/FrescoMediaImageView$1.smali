.class Lcom/twitter/media/ui/fresco/FrescoMediaImageView$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/ui/image/config/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/media/ui/fresco/FrescoMediaImageView;->a()Lcom/twitter/media/ui/image/config/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/media/ui/fresco/FrescoMediaImageView;


# direct methods
.method constructor <init>(Lcom/twitter/media/ui/fresco/FrescoMediaImageView;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$1;->a:Lcom/twitter/media/ui/fresco/FrescoMediaImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Lcom/twitter/media/ui/image/config/c;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$1;->a:Lcom/twitter/media/ui/fresco/FrescoMediaImageView;

    invoke-static {v0}, Lcom/twitter/media/ui/fresco/FrescoMediaImageView;->a(Lcom/twitter/media/ui/fresco/FrescoMediaImageView;)Lcom/twitter/media/ui/fresco/TwitterDraweeView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->getHierarchy()Lcc;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/generic/a;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/generic/a;->b(Landroid/graphics/drawable/Drawable;)V

    .line 127
    return-object p0
.end method

.method public a(IF)Lcom/twitter/media/ui/image/config/c;
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$1;->a:Lcom/twitter/media/ui/fresco/FrescoMediaImageView;

    invoke-static {v0, p2}, Lcom/twitter/media/ui/fresco/FrescoMediaImageView;->a(Lcom/twitter/media/ui/fresco/FrescoMediaImageView;F)F

    .line 96
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$1;->a:Lcom/twitter/media/ui/fresco/FrescoMediaImageView;

    invoke-static {v0}, Lcom/twitter/media/ui/fresco/FrescoMediaImageView;->a(Lcom/twitter/media/ui/fresco/FrescoMediaImageView;)Lcom/twitter/media/ui/fresco/TwitterDraweeView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->a(IF)V

    .line 97
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$1;->a:Lcom/twitter/media/ui/fresco/FrescoMediaImageView;

    invoke-static {v0}, Lcom/twitter/media/ui/fresco/FrescoMediaImageView;->a(Lcom/twitter/media/ui/fresco/FrescoMediaImageView;)Lcom/twitter/media/ui/fresco/TwitterDraweeView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$1;->a:Lcom/twitter/media/ui/fresco/FrescoMediaImageView;

    invoke-virtual {v1}, Lcom/twitter/media/ui/fresco/FrescoMediaImageView;->getRoundingConfig()Lcom/twitter/media/ui/image/config/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->setRoundingConfig(Lcom/twitter/media/ui/image/config/f;)V

    .line 98
    return-object p0
.end method

.method public a(Lcom/twitter/media/ui/image/config/g;)Lcom/twitter/media/ui/image/config/c;
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$1;->a:Lcom/twitter/media/ui/fresco/FrescoMediaImageView;

    invoke-static {v0}, Lcom/twitter/media/ui/fresco/FrescoMediaImageView;->a(Lcom/twitter/media/ui/fresco/FrescoMediaImageView;)Lcom/twitter/media/ui/fresco/TwitterDraweeView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->setRoundingStrategy(Lcom/twitter/media/ui/image/config/g;)V

    .line 105
    iget-object v0, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$1;->a:Lcom/twitter/media/ui/fresco/FrescoMediaImageView;

    invoke-static {v0}, Lcom/twitter/media/ui/fresco/FrescoMediaImageView;->a(Lcom/twitter/media/ui/fresco/FrescoMediaImageView;)Lcom/twitter/media/ui/fresco/TwitterDraweeView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/media/ui/fresco/FrescoMediaImageView$1;->a:Lcom/twitter/media/ui/fresco/FrescoMediaImageView;

    invoke-virtual {v1}, Lcom/twitter/media/ui/fresco/FrescoMediaImageView;->getRoundingConfig()Lcom/twitter/media/ui/image/config/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/fresco/TwitterDraweeView;->setRoundingConfig(Lcom/twitter/media/ui/image/config/f;)V

    .line 106
    return-object p0
.end method
