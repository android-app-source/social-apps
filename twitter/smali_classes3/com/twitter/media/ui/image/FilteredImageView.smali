.class public Lcom/twitter/media/ui/image/FilteredImageView;
.super Lcom/twitter/media/ui/image/BaseMediaImageView;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/media/ui/image/BaseMediaImageView",
        "<",
        "Lcom/twitter/media/ui/image/FilteredImageView;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private j:I

.field private k:Z

.field private l:Lcom/twitter/media/filters/FilterPreviewView;

.field private m:Landroid/graphics/Bitmap;

.field private n:Z

.field private o:Lcom/twitter/media/filters/Filters;

.field private p:Lcom/twitter/media/filters/b$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/media/ui/image/FilteredImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/media/ui/image/FilteredImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 42
    invoke-static {p1}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;)Z

    move-result v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/media/ui/image/FilteredImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    .line 43
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/media/ui/image/BaseMediaImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    new-instance v0, Lcom/twitter/media/ui/image/FixedSizeImageView;

    invoke-direct {v0, p1}, Lcom/twitter/media/ui/image/FixedSizeImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->a:Landroid/widget/ImageView;

    .line 49
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->a:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/twitter/media/ui/image/FilteredImageView;->addView(Landroid/view/View;)V

    .line 51
    if-eqz p4, :cond_0

    .line 52
    new-instance v0, Lcom/twitter/media/filters/FilterPreviewView;

    invoke-direct {v0, p1}, Lcom/twitter/media/filters/FilterPreviewView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    .line 53
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    iget-object v1, p0, Lcom/twitter/media/ui/image/FilteredImageView;->p:Lcom/twitter/media/filters/b$a;

    invoke-virtual {v0, v1}, Lcom/twitter/media/filters/FilterPreviewView;->setFilterRenderListener(Lcom/twitter/media/filters/b$a;)V

    .line 54
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/media/filters/FilterPreviewView;->setVisibility(I)V

    .line 56
    :cond_0
    return-void
.end method


# virtual methods
.method public a(IZ)V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    if-eqz v0, :cond_0

    .line 80
    iput p1, p0, Lcom/twitter/media/ui/image/FilteredImageView;->j:I

    .line 81
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    iget v1, p0, Lcom/twitter/media/ui/image/FilteredImageView;->j:I

    invoke-virtual {v0, v1}, Lcom/twitter/media/filters/FilterPreviewView;->setFilterId(I)V

    .line 82
    iget-boolean v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->k:Z

    if-eq v0, p2, :cond_0

    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->m:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 83
    iput-boolean p2, p0, Lcom/twitter/media/ui/image/FilteredImageView;->k:Z

    .line 84
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    iget-object v1, p0, Lcom/twitter/media/ui/image/FilteredImageView;->m:Landroid/graphics/Bitmap;

    iget-boolean v2, p0, Lcom/twitter/media/ui/image/FilteredImageView;->k:Z

    invoke-virtual {v0, v1, v2}, Lcom/twitter/media/filters/FilterPreviewView;->a(Landroid/graphics/Bitmap;Z)V

    .line 87
    :cond_0
    return-void
.end method

.method protected a(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 106
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->m:Landroid/graphics/Bitmap;

    .line 107
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->a:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 109
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 110
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/media/filters/FilterPreviewView;->setVisibility(I)V

    .line 113
    :cond_0
    return-void
.end method

.method protected a(Landroid/graphics/drawable/Drawable;Z)V
    .locals 3

    .prologue
    .line 117
    instance-of v0, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_1

    .line 118
    invoke-super {p0, p1, p2}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(Landroid/graphics/drawable/Drawable;Z)V

    .line 135
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 122
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->m:Landroid/graphics/Bitmap;

    .line 124
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    if-eqz v0, :cond_2

    .line 125
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/filters/FilterPreviewView;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    invoke-virtual {v0}, Lcom/twitter/media/filters/FilterPreviewView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    iget-object v1, p0, Lcom/twitter/media/ui/image/FilteredImageView;->m:Landroid/graphics/Bitmap;

    iget-boolean v2, p0, Lcom/twitter/media/ui/image/FilteredImageView;->k:Z

    invoke-virtual {v0, v1, v2}, Lcom/twitter/media/filters/FilterPreviewView;->a(Landroid/graphics/Bitmap;Z)V

    goto :goto_0

    .line 132
    :cond_2
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->a:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 133
    invoke-super {p0, p1, p2}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(Landroid/graphics/drawable/Drawable;Z)V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->n:Z

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    invoke-virtual {v0}, Lcom/twitter/media/filters/FilterPreviewView;->c()V

    .line 153
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->n:Z

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    invoke-virtual {v0}, Lcom/twitter/media/filters/FilterPreviewView;->d()V

    .line 159
    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    invoke-virtual {v0}, Lcom/twitter/media/filters/FilterPreviewView;->a()V

    .line 165
    :cond_0
    return-void
.end method

.method public getFilterIntensity()F
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    invoke-virtual {v0}, Lcom/twitter/media/filters/FilterPreviewView;->getFilterIntensity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getFilters()Lcom/twitter/media/filters/Filters;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->o:Lcom/twitter/media/filters/Filters;

    return-object v0
.end method

.method public getImageSize()Lcom/twitter/util/math/Size;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->a:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/twitter/util/math/Size;->a(Landroid/view/View;)Lcom/twitter/util/math/Size;

    move-result-object v0

    return-object v0
.end method

.method public getImageViewAnimator()Landroid/view/ViewPropertyAnimator;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    invoke-virtual {v0}, Lcom/twitter/media/filters/FilterPreviewView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    goto :goto_0
.end method

.method protected p()Z
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->m:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFilterIntensity(F)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    invoke-virtual {v0, p1}, Lcom/twitter/media/filters/FilterPreviewView;->setFilterIntensity(F)V

    .line 93
    :cond_0
    return-void
.end method

.method public setFilterRenderListener(Lcom/twitter/media/filters/b$a;)V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    invoke-virtual {v0, p1}, Lcom/twitter/media/filters/FilterPreviewView;->setFilterRenderListener(Lcom/twitter/media/filters/b$a;)V

    .line 171
    :cond_0
    iput-object p1, p0, Lcom/twitter/media/ui/image/FilteredImageView;->p:Lcom/twitter/media/filters/b$a;

    .line 172
    return-void
.end method

.method public setFilters(Lcom/twitter/media/filters/Filters;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 59
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    if-nez v0, :cond_0

    .line 71
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    invoke-virtual {p1}, Lcom/twitter/media/filters/Filters;->c()Lcom/twitter/media/filters/a;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/media/filters/FilterPreviewView;->a(Lcom/twitter/media/filters/Filters;Lcom/twitter/media/filters/a;)V

    .line 64
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    invoke-virtual {v0, v3}, Lcom/twitter/media/filters/FilterPreviewView;->setPreserveEGLContextOnPause(Z)V

    .line 65
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/media/ui/image/FilteredImageView;->addView(Landroid/view/View;I)V

    .line 66
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->m:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/twitter/media/ui/image/FilteredImageView;->l:Lcom/twitter/media/filters/FilterPreviewView;

    iget-object v1, p0, Lcom/twitter/media/ui/image/FilteredImageView;->m:Landroid/graphics/Bitmap;

    iget-boolean v2, p0, Lcom/twitter/media/ui/image/FilteredImageView;->k:Z

    invoke-virtual {v0, v1, v2}, Lcom/twitter/media/filters/FilterPreviewView;->a(Landroid/graphics/Bitmap;Z)V

    .line 69
    :cond_1
    iput-boolean v3, p0, Lcom/twitter/media/ui/image/FilteredImageView;->n:Z

    .line 70
    iput-object p1, p0, Lcom/twitter/media/ui/image/FilteredImageView;->o:Lcom/twitter/media/filters/Filters;

    goto :goto_0
.end method
