.class Lcom/twitter/media/ui/image/MediaImageView$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/media/ui/image/MediaImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Ljava/lang/Double;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/media/ui/image/MediaImageView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/media/ui/image/MediaImageView;)V
    .locals 1

    .prologue
    .line 393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/media/ui/image/MediaImageView$a;->a:Ljava/lang/ref/WeakReference;

    .line 395
    return-void
.end method


# virtual methods
.method public onEvent(Ljava/lang/Double;)V
    .locals 6

    .prologue
    .line 399
    iget-object v0, p0, Lcom/twitter/media/ui/image/MediaImageView$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    .line 400
    if-nez v0, :cond_1

    .line 411
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    invoke-static {v0}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/ui/image/MediaImageView;)Lcom/twitter/media/ui/AnimatingProgressBar;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 405
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v1, v2, v4

    if-nez v1, :cond_3

    .line 406
    :cond_2
    invoke-static {v0}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/ui/image/MediaImageView;)Lcom/twitter/media/ui/AnimatingProgressBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/AnimatingProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    .line 408
    :cond_3
    invoke-static {v0}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/ui/image/MediaImageView;)Lcom/twitter/media/ui/AnimatingProgressBar;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/AnimatingProgressBar;->a(I)V

    goto :goto_0
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 389
    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p0, p1}, Lcom/twitter/media/ui/image/MediaImageView$a;->onEvent(Ljava/lang/Double;)V

    return-void
.end method
