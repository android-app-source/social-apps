.class Lcom/twitter/media/ui/image/RichImageView$c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/media/ui/image/RichImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Matrix;

.field private final c:I

.field private final d:Landroid/graphics/drawable/shapes/Shape;

.field private final e:[F

.field private f:Landroid/graphics/Bitmap;

.field private g:Landroid/graphics/BitmapShader;

.field private h:I

.field private i:I


# direct methods
.method constructor <init>([F)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 495
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->a:Landroid/graphics/Paint;

    .line 496
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->b:Landroid/graphics/Matrix;

    .line 504
    iput v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->h:I

    .line 505
    iput v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->i:I

    .line 511
    iput-object p1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->e:[F

    .line 512
    new-instance v1, Landroid/graphics/drawable/shapes/RoundRectShape;

    invoke-direct {v1, p1, v3, v3}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    iput-object v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->d:Landroid/graphics/drawable/shapes/Shape;

    .line 514
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget v3, p1, v1

    .line 515
    int-to-float v4, v0

    cmpl-float v4, v3, v4

    if-lez v4, :cond_0

    .line 516
    float-to-int v0, v3

    .line 514
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 519
    :cond_1
    iput v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->c:I

    .line 520
    return-void
.end method

.method public static a(III)I
    .locals 2

    .prologue
    .line 583
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 584
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 586
    sparse-switch v1, :sswitch_data_0

    .line 606
    :goto_0
    return p2

    :sswitch_0
    move p2, v0

    .line 589
    goto :goto_0

    .line 592
    :sswitch_1
    const/4 v1, -0x2

    if-ne p1, v1, :cond_0

    .line 593
    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    goto :goto_0

    .line 594
    :cond_0
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    move p2, v0

    .line 595
    goto :goto_0

    .line 597
    :cond_1
    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p2

    goto :goto_0

    .line 586
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    .line 666
    instance-of v0, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_1

    .line 667
    check-cast p0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 685
    :cond_0
    :goto_0
    return-object v0

    .line 670
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 671
    if-lez v0, :cond_3

    .line 672
    :goto_1
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 673
    if-lez v1, :cond_2

    move p1, v1

    .line 676
    :cond_2
    :try_start_0
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, p1, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 677
    if-eqz v0, :cond_0

    .line 678
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 679
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    invoke-virtual {p0, v2, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 680
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 684
    :catch_0
    move-exception v0

    .line 685
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    move v0, p1

    .line 671
    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/media/ui/image/RichImageView$c;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->f:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/media/ui/image/RichImageView$c;)[F
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->e:[F

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 561
    iget v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->h:I

    return v0
.end method

.method public a(II)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/high16 v4, 0x3f000000    # 0.5f

    .line 529
    iget-object v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->d:Landroid/graphics/drawable/shapes/Shape;

    int-to-float v2, p1

    int-to-float v3, p2

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/shapes/Shape;->resize(FF)V

    .line 532
    iget-object v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->g:Landroid/graphics/BitmapShader;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->h:I

    if-ne v1, p1, :cond_1

    iget v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->i:I

    if-ne v1, p2, :cond_1

    .line 553
    :cond_0
    :goto_0
    return-void

    .line 540
    :cond_1
    iget v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->h:I

    mul-int/2addr v1, p2

    iget v2, p0, Lcom/twitter/media/ui/image/RichImageView$c;->i:I

    mul-int/2addr v2, p1

    if-le v1, v2, :cond_2

    .line 541
    int-to-float v1, p2

    iget v2, p0, Lcom/twitter/media/ui/image/RichImageView$c;->i:I

    int-to-float v2, v2

    div-float v2, v1, v2

    .line 542
    int-to-float v1, p1

    iget v3, p0, Lcom/twitter/media/ui/image/RichImageView$c;->h:I

    int-to-float v3, v3

    mul-float/2addr v3, v2

    sub-float/2addr v1, v3

    mul-float/2addr v1, v4

    .line 548
    :goto_1
    iget-object v3, p0, Lcom/twitter/media/ui/image/RichImageView$c;->b:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 549
    iget-object v2, p0, Lcom/twitter/media/ui/image/RichImageView$c;->b:Landroid/graphics/Matrix;

    add-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v1, v1

    add-float/2addr v0, v4

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 550
    iget-object v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->g:Landroid/graphics/BitmapShader;

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->g:Landroid/graphics/BitmapShader;

    iget-object v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 544
    :cond_2
    int-to-float v1, p1

    iget v2, p0, Lcom/twitter/media/ui/image/RichImageView$c;->h:I

    int-to-float v2, v2

    div-float v2, v1, v2

    .line 545
    int-to-float v1, p2

    iget v3, p0, Lcom/twitter/media/ui/image/RichImageView$c;->i:I

    int-to-float v3, v3

    mul-float/2addr v3, v2

    sub-float/2addr v1, v3

    mul-float/2addr v1, v4

    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_1
.end method

.method public a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 611
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 612
    invoke-virtual {v0, p2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 613
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 614
    iget v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->c:I

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/twitter/media/ui/image/RichImageView$c;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 615
    invoke-virtual {p0, v0}, Lcom/twitter/media/ui/image/RichImageView$c;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 619
    :goto_0
    return-void

    .line 616
    :catch_0
    move-exception v0

    .line 617
    :goto_1
    const-string/jumbo v1, "RichImageView"

    const-string/jumbo v2, "RichImageView.setImageURI failed"

    invoke-static {v1, v2, v0}, Lcqj;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 616
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public a(Landroid/content/res/Resources;I)V
    .locals 2

    .prologue
    .line 622
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 623
    iget v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->c:I

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/twitter/media/ui/image/RichImageView$c;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 624
    invoke-virtual {p0, v0}, Lcom/twitter/media/ui/image/RichImageView$c;->a(Landroid/graphics/Bitmap;)V

    .line 625
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 634
    if-eqz p1, :cond_0

    .line 635
    new-instance v0, Landroid/graphics/BitmapShader;

    sget-object v1, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v0, p1, v1, v2}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->g:Landroid/graphics/BitmapShader;

    .line 636
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->h:I

    .line 637
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->i:I

    .line 643
    :goto_0
    iput-object p1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->f:Landroid/graphics/Bitmap;

    .line 644
    iget-object v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->a:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->g:Landroid/graphics/BitmapShader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 645
    return-void

    .line 639
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->g:Landroid/graphics/BitmapShader;

    .line 640
    iput v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->h:I

    .line 641
    iput v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->i:I

    goto :goto_0
.end method

.method public a(Landroid/graphics/Canvas;II)V
    .locals 2

    .prologue
    .line 648
    int-to-float v0, p2

    int-to-float v1, p3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 649
    iget-object v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->d:Landroid/graphics/drawable/shapes/Shape;

    iget-object v1, p0, Lcom/twitter/media/ui/image/RichImageView$c;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1}, Landroid/graphics/drawable/shapes/Shape;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 650
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 628
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->c:I

    mul-int/lit8 v0, v0, 0x2

    .line 629
    invoke-static {p1, v0}, Lcom/twitter/media/ui/image/RichImageView$c;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 630
    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/media/ui/image/RichImageView$c;->a(Landroid/graphics/Bitmap;)V

    .line 631
    return-void

    .line 629
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 568
    iget v0, p0, Lcom/twitter/media/ui/image/RichImageView$c;->i:I

    return v0
.end method
