.class public Lcom/twitter/media/ui/image/UserImageView;
.super Lcom/twitter/media/ui/image/MediaImageView;
.source "Twttr"


# static fields
.field private static final a:[I

.field private static final k:Lcom/twitter/util/collection/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/e",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:Lbzi;

.field private q:Lcom/twitter/util/math/c;

.field private r:Landroid/graphics/drawable/GradientDrawable;

.field private s:Landroid/graphics/drawable/StateListDrawable;

.field private t:Z

.field private u:I

.field private v:[F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 48
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a7

    aput v2, v0, v1

    sput-object v0, Lcom/twitter/media/ui/image/UserImageView;->a:[I

    .line 50
    new-instance v0, Lcom/twitter/util/collection/e;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/twitter/util/collection/e;-><init>(ILcom/twitter/util/collection/e$c;)V

    sput-object v0, Lcom/twitter/media/ui/image/UserImageView;->k:Lcom/twitter/util/collection/e;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/media/ui/image/UserImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 70
    sget v0, Lbzk$a;->userImageViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/media/ui/image/UserImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v6, -0x3

    .line 74
    new-instance v4, Lcom/twitter/media/ui/image/RichImageView;

    invoke-direct {v4, p1}, Lcom/twitter/media/ui/image/RichImageView;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/media/ui/image/MediaImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILandroid/widget/ImageView;Z)V

    .line 54
    iput v6, p0, Lcom/twitter/media/ui/image/UserImageView;->m:I

    .line 55
    iput v6, p0, Lcom/twitter/media/ui/image/UserImageView;->n:I

    .line 61
    iput-boolean v5, p0, Lcom/twitter/media/ui/image/UserImageView;->t:Z

    .line 75
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 76
    sget-object v0, Lbzk$d;->UserImageView:[I

    invoke-virtual {p1, p2, v0, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 77
    sget v0, Lbzk$d;->UserImageView_userImageSize:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 78
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 79
    invoke-virtual {v0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x2d

    if-eq v3, v4, :cond_0

    const-string/jumbo v3, "0x"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 80
    :cond_0
    sget v0, Lbzk$d;->UserImageView_userImageSize:I

    invoke-virtual {v2, v0, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/media/ui/image/UserImageView;->n:I

    iput v0, p0, Lcom/twitter/media/ui/image/UserImageView;->m:I

    .line 84
    :goto_0
    sget v0, Lbzk$d;->UserImageView_imageCornerRadius:I

    invoke-virtual {v2, v0, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    int-to-float v0, v0

    .line 85
    sget v3, Lbzk$d;->UserImageView_roundingStrategy:I

    invoke-virtual {v2, v3, v7}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    .line 86
    sget v4, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->e:I

    if-ne v3, v4, :cond_3

    sget-object v0, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->c:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    .line 89
    :goto_1
    sget v3, Lbzk$b;->placeholder_bg:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iput v3, p0, Lcom/twitter/media/ui/image/UserImageView;->u:I

    .line 90
    iget v3, p0, Lcom/twitter/media/ui/image/UserImageView;->l:I

    if-nez v3, :cond_1

    .line 91
    sget v3, Lbzk$b;->light_transparent_black:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/media/ui/image/UserImageView;->l:I

    .line 93
    :cond_1
    invoke-virtual {p0, v0}, Lcom/twitter/media/ui/image/UserImageView;->setRoundingStrategy(Lcom/twitter/media/ui/image/config/g;)V

    .line 94
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 95
    const-string/jumbo v0, "profile"

    invoke-virtual {p0, v0}, Lcom/twitter/media/ui/image/UserImageView;->setImageType(Ljava/lang/String;)V

    .line 96
    return-void

    .line 82
    :cond_2
    sget v0, Lbzk$d;->UserImageView_userImageSize:I

    invoke-virtual {v2, v0, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/media/ui/image/UserImageView;->n:I

    iput v0, p0, Lcom/twitter/media/ui/image/UserImageView;->m:I

    goto :goto_0

    .line 87
    :cond_3
    invoke-static {v0}, Lcom/twitter/media/ui/image/config/d;->a(F)Lcom/twitter/media/ui/image/config/g;

    move-result-object v0

    goto :goto_1
.end method

.method private a([F)Landroid/graphics/drawable/StateListDrawable;
    .locals 3

    .prologue
    .line 332
    iget-object v0, p0, Lcom/twitter/media/ui/image/UserImageView;->s:Landroid/graphics/drawable/StateListDrawable;

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/twitter/media/ui/image/UserImageView;->s:Landroid/graphics/drawable/StateListDrawable;

    move-object v1, v0

    .line 341
    :goto_0
    if-eqz p1, :cond_1

    array-length v0, p1

    const/16 v2, 0x8

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    .line 342
    :goto_1
    if-eqz v0, :cond_2

    .line 343
    iget-object v0, p0, Lcom/twitter/media/ui/image/UserImageView;->r:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    .line 347
    :goto_2
    return-object v1

    .line 335
    :cond_0
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 336
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object v1, p0, Lcom/twitter/media/ui/image/UserImageView;->r:Landroid/graphics/drawable/GradientDrawable;

    .line 337
    iget-object v1, p0, Lcom/twitter/media/ui/image/UserImageView;->r:Landroid/graphics/drawable/GradientDrawable;

    iget v2, p0, Lcom/twitter/media/ui/image/UserImageView;->l:I

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 338
    sget-object v1, Lcom/twitter/media/ui/image/UserImageView;->a:[I

    iget-object v2, p0, Lcom/twitter/media/ui/image/UserImageView;->r:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    move-object v1, v0

    goto :goto_0

    .line 341
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 345
    :cond_2
    iget-object v0, p0, Lcom/twitter/media/ui/image/UserImageView;->r:Landroid/graphics/drawable/GradientDrawable;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    goto :goto_2
.end method

.method private static a(Lcom/twitter/util/math/Size;[FI)Ljava/lang/String;
    .locals 5

    .prologue
    .line 402
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/twitter/util/math/Size;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 403
    const-string/jumbo v0, "_color_"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 404
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 405
    if-eqz p1, :cond_0

    .line 406
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p1, v0

    .line 407
    const-string/jumbo v4, "_"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 406
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 411
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/media/ui/image/RichImageView;)V
    .locals 1

    .prologue
    .line 287
    iget-boolean v0, p0, Lcom/twitter/media/ui/image/UserImageView;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/media/ui/image/UserImageView;->s:Landroid/graphics/drawable/StateListDrawable;

    :goto_0
    invoke-virtual {p1, v0}, Lcom/twitter/media/ui/image/RichImageView;->setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 288
    return-void

    .line 287
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 291
    invoke-super {p0}, Lcom/twitter/media/ui/image/MediaImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    return v0
.end method

.method private a(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 3

    .prologue
    .line 295
    if-eqz p1, :cond_1

    .line 296
    iget v0, p0, Lcom/twitter/media/ui/image/UserImageView;->m:I

    invoke-static {v0}, Lcom/twitter/media/manager/UserImageRequest;->a(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 297
    iget v1, p0, Lcom/twitter/media/ui/image/UserImageView;->n:I

    invoke-static {v1}, Lcom/twitter/media/manager/UserImageRequest;->a(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    .line 298
    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v2, v0, :cond_0

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v2, v1, :cond_1

    .line 299
    :cond_0
    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 300
    iput v1, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 301
    const/4 v0, 0x1

    .line 304
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/graphics/drawable/Drawable;Z)V
    .locals 1

    .prologue
    .line 253
    invoke-super {p0, p1}, Lcom/twitter/media/ui/image/MediaImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 254
    iput-boolean p2, p0, Lcom/twitter/media/ui/image/UserImageView;->o:Z

    .line 255
    iget-boolean v0, p0, Lcom/twitter/media/ui/image/UserImageView;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/media/ui/image/UserImageView;->e:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 256
    invoke-direct {p0}, Lcom/twitter/media/ui/image/UserImageView;->c()V

    .line 258
    :cond_0
    return-void
.end method

.method private b()Z
    .locals 3

    .prologue
    .line 308
    iget v0, p0, Lcom/twitter/media/ui/image/UserImageView;->m:I

    invoke-static {v0}, Lcom/twitter/media/manager/UserImageRequest;->a(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getPaddingRight()I

    move-result v1

    add-int/2addr v1, v0

    .line 309
    iget v0, p0, Lcom/twitter/media/ui/image/UserImageView;->n:I

    invoke-static {v0}, Lcom/twitter/media/manager/UserImageRequest;->a(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getPaddingTop()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v2, v0

    .line 311
    iget-object v0, p0, Lcom/twitter/media/ui/image/UserImageView;->e:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/GradientDrawable;

    if-eqz v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/twitter/media/ui/image/UserImageView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/media/ui/image/UserImageView;->e:Landroid/graphics/drawable/Drawable;

    .line 313
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    if-eq v0, v2, :cond_1

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/twitter/media/ui/image/UserImageView;->e:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setSize(II)V

    .line 315
    const/4 v0, 0x1

    .line 318
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(II)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 240
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 241
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/RichImageView;

    .line 242
    invoke-virtual {v0}, Lcom/twitter/media/ui/image/RichImageView;->getCornerRadii()[F

    move-result-object v0

    .line 243
    if-eqz v0, :cond_0

    array-length v2, v0

    if-lez v2, :cond_0

    const/4 v2, 0x0

    aget v0, v0, v2

    :goto_0
    int-to-float v2, p1

    add-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 244
    invoke-virtual {v1, p2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 245
    return-object v1

    .line 243
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 359
    iget-object v0, p0, Lcom/twitter/media/ui/image/UserImageView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/media/ui/image/UserImageView;->o:Z

    if-eqz v0, :cond_0

    .line 398
    :goto_0
    return-void

    .line 368
    :cond_0
    iget v0, p0, Lcom/twitter/media/ui/image/UserImageView;->m:I

    invoke-static {v0}, Lcom/twitter/media/manager/UserImageRequest;->a(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getPaddingRight()I

    move-result v2

    add-int/2addr v2, v0

    .line 369
    iget v0, p0, Lcom/twitter/media/ui/image/UserImageView;->n:I

    invoke-static {v0}, Lcom/twitter/media/manager/UserImageRequest;->a(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getPaddingTop()I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v3, v0

    .line 371
    iget v4, p0, Lcom/twitter/media/ui/image/UserImageView;->u:I

    .line 372
    iget-object v5, p0, Lcom/twitter/media/ui/image/UserImageView;->v:[F

    .line 373
    invoke-static {v2, v3}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v6

    .line 374
    invoke-static {v6, v5, v4}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/util/math/Size;[FI)Ljava/lang/String;

    move-result-object v7

    .line 375
    sget-object v0, Lcom/twitter/media/ui/image/UserImageView;->k:Lcom/twitter/util/collection/e;

    invoke-virtual {v0, v7}, Lcom/twitter/util/collection/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 376
    if-nez v0, :cond_1

    .line 377
    new-instance v8, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v8}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 378
    invoke-virtual {v8, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 380
    invoke-virtual {v8, v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setSize(II)V

    .line 383
    if-eqz v5, :cond_2

    array-length v0, v5

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    .line 384
    :goto_1
    if-eqz v0, :cond_3

    .line 385
    invoke-virtual {v8, v5}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    .line 389
    :goto_2
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v0}, Lcom/twitter/media/util/a;->a(Lcom/twitter/util/math/Size;Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 390
    if-eqz v0, :cond_1

    .line 391
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 392
    invoke-virtual {v6}, Lcom/twitter/util/math/Size;->a()I

    move-result v3

    invoke-virtual {v6}, Lcom/twitter/util/math/Size;->b()I

    move-result v4

    invoke-virtual {v8, v1, v1, v3, v4}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 393
    invoke-virtual {v8, v2}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 394
    sget-object v2, Lcom/twitter/media/ui/image/UserImageView;->k:Lcom/twitter/util/collection/e;

    invoke-virtual {v2, v7, v0}, Lcom/twitter/util/collection/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    :cond_1
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-direct {p0, v2, v1}, Lcom/twitter/media/ui/image/UserImageView;->b(Landroid/graphics/drawable/Drawable;Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 383
    goto :goto_1

    .line 387
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    goto :goto_2
.end method


# virtual methods
.method public a(II)V
    .locals 1

    .prologue
    .line 130
    iput p1, p0, Lcom/twitter/media/ui/image/UserImageView;->m:I

    .line 131
    iput p2, p0, Lcom/twitter/media/ui/image/UserImageView;->n:I

    .line 132
    invoke-direct {p0}, Lcom/twitter/media/ui/image/UserImageView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->requestLayout()V

    .line 134
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->q()V

    .line 136
    :cond_0
    invoke-direct {p0}, Lcom/twitter/media/ui/image/UserImageView;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 139
    :cond_1
    return-void
.end method

.method public a(IILcom/twitter/media/ui/image/config/g;)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 222
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 226
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/twitter/media/ui/image/UserImageView;->setPadding(IIII)V

    .line 232
    invoke-virtual {p0, v0}, Lcom/twitter/media/ui/image/UserImageView;->setBorderSize(I)V

    .line 233
    invoke-virtual {p0, p3}, Lcom/twitter/media/ui/image/UserImageView;->setRoundingStrategy(Lcom/twitter/media/ui/image/config/g;)V

    .line 234
    invoke-direct {p0, v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->c(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 235
    invoke-virtual {p0, v0}, Lcom/twitter/media/ui/image/UserImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 236
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 279
    iget-boolean v0, p0, Lcom/twitter/media/ui/image/UserImageView;->t:Z

    if-eq v0, p1, :cond_0

    .line 280
    iput-boolean p1, p0, Lcom/twitter/media/ui/image/UserImageView;->t:Z

    .line 281
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/RichImageView;

    .line 282
    invoke-direct {p0, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/media/ui/image/RichImageView;)V

    .line 284
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/media/request/a$a;Z)Z
    .locals 2

    .prologue
    .line 186
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Use setUser or setUserImageUrl"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)Z
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;Z)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;Z)Z
    .locals 4

    .prologue
    .line 152
    if-eqz p1, :cond_0

    .line 153
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {p0, v0, v2, v3, p2}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;JZ)Z

    move-result v0

    .line 155
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 165
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;ZLcom/twitter/media/request/a$b;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;JZ)Z
    .locals 2

    .prologue
    .line 160
    invoke-static {p2, p3, p1}, Lcom/twitter/media/util/s;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p4, v1}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;ZLcom/twitter/media/request/a$b;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Lcom/twitter/media/request/a$b;)Z
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;ZLcom/twitter/media/request/a$b;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;ZLcom/twitter/media/request/a$b;)Z
    .locals 2

    .prologue
    .line 174
    invoke-static {p1}, Lcom/twitter/media/manager/UserImageRequest;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 175
    if-eqz p3, :cond_0

    .line 176
    invoke-virtual {v0, p3}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/b$b;)Lcom/twitter/media/request/b$a;

    .line 178
    :cond_0
    iget-object v1, p0, Lcom/twitter/media/ui/image/UserImageView;->p:Lbzi;

    .line 179
    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a$a;->a(Lbzi;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/media/ui/image/UserImageView;->q:Lcom/twitter/util/math/c;

    .line 180
    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/util/math/c;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 178
    invoke-super {p0, v0, p2}, Lcom/twitter/media/ui/image/MediaImageView;->a(Lcom/twitter/media/request/a$a;Z)Z

    move-result v0

    return v0
.end method

.method public b(II)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 249
    sget-object v0, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->b:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(IILcom/twitter/media/ui/image/config/g;)V

    .line 250
    return-void
.end method

.method public getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Lcom/twitter/media/ui/image/MediaImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 108
    invoke-direct {p0, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Landroid/view/ViewGroup$LayoutParams;)Z

    .line 109
    return-object v0
.end method

.method protected getRoundingConfig()Lcom/twitter/media/ui/image/config/f;
    .locals 3

    .prologue
    .line 263
    iget v0, p0, Lcom/twitter/media/ui/image/UserImageView;->m:I

    invoke-static {v0}, Lcom/twitter/media/manager/UserImageRequest;->a(I)I

    move-result v0

    int-to-float v0, v0

    .line 264
    iget v1, p0, Lcom/twitter/media/ui/image/UserImageView;->n:I

    invoke-static {v1}, Lcom/twitter/media/manager/UserImageRequest;->a(I)I

    move-result v1

    int-to-float v1, v1

    .line 265
    iget v2, p0, Lcom/twitter/media/ui/image/UserImageView;->j:I

    int-to-float v2, v2

    invoke-static {v0, v1, v2}, Lcom/twitter/media/ui/image/config/f;->a(FFF)Lcom/twitter/media/ui/image/config/f;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 100
    invoke-super {p0}, Lcom/twitter/media/ui/image/MediaImageView;->onAttachedToWindow()V

    .line 101
    invoke-direct {p0}, Lcom/twitter/media/ui/image/UserImageView;->a()Z

    .line 102
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 115
    if-nez v0, :cond_0

    .line 116
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Must set size before trying the measure the view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_0
    invoke-direct {p0}, Lcom/twitter/media/ui/image/UserImageView;->a()Z

    .line 119
    invoke-direct {p0}, Lcom/twitter/media/ui/image/UserImageView;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 122
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/twitter/media/ui/image/MediaImageView;->onMeasure(II)V

    .line 123
    return-void
.end method

.method protected q()V
    .locals 2

    .prologue
    .line 270
    invoke-super {p0}, Lcom/twitter/media/ui/image/MediaImageView;->q()V

    .line 271
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/RichImageView;

    .line 272
    invoke-virtual {v0}, Lcom/twitter/media/ui/image/RichImageView;->getCornerRadii()[F

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/media/ui/image/UserImageView;->v:[F

    .line 273
    iget-object v1, p0, Lcom/twitter/media/ui/image/UserImageView;->v:[F

    invoke-direct {p0, v1}, Lcom/twitter/media/ui/image/UserImageView;->a([F)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/media/ui/image/UserImageView;->s:Landroid/graphics/drawable/StateListDrawable;

    .line 274
    invoke-direct {p0, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/media/ui/image/RichImageView;)V

    .line 275
    invoke-direct {p0}, Lcom/twitter/media/ui/image/UserImageView;->c()V

    .line 276
    return-void
.end method

.method public setCropRectangle(Lcom/twitter/util/math/c;)V
    .locals 1

    .prologue
    .line 199
    iput-object p1, p0, Lcom/twitter/media/ui/image/UserImageView;->q:Lcom/twitter/util/math/c;

    .line 200
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getRequestBuilder()Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_0

    .line 202
    invoke-virtual {v0, p1}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/util/math/c;)Lcom/twitter/media/request/a$a;

    .line 203
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->e()V

    .line 205
    :cond_0
    return-void
.end method

.method public setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/media/ui/image/UserImageView;->b(Landroid/graphics/drawable/Drawable;Z)V

    .line 217
    return-void
.end method

.method public setDefaultDrawableColor(I)V
    .locals 0

    .prologue
    .line 211
    iput p1, p0, Lcom/twitter/media/ui/image/UserImageView;->u:I

    .line 212
    return-void
.end method

.method public setPadding(IIII)V
    .locals 0

    .prologue
    .line 143
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/media/ui/image/MediaImageView;->setPadding(IIII)V

    .line 144
    invoke-direct {p0}, Lcom/twitter/media/ui/image/UserImageView;->a()Z

    .line 145
    return-void
.end method

.method public setSize(I)V
    .locals 0

    .prologue
    .line 126
    invoke-virtual {p0, p1, p1}, Lcom/twitter/media/ui/image/UserImageView;->a(II)V

    .line 127
    return-void
.end method

.method public setTransformation(Lbzi;)V
    .locals 1

    .prologue
    .line 190
    iput-object p1, p0, Lcom/twitter/media/ui/image/UserImageView;->p:Lbzi;

    .line 191
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->getRequestBuilder()Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 192
    if-eqz v0, :cond_0

    .line 193
    invoke-virtual {v0, p1}, Lcom/twitter/media/request/a$a;->a(Lbzi;)Lcom/twitter/media/request/a$a;

    .line 194
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/UserImageView;->e()V

    .line 196
    :cond_0
    return-void
.end method
