.class public Lcom/twitter/media/ui/image/BadgeableUserImageView;
.super Lcom/twitter/media/ui/image/UserImageView;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/widget/b;


# instance fields
.field private final a:Lcom/twitter/ui/widget/a;

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/media/ui/image/BadgeableUserImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 27
    sget v0, Lbzk$a;->userImageViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/media/ui/image/BadgeableUserImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/media/ui/image/UserImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    invoke-virtual {p0, v3}, Lcom/twitter/media/ui/image/BadgeableUserImageView;->setWillNotDraw(Z)V

    .line 33
    sget-object v0, Lbzk$d;->BadgeableUserImageView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 34
    new-instance v1, Lcom/twitter/ui/widget/a;

    sget v2, Lbzk$d;->BadgeableUserImageView_badgeIndicatorStyle:I

    .line 35
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-direct {v1, p0, p1, v2}, Lcom/twitter/ui/widget/a;-><init>(Landroid/view/View;Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/twitter/media/ui/image/BadgeableUserImageView;->a:Lcom/twitter/ui/widget/a;

    .line 36
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 39
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BadgeableUserImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbzk$c;->badge_number_background_stroke_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/media/ui/image/BadgeableUserImageView;->k:I

    .line 40
    iget-object v0, p0, Lcom/twitter/media/ui/image/BadgeableUserImageView;->a:Lcom/twitter/ui/widget/a;

    iget v1, p0, Lcom/twitter/media/ui/image/BadgeableUserImageView;->k:I

    neg-int v1, v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/a;->a(I)V

    .line 41
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/twitter/media/ui/image/UserImageView;->draw(Landroid/graphics/Canvas;)V

    .line 82
    iget-object v0, p0, Lcom/twitter/media/ui/image/BadgeableUserImageView;->a:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/a;->a(Landroid/graphics/Canvas;)V

    .line 83
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0}, Lcom/twitter/media/ui/image/UserImageView;->drawableStateChanged()V

    .line 67
    iget-object v0, p0, Lcom/twitter/media/ui/image/BadgeableUserImageView;->a:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/a;->b()V

    .line 68
    return-void
.end method

.method getBadgeIndicator()Lcom/twitter/ui/widget/a;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/media/ui/image/BadgeableUserImageView;->a:Lcom/twitter/ui/widget/a;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    .line 72
    invoke-super/range {p0 .. p5}, Lcom/twitter/media/ui/image/UserImageView;->onLayout(ZIIII)V

    .line 75
    iget-object v0, p0, Lcom/twitter/media/ui/image/BadgeableUserImageView;->a:Lcom/twitter/ui/widget/a;

    iget v1, p0, Lcom/twitter/media/ui/image/BadgeableUserImageView;->k:I

    add-int v4, p4, v1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v1, p1

    move v2, p2

    move v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/ui/widget/a;->a(ZIIIILandroid/graphics/Rect;I)V

    .line 77
    return-void
.end method

.method public setBadgeMode(I)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/media/ui/image/BadgeableUserImageView;->a:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/a;->setBadgeMode(I)V

    .line 52
    return-void
.end method

.method public setBadgeNumber(I)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/media/ui/image/BadgeableUserImageView;->a:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/a;->setBadgeNumber(I)V

    .line 57
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/media/ui/image/BadgeableUserImageView;->a:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/a;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/media/ui/image/UserImageView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
