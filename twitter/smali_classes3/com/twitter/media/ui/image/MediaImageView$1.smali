.class Lcom/twitter/media/ui/image/MediaImageView$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/ui/image/config/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/media/ui/image/MediaImageView;->a()Lcom/twitter/media/ui/image/config/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/media/ui/image/MediaImageView;


# direct methods
.method constructor <init>(Lcom/twitter/media/ui/image/MediaImageView;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/twitter/media/ui/image/MediaImageView$1;->a:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Lcom/twitter/media/ui/image/config/c;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 163
    iget-object v0, p0, Lcom/twitter/media/ui/image/MediaImageView$1;->a:Lcom/twitter/media/ui/image/MediaImageView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 164
    return-object p0
.end method

.method public a(IF)Lcom/twitter/media/ui/image/config/c;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/twitter/media/ui/image/MediaImageView$1;->a:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-static {v0}, Lcom/twitter/media/ui/image/MediaImageView;->a(Lcom/twitter/media/ui/image/MediaImageView;)Landroid/widget/ImageView;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/media/ui/image/config/a;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/twitter/media/ui/image/MediaImageView$1;->a:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-static {v0}, Lcom/twitter/media/ui/image/MediaImageView;->a(Lcom/twitter/media/ui/image/MediaImageView;)Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/config/a;

    invoke-interface {v0, p1, p2}, Lcom/twitter/media/ui/image/config/a;->a(IF)V

    .line 134
    :cond_0
    return-object p0
.end method

.method public a(Lcom/twitter/media/ui/image/config/g;)Lcom/twitter/media/ui/image/config/c;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/media/ui/image/MediaImageView$1;->a:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-static {v0}, Lcom/twitter/media/ui/image/MediaImageView;->a(Lcom/twitter/media/ui/image/MediaImageView;)Landroid/widget/ImageView;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/media/ui/image/config/e;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/twitter/media/ui/image/MediaImageView$1;->a:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-static {v0}, Lcom/twitter/media/ui/image/MediaImageView;->a(Lcom/twitter/media/ui/image/MediaImageView;)Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/config/e;

    invoke-interface {v0, p1}, Lcom/twitter/media/ui/image/config/e;->setRoundingStrategy(Lcom/twitter/media/ui/image/config/g;)V

    .line 143
    :cond_0
    return-object p0
.end method
