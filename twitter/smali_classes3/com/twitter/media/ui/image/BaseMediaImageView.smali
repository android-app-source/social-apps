.class public abstract Lcom/twitter/media/ui/image/BaseMediaImageView;
.super Lcom/twitter/media/ui/image/AspectRatioFrameLayout;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/request/a$b;
.implements Lcom/twitter/media/ui/image/a;
.implements Lcom/twitter/media/ui/image/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/media/ui/image/BaseMediaImageView$a;,
        Lcom/twitter/media/ui/image/BaseMediaImageView$b;,
        Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/media/ui/image/BaseMediaImageView",
        "<TT;>;>",
        "Lcom/twitter/media/ui/image/AspectRatioFrameLayout;",
        "Lcom/twitter/media/request/a$b;",
        "Lcom/twitter/media/ui/image/a;",
        "Lcom/twitter/media/ui/image/d;"
    }
.end annotation


# static fields
.field private static final a:Lcom/twitter/media/request/process/a;

.field protected static final b:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

.field private static j:Lcom/twitter/media/request/ImageRequester$Factory;


# instance fields
.field protected d:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

.field protected e:Landroid/graphics/drawable/Drawable;

.field protected f:Landroid/widget/ImageView$ScaleType;

.field g:Lcom/twitter/media/request/a$a;

.field h:Z

.field i:F

.field private final k:Lcom/twitter/media/request/ImageRequester;

.field private l:Lcom/twitter/media/request/b$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/media/request/b$b",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;"
        }
    .end annotation
.end field

.field private m:Z

.field private n:Z

.field private o:Lcom/twitter/media/request/a;

.field private p:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private q:Z

.field private r:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field private s:Lcom/twitter/media/ui/image/BaseMediaImageView$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/media/ui/image/BaseMediaImageView$b",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final t:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcom/twitter/media/ui/image/BaseMediaImageView$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/media/ui/image/BaseMediaImageView$a",
            "<TT;>;"
        }
    .end annotation
.end field

.field private v:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->a:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    sput-object v0, Lcom/twitter/media/ui/image/BaseMediaImageView;->b:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    .line 74
    new-instance v0, Lcom/twitter/media/ui/image/BaseMediaImageView$1;

    invoke-direct {v0}, Lcom/twitter/media/ui/image/BaseMediaImageView$1;-><init>()V

    sput-object v0, Lcom/twitter/media/ui/image/BaseMediaImageView;->a:Lcom/twitter/media/request/process/a;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/media/ui/image/BaseMediaImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/twitter/media/request/ImageRequester;)V

    .line 130
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/twitter/media/request/ImageRequester;)V
    .locals 6

    .prologue
    .line 134
    sget-object v5, Lcom/twitter/media/ui/image/BaseMediaImageView;->b:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/media/ui/image/BaseMediaImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/twitter/media/request/ImageRequester;Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;)V

    .line 135
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/twitter/media/request/ImageRequester;Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 139
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 92
    sget-object v0, Lcom/twitter/media/ui/image/BaseMediaImageView;->b:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    iput-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->d:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    .line 95
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    iput-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->f:Landroid/widget/ImageView$ScaleType;

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->q:Z

    .line 120
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->t:Lrx/subjects/PublishSubject;

    .line 141
    sget-object v0, Lcom/twitter/media/ui/a$c;->BaseMediaImageView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 142
    sget v1, Lcom/twitter/media/ui/a$c;->BaseMediaImageView_defaultDrawable:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->e:Landroid/graphics/drawable/Drawable;

    .line 143
    sget v1, Lcom/twitter/media/ui/a$c;->BaseMediaImageView_errorDrawable:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->r:I

    .line 144
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->isInEditMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 145
    sget-object v1, Lcom/twitter/media/request/ImageRequester;->a:Lcom/twitter/media/request/ImageRequester;

    iput-object v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->k:Lcom/twitter/media/request/ImageRequester;

    .line 151
    :goto_0
    sget v1, Lcom/twitter/media/ui/a$c;->BaseMediaImageView_updateOnResize:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->m:Z

    .line 152
    sget v1, Lcom/twitter/media/ui/a$c;->BaseMediaImageView_scaleType:I

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 153
    invoke-static {}, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->values()[Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    move-result-object v2

    .line 154
    if-ltz v1, :cond_0

    array-length v3, v2

    if-ge v1, v3, :cond_0

    aget-object p5, v2, v1

    :cond_0
    iput-object p5, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->d:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    .line 156
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 157
    return-void

    .line 147
    :cond_1
    if-eqz p4, :cond_2

    :goto_1
    iput-object p4, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->k:Lcom/twitter/media/request/ImageRequester;

    .line 149
    iget-object v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->k:Lcom/twitter/media/request/ImageRequester;

    sget v2, Lcom/twitter/media/ui/a$c;->BaseMediaImageView_imageType:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/twitter/media/request/ImageRequester;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 147
    :cond_2
    invoke-static {p1}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(Landroid/content/Context;)Lcom/twitter/media/request/ImageRequester;

    move-result-object p4

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;)Lcom/twitter/media/request/ImageRequester;
    .locals 2

    .prologue
    .line 565
    sget-object v0, Lcom/twitter/media/ui/image/BaseMediaImageView;->j:Lcom/twitter/media/request/ImageRequester$Factory;

    if-nez v0, :cond_0

    .line 566
    invoke-static {}, Lcrl;->a()Lcrl;

    move-result-object v0

    const-class v1, Lcom/twitter/media/request/ImageRequester$Factory;

    invoke-virtual {v0, v1}, Lcrl;->c(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/ImageRequester$Factory;

    sput-object v0, Lcom/twitter/media/ui/image/BaseMediaImageView;->j:Lcom/twitter/media/request/ImageRequester$Factory;

    .line 567
    const-class v0, Lcom/twitter/media/ui/image/BaseMediaImageView;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 568
    sget-object v0, Lcom/twitter/media/ui/image/BaseMediaImageView;->j:Lcom/twitter/media/request/ImageRequester$Factory;

    if-nez v0, :cond_0

    .line 569
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "A default ImageRequester.Factory is not available."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572
    :cond_0
    sget-object v0, Lcom/twitter/media/ui/image/BaseMediaImageView;->j:Lcom/twitter/media/request/ImageRequester$Factory;

    invoke-interface {v0, p0}, Lcom/twitter/media/request/ImageRequester$Factory;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/ImageRequester;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/media/ui/image/BaseMediaImageView;)Lcom/twitter/media/request/a;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->o:Lcom/twitter/media/request/a;

    return-object v0
.end method

.method private c(Lcom/twitter/media/request/ImageResponse;)Z
    .locals 2

    .prologue
    .line 384
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a;

    .line 385
    invoke-virtual {v0}, Lcom/twitter/media/request/a;->w()Z

    move-result v1

    if-nez v1, :cond_0

    .line 386
    invoke-virtual {v0}, Lcom/twitter/media/request/a;->x()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 387
    invoke-virtual {v0}, Lcom/twitter/media/request/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->i:F

    const/high16 v1, 0x3e800000    # 0.25f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 385
    :goto_0
    return v0

    .line 387
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Lcom/twitter/media/request/ImageResponse;)V
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->l:Lcom/twitter/media/request/b$b;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->l:Lcom/twitter/media/request/b$b;

    invoke-interface {v0, p1}, Lcom/twitter/media/request/b$b;->a(Lcom/twitter/media/request/ResourceResponse;)V

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->s:Lcom/twitter/media/ui/image/BaseMediaImageView$b;

    if-eqz v0, :cond_1

    .line 396
    iget-object v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->s:Lcom/twitter/media/ui/image/BaseMediaImageView$b;

    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/BaseMediaImageView;

    invoke-interface {v1, v0, p1}, Lcom/twitter/media/ui/image/BaseMediaImageView$b;->a(Lcom/twitter/media/ui/image/BaseMediaImageView;Lcom/twitter/media/request/ImageResponse;)V

    .line 398
    :cond_1
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->t:Lrx/subjects/PublishSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    .line 400
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->n()V

    .line 401
    return-void
.end method

.method public static setImageRequesterFactory(Lcom/twitter/media/request/ImageRequester$Factory;)V
    .locals 1

    .prologue
    .line 559
    sput-object p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->j:Lcom/twitter/media/request/ImageRequester$Factory;

    .line 560
    const-class v0, Lcom/twitter/media/ui/image/BaseMediaImageView;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 561
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/media/request/a$a;)Lcom/twitter/media/request/a;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 476
    if-eqz p1, :cond_1

    .line 477
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->getImageSize()Lcom/twitter/util/math/Size;

    move-result-object v0

    iget v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->i:F

    invoke-virtual {v0, v1}, Lcom/twitter/util/math/Size;->a(F)Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->n:Z

    .line 478
    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a$a;->c(Z)Lcom/twitter/media/request/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a$a;

    iget-object v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->d:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    iget-object v1, v1, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->decoderScaleType:Lcom/twitter/media/decoder/ImageDecoder$ScaleType;

    .line 479
    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/decoder/ImageDecoder$ScaleType;)Lcom/twitter/media/request/a$a;

    .line 480
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->u:Lcom/twitter/media/ui/image/BaseMediaImageView$a;

    if-eqz v0, :cond_0

    .line 481
    iget-object v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->u:Lcom/twitter/media/ui/image/BaseMediaImageView$a;

    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/BaseMediaImageView;

    invoke-interface {v1, v0}, Lcom/twitter/media/ui/image/BaseMediaImageView$a;->a(Lcom/twitter/media/ui/image/BaseMediaImageView;)Lcom/twitter/util/math/c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/util/math/c;)Lcom/twitter/media/request/a$a;

    .line 483
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    .line 484
    invoke-virtual {v0}, Lcom/twitter/media/request/a;->C()Lcom/twitter/media/request/b$b;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->l:Lcom/twitter/media/request/b$b;

    .line 485
    invoke-virtual {v0, p0}, Lcom/twitter/media/request/a;->a(Lcom/twitter/media/request/b$b;)V

    .line 489
    :goto_0
    return-object v0

    .line 488
    :cond_1
    iput-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->l:Lcom/twitter/media/request/b$b;

    goto :goto_0
.end method

.method protected a(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 456
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 457
    return-void
.end method

.method protected abstract a(Landroid/graphics/drawable/Drawable;)V
.end method

.method protected a(Landroid/graphics/drawable/Drawable;Z)V
    .locals 0

    .prologue
    .line 464
    invoke-virtual {p0, p1}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 465
    return-void
.end method

.method public a(Lcom/twitter/media/request/ImageResponse;)V
    .locals 3

    .prologue
    .line 300
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a;

    .line 301
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->c()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/media/ui/image/BaseMediaImageView;->c(Lcom/twitter/media/request/ImageResponse;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 303
    new-instance v1, Lcom/twitter/media/ui/image/BaseMediaImageView$2;

    invoke-direct {v1, p0, v0}, Lcom/twitter/media/ui/image/BaseMediaImageView$2;-><init>(Lcom/twitter/media/ui/image/BaseMediaImageView;Lcom/twitter/media/request/a;)V

    invoke-virtual {p0, v1}, Lcom/twitter/media/ui/image/BaseMediaImageView;->post(Ljava/lang/Runnable;)Z

    .line 338
    :goto_0
    return-void

    .line 317
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 319
    invoke-virtual {v0}, Lcom/twitter/media/request/a;->q()Lcom/twitter/media/request/process/a;

    move-result-object v0

    sget-object v2, Lcom/twitter/media/ui/image/BaseMediaImageView;->a:Lcom/twitter/media/request/process/a;

    invoke-static {v0, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/process/a;

    .line 320
    invoke-interface {v0, v1, p1}, Lcom/twitter/media/request/process/a;->a(Landroid/content/Context;Lcom/twitter/media/request/ImageResponse;)Lcom/twitter/util/concurrent/g;

    move-result-object v0

    .line 321
    iput-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->p:Ljava/util/concurrent/Future;

    .line 322
    new-instance v1, Lcom/twitter/media/ui/image/BaseMediaImageView$3;

    invoke-direct {v1, p0, p1, v0}, Lcom/twitter/media/ui/image/BaseMediaImageView$3;-><init>(Lcom/twitter/media/ui/image/BaseMediaImageView;Lcom/twitter/media/request/ImageResponse;Lcom/twitter/util/concurrent/g;)V

    invoke-interface {v0, v1}, Lcom/twitter/util/concurrent/g;->a(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;

    goto :goto_0
.end method

.method protected a(Lcom/twitter/media/request/ImageResponse;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 359
    iput-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->h:Z

    .line 360
    iput-boolean v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->q:Z

    .line 361
    iput-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->v:Z

    .line 362
    if-eqz p2, :cond_0

    .line 363
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->f()Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    move-result-object v2

    sget-object v3, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->b:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    if-ne v2, v3, :cond_1

    :goto_0
    invoke-virtual {p0, p2, v0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(Landroid/graphics/drawable/Drawable;Z)V

    .line 365
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/media/ui/image/BaseMediaImageView;->d(Lcom/twitter/media/request/ImageResponse;)V

    .line 366
    return-void

    :cond_1
    move v0, v1

    .line 363
    goto :goto_0
.end method

.method a(Lcom/twitter/media/request/ImageResponse;Lcom/twitter/util/concurrent/g;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/media/request/ImageResponse;",
            "Lcom/twitter/util/concurrent/g",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 341
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a;

    iget-object v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->o:Lcom/twitter/media/request/a;

    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a;->a(Lcom/twitter/media/request/b;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 345
    :cond_1
    iput-object v2, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->p:Ljava/util/concurrent/Future;

    .line 346
    iput-object v2, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->o:Lcom/twitter/media/request/a;

    .line 347
    invoke-interface {p2}, Lcom/twitter/util/concurrent/g;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 351
    :try_start_0
    invoke-interface {p2}, Lcom/twitter/util/concurrent/g;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 352
    invoke-virtual {p0, p1, v0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(Lcom/twitter/media/request/ImageResponse;Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 353
    :catch_0
    move-exception v0

    .line 354
    :goto_1
    invoke-virtual {p0, p1}, Lcom/twitter/media/ui/image/BaseMediaImageView;->b(Lcom/twitter/media/request/ImageResponse;)V

    goto :goto_0

    .line 353
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public bridge synthetic a(Lcom/twitter/media/request/ResourceResponse;)V
    .locals 0

    .prologue
    .line 52
    check-cast p1, Lcom/twitter/media/request/ImageResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method

.method public a(Lcom/twitter/media/request/a$a;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 173
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->isInEditMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 195
    :cond_0
    :goto_0
    return v0

    .line 176
    :cond_1
    iput-object p1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->g:Lcom/twitter/media/request/a$a;

    .line 177
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->i:F

    .line 178
    if-nez p1, :cond_2

    .line 179
    iput-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->h:Z

    .line 180
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->j()Z

    .line 181
    if-eqz p2, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->l()V

    goto :goto_0

    .line 187
    :cond_2
    iget-object v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->k:Lcom/twitter/media/request/ImageRequester;

    invoke-virtual {p0, p1}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(Lcom/twitter/media/request/a$a;)Lcom/twitter/media/request/a;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/twitter/media/request/ImageRequester;->a(Lcom/twitter/media/request/a;)Z

    move-result v1

    .line 188
    if-eqz v1, :cond_3

    .line 189
    iput-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->h:Z

    .line 190
    if-eqz p2, :cond_3

    .line 191
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->l()V

    .line 194
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->e()V

    move v0, v1

    .line 195
    goto :goto_0
.end method

.method protected a(Lcom/twitter/media/request/a;)Z
    .locals 1

    .prologue
    .line 508
    iget-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->h:Z

    return v0
.end method

.method protected b(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 460
    invoke-virtual {p0, p1}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 461
    return-void
.end method

.method protected b(Lcom/twitter/media/request/ImageResponse;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 370
    iget-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->n:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->h:Z

    .line 371
    iget-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->h:Z

    if-eqz v0, :cond_0

    .line 372
    iput-boolean v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->q:Z

    .line 373
    iput-boolean v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->v:Z

    .line 374
    iget v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->r:I

    if-eqz v0, :cond_2

    .line 375
    iget v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->r:I

    invoke-virtual {p0, v0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(I)V

    .line 379
    :goto_1
    invoke-direct {p0, p1}, Lcom/twitter/media/ui/image/BaseMediaImageView;->d(Lcom/twitter/media/request/ImageResponse;)V

    .line 381
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 370
    goto :goto_0

    .line 377
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->l()V

    goto :goto_1
.end method

.method public b(Lcom/twitter/media/request/a$a;)Z
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(Lcom/twitter/media/request/a$a;Z)Z

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 276
    iget-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->v:Z

    if-nez v0, :cond_0

    .line 278
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->h:Z

    .line 280
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->o()V

    .line 281
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->l()V

    .line 286
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->j()Z

    .line 287
    return-void
.end method

.method public getDefaultDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->e:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getImageRequest()Lcom/twitter/media/request/a;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->k:Lcom/twitter/media/request/ImageRequester;

    invoke-interface {v0}, Lcom/twitter/media/request/ImageRequester;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    return-object v0
.end method

.method public getImageRequester()Lcom/twitter/media/request/ImageRequester;
    .locals 1

    .prologue
    .line 546
    invoke-static {}, Lcom/twitter/util/f;->d()V

    .line 547
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->k:Lcom/twitter/media/request/ImageRequester;

    return-object v0
.end method

.method public abstract getImageSize()Lcom/twitter/util/math/Size;
.end method

.method public abstract getImageViewAnimator()Landroid/view/ViewPropertyAnimator;
.end method

.method protected final getRequestBuilder()Lcom/twitter/media/request/a$a;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->g:Lcom/twitter/media/request/a$a;

    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->v:Z

    return v0
.end method

.method public i()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 267
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->t:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method public j()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 290
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->p:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->p:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 292
    iput-object v2, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->p:Ljava/util/concurrent/Future;

    .line 294
    :cond_0
    iput-object v2, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->o:Lcom/twitter/media/request/a;

    .line 295
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->k:Lcom/twitter/media/request/ImageRequester;

    invoke-interface {v0}, Lcom/twitter/media/request/ImageRequester;->c()Z

    move-result v0

    return v0
.end method

.method public k()V
    .locals 3

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 435
    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 436
    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 435
    invoke-virtual {p0, v0, v1}, Lcom/twitter/media/ui/image/BaseMediaImageView;->measure(II)V

    .line 437
    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/twitter/media/ui/image/BaseMediaImageView;->layout(IIII)V

    .line 438
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->requestLayout()V

    .line 439
    return-void
.end method

.method protected l()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 468
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 469
    iput-boolean v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->v:Z

    .line 470
    iput-boolean v1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->h:Z

    .line 471
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->q:Z

    .line 472
    return-void
.end method

.method protected m()V
    .locals 0

    .prologue
    .line 498
    return-void
.end method

.method protected n()V
    .locals 0

    .prologue
    .line 505
    return-void
.end method

.method o()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 512
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    .line 541
    :cond_0
    :goto_0
    return-void

    .line 515
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->getImageSize()Lcom/twitter/util/math/Size;

    move-result-object v3

    .line 516
    invoke-virtual {v3}, Lcom/twitter/util/math/Size;->e()Z

    move-result v2

    if-nez v2, :cond_0

    .line 519
    iget-object v2, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->k:Lcom/twitter/media/request/ImageRequester;

    invoke-interface {v2}, Lcom/twitter/media/request/ImageRequester;->a()Lcom/twitter/media/request/a;

    move-result-object v4

    .line 520
    if-eqz v4, :cond_0

    .line 524
    invoke-virtual {p0, v4}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(Lcom/twitter/media/request/a;)Z

    move-result v2

    .line 525
    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->k:Lcom/twitter/media/request/ImageRequester;

    invoke-interface {v2}, Lcom/twitter/media/request/ImageRequester;->b()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_2
    move v2, v0

    .line 526
    :goto_1
    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->m:Z

    if-eqz v2, :cond_0

    invoke-virtual {v4}, Lcom/twitter/media/request/a;->e()Lcom/twitter/util/math/Size;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/twitter/util/math/Size;->d(Lcom/twitter/util/math/Size;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 530
    :cond_3
    iget-object v2, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->g:Lcom/twitter/media/request/a$a;

    invoke-virtual {p0, v2}, Lcom/twitter/media/ui/image/BaseMediaImageView;->a(Lcom/twitter/media/request/a$a;)Lcom/twitter/media/request/a;

    move-result-object v2

    .line 531
    iget-object v3, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->o:Lcom/twitter/media/request/a;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 532
    iget-object v3, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->p:Ljava/util/concurrent/Future;

    if-eqz v3, :cond_4

    .line 533
    iget-object v3, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->p:Ljava/util/concurrent/Future;

    invoke-interface {v3, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 534
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->p:Ljava/util/concurrent/Future;

    .line 536
    :cond_4
    iput-object v2, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->o:Lcom/twitter/media/request/a;

    .line 538
    :cond_5
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->m()V

    .line 539
    iget-object v3, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->k:Lcom/twitter/media/request/ImageRequester;

    invoke-interface {v3, v2}, Lcom/twitter/media/request/ImageRequester;->a(Lcom/twitter/media/request/a;)Z

    .line 540
    iget-object v2, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->k:Lcom/twitter/media/request/ImageRequester;

    iget-boolean v3, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->v:Z

    if-nez v3, :cond_7

    iget-boolean v3, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->n:Z

    if-nez v3, :cond_7

    :goto_2
    invoke-interface {v2, v0}, Lcom/twitter/media/request/ImageRequester;->a(Z)V

    goto :goto_0

    :cond_6
    move v2, v1

    .line 525
    goto :goto_1

    :cond_7
    move v0, v1

    .line 540
    goto :goto_2
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 443
    invoke-super {p0}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->onDetachedFromWindow()V

    .line 444
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->f()V

    .line 445
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->k()V

    .line 446
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 426
    invoke-super/range {p0 .. p5}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->onLayout(ZIIII)V

    .line 427
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->o()V

    .line 428
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, -0x2

    .line 411
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 412
    iget v0, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v0, v4, :cond_3

    move v0, v1

    .line 413
    :goto_0
    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v3, v4, :cond_4

    .line 414
    :goto_1
    if-nez v0, :cond_0

    if-eqz v1, :cond_5

    .line 415
    :cond_0
    if-eqz v0, :cond_1

    if-nez v1, :cond_2

    :cond_1
    iget v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->c:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_5

    .line 416
    :cond_2
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    if-eqz v0, :cond_5

    .line 417
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    if-eqz v0, :cond_5

    .line 418
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Image view measures can\'t be determined"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v0, v2

    .line 412
    goto :goto_0

    :cond_4
    move v1, v2

    .line 413
    goto :goto_1

    .line 421
    :cond_5
    invoke-super {p0, p1, p2}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->onMeasure(II)V

    .line 422
    return-void
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 552
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->h()Z

    move-result v0

    return v0
.end method

.method public setCroppingRectangleProvider(Lcom/twitter/media/ui/image/BaseMediaImageView$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/media/ui/image/BaseMediaImageView$a",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 271
    iput-object p1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->u:Lcom/twitter/media/ui/image/BaseMediaImageView$a;

    .line 272
    return-void
.end method

.method public setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->e:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    .line 235
    iput-object p1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->e:Landroid/graphics/drawable/Drawable;

    .line 236
    iget-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->q:Z

    if-eqz v0, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->l()V

    .line 240
    :cond_0
    return-void
.end method

.method public setDefaultDrawableScaleType(Landroid/widget/ImageView$ScaleType;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->f:Landroid/widget/ImageView$ScaleType;

    .line 244
    return-void
.end method

.method public setErrorDrawableId(I)V
    .locals 0

    .prologue
    .line 247
    iput p1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->r:I

    .line 248
    return-void
.end method

.method public setFromMemoryOnly(Z)V
    .locals 1

    .prologue
    .line 211
    iget-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->n:Z

    if-eq v0, p1, :cond_0

    .line 212
    iput-boolean p1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->n:Z

    .line 213
    if-nez p1, :cond_0

    .line 215
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->o()V

    .line 218
    :cond_0
    return-void
.end method

.method public setImageType(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->k:Lcom/twitter/media/request/ImageRequester;

    invoke-interface {v0, p1}, Lcom/twitter/media/request/ImageRequester;->a(Ljava/lang/String;)V

    .line 222
    return-void
.end method

.method public setOnImageLoadedListener(Lcom/twitter/media/ui/image/BaseMediaImageView$b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/media/ui/image/BaseMediaImageView$b",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 256
    iput-object p1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->s:Lcom/twitter/media/ui/image/BaseMediaImageView$b;

    .line 257
    return-void
.end method

.method public setScaleType(Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;)V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->d:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    if-eq v0, p1, :cond_0

    .line 226
    iput-object p1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->d:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    .line 227
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->h:Z

    .line 228
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->j()Z

    .line 229
    invoke-virtual {p0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->o()V

    .line 231
    :cond_0
    return-void
.end method

.method public setUpdateOnResize(Z)V
    .locals 0

    .prologue
    .line 202
    iput-boolean p1, p0, Lcom/twitter/media/ui/image/BaseMediaImageView;->m:Z

    .line 203
    return-void
.end method
