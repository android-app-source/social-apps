.class public final Lcom/twitter/media/ui/a$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/media/ui/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static final actualImageScaleType:I = 0x7f010262

.field public static final aspect_ratio:I = 0x7f010192

.field public static final backgroundImage:I = 0x7f010263

.field public static final cornerRadius:I = 0x7f010016

.field public static final cornerRadiusBottomLeft:I = 0x7f010340

.field public static final cornerRadiusBottomRight:I = 0x7f010341

.field public static final cornerRadiusTopLeft:I = 0x7f01033e

.field public static final cornerRadiusTopRight:I = 0x7f01033f

.field public static final crossfadeDuration:I = 0x7f01019a

.field public static final defaultDrawable:I = 0x7f010018

.field public static final errorDrawable:I = 0x7f0101a9

.field public static final fadeDuration:I = 0x7f010257

.field public static final fadeIn:I = 0x7f01002d

.field public static final failureImage:I = 0x7f01025d

.field public static final failureImageScaleType:I = 0x7f01025e

.field public static final filterColor:I = 0x7f01019c

.field public static final filterMaxOpacity:I = 0x7f01019b

.field public static final fixedSize:I = 0x7f010248

.field public static final imageType:I = 0x7f0101aa

.field public static final loadingProgressBar:I = 0x7f0102bb

.field public static final max_aspect_ratio:I = 0x7f010194

.field public static final max_height:I = 0x7f010196

.field public static final max_width:I = 0x7f010195

.field public static final mediaImageViewStyle:I = 0x7f01004e

.field public static final min_aspect_ratio:I = 0x7f010193

.field public static final overlayDrawable:I = 0x7f010057

.field public static final overlayImage:I = 0x7f010264

.field public static final placeholderImage:I = 0x7f010259

.field public static final placeholderImageScaleType:I = 0x7f01025a

.field public static final pressedStateOverlayImage:I = 0x7f010265

.field public static final progressBarAutoRotateInterval:I = 0x7f010261

.field public static final progressBarImage:I = 0x7f01025f

.field public static final progressBarImageScaleType:I = 0x7f010260

.field public static final retryImage:I = 0x7f01025b

.field public static final retryImageScaleType:I = 0x7f01025c

.field public static final roundAsCircle:I = 0x7f010266

.field public static final roundBottomLeft:I = 0x7f01026b

.field public static final roundBottomRight:I = 0x7f01026a

.field public static final roundTopLeft:I = 0x7f010268

.field public static final roundTopRight:I = 0x7f010269

.field public static final roundWithOverlayColor:I = 0x7f01026c

.field public static final roundedCornerRadius:I = 0x7f010267

.field public static final roundingBorderColor:I = 0x7f01026e

.field public static final roundingBorderWidth:I = 0x7f01026d

.field public static final scaleFactor:I = 0x7f0102ba

.field public static final scaleMode:I = 0x7f010197

.field public static final scaleType:I = 0x7f0101ac

.field public static final singleImageView:I = 0x7f0102b9

.field public static final svg:I = 0x7f010348

.field public static final twitterDraweeViewStyle:I = 0x7f0100d5

.field public static final updateOnResize:I = 0x7f0101ab

.field public static final viewAspectRatio:I = 0x7f010258
