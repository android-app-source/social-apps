.class public final Lcom/twitter/media/ui/a$c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/media/ui/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# static fields
.field public static final AspectRatioFrameLayout:[I

.field public static final AspectRatioFrameLayout_aspect_ratio:I = 0x0

.field public static final AspectRatioFrameLayout_max_aspect_ratio:I = 0x2

.field public static final AspectRatioFrameLayout_max_height:I = 0x4

.field public static final AspectRatioFrameLayout_max_width:I = 0x3

.field public static final AspectRatioFrameLayout_min_aspect_ratio:I = 0x1

.field public static final AspectRatioFrameLayout_scaleMode:I = 0x5

.field public static final BackgroundImageView:[I

.field public static final BackgroundImageView_crossfadeDuration:I = 0x1

.field public static final BackgroundImageView_filterColor:I = 0x3

.field public static final BackgroundImageView_filterMaxOpacity:I = 0x2

.field public static final BackgroundImageView_overlayDrawable:I = 0x0

.field public static final BaseMediaImageView:[I

.field public static final BaseMediaImageView_defaultDrawable:I = 0x0

.field public static final BaseMediaImageView_errorDrawable:I = 0x1

.field public static final BaseMediaImageView_imageType:I = 0x2

.field public static final BaseMediaImageView_scaleType:I = 0x4

.field public static final BaseMediaImageView_updateOnResize:I = 0x3

.field public static final FixedSizeImageView:[I

.field public static final FixedSizeImageView_fixedSize:I = 0x0

.field public static final GenericDraweeView:[I

.field public static final GenericDraweeView_actualImageScaleType:I = 0xb

.field public static final GenericDraweeView_backgroundImage:I = 0xc

.field public static final GenericDraweeView_fadeDuration:I = 0x0

.field public static final GenericDraweeView_failureImage:I = 0x6

.field public static final GenericDraweeView_failureImageScaleType:I = 0x7

.field public static final GenericDraweeView_overlayImage:I = 0xd

.field public static final GenericDraweeView_placeholderImage:I = 0x2

.field public static final GenericDraweeView_placeholderImageScaleType:I = 0x3

.field public static final GenericDraweeView_pressedStateOverlayImage:I = 0xe

.field public static final GenericDraweeView_progressBarAutoRotateInterval:I = 0xa

.field public static final GenericDraweeView_progressBarImage:I = 0x8

.field public static final GenericDraweeView_progressBarImageScaleType:I = 0x9

.field public static final GenericDraweeView_retryImage:I = 0x4

.field public static final GenericDraweeView_retryImageScaleType:I = 0x5

.field public static final GenericDraweeView_roundAsCircle:I = 0xf

.field public static final GenericDraweeView_roundBottomLeft:I = 0x14

.field public static final GenericDraweeView_roundBottomRight:I = 0x13

.field public static final GenericDraweeView_roundTopLeft:I = 0x11

.field public static final GenericDraweeView_roundTopRight:I = 0x12

.field public static final GenericDraweeView_roundWithOverlayColor:I = 0x15

.field public static final GenericDraweeView_roundedCornerRadius:I = 0x10

.field public static final GenericDraweeView_roundingBorderColor:I = 0x17

.field public static final GenericDraweeView_roundingBorderWidth:I = 0x16

.field public static final GenericDraweeView_viewAspectRatio:I = 0x1

.field public static final MediaImageView:[I

.field public static final MediaImageView_fadeIn:I = 0x0

.field public static final MediaImageView_loadingProgressBar:I = 0x3

.field public static final MediaImageView_scaleFactor:I = 0x2

.field public static final MediaImageView_singleImageView:I = 0x1

.field public static final RichImageView:[I

.field public static final RichImageView_cornerRadius:I = 0x0

.field public static final RichImageView_cornerRadiusBottomLeft:I = 0x4

.field public static final RichImageView_cornerRadiusBottomRight:I = 0x5

.field public static final RichImageView_cornerRadiusTopLeft:I = 0x2

.field public static final RichImageView_cornerRadiusTopRight:I = 0x3

.field public static final RichImageView_overlayDrawable:I = 0x1

.field public static final SVGImageView:[I

.field public static final SVGImageView_svg:I

.field public static final TwitterDraweeView:[I

.field public static final TwitterDraweeView_defaultDrawable:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 138
    new-array v0, v5, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/media/ui/a$c;->AspectRatioFrameLayout:[I

    .line 145
    new-array v0, v4, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/twitter/media/ui/a$c;->BackgroundImageView:[I

    .line 150
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/twitter/media/ui/a$c;->BaseMediaImageView:[I

    .line 156
    new-array v0, v3, [I

    const v1, 0x7f010248

    aput v1, v0, v2

    sput-object v0, Lcom/twitter/media/ui/a$c;->FixedSizeImageView:[I

    .line 158
    const/16 v0, 0x18

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/twitter/media/ui/a$c;->GenericDraweeView:[I

    .line 183
    new-array v0, v4, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/twitter/media/ui/a$c;->MediaImageView:[I

    .line 188
    new-array v0, v5, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/twitter/media/ui/a$c;->RichImageView:[I

    .line 195
    new-array v0, v3, [I

    const v1, 0x7f010348

    aput v1, v0, v2

    sput-object v0, Lcom/twitter/media/ui/a$c;->SVGImageView:[I

    .line 197
    new-array v0, v3, [I

    const v1, 0x7f010018

    aput v1, v0, v2

    sput-object v0, Lcom/twitter/media/ui/a$c;->TwitterDraweeView:[I

    return-void

    .line 138
    nop

    :array_0
    .array-data 4
        0x7f010192
        0x7f010193
        0x7f010194
        0x7f010195
        0x7f010196
        0x7f010197
    .end array-data

    .line 145
    :array_1
    .array-data 4
        0x7f010057
        0x7f01019a
        0x7f01019b
        0x7f01019c
    .end array-data

    .line 150
    :array_2
    .array-data 4
        0x7f010018
        0x7f0101a9
        0x7f0101aa
        0x7f0101ab
        0x7f0101ac
    .end array-data

    .line 158
    :array_3
    .array-data 4
        0x7f010257
        0x7f010258
        0x7f010259
        0x7f01025a
        0x7f01025b
        0x7f01025c
        0x7f01025d
        0x7f01025e
        0x7f01025f
        0x7f010260
        0x7f010261
        0x7f010262
        0x7f010263
        0x7f010264
        0x7f010265
        0x7f010266
        0x7f010267
        0x7f010268
        0x7f010269
        0x7f01026a
        0x7f01026b
        0x7f01026c
        0x7f01026d
        0x7f01026e
    .end array-data

    .line 183
    :array_4
    .array-data 4
        0x7f01002d
        0x7f0102b9
        0x7f0102ba
        0x7f0102bb
    .end array-data

    .line 188
    :array_5
    .array-data 4
        0x7f010016
        0x7f010057
        0x7f01033e
        0x7f01033f
        0x7f010340
        0x7f010341
    .end array-data
.end method
