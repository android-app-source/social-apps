.class public Lcom/twitter/ui/user/BaseUserView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/ui/image/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/ui/user/BaseUserView$a;
    }
.end annotation


# instance fields
.field protected final b:I

.field protected final c:I

.field protected final d:I

.field protected final e:I

.field protected final e_:I

.field protected f:J

.field protected g:Ljava/lang/String;

.field protected h:Landroid/widget/TextView;

.field protected i:Landroid/widget/TextView;

.field protected j:Landroid/widget/TextView;

.field protected k:Landroid/widget/TextView;

.field protected l:Landroid/widget/TextView;

.field protected m:Lcom/twitter/media/ui/image/UserImageView;

.field protected n:Landroid/widget/ImageView;

.field protected o:Landroid/widget/ImageView;

.field protected p:Landroid/widget/ImageView;

.field protected q:Landroid/view/View;

.field private final r:I

.field private final s:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 58
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/ui/user/BaseUserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 62
    sget v0, Lcjo$a;->userViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/ui/user/BaseUserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 66
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    sget-object v0, Lcjo$h;->UserView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 68
    sget v1, Lcjo$h;->UserView_promotedDrawable:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/user/BaseUserView;->r:I

    .line 69
    sget v1, Lcjo$h;->UserView_politicalDrawable:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/user/BaseUserView;->s:I

    .line 70
    sget v1, Lcjo$h;->UserView_actionButtonPadding:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 71
    sget v2, Lcjo$h;->UserView_actionButtonPaddingLeft:I

    .line 72
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/ui/user/BaseUserView;->e_:I

    .line 73
    sget v2, Lcjo$h;->UserView_actionButtonPaddingTop:I

    .line 74
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/ui/user/BaseUserView;->b:I

    .line 75
    sget v2, Lcjo$h;->UserView_actionButtonPaddingRight:I

    .line 76
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/ui/user/BaseUserView;->c:I

    .line 77
    sget v2, Lcjo$h;->UserView_actionButtonPaddingBottom:I

    .line 78
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/user/BaseUserView;->d:I

    .line 79
    sget v1, Lcjo$h;->UserView_profileTextColor:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/user/BaseUserView;->e:I

    .line 80
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 81
    return-void
.end method

.method public static a(Lcgi;ZLandroid/widget/TextView;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 230
    if-eqz p0, :cond_3

    .line 231
    invoke-virtual {p0}, Lcgi;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 233
    invoke-virtual {p2, v3}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 254
    :goto_0
    return-void

    .line 236
    :cond_0
    invoke-virtual {p0}, Lcgi;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 242
    :goto_1
    if-eqz p1, :cond_2

    .line 243
    invoke-virtual {p2, v1, v1, p3, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 247
    :goto_2
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 248
    invoke-virtual {p2, p0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move p3, p4

    .line 239
    goto :goto_1

    .line 245
    :cond_2
    invoke-virtual {p2, p3, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_2

    .line 251
    :cond_3
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 252
    invoke-virtual {p2, v3}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->l:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 178
    return-void
.end method

.method public a(Lcgi;Z)V
    .locals 3

    .prologue
    .line 225
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->j:Landroid/widget/TextView;

    iget v1, p0, Lcom/twitter/ui/user/BaseUserView;->s:I

    iget v2, p0, Lcom/twitter/ui/user/BaseUserView;->r:I

    invoke-static {p1, p2, v0, v1, v2}, Lcom/twitter/ui/user/BaseUserView;->a(Lcgi;ZLandroid/widget/TextView;II)V

    .line 226
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/twitter/model/core/v;)V
    .locals 3

    .prologue
    .line 142
    iget-object v1, p0, Lcom/twitter/ui/user/BaseUserView;->l:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 143
    if-eqz p2, :cond_2

    iget-object v0, p2, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->l:Landroid/widget/TextView;

    invoke-static {p1}, Lcnf;->a(Ljava/lang/CharSequence;)Lcnf;

    move-result-object v1

    .line 145
    invoke-virtual {v1, p2}, Lcnf;->a(Lcom/twitter/model/core/v;)Lcnf;

    move-result-object v1

    iget v2, p0, Lcom/twitter/ui/user/BaseUserView;->e:I

    .line 146
    invoke-virtual {v1, v2}, Lcnf;->a(I)Lcnf;

    move-result-object v1

    .line 147
    invoke-virtual {v1}, Lcnf;->a()Landroid/text/SpannableStringBuilder;

    move-result-object v1

    .line 144
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    :goto_1
    return-void

    .line 142
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 130
    iput-object p1, p0, Lcom/twitter/ui/user/BaseUserView;->g:Ljava/lang/String;

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 132
    invoke-static {p2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/twitter/ui/user/BaseUserView;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->i:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v1, p0, Lcom/twitter/ui/user/BaseUserView;->h:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v1, p0, Lcom/twitter/ui/user/BaseUserView;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->l:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->l:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 160
    iget-object v1, p0, Lcom/twitter/ui/user/BaseUserView;->l:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 163
    :cond_0
    return-void

    .line 160
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isActivated()Z

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->m:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/UserImageView;->e()V

    .line 118
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->m:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/UserImageView;->f()V

    .line 123
    return-void
.end method

.method public getBestName()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getImageView()Lcom/twitter/media/ui/image/UserImageView;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->m:Lcom/twitter/media/ui/image/UserImageView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    return-object v0
.end method

.method public getPromotedContent()Lcgi;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->j:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    .line 266
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUserId()J
    .locals 2

    .prologue
    .line 270
    iget-wide v0, p0, Lcom/twitter/ui/user/BaseUserView;->f:J

    return-wide v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->g:Ljava/lang/String;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 86
    sget v0, Lcjo$e;->screenname_item:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->i:Landroid/widget/TextView;

    .line 87
    sget v0, Lcjo$e;->protected_item:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->o:Landroid/widget/ImageView;

    .line 88
    sget v0, Lcjo$e;->verified_item:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->p:Landroid/widget/ImageView;

    .line 89
    sget v0, Lcjo$e;->name_item:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->h:Landroid/widget/TextView;

    .line 90
    sget v0, Lcjo$e;->user_image:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->m:Lcom/twitter/media/ui/image/UserImageView;

    .line 91
    sget v0, Lcjo$e;->extra_info:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->k:Landroid/widget/TextView;

    .line 92
    sget v0, Lcjo$e;->promoted:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->j:Landroid/widget/TextView;

    .line 93
    sget v0, Lcjo$e;->profile_description_item:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->l:Landroid/widget/TextView;

    .line 94
    sget v0, Lcjo$e;->muted_item:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->n:Landroid/widget/ImageView;

    .line 95
    sget v0, Lcjo$e;->follows_you:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->q:Landroid/view/View;

    .line 96
    return-void
.end method

.method public setExtraInfo(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 216
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->k:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 222
    :goto_0
    return-void

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->k:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setFollowsYou(Z)V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->q:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 190
    iget-object v1, p0, Lcom/twitter/ui/user/BaseUserView;->q:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 192
    :cond_0
    return-void

    .line 190
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setMuted(Z)V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setActivated(Z)V

    .line 204
    if-eqz p1, :cond_0

    .line 205
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->n:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 209
    :goto_0
    return-void

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->n:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setProfileDescriptionMaxLines(I)V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->l:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->l:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 172
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 174
    :cond_0
    return-void
.end method

.method public setProfileDescriptionTextSize(F)V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->l:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 167
    return-void
.end method

.method public setProtected(Z)V
    .locals 2

    .prologue
    .line 181
    if-eqz p1, :cond_0

    .line 182
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->o:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->o:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setUser(Lcom/twitter/model/core/TwitterUser;)V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->m:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, p1}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 104
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {p0, v0, v1}, Lcom/twitter/ui/user/BaseUserView;->setUserId(J)V

    .line 105
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/ui/user/BaseUserView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->m:Z

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/BaseUserView;->setVerified(Z)V

    .line 107
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->l:Z

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/BaseUserView;->setProtected(Z)V

    .line 108
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->c(I)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/BaseUserView;->setFollowsYou(Z)V

    .line 109
    return-void
.end method

.method public setUserId(J)V
    .locals 1

    .prologue
    .line 99
    iput-wide p1, p0, Lcom/twitter/ui/user/BaseUserView;->f:J

    .line 100
    return-void
.end method

.method public setUserImageUrl(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->m:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, p1}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 113
    return-void
.end method

.method public setVerified(Z)V
    .locals 2

    .prologue
    .line 195
    if-eqz p1, :cond_0

    .line 196
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->p:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 200
    :goto_0
    return-void

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/user/BaseUserView;->p:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
