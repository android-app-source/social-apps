.class public Lcom/twitter/ui/user/UserApprovalView;
.super Lcom/twitter/ui/user/BaseUserView;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/ui/user/UserApprovalView$a;
    }
.end annotation


# instance fields
.field private r:Lcom/twitter/ui/user/BaseUserView$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserApprovalView;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcom/twitter/ui/user/UserApprovalView$a;

.field private t:Lcom/twitter/ui/user/UserApprovalView$a;

.field private u:Lcom/twitter/ui/widget/ActionButton;

.field private v:Landroid/view/View;

.field private w:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/twitter/ui/user/BaseUserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/ui/user/UserApprovalView;->w:Z

    .line 36
    return-void
.end method

.method private b(I)Lcom/twitter/ui/user/UserApprovalView$a;
    .locals 1

    .prologue
    .line 167
    packed-switch p1, :pswitch_data_0

    .line 175
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 169
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->s:Lcom/twitter/ui/user/UserApprovalView$a;

    goto :goto_0

    .line 172
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->t:Lcom/twitter/ui/user/UserApprovalView$a;

    goto :goto_0

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(II)V
    .locals 5

    .prologue
    .line 103
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->u:Lcom/twitter/ui/widget/ActionButton;

    .line 105
    :goto_0
    if-lez p2, :cond_0

    .line 106
    invoke-virtual {v0, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 107
    iget v1, p0, Lcom/twitter/ui/user/UserApprovalView;->e_:I

    iget v2, p0, Lcom/twitter/ui/user/UserApprovalView;->b:I

    iget v3, p0, Lcom/twitter/ui/user/UserApprovalView;->c:I

    iget v4, p0, Lcom/twitter/ui/user/UserApprovalView;->d:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 110
    :cond_0
    return-void

    .line 104
    :cond_1
    invoke-direct {p0, p1}, Lcom/twitter/ui/user/UserApprovalView;->b(I)Lcom/twitter/ui/user/UserApprovalView$a;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/ui/user/UserApprovalView$a;->a:Landroid/widget/ImageButton;

    goto :goto_0
.end method

.method public a(IILcom/twitter/ui/user/BaseUserView$a;)V
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserApprovalView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 83
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->u:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, p2}, Lcom/twitter/ui/widget/ActionButton;->a(I)V

    .line 85
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->u:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    :goto_0
    return-void

    .line 87
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/ui/user/UserApprovalView;->b(I)Lcom/twitter/ui/user/UserApprovalView$a;

    move-result-object v0

    .line 88
    iget-object v1, v0, Lcom/twitter/ui/user/UserApprovalView$a;->a:Landroid/widget/ImageButton;

    .line 89
    iget-object v0, v0, Lcom/twitter/ui/user/UserApprovalView$a;->b:Landroid/widget/FrameLayout;

    .line 90
    if-nez p2, :cond_1

    .line 91
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 93
    :cond_1
    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    invoke-virtual {v1, p2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 95
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 96
    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iput-object p3, p0, Lcom/twitter/ui/user/UserApprovalView;->r:Lcom/twitter/ui/user/BaseUserView$a;

    goto :goto_0
.end method

.method public a(IZ)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/twitter/ui/user/UserApprovalView;->b(I)Lcom/twitter/ui/user/UserApprovalView$a;

    move-result-object v0

    .line 66
    if-nez v0, :cond_0

    .line 71
    :goto_0
    return-void

    .line 69
    :cond_0
    iput-boolean p2, v0, Lcom/twitter/ui/user/UserApprovalView$a;->c:Z

    .line 70
    invoke-virtual {p0}, Lcom/twitter/ui/user/UserApprovalView;->refreshDrawableState()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0, p1, p2}, Lcom/twitter/ui/user/BaseUserView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->u:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ActionButton;->setUsername(Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->u:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/ActionButton;->isChecked()Z

    move-result v0

    .line 78
    :goto_0
    return v0

    .line 77
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/ui/user/UserApprovalView;->b(I)Lcom/twitter/ui/user/UserApprovalView$a;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/twitter/ui/user/UserApprovalView$a;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 181
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->s:Lcom/twitter/ui/user/UserApprovalView$a;

    iget-object v0, v0, Lcom/twitter/ui/user/UserApprovalView$a;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 182
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->t:Lcom/twitter/ui/user/UserApprovalView$a;

    iget-object v0, v0, Lcom/twitter/ui/user/UserApprovalView$a;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 183
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->v:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 184
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 187
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->s:Lcom/twitter/ui/user/UserApprovalView$a;

    iget-object v0, v0, Lcom/twitter/ui/user/UserApprovalView$a;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 188
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->t:Lcom/twitter/ui/user/UserApprovalView$a;

    iget-object v0, v0, Lcom/twitter/ui/user/UserApprovalView$a;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->v:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 190
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->u:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ActionButton;->setVisibility(I)V

    .line 191
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 53
    invoke-super {p0}, Lcom/twitter/ui/user/BaseUserView;->drawableStateChanged()V

    .line 54
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->s:Lcom/twitter/ui/user/UserApprovalView$a;

    iget-object v0, v0, Lcom/twitter/ui/user/UserApprovalView$a;->a:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/ui/user/UserApprovalView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageButton;->setImageState([IZ)V

    .line 55
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->t:Lcom/twitter/ui/user/UserApprovalView$a;

    iget-object v0, v0, Lcom/twitter/ui/user/UserApprovalView$a;->a:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/ui/user/UserApprovalView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageButton;->setImageState([IZ)V

    .line 56
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 194
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->s:Lcom/twitter/ui/user/UserApprovalView$a;

    iget-object v0, v0, Lcom/twitter/ui/user/UserApprovalView$a;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 195
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->t:Lcom/twitter/ui/user/UserApprovalView$a;

    iget-object v0, v0, Lcom/twitter/ui/user/UserApprovalView$a;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 196
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->u:Lcom/twitter/ui/widget/ActionButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ActionButton;->setVisibility(I)V

    .line 197
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 114
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->r:Lcom/twitter/ui/user/BaseUserView$a;

    if-eqz v0, :cond_2

    .line 115
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    .line 116
    sget v0, Lcjo$e;->action_button_deny:I

    if-eq v4, v0, :cond_0

    sget v0, Lcjo$e;->action_button_deny_frame:I

    if-ne v4, v0, :cond_3

    .line 117
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/UserApprovalView;->setState(I)V

    .line 118
    invoke-virtual {p0}, Lcom/twitter/ui/user/UserApprovalView;->g()V

    .line 132
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->r:Lcom/twitter/ui/user/BaseUserView$a;

    iget-wide v2, p0, Lcom/twitter/ui/user/UserApprovalView;->f:J

    const/4 v5, -0x1

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/twitter/ui/user/BaseUserView$a;->a(Lcom/twitter/ui/user/BaseUserView;JII)V

    .line 134
    :cond_2
    return-void

    .line 119
    :cond_3
    sget v0, Lcjo$e;->action_button_accept:I

    if-eq v4, v0, :cond_4

    sget v0, Lcjo$e;->action_button_accept_frame:I

    if-ne v4, v0, :cond_6

    .line 121
    :cond_4
    invoke-virtual {p0, v1}, Lcom/twitter/ui/user/UserApprovalView;->setState(I)V

    .line 122
    iget-boolean v0, p0, Lcom/twitter/ui/user/UserApprovalView;->w:Z

    if-eqz v0, :cond_5

    .line 123
    invoke-virtual {p0}, Lcom/twitter/ui/user/UserApprovalView;->d()V

    goto :goto_0

    .line 125
    :cond_5
    invoke-virtual {p0}, Lcom/twitter/ui/user/UserApprovalView;->g()V

    goto :goto_0

    .line 127
    :cond_6
    sget v0, Lcjo$e;->action_button:I

    if-eq v4, v0, :cond_7

    sget v0, Lcjo$e;->action_button_frame:I

    if-ne v4, v0, :cond_1

    .line 128
    :cond_7
    invoke-virtual {p0, v2, v1}, Lcom/twitter/ui/user/UserApprovalView;->a(IZ)V

    .line 129
    invoke-virtual {p0, v1, v2}, Lcom/twitter/ui/user/UserApprovalView;->a(IZ)V

    .line 130
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->u:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/ActionButton;->toggle()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 40
    invoke-super {p0}, Lcom/twitter/ui/user/BaseUserView;->onFinishInflate()V

    .line 41
    sget v0, Lcjo$e;->action_button_accept_frame:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/UserApprovalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 42
    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 43
    new-instance v2, Lcom/twitter/ui/user/UserApprovalView$a;

    invoke-direct {v2, v1, v0}, Lcom/twitter/ui/user/UserApprovalView$a;-><init>(Landroid/widget/ImageButton;Landroid/widget/FrameLayout;)V

    iput-object v2, p0, Lcom/twitter/ui/user/UserApprovalView;->s:Lcom/twitter/ui/user/UserApprovalView$a;

    .line 44
    sget v0, Lcjo$e;->action_button_deny_frame:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/UserApprovalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 45
    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 46
    new-instance v2, Lcom/twitter/ui/user/UserApprovalView$a;

    invoke-direct {v2, v1, v0}, Lcom/twitter/ui/user/UserApprovalView$a;-><init>(Landroid/widget/ImageButton;Landroid/widget/FrameLayout;)V

    iput-object v2, p0, Lcom/twitter/ui/user/UserApprovalView;->t:Lcom/twitter/ui/user/UserApprovalView$a;

    .line 47
    sget v0, Lcjo$e;->action_button_frame:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/UserApprovalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->v:Landroid/view/View;

    .line 48
    sget v0, Lcjo$e;->action_button:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/UserApprovalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/ActionButton;

    iput-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->u:Lcom/twitter/ui/widget/ActionButton;

    .line 49
    return-void
.end method

.method public setState(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 137
    packed-switch p1, :pswitch_data_0

    .line 157
    invoke-virtual {p0, v1, v1}, Lcom/twitter/ui/user/UserApprovalView;->a(IZ)V

    .line 158
    invoke-virtual {p0, v2, v1}, Lcom/twitter/ui/user/UserApprovalView;->a(IZ)V

    .line 159
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->u:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ActionButton;->setChecked(Z)V

    .line 163
    :goto_0
    return-void

    .line 139
    :pswitch_0
    invoke-virtual {p0, v1, v2}, Lcom/twitter/ui/user/UserApprovalView;->a(IZ)V

    .line 140
    invoke-virtual {p0, v2, v1}, Lcom/twitter/ui/user/UserApprovalView;->a(IZ)V

    .line 141
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->u:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ActionButton;->setChecked(Z)V

    goto :goto_0

    .line 145
    :pswitch_1
    invoke-virtual {p0, v1, v1}, Lcom/twitter/ui/user/UserApprovalView;->a(IZ)V

    .line 146
    invoke-virtual {p0, v2, v2}, Lcom/twitter/ui/user/UserApprovalView;->a(IZ)V

    .line 147
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->u:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ActionButton;->setChecked(Z)V

    goto :goto_0

    .line 151
    :pswitch_2
    invoke-virtual {p0, v1, v2}, Lcom/twitter/ui/user/UserApprovalView;->a(IZ)V

    .line 152
    invoke-virtual {p0, v2, v1}, Lcom/twitter/ui/user/UserApprovalView;->a(IZ)V

    .line 153
    iget-object v0, p0, Lcom/twitter/ui/user/UserApprovalView;->u:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/ActionButton;->setChecked(Z)V

    goto :goto_0

    .line 137
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
