.class public Lcom/twitter/ui/user/UserView;
.super Lcom/twitter/ui/user/BaseUserView;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private A:Lcom/twitter/ui/user/BaseUserView$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;"
        }
    .end annotation
.end field

.field private B:Lcom/twitter/ui/user/BaseUserView$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;"
        }
    .end annotation
.end field

.field private C:Lcom/twitter/ui/user/BaseUserView$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;"
        }
    .end annotation
.end field

.field private D:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

.field private E:Ljava/lang/String;

.field private F:Z

.field private G:I

.field public r:Lcom/twitter/ui/widget/ActionButton;

.field public s:Landroid/widget/CheckBox;

.field protected t:Lcom/twitter/ui/widget/ActionButton;

.field private u:Lcom/twitter/ui/widget/ToggleTwitterButton;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Z

.field private y:Lcom/twitter/ui/user/BaseUserView$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lcom/twitter/ui/user/BaseUserView$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/twitter/ui/user/BaseUserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/ui/user/UserView;->F:Z

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/ui/user/UserView;->G:I

    .line 54
    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->t:Lcom/twitter/ui/widget/ActionButton;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->u:Lcom/twitter/ui/widget/ToggleTwitterButton;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setFollowButtonText(Z)V
    .locals 2

    .prologue
    .line 223
    iget-object v1, p0, Lcom/twitter/ui/user/UserView;->u:Lcom/twitter/ui/widget/ToggleTwitterButton;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->w:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setText(Ljava/lang/CharSequence;)V

    .line 224
    return-void

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->v:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public a(ILcom/twitter/ui/user/BaseUserView$a;)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->t:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ActionButton;->a(I)V

    .line 118
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->t:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->t:Lcom/twitter/ui/widget/ActionButton;

    iget-object v1, p0, Lcom/twitter/ui/user/UserView;->i:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ActionButton;->setUsername(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0, p2}, Lcom/twitter/ui/user/UserView;->setActionButtonClickListener(Lcom/twitter/ui/user/BaseUserView$a;)V

    .line 121
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0, p1, p2}, Lcom/twitter/ui/user/BaseUserView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->t:Lcom/twitter/ui/widget/ActionButton;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->t:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ActionButton;->setUsername(Ljava/lang/String;)V

    .line 84
    :cond_0
    return-void
.end method

.method public a(ZZ)V
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lcom/twitter/ui/user/UserView;->setMuted(Z)V

    .line 88
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->n:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->n:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 91
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/twitter/ui/user/UserView;->v:Ljava/lang/String;

    .line 202
    iput-object p2, p0, Lcom/twitter/ui/user/UserView;->w:Ljava/lang/String;

    .line 203
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 150
    iput-boolean p1, p0, Lcom/twitter/ui/user/UserView;->F:Z

    .line 151
    return-void
.end method

.method public c(Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 191
    iput-boolean p1, p0, Lcom/twitter/ui/user/UserView;->x:Z

    .line 192
    invoke-direct {p0}, Lcom/twitter/ui/user/UserView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    iget-object v3, p0, Lcom/twitter/ui/user/UserView;->t:Lcom/twitter/ui/widget/ActionButton;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/twitter/ui/widget/ActionButton;->setVisibility(I)V

    .line 195
    :cond_0
    invoke-direct {p0}, Lcom/twitter/ui/user/UserView;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->u:Lcom/twitter/ui/widget/ToggleTwitterButton;

    if-eqz p1, :cond_3

    :goto_1
    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setVisibility(I)V

    .line 198
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 193
    goto :goto_0

    :cond_3
    move v2, v1

    .line 196
    goto :goto_1
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 275
    iget v0, p0, Lcom/twitter/ui/user/UserView;->G:I

    return v0
.end method

.method public getScribeComponent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->E:Ljava/lang/String;

    return-object v0
.end method

.method public getScribeItem()Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->D:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/twitter/ui/user/UserView;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/ui/user/UserView;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()V
    .locals 1

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/twitter/ui/user/UserView;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/ui/user/UserView;->x:Z

    if-nez v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->t:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/ActionButton;->toggle()V

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    invoke-direct {p0}, Lcom/twitter/ui/user/UserView;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->u:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->d()V

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/twitter/ui/user/UserView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/ui/user/UserView;->x:Z

    if-nez v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->t:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/ActionButton;->isChecked()Z

    move-result v0

    .line 181
    :goto_0
    return v0

    .line 178
    :cond_0
    invoke-direct {p0}, Lcom/twitter/ui/user/UserView;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->u:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->e()Z

    move-result v0

    goto :goto_0

    .line 181
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 234
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    .line 236
    sget v0, Lcjo$e;->follow_button:I

    if-ne v4, v0, :cond_2

    .line 237
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->y:Lcom/twitter/ui/user/BaseUserView$a;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->y:Lcom/twitter/ui/user/BaseUserView$a;

    iget-wide v2, p0, Lcom/twitter/ui/user/UserView;->f:J

    iget v5, p0, Lcom/twitter/ui/user/UserView;->G:I

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/twitter/ui/user/BaseUserView$a;->a(Lcom/twitter/ui/user/BaseUserView;JII)V

    .line 240
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/ui/user/UserView;->F:Z

    if-eqz v0, :cond_1

    .line 242
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->u:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->d()V

    .line 243
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->u:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->e()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/ui/user/UserView;->setFollowButtonText(Z)V

    .line 272
    :cond_1
    :goto_0
    return-void

    .line 245
    :cond_2
    sget v0, Lcjo$e;->action_button:I

    if-ne v4, v0, :cond_4

    .line 246
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->y:Lcom/twitter/ui/user/BaseUserView$a;

    if-eqz v0, :cond_3

    .line 247
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->y:Lcom/twitter/ui/user/BaseUserView$a;

    iget-wide v2, p0, Lcom/twitter/ui/user/UserView;->f:J

    iget v5, p0, Lcom/twitter/ui/user/UserView;->G:I

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/twitter/ui/user/BaseUserView$a;->a(Lcom/twitter/ui/user/BaseUserView;JII)V

    .line 249
    :cond_3
    iget-boolean v0, p0, Lcom/twitter/ui/user/UserView;->F:Z

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->t:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/ActionButton;->toggle()V

    goto :goto_0

    .line 253
    :cond_4
    sget v0, Lcjo$e;->block_button:I

    if-ne v4, v0, :cond_6

    .line 254
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->z:Lcom/twitter/ui/user/BaseUserView$a;

    if-eqz v0, :cond_5

    .line 255
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->z:Lcom/twitter/ui/user/BaseUserView$a;

    iget-wide v2, p0, Lcom/twitter/ui/user/UserView;->f:J

    iget v5, p0, Lcom/twitter/ui/user/UserView;->G:I

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/twitter/ui/user/BaseUserView$a;->a(Lcom/twitter/ui/user/BaseUserView;JII)V

    .line 258
    :cond_5
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->r:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/ActionButton;->toggle()V

    goto :goto_0

    .line 259
    :cond_6
    sget v0, Lcjo$e;->user_checkbox:I

    if-ne v4, v0, :cond_7

    .line 260
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->A:Lcom/twitter/ui/user/BaseUserView$a;

    if-eqz v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->A:Lcom/twitter/ui/user/BaseUserView$a;

    iget-wide v2, p0, Lcom/twitter/ui/user/UserView;->f:J

    iget v5, p0, Lcom/twitter/ui/user/UserView;->G:I

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/twitter/ui/user/BaseUserView$a;->a(Lcom/twitter/ui/user/BaseUserView;JII)V

    goto :goto_0

    .line 263
    :cond_7
    sget v0, Lcjo$e;->muted_item:I

    if-ne v4, v0, :cond_8

    .line 264
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->B:Lcom/twitter/ui/user/BaseUserView$a;

    if-eqz v0, :cond_1

    .line 265
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->B:Lcom/twitter/ui/user/BaseUserView$a;

    iget-wide v2, p0, Lcom/twitter/ui/user/UserView;->f:J

    iget v5, p0, Lcom/twitter/ui/user/UserView;->G:I

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/twitter/ui/user/BaseUserView$a;->a(Lcom/twitter/ui/user/BaseUserView;JII)V

    goto :goto_0

    .line 267
    :cond_8
    sget v0, Lcjo$e;->user_image:I

    if-ne v4, v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->C:Lcom/twitter/ui/user/BaseUserView$a;

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->C:Lcom/twitter/ui/user/BaseUserView$a;

    iget-wide v2, p0, Lcom/twitter/ui/user/UserView;->f:J

    iget v5, p0, Lcom/twitter/ui/user/UserView;->G:I

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/twitter/ui/user/BaseUserView$a;->a(Lcom/twitter/ui/user/BaseUserView;JII)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/twitter/ui/user/BaseUserView;->onFinishInflate()V

    .line 59
    sget v0, Lcjo$e;->action_button:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/UserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/ActionButton;

    iput-object v0, p0, Lcom/twitter/ui/user/UserView;->t:Lcom/twitter/ui/widget/ActionButton;

    .line 60
    sget v0, Lcjo$e;->follow_button:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/UserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/ToggleTwitterButton;

    iput-object v0, p0, Lcom/twitter/ui/user/UserView;->u:Lcom/twitter/ui/widget/ToggleTwitterButton;

    .line 61
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->u:Lcom/twitter/ui/widget/ToggleTwitterButton;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->u:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    :cond_0
    sget v0, Lcjo$e;->block_button:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/UserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/ActionButton;

    iput-object v0, p0, Lcom/twitter/ui/user/UserView;->r:Lcom/twitter/ui/widget/ActionButton;

    .line 66
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->r:Lcom/twitter/ui/widget/ActionButton;

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->r:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    :cond_1
    sget v0, Lcjo$e;->user_checkbox:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/UserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/twitter/ui/user/UserView;->s:Landroid/widget/CheckBox;

    .line 70
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->s:Landroid/widget/CheckBox;

    if-eqz v0, :cond_2

    .line 71
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    :cond_2
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->m:Lcom/twitter/media/ui/image/UserImageView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->C:Lcom/twitter/ui/user/BaseUserView$a;

    if-eqz v0, :cond_3

    .line 74
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->m:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, p0}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    :cond_3
    return-void
.end method

.method public setActionButtonClickListener(Lcom/twitter/ui/user/BaseUserView$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 124
    iput-object p1, p0, Lcom/twitter/ui/user/UserView;->y:Lcom/twitter/ui/user/BaseUserView$a;

    .line 125
    return-void
.end method

.method public setBlockButtonClickListener(Lcom/twitter/ui/user/BaseUserView$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 128
    iput-object p1, p0, Lcom/twitter/ui/user/UserView;->z:Lcom/twitter/ui/user/BaseUserView$a;

    .line 129
    return-void
.end method

.method public setCheckBoxClickListener(Lcom/twitter/ui/user/BaseUserView$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 139
    iput-object p1, p0, Lcom/twitter/ui/user/UserView;->A:Lcom/twitter/ui/user/BaseUserView$a;

    .line 140
    return-void
.end method

.method public setFollowBackgroundResource(I)V
    .locals 1

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/twitter/ui/user/UserView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->t:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ActionButton;->setBackgroundResource(I)V

    .line 188
    :cond_0
    return-void
.end method

.method public setFollowVisibility(I)V
    .locals 1

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/twitter/ui/user/UserView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->t:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ActionButton;->setVisibility(I)V

    .line 209
    :cond_0
    invoke-direct {p0}, Lcom/twitter/ui/user/UserView;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/ui/user/UserView;->x:Z

    if-eqz v0, :cond_1

    .line 210
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->u:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setVisibility(I)V

    .line 212
    :cond_1
    return-void
.end method

.method public setIsFollowing(Z)V
    .locals 1

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/twitter/ui/user/UserView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->t:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ActionButton;->setChecked(Z)V

    .line 169
    :cond_0
    invoke-direct {p0}, Lcom/twitter/ui/user/UserView;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->u:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setToggledOn(Z)V

    .line 171
    invoke-direct {p0, p1}, Lcom/twitter/ui/user/UserView;->setFollowButtonText(Z)V

    .line 173
    :cond_1
    return-void
.end method

.method public setMutedViewClickListener(Lcom/twitter/ui/user/BaseUserView$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 132
    iput-object p1, p0, Lcom/twitter/ui/user/UserView;->B:Lcom/twitter/ui/user/BaseUserView$a;

    .line 133
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->n:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 134
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    :cond_0
    return-void
.end method

.method public setPosition(I)V
    .locals 0

    .prologue
    .line 279
    iput p1, p0, Lcom/twitter/ui/user/UserView;->G:I

    .line 280
    return-void
.end method

.method public setProfileClickListener(Lcom/twitter/ui/user/BaseUserView$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 143
    iput-object p1, p0, Lcom/twitter/ui/user/UserView;->C:Lcom/twitter/ui/user/BaseUserView$a;

    .line 144
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->m:Lcom/twitter/media/ui/image/UserImageView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 145
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->m:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, p0}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    :cond_0
    return-void
.end method

.method public setScribeComponent(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/twitter/ui/user/UserView;->E:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public setScribeItem(Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/twitter/ui/user/UserView;->D:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 95
    return-void
.end method

.method public setShowIconOnFollowButton(Z)V
    .locals 1

    .prologue
    .line 227
    invoke-direct {p0}, Lcom/twitter/ui/user/UserView;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/twitter/ui/user/UserView;->u:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setShowIcon(Z)V

    .line 230
    :cond_0
    return-void
.end method
