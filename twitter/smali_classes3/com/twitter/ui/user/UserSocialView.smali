.class public Lcom/twitter/ui/user/UserSocialView;
.super Lcom/twitter/ui/user/UserView;
.source "Twttr"


# instance fields
.field private u:Lcom/twitter/ui/socialproof/SocialBylineView;

.field private v:F

.field private w:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    sget v0, Lcjo$a;->userViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/ui/user/UserSocialView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/twitter/ui/user/UserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    sget-object v0, Lcjo$h;->UserSocialView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 38
    sget v1, Lcjo$h;->UserSocialView_contentSize:I

    .line 39
    invoke-static {}, Lcni;->a()F

    move-result v2

    .line 38
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/ui/user/UserSocialView;->v:F

    .line 40
    sget v1, Lcjo$h;->UserSocialView_bylineSize:I

    .line 41
    invoke-static {}, Lcni;->b()F

    move-result v2

    .line 40
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/ui/user/UserSocialView;->w:F

    .line 42
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 43
    return-void
.end method

.method private a(FF)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 228
    iget-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 229
    iget-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 230
    iget-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->u:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-virtual {v0, p2}, Lcom/twitter/ui/socialproof/SocialBylineView;->setLabelSize(F)V

    .line 232
    iget-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->k:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 236
    iget-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 238
    :cond_1
    return-void
.end method

.method private a(ILjava/lang/String;Z)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 145
    iget-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->u:Lcom/twitter/ui/socialproof/SocialBylineView;

    .line 146
    if-lez p1, :cond_0

    .line 147
    invoke-virtual {v0, p1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setIcon(I)V

    .line 149
    :cond_0
    invoke-virtual {v0, p2}, Lcom/twitter/ui/socialproof/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    .line 150
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setVisibility(I)V

    .line 151
    invoke-virtual {v0, p3}, Lcom/twitter/ui/socialproof/SocialBylineView;->setRenderRTL(Z)V

    .line 152
    return-void
.end method


# virtual methods
.method public a(IILjava/lang/String;IZ)V
    .locals 7
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 82
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/ui/user/UserSocialView;->a(IILjava/lang/String;IZLjava/lang/String;)V

    .line 83
    return-void
.end method

.method public a(IILjava/lang/String;IZLjava/lang/String;)V
    .locals 5
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 98
    if-lez p2, :cond_2

    invoke-static {p3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 99
    sparse-switch p1, :sswitch_data_0

    move-object p6, v0

    .line 139
    :goto_0
    :pswitch_0
    if-eqz p6, :cond_0

    .line 140
    invoke-direct {p0, p2, p6, p5}, Lcom/twitter/ui/user/UserSocialView;->a(ILjava/lang/String;Z)V

    .line 142
    :cond_0
    return-void

    .line 101
    :sswitch_0
    if-lez p4, :cond_1

    .line 102
    invoke-virtual {p0}, Lcom/twitter/ui/user/UserSocialView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcjo$g;->social_follow_and_more_follow:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p3, v2, v3

    .line 103
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 102
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p6

    goto :goto_0

    .line 105
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/ui/user/UserSocialView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcjo$g;->social_follow_and_follow:I

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p3, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p6

    goto :goto_0

    .line 110
    :sswitch_1
    invoke-virtual {p0}, Lcom/twitter/ui/user/UserSocialView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcjo$g;->social_both_follow:I

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p3, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p6

    goto :goto_0

    .line 114
    :sswitch_2
    invoke-virtual {p0}, Lcom/twitter/ui/user/UserSocialView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcjo$g;->social_provides_support:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p6

    goto :goto_0

    .line 122
    :cond_2
    invoke-static {p6}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 123
    packed-switch p1, :pswitch_data_0

    move-object p6, v0

    .line 131
    goto :goto_0

    .line 136
    :cond_3
    iget-object v1, p0, Lcom/twitter/ui/user/UserSocialView;->u:Lcom/twitter/ui/socialproof/SocialBylineView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/twitter/ui/socialproof/SocialBylineView;->setVisibility(I)V

    .line 137
    iget-object v1, p0, Lcom/twitter/ui/user/UserSocialView;->u:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/socialproof/SocialBylineView;->setIconDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object p6, v0

    goto :goto_0

    .line 99
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x28 -> :sswitch_0
        0x2d -> :sswitch_2
    .end sparse-switch

    .line 123
    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(IIZ)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 162
    iget-object v2, p0, Lcom/twitter/ui/user/UserSocialView;->u:Lcom/twitter/ui/socialproof/SocialBylineView;

    .line 164
    iget-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->t:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/ActionButton;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 166
    :goto_0
    invoke-static {p2}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    .line 167
    invoke-virtual {p0}, Lcom/twitter/ui/user/UserSocialView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lcjo$g;->social_following:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 173
    :goto_1
    if-lez p1, :cond_3

    if-eqz v0, :cond_3

    .line 174
    invoke-virtual {v2, p1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setIcon(I)V

    .line 175
    invoke-virtual {v2, v0}, Lcom/twitter/ui/socialproof/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    .line 176
    invoke-virtual {v2, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setVisibility(I)V

    .line 177
    invoke-virtual {v2, p3}, Lcom/twitter/ui/socialproof/SocialBylineView;->setRenderRTL(Z)V

    .line 181
    :goto_2
    return-void

    :cond_0
    move v0, v1

    .line 164
    goto :goto_0

    .line 168
    :cond_1
    invoke-static {p2}, Lcom/twitter/model/core/g;->c(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 169
    invoke-virtual {p0}, Lcom/twitter/ui/user/UserSocialView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lcjo$g;->social_follows_you:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 171
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 179
    :cond_3
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Lcom/twitter/ui/socialproof/SocialBylineView;->setVisibility(I)V

    goto :goto_2
.end method

.method public a(Lcom/twitter/model/core/TwitterSocialProof;Z)V
    .locals 7

    .prologue
    .line 56
    if-eqz p1, :cond_2

    .line 57
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 58
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->k:I

    invoke-static {v0}, Lcom/twitter/ui/socialproof/a;->a(I)I

    move-result v0

    iget-object v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->l:Ljava/lang/String;

    invoke-direct {p0, v0, v1, p2}, Lcom/twitter/ui/user/UserSocialView;->a(ILjava/lang/String;Z)V

    .line 69
    :goto_0
    return-void

    .line 61
    :cond_0
    iget v1, p1, Lcom/twitter/model/core/TwitterSocialProof;->b:I

    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->b:I

    invoke-static {v0}, Lcom/twitter/ui/socialproof/a;->a(I)I

    move-result v2

    iget-object v3, p1, Lcom/twitter/model/core/TwitterSocialProof;->c:Ljava/lang/String;

    iget-object v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->h:Ljava/lang/String;

    if-nez v0, :cond_1

    iget v4, p1, Lcom/twitter/model/core/TwitterSocialProof;->i:I

    :goto_1
    iget-object v6, p1, Lcom/twitter/model/core/TwitterSocialProof;->j:Ljava/lang/String;

    move-object v0, p0

    move v5, p2

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/ui/user/UserSocialView;->a(IILjava/lang/String;IZLjava/lang/String;)V

    goto :goto_0

    :cond_1
    iget v0, p1, Lcom/twitter/model/core/TwitterSocialProof;->i:I

    add-int/lit8 v4, v0, 0x1

    goto :goto_1

    .line 67
    :cond_2
    iget-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->u:Lcom/twitter/ui/socialproof/SocialBylineView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;IZ)V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->u:Lcom/twitter/ui/socialproof/SocialBylineView;

    .line 194
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 195
    if-lez p2, :cond_0

    .line 196
    invoke-virtual {v0, p2}, Lcom/twitter/ui/socialproof/SocialBylineView;->setIcon(I)V

    .line 200
    :goto_0
    invoke-virtual {v0, p1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    .line 201
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setVisibility(I)V

    .line 202
    invoke-virtual {v0, p3}, Lcom/twitter/ui/socialproof/SocialBylineView;->setRenderRTL(Z)V

    .line 206
    :goto_1
    return-void

    .line 198
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setIconDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 204
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/ui/user/UserSocialView;->a(Ljava/lang/String;IZ)V

    .line 190
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 241
    invoke-virtual {p0}, Lcom/twitter/ui/user/UserSocialView;->getImageView()Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setSize(I)V

    .line 242
    iget-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->u:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-static {v1}, Lcom/twitter/media/manager/UserImageRequest;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setMinIconWidth(I)V

    .line 243
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 47
    invoke-super {p0}, Lcom/twitter/ui/user/UserView;->onFinishInflate()V

    .line 48
    sget v0, Lcjo$e;->social_byline:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/UserSocialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/socialproof/SocialBylineView;

    iput-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->u:Lcom/twitter/ui/socialproof/SocialBylineView;

    .line 49
    iget v0, p0, Lcom/twitter/ui/user/UserSocialView;->v:F

    iget v1, p0, Lcom/twitter/ui/user/UserSocialView;->w:F

    invoke-direct {p0, v0, v1}, Lcom/twitter/ui/user/UserSocialView;->a(FF)V

    .line 50
    return-void
.end method

.method public setContentSize(F)V
    .locals 1

    .prologue
    .line 214
    iput p1, p0, Lcom/twitter/ui/user/UserSocialView;->v:F

    .line 215
    invoke-static {p1}, Lcni;->a(F)F

    move-result v0

    .line 216
    iput v0, p0, Lcom/twitter/ui/user/UserSocialView;->w:F

    .line 218
    invoke-direct {p0, p1, v0}, Lcom/twitter/ui/user/UserSocialView;->a(FF)V

    .line 219
    return-void
.end method

.method public setScreenNameColor(I)V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 252
    return-void
.end method

.method public setUserImageSize(I)V
    .locals 2

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/twitter/ui/user/UserSocialView;->getImageView()Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/media/ui/image/UserImageView;->setSize(I)V

    .line 247
    iget-object v0, p0, Lcom/twitter/ui/user/UserSocialView;->u:Lcom/twitter/ui/socialproof/SocialBylineView;

    invoke-static {p1}, Lcom/twitter/media/manager/UserImageRequest;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setMinIconWidth(I)V

    .line 248
    return-void
.end method
