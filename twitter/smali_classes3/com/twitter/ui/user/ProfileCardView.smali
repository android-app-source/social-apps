.class public Lcom/twitter/ui/user/ProfileCardView;
.super Lcom/twitter/ui/user/UserSocialView;
.source "Twttr"


# instance fields
.field private final u:F

.field private v:Lcom/twitter/media/ui/image/MediaImageView;

.field private final w:I

.field private final x:I

.field private final y:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field private final z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/twitter/ui/user/UserSocialView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    invoke-virtual {p0}, Lcom/twitter/ui/user/ProfileCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 35
    sget v1, Lcjo$b;->twitter_blue:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/user/ProfileCardView;->w:I

    .line 36
    sget v1, Lcjo$c;->profile_card_banner_image_corner_radius:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/user/ProfileCardView;->x:I

    .line 39
    sget-object v1, Lcjo$h;->ProfileCardView:[I

    invoke-virtual {p1, p2, v1, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 40
    sget v2, Lcjo$h;->ProfileCardView_profileUserImageStrokeWidth:I

    sget v3, Lcjo$c;->profile_card_user_image_border_stroke:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/ui/user/ProfileCardView;->y:I

    .line 42
    sget v2, Lcjo$h;->ProfileCardView_profileDescriptionFontSize:I

    .line 43
    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 44
    sget v3, Lcjo$h;->ProfileCardView_profileImageTopMarginRatio:I

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Lcom/twitter/ui/user/ProfileCardView;->u:F

    .line 47
    if-lez v2, :cond_0

    .line 48
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    :goto_0
    iput v0, p0, Lcom/twitter/ui/user/ProfileCardView;->z:F

    .line 49
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 50
    return-void

    .line 48
    :cond_0
    sget v0, Lcni;->a:F

    goto :goto_0
.end method

.method private k()V
    .locals 5

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/ui/user/ProfileCardView;->m:Lcom/twitter/media/ui/image/UserImageView;

    iget v1, p0, Lcom/twitter/ui/user/ProfileCardView;->y:I

    sget v2, Lcjo$b;->app_background:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/media/ui/image/UserImageView;->b(II)V

    .line 77
    iget-object v0, p0, Lcom/twitter/ui/user/ProfileCardView;->m:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/UserImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 78
    iget v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    neg-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/twitter/ui/user/ProfileCardView;->u:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 79
    iget v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 81
    iget-object v1, p0, Lcom/twitter/ui/user/ProfileCardView;->m:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    invoke-direct {p0}, Lcom/twitter/ui/user/ProfileCardView;->l()V

    .line 84
    return-void
.end method

.method private l()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 89
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 90
    const/16 v1, 0x8

    new-array v1, v1, [F

    const/4 v2, 0x0

    iget v3, p0, Lcom/twitter/ui/user/ProfileCardView;->x:I

    int-to-float v3, v3

    aput v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/twitter/ui/user/ProfileCardView;->x:I

    int-to-float v3, v3

    aput v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/twitter/ui/user/ProfileCardView;->x:I

    int-to-float v3, v3

    aput v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/twitter/ui/user/ProfileCardView;->x:I

    int-to-float v3, v3

    aput v3, v1, v2

    const/4 v2, 0x4

    aput v4, v1, v2

    const/4 v2, 0x5

    aput v4, v1, v2

    const/4 v2, 0x6

    aput v4, v1, v2

    const/4 v2, 0x7

    aput v4, v1, v2

    .line 92
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    .line 95
    iget-object v1, p0, Lcom/twitter/ui/user/ProfileCardView;->v:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 96
    return-void
.end method

.method private setBannerImageContent(Lcom/twitter/model/core/TwitterUser;)V
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/ui/user/ProfileCardView;->v:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 126
    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->i:I

    if-eqz v1, :cond_0

    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->i:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 127
    iget-object v1, p0, Lcom/twitter/ui/user/ProfileCardView;->v:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 128
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->F:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/twitter/ui/user/ProfileCardView;->v:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->F:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/media/util/HeaderImageVariant;->j:Lcom/twitter/media/request/a$c;

    .line 130
    invoke-virtual {v1, v2}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/a$c;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    .line 129
    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 134
    :goto_1
    return-void

    .line 126
    :cond_0
    iget v1, p0, Lcom/twitter/ui/user/ProfileCardView;->w:I

    goto :goto_0

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/twitter/ui/user/ProfileCardView;->v:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    goto :goto_1
.end method


# virtual methods
.method public c()V
    .locals 4

    .prologue
    .line 55
    sget v0, Lcjo$e;->user_info_layout:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/ProfileCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 56
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 57
    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 58
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 59
    sget v0, Lcjo$e;->spacer:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/ProfileCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 60
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/ui/user/ProfileCardView;->m:Lcom/twitter/media/ui/image/UserImageView;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/twitter/ui/user/ProfileCardView;->m:Lcom/twitter/media/ui/image/UserImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setVisibility(I)V

    .line 115
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Lcom/twitter/ui/user/UserSocialView;->onFinishInflate()V

    .line 65
    sget v0, Lcjo$e;->banner_image:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/ProfileCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/ui/user/ProfileCardView;->v:Lcom/twitter/media/ui/image/MediaImageView;

    .line 66
    invoke-direct {p0}, Lcom/twitter/ui/user/ProfileCardView;->k()V

    .line 67
    return-void
.end method

.method public setUser(Lcom/twitter/model/core/TwitterUser;)V
    .locals 2

    .prologue
    .line 104
    invoke-super {p0, p1}, Lcom/twitter/ui/user/UserSocialView;->setUser(Lcom/twitter/model/core/TwitterUser;)V

    .line 105
    invoke-direct {p0, p1}, Lcom/twitter/ui/user/ProfileCardView;->setBannerImageContent(Lcom/twitter/model/core/TwitterUser;)V

    .line 106
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/ui/user/ProfileCardView;->a(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    .line 107
    iget v0, p0, Lcom/twitter/ui/user/ProfileCardView;->z:F

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/ProfileCardView;->setProfileDescriptionTextSize(F)V

    .line 108
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/ui/user/ProfileCardView;->setIsFollowing(Z)V

    .line 109
    return-void
.end method
