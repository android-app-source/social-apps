.class public Lcom/twitter/ui/view/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/view/d;


# instance fields
.field private final a:Lcom/twitter/android/composer/ComposerDockLayout$a;

.field private final b:Landroid/support/v4/app/FragmentActivity;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/composer/ComposerDockLayout$a;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/twitter/ui/view/b;->b:Landroid/support/v4/app/FragmentActivity;

    .line 21
    iput-object p2, p0, Lcom/twitter/ui/view/b;->a:Lcom/twitter/android/composer/ComposerDockLayout$a;

    .line 22
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 26
    if-ne p1, v1, :cond_1

    .line 27
    iget-object v0, p0, Lcom/twitter/ui/view/b;->a:Lcom/twitter/android/composer/ComposerDockLayout$a;

    invoke-interface {v0, v1}, Lcom/twitter/android/composer/ComposerDockLayout$a;->a(I)V

    .line 37
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 34
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:navigation_bar::compose:click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 35
    iget-object v0, p0, Lcom/twitter/ui/view/b;->b:Landroid/support/v4/app/FragmentActivity;

    iget-object v1, p0, Lcom/twitter/ui/view/b;->b:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v1}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
