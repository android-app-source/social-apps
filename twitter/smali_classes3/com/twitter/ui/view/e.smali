.class public Lcom/twitter/ui/view/e;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/ui/view/FabAnimator;

.field private b:Lcom/twitter/ui/view/d;

.field private c:I


# direct methods
.method constructor <init>(Lcom/twitter/ui/view/f;Lcom/twitter/ui/view/FabAnimator;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/ui/view/e;->c:I

    .line 42
    iput-object p2, p0, Lcom/twitter/ui/view/e;->a:Lcom/twitter/ui/view/FabAnimator;

    .line 43
    iget v0, p0, Lcom/twitter/ui/view/e;->c:I

    invoke-virtual {p1, v0}, Lcom/twitter/ui/view/f;->a(I)V

    .line 44
    new-instance v0, Lcom/twitter/ui/view/e$1;

    invoke-direct {v0, p0}, Lcom/twitter/ui/view/e$1;-><init>(Lcom/twitter/ui/view/e;)V

    invoke-virtual {p1, v0}, Lcom/twitter/ui/view/f;->a(Landroid/view/View$OnClickListener;)V

    .line 52
    return-void
.end method

.method static synthetic a(Lcom/twitter/ui/view/e;)Lcom/twitter/ui/view/d;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/twitter/ui/view/e;->b:Lcom/twitter/ui/view/d;

    return-object v0
.end method

.method public static a(Lcom/twitter/ui/widget/DefaultFab;)Lcom/twitter/ui/view/e;
    .locals 3

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/ui/view/f;

    invoke-direct {v0, p0}, Lcom/twitter/ui/view/f;-><init>(Lcom/twitter/ui/widget/DefaultFab;)V

    .line 36
    invoke-static {v0}, Lcom/twitter/ui/view/FabAnimator;->a(Lcom/twitter/ui/view/f;)Lcom/twitter/ui/view/FabAnimator;

    move-result-object v1

    .line 37
    new-instance v2, Lcom/twitter/ui/view/e;

    invoke-direct {v2, v0, v1}, Lcom/twitter/ui/view/e;-><init>(Lcom/twitter/ui/view/f;Lcom/twitter/ui/view/FabAnimator;)V

    return-object v2
.end method

.method static synthetic b(Lcom/twitter/ui/view/e;)I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/twitter/ui/view/e;->c:I

    return v0
.end method


# virtual methods
.method public a(Landroid/net/Uri;)I
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/twitter/app/main/MainActivity;->d:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    .line 72
    :goto_0
    return v0

    .line 70
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 59
    iget v0, p0, Lcom/twitter/ui/view/e;->c:I

    if-ne p1, v0, :cond_0

    .line 66
    :goto_0
    return-void

    .line 62
    :cond_0
    iget v0, p0, Lcom/twitter/ui/view/e;->c:I

    if-le p1, v0, :cond_1

    sget-object v0, Lcom/twitter/ui/view/FabAnimator$Direction;->a:Lcom/twitter/ui/view/FabAnimator$Direction;

    .line 64
    :goto_1
    iput p1, p0, Lcom/twitter/ui/view/e;->c:I

    .line 65
    iget-object v1, p0, Lcom/twitter/ui/view/e;->a:Lcom/twitter/ui/view/FabAnimator;

    iget v2, p0, Lcom/twitter/ui/view/e;->c:I

    invoke-virtual {v1, v2, v0}, Lcom/twitter/ui/view/FabAnimator;->a(ILcom/twitter/ui/view/FabAnimator$Direction;)V

    goto :goto_0

    .line 62
    :cond_1
    sget-object v0, Lcom/twitter/ui/view/FabAnimator$Direction;->b:Lcom/twitter/ui/view/FabAnimator$Direction;

    goto :goto_1
.end method

.method public a(Lcom/twitter/ui/view/d;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/twitter/ui/view/e;->b:Lcom/twitter/ui/view/d;

    .line 56
    return-void
.end method
