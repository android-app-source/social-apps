.class public Lcom/twitter/ui/view/i;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/ui/widget/DefaultFab;

.field private final b:Lcom/twitter/android/moments/ui/guide/l$a;


# direct methods
.method constructor <init>(Lcom/twitter/ui/widget/DefaultFab;Lcom/twitter/android/moments/ui/guide/l$a;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/twitter/ui/view/i;->a:Lcom/twitter/ui/widget/DefaultFab;

    .line 50
    iput-object p2, p0, Lcom/twitter/ui/view/i;->b:Lcom/twitter/android/moments/ui/guide/l$a;

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/twitter/ui/view/i;)Lcom/twitter/android/moments/ui/guide/l$a;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/twitter/ui/view/i;->b:Lcom/twitter/android/moments/ui/guide/l$a;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;JLxh;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/ui/view/i;
    .locals 7

    .prologue
    .line 40
    const v0, 0x7f13043e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/twitter/ui/widget/DefaultFab;

    .line 41
    invoke-static {}, Laad;->a()Laad;

    move-result-object v5

    move-object v0, p0

    move-object v1, p3

    move-wide v2, p1

    move-object v4, p4

    .line 43
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/moments/ui/guide/b;->a(Landroid/app/Activity;Lxh;JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;Laad;)Lcom/twitter/android/moments/ui/guide/b;

    move-result-object v0

    .line 45
    new-instance v1, Lcom/twitter/ui/view/i;

    invoke-direct {v1, v6, v0}, Lcom/twitter/ui/view/i;-><init>(Lcom/twitter/ui/widget/DefaultFab;Lcom/twitter/android/moments/ui/guide/l$a;)V

    return-object v1
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/moments/ui/guide/g;)Lcom/twitter/ui/view/i;
    .locals 3

    .prologue
    .line 29
    const v0, 0x7f13043e

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/DefaultFab;

    .line 30
    invoke-static {}, Laad;->a()Laad;

    move-result-object v1

    .line 32
    invoke-static {p0, p1, v1}, Lcom/twitter/android/moments/ui/guide/ah;->a(Landroid/app/Activity;Lcom/twitter/android/moments/ui/guide/g;Laad;)Lcom/twitter/android/moments/ui/guide/ah;

    move-result-object v1

    .line 33
    new-instance v2, Lcom/twitter/ui/view/i;

    invoke-direct {v2, v0, v1}, Lcom/twitter/ui/view/i;-><init>(Lcom/twitter/ui/widget/DefaultFab;Lcom/twitter/android/moments/ui/guide/l$a;)V

    return-object v2
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 54
    invoke-static {}, Lbrz;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android_fab_new_list_moment_collection_5378"

    .line 55
    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/twitter/ui/view/i;->a:Lcom/twitter/ui/widget/DefaultFab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/DefaultFab;->setVisibility(I)V

    .line 57
    iget-object v0, p0, Lcom/twitter/ui/view/i;->a:Lcom/twitter/ui/widget/DefaultFab;

    new-instance v1, Lcom/twitter/ui/view/i$1;

    invoke-direct {v1, p0}, Lcom/twitter/ui/view/i$1;-><init>(Lcom/twitter/ui/view/i;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/DefaultFab;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/view/i;->a:Lcom/twitter/ui/widget/DefaultFab;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/DefaultFab;->setVisibility(I)V

    goto :goto_0
.end method
