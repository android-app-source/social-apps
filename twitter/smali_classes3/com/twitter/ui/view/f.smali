.class public Lcom/twitter/ui/view/f;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/graphics/drawable/Drawable;

.field private final b:Landroid/graphics/drawable/Drawable;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/twitter/ui/widget/DefaultFab;


# direct methods
.method public constructor <init>(Lcom/twitter/ui/widget/DefaultFab;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-virtual {p1}, Lcom/twitter/ui/widget/DefaultFab;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 23
    const v1, 0x7f020817

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/ui/view/f;->a:Landroid/graphics/drawable/Drawable;

    .line 24
    const v1, 0x7f020815

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/ui/view/f;->b:Landroid/graphics/drawable/Drawable;

    .line 25
    const v1, 0x7f0a00d8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/ui/view/f;->c:Ljava/lang/String;

    .line 26
    const v1, 0x7f0a00d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/view/f;->d:Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/twitter/ui/view/f;->e:Lcom/twitter/ui/widget/DefaultFab;

    .line 28
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/ui/widget/DefaultFab;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/ui/view/f;->e:Lcom/twitter/ui/widget/DefaultFab;

    return-object v0
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 38
    packed-switch p1, :pswitch_data_0

    .line 50
    iget-object v1, p0, Lcom/twitter/ui/view/f;->a:Landroid/graphics/drawable/Drawable;

    .line 51
    iget-object v0, p0, Lcom/twitter/ui/view/f;->c:Ljava/lang/String;

    .line 55
    :goto_0
    iget-object v2, p0, Lcom/twitter/ui/view/f;->e:Lcom/twitter/ui/widget/DefaultFab;

    invoke-virtual {v2, v1}, Lcom/twitter/ui/widget/DefaultFab;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 56
    iget-object v1, p0, Lcom/twitter/ui/view/f;->e:Lcom/twitter/ui/widget/DefaultFab;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/DefaultFab;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 57
    return-void

    .line 40
    :pswitch_0
    iget-object v1, p0, Lcom/twitter/ui/view/f;->b:Landroid/graphics/drawable/Drawable;

    .line 41
    iget-object v0, p0, Lcom/twitter/ui/view/f;->d:Ljava/lang/String;

    goto :goto_0

    .line 45
    :pswitch_1
    iget-object v1, p0, Lcom/twitter/ui/view/f;->a:Landroid/graphics/drawable/Drawable;

    .line 46
    iget-object v0, p0, Lcom/twitter/ui/view/f;->c:Ljava/lang/String;

    goto :goto_0

    .line 38
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/ui/view/f;->e:Lcom/twitter/ui/widget/DefaultFab;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/DefaultFab;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    return-void
.end method
