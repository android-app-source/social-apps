.class public Lcom/twitter/ui/widget/TwitterButton;
.super Landroid/widget/Button;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final a:Landroid/util/SparseIntArray;

.field private static final g:[I


# instance fields
.field private A:I

.field private B:I

.field private final C:Landroid/graphics/Rect;

.field private D:Z

.field private E:I

.field private final F:Landroid/graphics/Rect;

.field private G:I

.field private H:I

.field private I:Landroid/graphics/Bitmap;

.field private J:Ljava/lang/String;

.field private K:I

.field private L:I

.field private final M:Landroid/graphics/Paint;

.field private N:I

.field private O:Z

.field private P:Z

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:F

.field private U:F

.field private V:I

.field private W:Landroid/graphics/Paint;

.field private aa:Landroid/graphics/Bitmap;

.field private ab:Landroid/graphics/Canvas;

.field private ac:Landroid/graphics/Bitmap;

.field private ad:Landroid/graphics/Canvas;

.field private ae:Z

.field protected final b:Landroid/text/TextPaint;

.field protected c:F

.field protected d:F

.field protected e:Ljava/lang/String;

.field protected f:Z

.field private final h:Landroid/graphics/Rect;

.field private i:I

.field private j:Z

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:F

.field private u:Z

.field private v:Z

.field private final w:Landroid/graphics/Rect;

.field private final x:Landroid/graphics/RectF;

.field private final y:Landroid/graphics/Paint;

.field private z:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/twitter/ui/widget/TwitterButton;->a:Landroid/util/SparseIntArray;

    .line 87
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/ui/widget/TwitterButton;->g:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x12
        0x14
        0x16
        0x18
        0x1a
        0x1c
        0x1e
        0x20
        0x22
        0x24
        0x28
        0x2a
        0x2c
        0x30
        0x34
        0x38
        0x3c
        0x40
        0x44
        0x48
        0x50
        0x54
        0x5a
        0x60
        0x66
        0x70
        0x78
        0x80
        0x88
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/ui/widget/TwitterButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 183
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 186
    sget v0, Lckh$b;->buttonStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/ui/widget/TwitterButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 187
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 190
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 97
    new-instance v0, Landroid/text/TextPaint;

    const/16 v1, 0x81

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    .line 103
    iput-boolean v2, p0, Lcom/twitter/ui/widget/TwitterButton;->f:Z

    .line 104
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->h:Landroid/graphics/Rect;

    .line 106
    iput-boolean v3, p0, Lcom/twitter/ui/widget/TwitterButton;->j:Z

    .line 118
    iput-boolean v2, p0, Lcom/twitter/ui/widget/TwitterButton;->v:Z

    .line 119
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->w:Landroid/graphics/Rect;

    .line 120
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->x:Landroid/graphics/RectF;

    .line 121
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->y:Landroid/graphics/Paint;

    .line 127
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->C:Landroid/graphics/Rect;

    .line 130
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->F:Landroid/graphics/Rect;

    .line 139
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->M:Landroid/graphics/Paint;

    .line 142
    iput-boolean v2, p0, Lcom/twitter/ui/widget/TwitterButton;->P:Z

    .line 144
    iput-boolean v3, p0, Lcom/twitter/ui/widget/TwitterButton;->R:Z

    .line 191
    invoke-direct {p0, p1, p2, p3, v2}, Lcom/twitter/ui/widget/TwitterButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 192
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 195
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 97
    new-instance v0, Landroid/text/TextPaint;

    const/16 v1, 0x81

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    .line 103
    iput-boolean v3, p0, Lcom/twitter/ui/widget/TwitterButton;->f:Z

    .line 104
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->h:Landroid/graphics/Rect;

    .line 106
    iput-boolean v2, p0, Lcom/twitter/ui/widget/TwitterButton;->j:Z

    .line 118
    iput-boolean v3, p0, Lcom/twitter/ui/widget/TwitterButton;->v:Z

    .line 119
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->w:Landroid/graphics/Rect;

    .line 120
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->x:Landroid/graphics/RectF;

    .line 121
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->y:Landroid/graphics/Paint;

    .line 127
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->C:Landroid/graphics/Rect;

    .line 130
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->F:Landroid/graphics/Rect;

    .line 139
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->M:Landroid/graphics/Paint;

    .line 142
    iput-boolean v3, p0, Lcom/twitter/ui/widget/TwitterButton;->P:Z

    .line 144
    iput-boolean v2, p0, Lcom/twitter/ui/widget/TwitterButton;->R:Z

    .line 196
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/ui/widget/TwitterButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 197
    return-void
.end method

.method private static a(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 165
    sget-object v1, Lcom/twitter/ui/widget/TwitterButton;->g:[I

    array-length v1, v1

    .line 167
    sget-object v2, Lcom/twitter/ui/widget/TwitterButton;->g:[I

    aget v2, v2, v0

    if-lt p0, v2, :cond_0

    sget-object v2, Lcom/twitter/ui/widget/TwitterButton;->g:[I

    add-int/lit8 v1, v1, -0x1

    aget v1, v2, v1

    if-le p0, v1, :cond_1

    .line 178
    :cond_0
    :goto_0
    return v0

    .line 171
    :cond_1
    sget-object v0, Lcom/twitter/ui/widget/TwitterButton;->g:[I

    invoke-static {v0, p0}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    .line 172
    if-gez v0, :cond_2

    .line 175
    xor-int/lit8 v0, v0, -0x1

    add-int/lit8 v0, v0, -0x1

    .line 178
    :cond_2
    sget-object v1, Lcom/twitter/ui/widget/TwitterButton;->g:[I

    aget v0, v1, v0

    goto :goto_0
.end method

.method private a(ID)I
    .locals 6

    .prologue
    const/4 v4, 0x2

    .line 430
    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 431
    invoke-static {p1, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 433
    aget v1, v0, v4

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, p2

    double-to-float v2, v2

    mul-float/2addr v1, v2

    aput v1, v0, v4

    .line 434
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 453
    if-nez p1, :cond_1

    .line 471
    :cond_0
    :goto_0
    return-object v1

    .line 456
    :cond_1
    add-int v2, p2, p3

    .line 457
    sget-object v0, Lcom/twitter/ui/widget/TwitterButton;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 458
    if-nez v0, :cond_2

    .line 459
    invoke-static {v2}, Lcom/twitter/ui/widget/TwitterButton;->a(I)I

    move-result v0

    .line 460
    if-eqz v0, :cond_0

    .line 463
    sget-object v3, Lcom/twitter/ui/widget/TwitterButton;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v2, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 465
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "h"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 466
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string/jumbo v3, "drawable"

    .line 467
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 466
    invoke-virtual {v2, v0, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 469
    if-nez v0, :cond_3

    move-object v0, v1

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_3
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 470
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static a(Landroid/graphics/Paint;I)Landroid/graphics/Paint;
    .locals 2

    .prologue
    .line 415
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, p1, v1}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 416
    return-object p0
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 445
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ac:Landroid/graphics/Bitmap;

    .line 446
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/twitter/ui/widget/TwitterButton;->ac:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ab:Landroid/graphics/Canvas;

    .line 447
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->aa:Landroid/graphics/Bitmap;

    .line 448
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/twitter/ui/widget/TwitterButton;->aa:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ad:Landroid/graphics/Canvas;

    .line 449
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 200
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    .line 202
    sget-object v0, Lckh$k;->TwitterButton:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 205
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 207
    iput v6, p0, Lcom/twitter/ui/widget/TwitterButton;->H:I

    .line 208
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->isInEditMode()Z

    move-result v5

    if-nez v5, :cond_0

    .line 209
    const-string/jumbo v5, "font_size"

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->H:I

    .line 212
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 213
    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 214
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    .line 216
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v6

    iput v6, p0, Lcom/twitter/ui/widget/TwitterButton;->r:I

    .line 217
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 218
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    int-to-float v6, v5

    invoke-virtual {v0, v6}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 219
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    iget v6, p0, Lcom/twitter/ui/widget/TwitterButton;->A:I

    invoke-virtual {v0, v6}, Landroid/text/TextPaint;->setColor(I)V

    .line 220
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_4

    .line 221
    invoke-static {p1}, Lcom/twitter/ui/widget/i;->a(Landroid/content/Context;)Lcom/twitter/ui/widget/i;

    move-result-object v0

    .line 222
    iget-object v6, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    iget-object v0, v0, Lcom/twitter/ui/widget/i;->c:Landroid/graphics/Typeface;

    invoke-virtual {v6, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 226
    :goto_0
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->H:I

    add-int/lit8 v0, v0, -0x10

    int-to-float v0, v0

    mul-float/2addr v0, v3

    .line 227
    iget-object v6, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    int-to-float v5, v5

    add-float/2addr v5, v0

    invoke-virtual {v6, v5}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 228
    iget v5, p0, Lcom/twitter/ui/widget/TwitterButton;->r:I

    int-to-float v5, v5

    add-float/2addr v0, v5

    float-to-int v0, v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->r:I

    .line 230
    sget v0, Lckh$k;->TwitterButton_iconAndLabelMargin:I

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->G:I

    .line 234
    sget v0, Lckh$k;->TwitterButton_nodpiBaseIconName:I

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->J:Ljava/lang/String;

    .line 235
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->J:Ljava/lang/String;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->Q:Z

    .line 236
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->Q:Z

    if-eqz v0, :cond_1

    .line 237
    sget v0, Lckh$k;->TwitterButton_iconSize:I

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 238
    iget-object v5, p0, Lcom/twitter/ui/widget/TwitterButton;->J:Ljava/lang/String;

    invoke-virtual {p0, v5, v0}, Lcom/twitter/ui/widget/TwitterButton;->a(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->I:Landroid/graphics/Bitmap;

    .line 239
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->I:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->Q:Z

    .line 242
    :cond_1
    invoke-direct {p0, v4, v3}, Lcom/twitter/ui/widget/TwitterButton;->a(Landroid/content/res/TypedArray;F)V

    .line 243
    invoke-virtual {p0, v2}, Lcom/twitter/ui/widget/TwitterButton;->setFocusable(Z)V

    .line 245
    invoke-virtual {p0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 246
    iput-boolean v1, p0, Lcom/twitter/ui/widget/TwitterButton;->v:Z

    .line 247
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 248
    invoke-virtual {p0, v2}, Lcom/twitter/ui/widget/TwitterButton;->setEnabled(Z)V

    .line 251
    :cond_2
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 254
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    .line 255
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/TwitterButton;->setElevation(F)V

    .line 256
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/TwitterButton;->setStateListAnimator(Landroid/animation/StateListAnimator;)V

    .line 258
    :cond_3
    return-void

    .line 224
    :cond_4
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    sget-object v6, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-static {v6, v1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_0

    :cond_5
    move v0, v2

    .line 235
    goto :goto_1

    :cond_6
    move v0, v2

    .line 239
    goto :goto_2

    .line 212
    :array_0
    .array-data 4
        0x1010095
        0x1010155
    .end array-data
.end method

.method private a(Landroid/content/res/TypedArray;F)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 295
    sget v0, Lckh$k;->TwitterButton_fillColor:I

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->k:I

    .line 296
    sget v0, Lckh$k;->TwitterButton_fillPressedColor:I

    iget v3, p0, Lcom/twitter/ui/widget/TwitterButton;->k:I

    .line 297
    invoke-direct {p0, v3}, Lcom/twitter/ui/widget/TwitterButton;->b(I)I

    move-result v3

    .line 296
    invoke-virtual {p1, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->l:I

    .line 298
    sget v0, Lckh$k;->TwitterButton_strokeColor:I

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->m:I

    .line 299
    sget v0, Lckh$k;->TwitterButton_strokePressedColor:I

    iget v3, p0, Lcom/twitter/ui/widget/TwitterButton;->m:I

    invoke-virtual {p1, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->n:I

    .line 301
    sget v0, Lckh$k;->TwitterButton_cornerRadius:I

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->p:I

    .line 303
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->y:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 304
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->y:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 306
    sget v3, Lckh$k;->TwitterButton_bounded:I

    invoke-direct {p0}, Lcom/twitter/ui/widget/TwitterButton;->e()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p1, v3, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->u:Z

    .line 307
    iput p2, p0, Lcom/twitter/ui/widget/TwitterButton;->t:F

    .line 308
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->s:I

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->s:I

    .line 311
    sget v0, Lckh$k;->TwitterButton_labelColor:I

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->A:I

    .line 312
    sget v0, Lckh$k;->TwitterButton_labelPressedColor:I

    iget v3, p0, Lcom/twitter/ui/widget/TwitterButton;->A:I

    invoke-virtual {p1, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->B:I

    .line 314
    sget v0, Lckh$k;->TwitterButton_labelMargin:I

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->E:I

    .line 317
    sget v0, Lckh$k;->TwitterButton_iconCanBeFlipped:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->O:Z

    .line 318
    sget v0, Lckh$k;->TwitterButton_iconColor:I

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->K:I

    .line 319
    sget v0, Lckh$k;->TwitterButton_iconPressedColor:I

    iget v1, p0, Lcom/twitter/ui/widget/TwitterButton;->K:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->L:I

    .line 320
    sget v0, Lckh$k;->TwitterButton_iconMargin:I

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->V:I

    .line 321
    sget v0, Lckh$k;->TwitterButton_iconLayout:I

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 322
    invoke-direct {p0, v0}, Lcom/twitter/ui/widget/TwitterButton;->setIconLayout(I)V

    .line 325
    sget v0, Lckh$k;->TwitterButton_knockout:I

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ae:Z

    .line 326
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ae:Z

    if-eqz v0, :cond_0

    .line 327
    invoke-direct {p0}, Lcom/twitter/ui/widget/TwitterButton;->g()V

    .line 330
    :cond_0
    invoke-direct {p0}, Lcom/twitter/ui/widget/TwitterButton;->d()V

    .line 331
    return-void

    :cond_1
    move v0, v2

    .line 306
    goto :goto_0
.end method

.method private b(I)I
    .locals 4

    .prologue
    .line 420
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    const/16 v1, 0xff

    if-ge v0, v1, :cond_0

    .line 421
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4d

    .line 422
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result p1

    .line 426
    :goto_0
    return p1

    .line 424
    :cond_0
    const-wide v0, -0x402ccccccccccccdL    # -0.3

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->a(ID)I

    goto :goto_0
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 541
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 542
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->z:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 544
    :cond_0
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 335
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 336
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 337
    iget v1, p0, Lcom/twitter/ui/widget/TwitterButton;->p:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 338
    iget v1, p0, Lcom/twitter/ui/widget/TwitterButton;->k:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 340
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 341
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 342
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 343
    iget v2, p0, Lcom/twitter/ui/widget/TwitterButton;->p:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 344
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 346
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    iget v3, p0, Lcom/twitter/ui/widget/TwitterButton;->l:I

    .line 347
    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-direct {v2, v3, v0, v1}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iput-object v2, p0, Lcom/twitter/ui/widget/TwitterButton;->z:Landroid/graphics/drawable/Drawable;

    .line 352
    :goto_0
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->z:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/TwitterButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 353
    return-void

    .line 349
    :cond_0
    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->z:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 410
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->k:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 438
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->W:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 439
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->W:Landroid/graphics/Paint;

    .line 440
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->W:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 442
    :cond_0
    return-void
.end method

.method private getIconHeight()I
    .locals 1

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 283
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getIconWidth()I
    .locals 1

    .prologue
    .line 277
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 278
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setIconLayout(I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 356
    packed-switch p1, :pswitch_data_0

    .line 371
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/b;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->S:Z

    .line 375
    :goto_1
    return-void

    .line 358
    :pswitch_0
    iput-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->S:Z

    goto :goto_1

    .line 362
    :pswitch_1
    iput-boolean v1, p0, Lcom/twitter/ui/widget/TwitterButton;->S:Z

    goto :goto_1

    .line 366
    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/b;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->S:Z

    goto :goto_1

    :cond_0
    move v0, v1

    .line 371
    goto :goto_0

    .line 356
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 568
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 569
    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v5, v0, v2}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 571
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move-object v0, p1

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 262
    if-eqz p1, :cond_1

    .line 263
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->H:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->H:I

    add-int/lit8 v0, v0, -0x10

    div-int/lit8 v0, v0, 0x2

    int-to-double v0, v0

    .line 264
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    :goto_0
    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->N:I

    .line 265
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->N:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/ui/widget/TwitterButton;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 268
    :goto_1
    return-object v0

    .line 264
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 268
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 772
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->e:Ljava/lang/String;

    iget v1, p0, Lcom/twitter/ui/widget/TwitterButton;->c:F

    iget v2, p0, Lcom/twitter/ui/widget/TwitterButton;->d:F

    iget-object v3, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 773
    return-void
.end method

.method a()Z
    .locals 1

    .prologue
    .line 522
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->Q:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->R:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 1

    .prologue
    .line 552
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->O:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->I:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 560
    :cond_0
    :goto_0
    return-void

    .line 555
    :cond_1
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->I:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/TwitterButton;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->I:Landroid/graphics/Bitmap;

    .line 556
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->P:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->P:Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method c()Z
    .locals 1

    .prologue
    .line 563
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->P:Z

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 738
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->f()V

    .line 739
    invoke-direct {p0}, Lcom/twitter/ui/widget/TwitterButton;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->u:Z

    if-eqz v0, :cond_2

    .line 740
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ae:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ab:Landroid/graphics/Canvas;

    if-nez v0, :cond_1

    .line 741
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->a(II)V

    .line 743
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ae:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ab:Landroid/graphics/Canvas;

    .line 744
    :goto_0
    iget-object v1, p0, Lcom/twitter/ui/widget/TwitterButton;->z:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/twitter/ui/widget/TwitterButton;->w:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 745
    iget-object v1, p0, Lcom/twitter/ui/widget/TwitterButton;->z:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 746
    iget-boolean v1, p0, Lcom/twitter/ui/widget/TwitterButton;->u:Z

    if-eqz v1, :cond_2

    .line 749
    iget-object v1, p0, Lcom/twitter/ui/widget/TwitterButton;->y:Landroid/graphics/Paint;

    iget v2, p0, Lcom/twitter/ui/widget/TwitterButton;->o:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 750
    iget-object v1, p0, Lcom/twitter/ui/widget/TwitterButton;->x:Landroid/graphics/RectF;

    iget v2, p0, Lcom/twitter/ui/widget/TwitterButton;->p:I

    int-to-float v2, v2

    iget v3, p0, Lcom/twitter/ui/widget/TwitterButton;->p:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/twitter/ui/widget/TwitterButton;->y:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 754
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ae:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ad:Landroid/graphics/Canvas;

    .line 755
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 756
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v1

    iget v2, p0, Lcom/twitter/ui/widget/TwitterButton;->T:F

    iget v3, p0, Lcom/twitter/ui/widget/TwitterButton;->U:F

    iget-object v4, p0, Lcom/twitter/ui/widget/TwitterButton;->M:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 758
    :cond_3
    iget-boolean v1, p0, Lcom/twitter/ui/widget/TwitterButton;->ae:Z

    if-eqz v1, :cond_4

    .line 759
    iget-object v1, p0, Lcom/twitter/ui/widget/TwitterButton;->ad:Landroid/graphics/Canvas;

    const/high16 v2, -0x1000000

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 761
    :cond_4
    iget-boolean v1, p0, Lcom/twitter/ui/widget/TwitterButton;->D:Z

    if-eqz v1, :cond_5

    .line 762
    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/TwitterButton;->a(Landroid/graphics/Canvas;)V

    .line 765
    :cond_5
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ae:Z

    if-eqz v0, :cond_6

    .line 766
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ab:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/twitter/ui/widget/TwitterButton;->aa:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/twitter/ui/widget/TwitterButton;->W:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v5, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 767
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ac:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v5, v5, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 769
    :cond_6
    return-void

    :cond_7
    move-object v0, p1

    .line 743
    goto :goto_0

    :cond_8
    move-object v0, p1

    .line 754
    goto :goto_1
.end method

.method protected f()V
    .locals 2

    .prologue
    .line 526
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->f:Z

    if-eqz v0, :cond_0

    .line 527
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->n:I

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->o:I

    .line 528
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->l:I

    invoke-direct {p0, v0}, Lcom/twitter/ui/widget/TwitterButton;->c(I)V

    .line 529
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    iget v1, p0, Lcom/twitter/ui/widget/TwitterButton;->B:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 530
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->M:Landroid/graphics/Paint;

    iget v1, p0, Lcom/twitter/ui/widget/TwitterButton;->L:I

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->a(Landroid/graphics/Paint;I)Landroid/graphics/Paint;

    .line 537
    :goto_0
    return-void

    .line 532
    :cond_0
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->m:I

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->o:I

    .line 533
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->k:I

    invoke-direct {p0, v0}, Lcom/twitter/ui/widget/TwitterButton;->c(I)V

    .line 534
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    iget v1, p0, Lcom/twitter/ui/widget/TwitterButton;->A:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 535
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->M:Landroid/graphics/Paint;

    iget v1, p0, Lcom/twitter/ui/widget/TwitterButton;->K:I

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->a(Landroid/graphics/Paint;I)Landroid/graphics/Paint;

    goto :goto_0
.end method

.method getIcon()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->I:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 666
    iget v1, p0, Lcom/twitter/ui/widget/TwitterButton;->q:I

    .line 667
    iget v2, p0, Lcom/twitter/ui/widget/TwitterButton;->r:I

    .line 668
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->S:Z

    .line 670
    iget-boolean v3, p0, Lcom/twitter/ui/widget/TwitterButton;->u:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/twitter/ui/widget/TwitterButton;->f:Z

    if-nez v3, :cond_0

    .line 674
    iget-object v3, p0, Lcom/twitter/ui/widget/TwitterButton;->x:Landroid/graphics/RectF;

    iget v4, p0, Lcom/twitter/ui/widget/TwitterButton;->t:F

    div-float/2addr v4, v7

    iget v5, p0, Lcom/twitter/ui/widget/TwitterButton;->t:F

    div-float/2addr v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->inset(FF)V

    .line 675
    iget-object v3, p0, Lcom/twitter/ui/widget/TwitterButton;->x:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/twitter/ui/widget/TwitterButton;->w:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 678
    :cond_0
    iget-boolean v3, p0, Lcom/twitter/ui/widget/TwitterButton;->D:Z

    if-nez v3, :cond_3

    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 679
    invoke-direct {p0}, Lcom/twitter/ui/widget/TwitterButton;->getIconWidth()I

    move-result v3

    sub-int v3, v1, v3

    int-to-float v3, v3

    div-float/2addr v3, v7

    iput v3, p0, Lcom/twitter/ui/widget/TwitterButton;->T:F

    .line 692
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->a()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 693
    if-eqz v0, :cond_5

    .line 694
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 695
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->V:I

    invoke-direct {p0}, Lcom/twitter/ui/widget/TwitterButton;->getIconWidth()I

    move-result v3

    add-int/2addr v0, v3

    iget v3, p0, Lcom/twitter/ui/widget/TwitterButton;->G:I

    add-int/2addr v0, v3

    iget-object v3, p0, Lcom/twitter/ui/widget/TwitterButton;->C:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v3

    int-to-float v0, v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->c:F

    .line 712
    :goto_1
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    const-string/jumbo v3, "X"

    const/4 v4, 0x0

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/twitter/ui/widget/TwitterButton;->F:Landroid/graphics/Rect;

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 713
    int-to-float v0, v2

    div-float/2addr v0, v7

    iget-object v3, p0, Lcom/twitter/ui/widget/TwitterButton;->F:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v0, v3

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->d:F

    .line 714
    int-to-float v0, v2

    div-float/2addr v0, v7

    invoke-direct {p0}, Lcom/twitter/ui/widget/TwitterButton;->getIconHeight()I

    move-result v2

    iget v3, p0, Lcom/twitter/ui/widget/TwitterButton;->N:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v2, v7

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->U:F

    .line 722
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->j:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->D:Z

    if-eqz v0, :cond_2

    .line 723
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->i:I

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    .line 724
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/b;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 725
    iget v1, p0, Lcom/twitter/ui/widget/TwitterButton;->c:F

    int-to-float v2, v0

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/twitter/ui/widget/TwitterButton;->c:F

    .line 726
    iget v1, p0, Lcom/twitter/ui/widget/TwitterButton;->T:F

    int-to-float v0, v0

    sub-float v0, v1, v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->T:F

    .line 732
    :cond_2
    :goto_2
    return-void

    .line 680
    :cond_3
    if-eqz v0, :cond_4

    .line 681
    iget v3, p0, Lcom/twitter/ui/widget/TwitterButton;->V:I

    int-to-float v3, v3

    iput v3, p0, Lcom/twitter/ui/widget/TwitterButton;->T:F

    .line 682
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 683
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->b()V

    goto/16 :goto_0

    .line 686
    :cond_4
    iget v3, p0, Lcom/twitter/ui/widget/TwitterButton;->V:I

    sub-int v3, v1, v3

    invoke-direct {p0}, Lcom/twitter/ui/widget/TwitterButton;->getIconWidth()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iput v3, p0, Lcom/twitter/ui/widget/TwitterButton;->T:F

    .line 687
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->c()Z

    move-result v3

    if-nez v3, :cond_1

    .line 688
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->b()V

    goto/16 :goto_0

    .line 697
    :cond_5
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    sget-object v3, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 698
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->V:I

    sub-int v0, v1, v0

    invoke-direct {p0}, Lcom/twitter/ui/widget/TwitterButton;->getIconWidth()I

    move-result v3

    sub-int/2addr v0, v3

    iget v3, p0, Lcom/twitter/ui/widget/TwitterButton;->G:I

    sub-int/2addr v0, v3

    int-to-float v0, v0

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->c:F

    goto/16 :goto_1

    .line 701
    :cond_6
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->e:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/twitter/ui/widget/TwitterButton;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    .line 706
    :goto_3
    int-to-float v3, v1

    div-float/2addr v3, v7

    iget-object v4, p0, Lcom/twitter/ui/widget/TwitterButton;->C:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/twitter/ui/widget/TwitterButton;->C:Landroid/graphics/Rect;

    .line 707
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v0, v4

    div-float/2addr v0, v7

    add-float/2addr v0, v3

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->c:F

    goto/16 :goto_1

    .line 701
    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    .line 728
    :cond_8
    iget v1, p0, Lcom/twitter/ui/widget/TwitterButton;->c:F

    int-to-float v2, v0

    add-float/2addr v1, v2

    iput v1, p0, Lcom/twitter/ui/widget/TwitterButton;->c:F

    .line 729
    iget v1, p0, Lcom/twitter/ui/widget/TwitterButton;->T:F

    int-to-float v0, v0

    add-float/2addr v0, v1

    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->T:F

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const/high16 v11, -0x80000000

    const/4 v10, 0x0

    const/4 v1, 0x0

    .line 586
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 589
    iget v3, p0, Lcom/twitter/ui/widget/TwitterButton;->r:I

    .line 591
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    .line 592
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 593
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v7

    .line 594
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 596
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->D:Z

    .line 597
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    iget-object v9, p0, Lcom/twitter/ui/widget/TwitterButton;->C:Landroid/graphics/Rect;

    invoke-virtual {v0, v5, v1, v8, v9}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 598
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->C:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 603
    iget-boolean v8, p0, Lcom/twitter/ui/widget/TwitterButton;->D:Z

    if-eqz v8, :cond_1

    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->a()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 604
    iget v8, p0, Lcom/twitter/ui/widget/TwitterButton;->V:I

    invoke-direct {p0}, Lcom/twitter/ui/widget/TwitterButton;->getIconWidth()I

    move-result v9

    add-int/2addr v8, v9

    iget v9, p0, Lcom/twitter/ui/widget/TwitterButton;->G:I

    add-int/2addr v8, v9

    add-int/2addr v0, v8

    iget v8, p0, Lcom/twitter/ui/widget/TwitterButton;->E:I

    add-int/2addr v0, v8

    .line 613
    :goto_1
    iput v0, p0, Lcom/twitter/ui/widget/TwitterButton;->i:I

    .line 616
    if-ne v6, v12, :cond_3

    .line 618
    iput-boolean v1, p0, Lcom/twitter/ui/widget/TwitterButton;->j:Z

    .line 626
    :goto_2
    if-ne v7, v12, :cond_5

    .line 635
    :goto_3
    iget-boolean v3, p0, Lcom/twitter/ui/widget/TwitterButton;->D:Z

    if-eqz v3, :cond_8

    if-ge v4, v0, :cond_8

    .line 637
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 638
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->V:I

    invoke-direct {p0}, Lcom/twitter/ui/widget/TwitterButton;->getIconWidth()I

    move-result v3

    add-int/2addr v0, v3

    iget v3, p0, Lcom/twitter/ui/widget/TwitterButton;->G:I

    add-int/2addr v0, v3

    iget v3, p0, Lcom/twitter/ui/widget/TwitterButton;->E:I

    add-int/2addr v0, v3

    sub-int v0, v4, v0

    .line 642
    :goto_4
    iget-object v3, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getEllipsize()Landroid/text/TextUtils$TruncateAt;

    move-result-object v6

    invoke-static {v5, v3, v0, v6}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->e:Ljava/lang/String;

    .line 643
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/twitter/ui/widget/TwitterButton;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/ui/widget/TwitterButton;->e:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    iget-object v6, p0, Lcom/twitter/ui/widget/TwitterButton;->C:Landroid/graphics/Rect;

    invoke-virtual {v0, v3, v1, v5, v6}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 648
    :goto_5
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->x:Landroid/graphics/RectF;

    int-to-float v1, v4

    int-to-float v3, v2

    invoke-virtual {v0, v10, v10, v1, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 649
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->x:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/twitter/ui/widget/TwitterButton;->w:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 650
    invoke-virtual {p0, v4, v2}, Lcom/twitter/ui/widget/TwitterButton;->setMeasuredDimension(II)V

    .line 651
    iput v4, p0, Lcom/twitter/ui/widget/TwitterButton;->q:I

    .line 652
    return-void

    :cond_0
    move v0, v1

    .line 596
    goto/16 :goto_0

    .line 606
    :cond_1
    iget-boolean v8, p0, Lcom/twitter/ui/widget/TwitterButton;->D:Z

    if-eqz v8, :cond_2

    .line 607
    iget-object v8, p0, Lcom/twitter/ui/widget/TwitterButton;->b:Landroid/text/TextPaint;

    sget-object v9, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v8, v9}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 608
    iget v8, p0, Lcom/twitter/ui/widget/TwitterButton;->E:I

    add-int/2addr v0, v8

    iget v8, p0, Lcom/twitter/ui/widget/TwitterButton;->E:I

    add-int/2addr v0, v8

    goto :goto_1

    .line 609
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 610
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->V:I

    invoke-direct {p0}, Lcom/twitter/ui/widget/TwitterButton;->getIconWidth()I

    move-result v8

    add-int/2addr v0, v8

    iget v8, p0, Lcom/twitter/ui/widget/TwitterButton;->V:I

    add-int/2addr v0, v8

    goto :goto_1

    .line 619
    :cond_3
    if-ne v6, v11, :cond_4

    .line 620
    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    goto :goto_2

    :cond_4
    move v4, v0

    .line 622
    goto :goto_2

    .line 628
    :cond_5
    if-ne v7, v11, :cond_6

    .line 629
    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto/16 :goto_3

    :cond_6
    move v2, v3

    .line 631
    goto/16 :goto_3

    .line 640
    :cond_7
    iget v0, p0, Lcom/twitter/ui/widget/TwitterButton;->E:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v4, v0

    goto :goto_4

    .line 645
    :cond_8
    iput-object v5, p0, Lcom/twitter/ui/widget/TwitterButton;->e:Ljava/lang/String;

    goto :goto_5

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 656
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/Button;->onSizeChanged(IIII)V

    .line 659
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ae:Z

    if-eqz v0, :cond_1

    if-lez p1, :cond_1

    if-lez p2, :cond_1

    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    .line 660
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/twitter/ui/widget/TwitterButton;->a(II)V

    .line 662
    :cond_1
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 477
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 504
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->f()V

    .line 505
    return v2

    .line 479
    :pswitch_0
    iput-boolean v1, p0, Lcom/twitter/ui/widget/TwitterButton;->f:Z

    .line 480
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->invalidate()V

    goto :goto_0

    .line 485
    :pswitch_1
    iput-boolean v2, p0, Lcom/twitter/ui/widget/TwitterButton;->f:Z

    .line 486
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->invalidate()V

    goto :goto_0

    .line 490
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->h:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 491
    iget-object v0, p0, Lcom/twitter/ui/widget/TwitterButton;->h:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    add-int/2addr v3, v4

    .line 492
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    add-int/2addr v4, v5

    .line 491
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 493
    :goto_1
    iget-boolean v3, p0, Lcom/twitter/ui/widget/TwitterButton;->f:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    xor-int/2addr v0, v3

    .line 494
    if-eqz v0, :cond_0

    .line 495
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->f:Z

    if-nez v0, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/twitter/ui/widget/TwitterButton;->f:Z

    .line 496
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->invalidate()V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 491
    goto :goto_1

    :cond_2
    move v0, v2

    .line 493
    goto :goto_2

    :cond_3
    move v1, v2

    .line 495
    goto :goto_3

    .line 477
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setButtonAppearance(I)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 399
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 400
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 401
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lckh$k;->TwitterButton:[I

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 403
    invoke-direct {p0, v1, v0}, Lcom/twitter/ui/widget/TwitterButton;->a(Landroid/content/res/TypedArray;F)V

    .line 405
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->invalidate()V

    .line 406
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 407
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 379
    invoke-super {p0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 380
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->v:Z

    if-eqz v0, :cond_0

    .line 381
    if-eqz p1, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/TwitterButton;->setAlpha(F)V

    .line 382
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->invalidate()V

    .line 384
    :cond_0
    return-void

    .line 381
    :cond_1
    const v0, 0x3f19999a    # 0.6f

    goto :goto_0
.end method

.method public setKnockout(Z)V
    .locals 1

    .prologue
    .line 387
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->ae:Z

    if-eq v0, p1, :cond_0

    .line 388
    invoke-direct {p0}, Lcom/twitter/ui/widget/TwitterButton;->g()V

    .line 389
    iput-boolean p1, p0, Lcom/twitter/ui/widget/TwitterButton;->ae:Z

    .line 390
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->invalidate()V

    .line 392
    :cond_0
    return-void
.end method

.method public setShowIcon(Z)V
    .locals 1

    .prologue
    .line 515
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->Q:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/ui/widget/TwitterButton;->R:Z

    if-eq v0, p1, :cond_0

    .line 516
    iput-boolean p1, p0, Lcom/twitter/ui/widget/TwitterButton;->R:Z

    .line 517
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->invalidate()V

    .line 519
    :cond_0
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 0

    .prologue
    .line 579
    invoke-super {p0, p1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 580
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->requestLayout()V

    .line 581
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TwitterButton;->invalidate()V

    .line 582
    return-void
.end method
