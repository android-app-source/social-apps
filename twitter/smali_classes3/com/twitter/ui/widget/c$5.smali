.class Lcom/twitter/ui/widget/c$5;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/ui/widget/c;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/ui/widget/c$b;

.field final synthetic b:Lcom/twitter/ui/widget/c;


# direct methods
.method constructor <init>(Lcom/twitter/ui/widget/c;Lcom/twitter/ui/widget/c$b;)V
    .locals 0

    .prologue
    .line 398
    iput-object p1, p0, Lcom/twitter/ui/widget/c$5;->b:Lcom/twitter/ui/widget/c;

    iput-object p2, p0, Lcom/twitter/ui/widget/c$5;->a:Lcom/twitter/ui/widget/c$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 407
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 3

    .prologue
    .line 411
    iget-object v0, p0, Lcom/twitter/ui/widget/c$5;->a:Lcom/twitter/ui/widget/c$b;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/c$b;->j()V

    .line 412
    iget-object v0, p0, Lcom/twitter/ui/widget/c$5;->a:Lcom/twitter/ui/widget/c$b;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/c$b;->a()V

    .line 413
    iget-object v0, p0, Lcom/twitter/ui/widget/c$5;->a:Lcom/twitter/ui/widget/c$b;

    iget-object v1, p0, Lcom/twitter/ui/widget/c$5;->a:Lcom/twitter/ui/widget/c$b;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/c$b;->g()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/c$b;->b(F)V

    .line 414
    iget-object v0, p0, Lcom/twitter/ui/widget/c$5;->b:Lcom/twitter/ui/widget/c;

    iget-object v1, p0, Lcom/twitter/ui/widget/c$5;->b:Lcom/twitter/ui/widget/c;

    invoke-static {v1}, Lcom/twitter/ui/widget/c;->d(Lcom/twitter/ui/widget/c;)F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    const/high16 v2, 0x40a00000    # 5.0f

    rem-float/2addr v1, v2

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/c;->a(Lcom/twitter/ui/widget/c;F)F

    .line 415
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, Lcom/twitter/ui/widget/c$5;->b:Lcom/twitter/ui/widget/c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/c;->a(Lcom/twitter/ui/widget/c;F)F

    .line 402
    return-void
.end method
