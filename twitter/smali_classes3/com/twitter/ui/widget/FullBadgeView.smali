.class public Lcom/twitter/ui/widget/FullBadgeView;
.super Lcom/twitter/ui/widget/TypefacesTextView;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/widget/b;


# instance fields
.field private a:Ljava/lang/String;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/ui/widget/FullBadgeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/ui/widget/FullBadgeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/ui/widget/TypefacesTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 16
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/ui/widget/FullBadgeView;->a:Ljava/lang/String;

    .line 19
    const/16 v0, 0x9

    iput v0, p0, Lcom/twitter/ui/widget/FullBadgeView;->b:I

    .line 31
    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 55
    if-gtz p1, :cond_0

    .line 56
    const-string/jumbo v0, ""

    .line 60
    :goto_0
    return-object v0

    .line 58
    :cond_0
    int-to-long v0, p1

    iget v2, p0, Lcom/twitter/ui/widget/FullBadgeView;->b:I

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/util/r;->a(JJ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public setBadgeMode(I)V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public setBadgeNumber(I)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/twitter/ui/widget/FullBadgeView;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/twitter/ui/widget/FullBadgeView;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 41
    iput-object v0, p0, Lcom/twitter/ui/widget/FullBadgeView;->a:Ljava/lang/String;

    .line 42
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 43
    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/FullBadgeView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/FullBadgeView;->setVisibility(I)V

    .line 48
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/ui/widget/FullBadgeView;->invalidate()V

    .line 50
    :cond_0
    return-void

    .line 46
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/FullBadgeView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setMaxBadgeCount(I)V
    .locals 0

    .prologue
    .line 34
    iput p1, p0, Lcom/twitter/ui/widget/FullBadgeView;->b:I

    .line 35
    return-void
.end method
