.class abstract Lcom/twitter/ui/widget/CroppableImageView$a;
.super Landroid/animation/AnimatorListenerAdapter;
.source "Twttr"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/ui/widget/CroppableImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/ui/widget/CroppableImageView;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/animation/ValueAnimator;


# direct methods
.method constructor <init>(Lcom/twitter/ui/widget/CroppableImageView;)V
    .locals 1

    .prologue
    .line 444
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 445
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView$a;->a:Ljava/lang/ref/WeakReference;

    .line 446
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView$a;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 479
    return-void
.end method

.method protected a(Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 471
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 472
    invoke-virtual {p1, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 473
    invoke-virtual {p1, p0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 474
    iput-object p1, p0, Lcom/twitter/ui/widget/CroppableImageView$a;->b:Landroid/animation/ValueAnimator;

    .line 475
    return-void
.end method

.method protected b()Lcom/twitter/ui/widget/CroppableImageView;
    .locals 2

    .prologue
    .line 483
    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/CroppableImageView;

    .line 484
    if-nez v0, :cond_0

    .line 485
    iget-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView$a;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 487
    :cond_0
    return-object v0
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 461
    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/CroppableImageView;

    .line 462
    if-nez v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView$a;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 468
    :goto_0
    return-void

    .line 467
    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/CroppableImageView;->a(Lcom/twitter/ui/widget/CroppableImageView;Z)V

    goto :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/CroppableImageView;

    .line 451
    if-nez v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView$a;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 457
    :goto_0
    return-void

    .line 456
    :cond_0
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/CroppableImageView;->a(Lcom/twitter/ui/widget/CroppableImageView;Z)V

    goto :goto_0
.end method
