.class public Lcom/twitter/ui/widget/i;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static g:Lcom/twitter/ui/widget/i;

.field private static volatile h:Z


# instance fields
.field public final a:Landroid/graphics/Typeface;

.field public final b:Landroid/graphics/Typeface;

.field public final c:Landroid/graphics/Typeface;

.field public final d:Landroid/graphics/Typeface;

.field public final e:Landroid/graphics/Typeface;

.field public final f:Z

.field private final i:Landroid/content/Context;

.field private final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/i;->i:Landroid/content/Context;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/i;->j:Ljava/util/Map;

    .line 52
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/i;->k:Landroid/util/SparseArray;

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/ui/widget/i;->a(Landroid/content/res/Resources;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/ui/widget/i;->f:Z

    .line 54
    const-string/jumbo v0, "fonts/light.ttf"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/ui/widget/i;->a(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    .line 55
    const-string/jumbo v0, "fonts/lightItalic.ttf"

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/twitter/ui/widget/i;->a(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/i;->b:Landroid/graphics/Typeface;

    .line 56
    const-string/jumbo v0, "fonts/bold.ttf"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/ui/widget/i;->a(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/i;->c:Landroid/graphics/Typeface;

    .line 57
    const-string/jumbo v0, "fonts/boldItalic.ttf"

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/twitter/ui/widget/i;->a(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/i;->d:Landroid/graphics/Typeface;

    .line 58
    iget-boolean v0, p0, Lcom/twitter/ui/widget/i;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    :goto_0
    iput-object v0, p0, Lcom/twitter/ui/widget/i;->e:Landroid/graphics/Typeface;

    .line 59
    return-void

    .line 58
    :cond_0
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/ui/widget/i;
    .locals 2

    .prologue
    .line 83
    const-class v1, Lcom/twitter/ui/widget/i;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/ui/widget/i;->g:Lcom/twitter/ui/widget/i;

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Lcom/twitter/ui/widget/i;

    invoke-direct {v0, p0}, Lcom/twitter/ui/widget/i;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/ui/widget/i;->g:Lcom/twitter/ui/widget/i;

    .line 85
    const-class v0, Lcom/twitter/ui/widget/i;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 87
    :cond_0
    sget-object v0, Lcom/twitter/ui/widget/i;->g:Lcom/twitter/ui/widget/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Z)V
    .locals 1

    .prologue
    .line 91
    sput-boolean p0, Lcom/twitter/ui/widget/i;->h:Z

    .line 92
    const-class v0, Lcom/twitter/ui/widget/i;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 93
    return-void
.end method

.method private static a(Landroid/content/res/Resources;)Z
    .locals 2

    .prologue
    .line 214
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v1, 0x140

    if-ge v0, v1, :cond_0

    .line 215
    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 214
    :goto_0
    return v0

    .line 215
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)Landroid/graphics/Typeface;
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/twitter/ui/widget/i;->k:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    .line 164
    if-nez v0, :cond_0

    .line 165
    packed-switch p1, :pswitch_data_0

    .line 183
    :pswitch_0
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-static {v0, p1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 187
    :goto_0
    iget-object v1, p0, Lcom/twitter/ui/widget/i;->k:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 189
    :cond_0
    return-object v0

    .line 171
    :pswitch_1
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_0

    .line 175
    :pswitch_2
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    goto :goto_0

    .line 179
    :pswitch_3
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-static {v0, p1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0

    .line 165
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public a(I)Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 70
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/twitter/ui/widget/i;->d:Landroid/graphics/Typeface;

    .line 77
    :goto_0
    return-object v0

    .line 72
    :cond_0
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/twitter/ui/widget/i;->c:Landroid/graphics/Typeface;

    goto :goto_0

    .line 74
    :cond_1
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_2

    .line 75
    iget-object v0, p0, Lcom/twitter/ui/widget/i;->b:Landroid/graphics/Typeface;

    goto :goto_0

    .line 77
    :cond_2
    iget-object v0, p0, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method public a(Landroid/graphics/Typeface;Z)Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/twitter/ui/widget/i;->f:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/twitter/ui/widget/i;->h:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 205
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    .line 206
    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/i;->b(I)Landroid/graphics/Typeface;

    move-result-object p1

    .line 210
    :cond_0
    return-object p1

    .line 205
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I)Landroid/graphics/Typeface;
    .locals 2

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/twitter/ui/widget/i;->f:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/twitter/ui/widget/i;->h:Z

    if-nez v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/twitter/ui/widget/i;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/twitter/ui/widget/i;->j:Ljava/util/Map;

    iget-object v1, p0, Lcom/twitter/ui/widget/i;->i:Landroid/content/Context;

    .line 152
    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 151
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/widget/i;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    .line 158
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p2}, Lcom/twitter/ui/widget/i;->c(I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0
.end method

.method public b(I)Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 107
    packed-switch p1, :pswitch_data_0

    .line 118
    iget-object v0, p0, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    :goto_0
    return-object v0

    .line 109
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/ui/widget/i;->d:Landroid/graphics/Typeface;

    goto :goto_0

    .line 112
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/ui/widget/i;->c:Landroid/graphics/Typeface;

    goto :goto_0

    .line 115
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/ui/widget/i;->b:Landroid/graphics/Typeface;

    goto :goto_0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
