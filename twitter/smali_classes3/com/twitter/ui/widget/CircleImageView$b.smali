.class Lcom/twitter/ui/widget/CircleImageView$b;
.super Landroid/graphics/drawable/shapes/OvalShape;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/ui/widget/CircleImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/ui/widget/CircleImageView;

.field private final b:Landroid/graphics/RadialGradient;

.field private final c:I

.field private final d:Landroid/graphics/Paint;

.field private final e:I


# direct methods
.method constructor <init>(Lcom/twitter/ui/widget/CircleImageView;II)V
    .locals 7

    .prologue
    .line 256
    iput-object p1, p0, Lcom/twitter/ui/widget/CircleImageView$b;->a:Lcom/twitter/ui/widget/CircleImageView;

    invoke-direct {p0}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    .line 257
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/CircleImageView$b;->d:Landroid/graphics/Paint;

    .line 258
    iput p2, p0, Lcom/twitter/ui/widget/CircleImageView$b;->c:I

    .line 259
    iput p3, p0, Lcom/twitter/ui/widget/CircleImageView$b;->e:I

    .line 260
    new-instance v0, Landroid/graphics/RadialGradient;

    iget v1, p0, Lcom/twitter/ui/widget/CircleImageView$b;->e:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/twitter/ui/widget/CircleImageView$b;->e:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, Lcom/twitter/ui/widget/CircleImageView$b;->c:I

    int-to-float v3, v3

    const/4 v4, 0x2

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    const/4 v5, 0x0

    sget-object v6, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v6}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/twitter/ui/widget/CircleImageView$b;->b:Landroid/graphics/RadialGradient;

    .line 264
    iget-object v0, p0, Lcom/twitter/ui/widget/CircleImageView$b;->d:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/twitter/ui/widget/CircleImageView$b;->b:Landroid/graphics/RadialGradient;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 265
    return-void

    .line 260
    nop

    :array_0
    .array-data 4
        0x3d000000    # 0.03125f
        0x0
    .end array-data
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 6

    .prologue
    .line 269
    iget-object v0, p0, Lcom/twitter/ui/widget/CircleImageView$b;->a:Lcom/twitter/ui/widget/CircleImageView;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/CircleImageView;->getWidth()I

    move-result v0

    .line 270
    iget-object v1, p0, Lcom/twitter/ui/widget/CircleImageView$b;->a:Lcom/twitter/ui/widget/CircleImageView;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/CircleImageView;->getHeight()I

    move-result v1

    .line 271
    div-int/lit8 v2, v0, 0x2

    int-to-float v2, v2

    div-int/lit8 v3, v1, 0x2

    int-to-float v3, v3

    iget v4, p0, Lcom/twitter/ui/widget/CircleImageView$b;->e:I

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/twitter/ui/widget/CircleImageView$b;->c:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    iget-object v5, p0, Lcom/twitter/ui/widget/CircleImageView$b;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 273
    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/twitter/ui/widget/CircleImageView$b;->e:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, p2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 274
    return-void
.end method
