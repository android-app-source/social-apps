.class public Lcom/twitter/ui/widget/Tooltip;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BlacklistedBaseClass"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/ui/widget/Tooltip$d;,
        Lcom/twitter/ui/widget/Tooltip$c;,
        Lcom/twitter/ui/widget/Tooltip$b;,
        Lcom/twitter/ui/widget/Tooltip$a;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/ui/widget/Tooltip$a;


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Landroid/view/ViewGroup;

.field private d:Landroid/view/View;

.field private e:Lcom/twitter/ui/widget/Tooltip$d;

.field private final f:[I

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Lcom/twitter/ui/widget/Tooltip$b;

.field private l:Lcom/twitter/ui/widget/Tooltip$c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 102
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->f:[I

    return-void
.end method

.method private a(ILjava/lang/String;)Landroid/view/View;
    .locals 1

    .prologue
    .line 543
    if-eqz p1, :cond_0

    .line 544
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->b:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 546
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/ui/widget/Tooltip;ILjava/lang/String;)Landroid/view/View;
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/twitter/ui/widget/Tooltip;->a(ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;I)Lcom/twitter/ui/widget/Tooltip$a;
    .locals 2

    .prologue
    .line 121
    sget-object v0, Lcom/twitter/ui/widget/Tooltip;->a:Lcom/twitter/ui/widget/Tooltip$a;

    if-eqz v0, :cond_0

    .line 122
    sget-object v0, Lcom/twitter/ui/widget/Tooltip;->a:Lcom/twitter/ui/widget/Tooltip$a;

    .line 124
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/ui/widget/Tooltip$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/ui/widget/Tooltip$a;-><init>(Landroid/content/Context;ILcom/twitter/ui/widget/Tooltip$1;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip$a;
    .locals 2

    .prologue
    .line 129
    sget-object v0, Lcom/twitter/ui/widget/Tooltip;->a:Lcom/twitter/ui/widget/Tooltip$a;

    if-eqz v0, :cond_0

    .line 130
    sget-object v0, Lcom/twitter/ui/widget/Tooltip;->a:Lcom/twitter/ui/widget/Tooltip$a;

    .line 132
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/ui/widget/Tooltip$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/ui/widget/Tooltip$a;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/ui/widget/Tooltip$1;)V

    goto :goto_0
.end method

.method static synthetic a(ILjava/lang/String;ILjava/lang/CharSequence;IILcom/twitter/ui/widget/Tooltip$c;ZZLjava/lang/String;)Lcom/twitter/ui/widget/Tooltip;
    .locals 1

    .prologue
    .line 76
    invoke-static/range {p0 .. p9}, Lcom/twitter/ui/widget/Tooltip;->b(ILjava/lang/String;ILjava/lang/CharSequence;IILcom/twitter/ui/widget/Tooltip$c;ZZLjava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 421
    iget-boolean v0, p0, Lcom/twitter/ui/widget/Tooltip;->h:Z

    if-nez v0, :cond_2

    .line 422
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/Tooltip$d;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 427
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 428
    if-eqz v0, :cond_1

    .line 433
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 434
    invoke-virtual {v0, p0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 435
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 438
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/ui/widget/Tooltip;->h:Z

    .line 440
    :cond_2
    return-void
.end method

.method private a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, -0x2

    .line 551
    const-string/jumbo v0, "text"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 552
    new-instance v0, Lcom/twitter/ui/widget/Tooltip$d;

    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip;->b:Landroid/app/Activity;

    iget-object v3, p0, Lcom/twitter/ui/widget/Tooltip;->c:Landroid/view/ViewGroup;

    const-string/jumbo v2, "arrowDirection"

    .line 553
    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string/jumbo v2, "styleId"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    const/4 v7, 0x0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/twitter/ui/widget/Tooltip$d;-><init>(Landroid/app/Activity;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/CharSequence;IILcom/twitter/ui/widget/Tooltip$1;)V

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    .line 554
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/Tooltip$d;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 556
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v8, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 559
    iget-boolean v0, p0, Lcom/twitter/ui/widget/Tooltip;->i:Z

    if-eqz v0, :cond_0

    .line 560
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    invoke-static {v0, v9}, Lcom/twitter/ui/widget/Tooltip$d;->a(Lcom/twitter/ui/widget/Tooltip$d;Z)V

    .line 565
    :goto_0
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->d:Landroid/view/View;

    .line 566
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->d:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setBackgroundColor(I)V

    .line 567
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 568
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip;->b()V

    .line 569
    return-void

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    const-string/jumbo v1, "animate"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/Tooltip$d;->a(Lcom/twitter/ui/widget/Tooltip$d;Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/ui/widget/Tooltip;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip;->a()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/ui/widget/Tooltip;Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/view/View;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/ui/widget/Tooltip;)Lcom/twitter/ui/widget/Tooltip$c;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->l:Lcom/twitter/ui/widget/Tooltip$c;

    return-object v0
.end method

.method private static b(ILjava/lang/String;ILjava/lang/CharSequence;IILcom/twitter/ui/widget/Tooltip$c;ZZLjava/lang/String;)Lcom/twitter/ui/widget/Tooltip;
    .locals 3

    .prologue
    .line 336
    new-instance v0, Lcom/twitter/ui/widget/Tooltip;

    invoke-direct {v0}, Lcom/twitter/ui/widget/Tooltip;-><init>()V

    .line 338
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 339
    const-string/jumbo v2, "targetViewId"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 340
    const-string/jumbo v2, "targetViewTag"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    const-string/jumbo v2, "containerId"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 342
    const-string/jumbo v2, "text"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 343
    const-string/jumbo v2, "styleId"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 344
    const-string/jumbo v2, "arrowDirection"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 345
    const-string/jumbo v2, "dismissOnPause"

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 346
    const-string/jumbo v2, "animate"

    invoke-virtual {v1, v2, p8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 347
    const-string/jumbo v2, "fragmentTag"

    invoke-virtual {v1, v2, p9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip;->setArguments(Landroid/os/Bundle;)V

    .line 351
    invoke-virtual {v0, p6}, Lcom/twitter/ui/widget/Tooltip;->a(Lcom/twitter/ui/widget/Tooltip$c;)V

    .line 352
    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 443
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->d:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 444
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 448
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 449
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 450
    const v1, 0x60028

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 454
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 457
    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 458
    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_0

    .line 459
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v1, v1, 0x400

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 462
    :cond_0
    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/ui/widget/Tooltip;->d:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 464
    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/twitter/ui/widget/Tooltip;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->b:Landroid/app/Activity;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 467
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip;->d:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 470
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;)Lcom/twitter/ui/widget/Tooltip;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/Tooltip$d;->a(Ljava/lang/CharSequence;)V

    .line 404
    :cond_0
    return-object p0
.end method

.method public a(Lcom/twitter/ui/widget/Tooltip$c;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/twitter/ui/widget/Tooltip;->l:Lcom/twitter/ui/widget/Tooltip$c;

    .line 369
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 377
    iget-boolean v0, p0, Lcom/twitter/ui/widget/Tooltip;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    if-eqz v0, :cond_0

    .line 378
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip;->c()V

    .line 380
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    new-instance v1, Lcom/twitter/ui/widget/Tooltip$1;

    invoke-direct {v1, p0}, Lcom/twitter/ui/widget/Tooltip$1;-><init>(Lcom/twitter/ui/widget/Tooltip;)V

    invoke-static {v0, p1, v1}, Lcom/twitter/ui/widget/Tooltip$d;->a(Lcom/twitter/ui/widget/Tooltip$d;ZLjava/lang/Runnable;)V

    .line 390
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/ui/widget/Tooltip;->g:Z

    .line 392
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 416
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 417
    iput-object p1, p0, Lcom/twitter/ui/widget/Tooltip;->b:Landroid/app/Activity;

    .line 418
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 599
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->l:Lcom/twitter/ui/widget/Tooltip$c;

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->l:Lcom/twitter/ui/widget/Tooltip$c;

    invoke-interface {v0, p0, v1}, Lcom/twitter/ui/widget/Tooltip$c;->a(Lcom/twitter/ui/widget/Tooltip;I)V

    .line 604
    :goto_0
    return-void

    .line 602
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->k:Lcom/twitter/ui/widget/Tooltip$b;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$b;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 409
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 410
    new-instance v0, Lcom/twitter/ui/widget/Tooltip$b;

    invoke-direct {v0, p0}, Lcom/twitter/ui/widget/Tooltip$b;-><init>(Lcom/twitter/ui/widget/Tooltip;)V

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->k:Lcom/twitter/ui/widget/Tooltip$b;

    .line 411
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/ui/widget/Tooltip;->i:Z

    .line 412
    return-void

    .line 411
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 590
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->l:Lcom/twitter/ui/widget/Tooltip$c;

    .line 591
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 592
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 573
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip;->c()V

    .line 574
    iget-boolean v0, p0, Lcom/twitter/ui/widget/Tooltip;->j:Z

    if-eqz v0, :cond_0

    .line 575
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/Tooltip;->a(Z)V

    .line 577
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 578
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 498
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 500
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    if-nez v0, :cond_2

    .line 501
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 502
    const-string/jumbo v0, "targetViewId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 503
    const-string/jumbo v0, "targetViewTag"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 504
    const-string/jumbo v0, "containerId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 505
    const-string/jumbo v4, "dismissOnPause"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/twitter/ui/widget/Tooltip;->j:Z

    .line 507
    if-nez v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->c:Landroid/view/ViewGroup;

    .line 513
    :goto_0
    invoke-direct {p0, v2, v3}, Lcom/twitter/ui/widget/Tooltip;->a(ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 514
    if-eqz v0, :cond_1

    .line 515
    invoke-direct {p0, v0, v1}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 539
    :goto_1
    return-void

    .line 510
    :cond_0
    iget-object v4, p0, Lcom/twitter/ui/widget/Tooltip;->b:Landroid/app/Activity;

    invoke-virtual {v4, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->c:Landroid/view/ViewGroup;

    goto :goto_0

    .line 517
    :cond_1
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v4, Lcom/twitter/ui/widget/Tooltip$2;

    invoke-direct {v4, p0, v2, v3, v1}, Lcom/twitter/ui/widget/Tooltip$2;-><init>(Lcom/twitter/ui/widget/Tooltip;ILjava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v0, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_1

    .line 537
    :cond_2
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip;->b()V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 582
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 585
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip;->getTag()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 586
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 477
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->d:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/Tooltip$d;

    iget-object v3, p0, Lcom/twitter/ui/widget/Tooltip;->f:[I

    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/Tooltip$d;->getLocationOnScreen([I)V

    .line 482
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    .line 483
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    .line 484
    iget-object v4, p0, Lcom/twitter/ui/widget/Tooltip;->f:[I

    aget v4, v4, v2

    int-to-float v4, v4

    cmpl-float v4, v0, v4

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/twitter/ui/widget/Tooltip;->f:[I

    aget v4, v4, v2

    iget-object v5, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    .line 485
    invoke-virtual {v5}, Lcom/twitter/ui/widget/Tooltip$d;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v0, v0, v4

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->f:[I

    aget v0, v0, v1

    int-to-float v0, v0

    cmpl-float v0, v3, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->f:[I

    aget v0, v0, v1

    iget-object v4, p0, Lcom/twitter/ui/widget/Tooltip;->e:Lcom/twitter/ui/widget/Tooltip$d;

    .line 487
    invoke-virtual {v4}, Lcom/twitter/ui/widget/Tooltip$d;->getHeight()I

    move-result v4

    add-int/2addr v0, v4

    int-to-float v0, v0

    cmpg-float v0, v3, v0

    if-gez v0, :cond_1

    move v0, v1

    .line 489
    :goto_0
    if-nez v0, :cond_0

    .line 490
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip;->k:Lcom/twitter/ui/widget/Tooltip$b;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$b;->sendEmptyMessage(I)Z

    .line 493
    :cond_0
    return v2

    :cond_1
    move v0, v2

    .line 487
    goto :goto_0
.end method
