.class Lcom/twitter/ui/widget/c$2;
.super Landroid/view/animation/Animation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/ui/widget/c;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/ui/widget/c$b;

.field final synthetic b:Lcom/twitter/ui/widget/c;


# direct methods
.method constructor <init>(Lcom/twitter/ui/widget/c;Lcom/twitter/ui/widget/c$b;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/twitter/ui/widget/c$2;->b:Lcom/twitter/ui/widget/c;

    iput-object p2, p0, Lcom/twitter/ui/widget/c$2;->a:Lcom/twitter/ui/widget/c$b;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method public applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 4

    .prologue
    .line 331
    iget-object v0, p0, Lcom/twitter/ui/widget/c$2;->a:Lcom/twitter/ui/widget/c$b;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/c$b;->i()F

    move-result v0

    const v1, 0x3f4ccccd    # 0.8f

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v2

    double-to-float v0, v0

    .line 333
    iget-object v1, p0, Lcom/twitter/ui/widget/c$2;->a:Lcom/twitter/ui/widget/c$b;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/c$b;->e()F

    move-result v1

    iget-object v2, p0, Lcom/twitter/ui/widget/c$2;->a:Lcom/twitter/ui/widget/c$b;

    .line 334
    invoke-virtual {v2}, Lcom/twitter/ui/widget/c$b;->f()F

    move-result v2

    iget-object v3, p0, Lcom/twitter/ui/widget/c$2;->a:Lcom/twitter/ui/widget/c$b;

    invoke-virtual {v3}, Lcom/twitter/ui/widget/c$b;->e()F

    move-result v3

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    .line 336
    iget-object v2, p0, Lcom/twitter/ui/widget/c$2;->a:Lcom/twitter/ui/widget/c$b;

    invoke-virtual {v2, v1}, Lcom/twitter/ui/widget/c$b;->b(F)V

    .line 337
    iget-object v1, p0, Lcom/twitter/ui/widget/c$2;->a:Lcom/twitter/ui/widget/c$b;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/c$b;->i()F

    move-result v1

    iget-object v2, p0, Lcom/twitter/ui/widget/c$2;->a:Lcom/twitter/ui/widget/c$b;

    .line 338
    invoke-virtual {v2}, Lcom/twitter/ui/widget/c$b;->i()F

    move-result v2

    sub-float/2addr v0, v2

    mul-float/2addr v0, p1

    add-float/2addr v0, v1

    .line 339
    iget-object v1, p0, Lcom/twitter/ui/widget/c$2;->a:Lcom/twitter/ui/widget/c$b;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/c$b;->d(F)V

    .line 340
    iget-object v0, p0, Lcom/twitter/ui/widget/c$2;->a:Lcom/twitter/ui/widget/c$b;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/c$b;->e(F)V

    .line 341
    return-void
.end method
