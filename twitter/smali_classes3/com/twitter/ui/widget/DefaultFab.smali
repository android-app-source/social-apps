.class public Lcom/twitter/ui/widget/DefaultFab;
.super Landroid/support/design/widget/FloatingActionButton;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/support/design/widget/FloatingActionButton;-><init>(Landroid/content/Context;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/FloatingActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/support/design/widget/FloatingActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/support/design/widget/FloatingActionButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/twitter/ui/widget/DefaultFab;->cancelLongPress()V

    .line 37
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/twitter/ui/widget/DefaultFab;->setPressed(Z)V

    .line 39
    :cond_1
    return v0
.end method

.method public setVisibility(I)V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/support/design/widget/FloatingActionButton;->setVisibility(I)V

    .line 50
    return-void
.end method
