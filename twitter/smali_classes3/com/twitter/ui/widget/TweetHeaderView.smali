.class public Lcom/twitter/ui/widget/TweetHeaderView;
.super Lcom/twitter/ui/widget/TouchableView;
.source "Twttr"


# static fields
.field private static final a:[I


# instance fields
.field private A:Landroid/graphics/drawable/Drawable;

.field private B:I

.field private C:I

.field private D:I

.field private E:F

.field private F:F

.field private G:F

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:Z

.field private final b:Z

.field private final c:Landroid/graphics/Rect;

.field private final d:Landroid/graphics/Rect;

.field private final e:Landroid/text/TextPaint;

.field private f:Landroid/view/View$OnClickListener;

.field private g:F

.field private h:I

.field private i:I

.field private j:Landroid/content/res/ColorStateList;

.field private k:Landroid/content/res/ColorStateList;

.field private l:Landroid/content/res/ColorStateList;

.field private m:Landroid/content/res/ColorStateList;

.field private n:Landroid/content/res/ColorStateList;

.field private o:Landroid/content/res/ColorStateList;

.field private p:Lcom/twitter/ui/widget/i;

.field private q:Landroid/text/StaticLayout;

.field private r:I

.field private s:Landroid/text/StaticLayout;

.field private t:I

.field private u:Landroid/text/StaticLayout;

.field private v:I

.field private w:I

.field private x:Lcom/twitter/ui/widget/TouchableView$a;

.field private y:Landroid/graphics/drawable/Drawable;

.field private z:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcjo$a;->state_name_username_pressed:I

    aput v2, v0, v1

    sput-object v0, Lcom/twitter/ui/widget/TweetHeaderView;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/ui/widget/TweetHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 105
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/ui/widget/TouchableView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->b:Z

    .line 41
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->c:Landroid/graphics/Rect;

    .line 42
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->d:Landroid/graphics/Rect;

    .line 43
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    .line 83
    iput-boolean v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->L:Z

    .line 106
    sget-object v0, Lcjo$h;->TweetHeaderView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 107
    invoke-direct {p0, p1, v0}, Lcom/twitter/ui/widget/TweetHeaderView;->a(Landroid/content/Context;Landroid/content/res/TypedArray;)V

    .line 108
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 109
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 94
    sget v0, Lcjo$a;->tweetHeaderViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/ui/widget/TweetHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 98
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/ui/widget/TouchableView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->b:Z

    .line 41
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->c:Landroid/graphics/Rect;

    .line 42
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->d:Landroid/graphics/Rect;

    .line 43
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    .line 83
    iput-boolean v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->L:Z

    .line 99
    sget-object v0, Lcjo$h;->TweetHeaderView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 100
    invoke-direct {p0, p1, v0}, Lcom/twitter/ui/widget/TweetHeaderView;->a(Landroid/content/Context;Landroid/content/res/TypedArray;)V

    .line 101
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 102
    return-void
.end method

.method static synthetic a(Lcom/twitter/ui/widget/TweetHeaderView;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->f:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/content/res/TypedArray;)V
    .locals 2

    .prologue
    .line 112
    invoke-static {p1}, Lcom/twitter/ui/widget/i;->a(Landroid/content/Context;)Lcom/twitter/ui/widget/i;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->p:Lcom/twitter/ui/widget/i;

    .line 114
    sget v0, Lcjo$h;->TweetHeaderView_nameColor:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->j:Landroid/content/res/ColorStateList;

    .line 115
    sget v0, Lcjo$h;->TweetHeaderView_timestampColor:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->l:Landroid/content/res/ColorStateList;

    .line 116
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->l:Landroid/content/res/ColorStateList;

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->k:Landroid/content/res/ColorStateList;

    .line 117
    sget v0, Lcjo$h;->TweetHeaderView_usernameColor:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->m:Landroid/content/res/ColorStateList;

    .line 118
    sget v0, Lcjo$h;->TweetHeaderView_protectedDrawableColor:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->n:Landroid/content/res/ColorStateList;

    .line 119
    sget v0, Lcjo$h;->TweetHeaderView_verifiedDrawableColor:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->o:Landroid/content/res/ColorStateList;

    .line 120
    sget v0, Lcjo$h;->TweetHeaderView_protectedDrawable:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->A:Landroid/graphics/drawable/Drawable;

    .line 121
    sget v0, Lcjo$h;->TweetHeaderView_verifiedDrawable:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->z:Landroid/graphics/drawable/Drawable;

    .line 122
    sget v0, Lcjo$h;->TweetHeaderView_android_lineSpacingMultiplier:I

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->g:F

    .line 123
    sget v0, Lcjo$h;->TweetHeaderView_android_lineSpacingExtra:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->h:I

    .line 124
    sget v0, Lcjo$h;->TweetHeaderView_headerIconSpacing:I

    const/4 v1, 0x4

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->i:I

    .line 127
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->drawableStateChanged()V

    .line 128
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->O:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->J:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 160
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->b:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/twitter/util/b;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->J:Ljava/lang/String;

    .line 161
    invoke-static {v0}, Lcom/twitter/util/s;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 162
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->J:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\u00b7"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->K:Ljava/lang/String;

    .line 169
    :goto_0
    return-void

    .line 164
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\u00b7 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->J:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->K:Ljava/lang/String;

    goto :goto_0

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->J:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->K:Ljava/lang/String;

    goto :goto_0
.end method

.method private e()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 177
    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    .line 178
    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    .line 179
    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    .line 180
    return-void
.end method


# virtual methods
.method public a(FFF)V
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->E:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->F:F

    cmpl-float v0, p2, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->G:F

    cmpl-float v0, p3, v0

    if-eqz v0, :cond_1

    .line 211
    :cond_0
    iput p1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->E:F

    .line 212
    iput p2, p0, Lcom/twitter/ui/widget/TweetHeaderView;->F:F

    .line 213
    iput p3, p0, Lcom/twitter/ui/widget/TweetHeaderView;->G:F

    .line 214
    invoke-direct {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->e()V

    .line 215
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->requestLayout()V

    .line 216
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->invalidate()V

    .line 218
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 132
    invoke-direct {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->e()V

    .line 133
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->d:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    .line 134
    if-eqz p4, :cond_1

    .line 135
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->z:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    .line 142
    :goto_0
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object p1, v0

    :cond_0
    iput-object p1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->H:Ljava/lang/String;

    .line 143
    invoke-static {p2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, v0

    :goto_1
    iput-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->I:Ljava/lang/String;

    .line 144
    invoke-static {p3}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 145
    iput-object p3, p0, Lcom/twitter/ui/widget/TweetHeaderView;->J:Ljava/lang/String;

    .line 149
    :goto_2
    invoke-direct {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->d()V

    .line 150
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->b()V

    .line 152
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->invalidate()V

    .line 153
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->requestLayout()V

    .line 154
    return-void

    .line 136
    :cond_1
    if-eqz p5, :cond_2

    .line 137
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->A:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 139
    :cond_2
    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 143
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 147
    :cond_4
    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->J:Ljava/lang/String;

    goto :goto_2
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 172
    iput-boolean p1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->O:Z

    .line 173
    invoke-direct {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->d()V

    .line 174
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 205
    iput-boolean p1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->N:Z

    .line 206
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->l:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/TweetHeaderView;->setTimestampColor(Landroid/content/res/ColorStateList;)V

    .line 198
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 242
    invoke-super {p0}, Lcom/twitter/ui/widget/TouchableView;->drawableStateChanged()V

    .line 243
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->getDrawableState()[I

    move-result-object v0

    .line 245
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->j:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_0

    .line 246
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->j:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v4}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->B:I

    .line 248
    :cond_0
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->k:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_1

    .line 249
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->k:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v4}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->D:I

    .line 251
    :cond_1
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->m:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_2

    .line 252
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->m:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v4}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->C:I

    .line 254
    :cond_2
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->A:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->n:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_3

    .line 255
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->A:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 256
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->A:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/twitter/ui/widget/TweetHeaderView;->n:Landroid/content/res/ColorStateList;

    invoke-virtual {v2, v0, v4}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 258
    :cond_3
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->z:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->o:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_4

    .line 259
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->z:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 260
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->z:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/twitter/ui/widget/TweetHeaderView;->o:Landroid/content/res/ColorStateList;

    invoke-virtual {v2, v0, v4}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 262
    :cond_4
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v2, -0x1

    .line 428
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->getWidth()I

    move-result v0

    .line 431
    iget-boolean v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->b:Z

    if-eqz v1, :cond_a

    .line 432
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_6

    .line 433
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int v1, v0, v1

    .line 434
    iget v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->i:I

    sub-int v0, v1, v0

    .line 439
    :goto_0
    iget-object v3, p0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    if-eqz v3, :cond_7

    .line 440
    iget-object v3, p0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getEllipsizedWidth()I

    move-result v3

    sub-int v3, v0, v3

    .line 441
    iget-boolean v4, p0, Lcom/twitter/ui/widget/TweetHeaderView;->N:Z

    if-nez v4, :cond_0

    .line 442
    iget v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->i:I

    sub-int v0, v3, v0

    .line 448
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    if-eqz v4, :cond_8

    .line 449
    iget-object v4, p0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getEllipsizedWidth()I

    move-result v4

    sub-int v4, v0, v4

    .line 450
    iget v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->i:I

    sub-int v0, v4, v0

    .line 455
    :goto_2
    iget-object v6, p0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    if-eqz v6, :cond_1

    .line 456
    iget-boolean v2, p0, Lcom/twitter/ui/widget/TweetHeaderView;->O:Z

    if-eqz v2, :cond_9

    .line 457
    iget-object v2, p0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    sub-int v2, v0, v2

    .line 503
    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    if-eqz v0, :cond_2

    .line 504
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 505
    int-to-float v0, v4

    iget v6, p0, Lcom/twitter/ui/widget/TweetHeaderView;->t:I

    int-to-float v6, v6

    invoke-virtual {p1, v0, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 506
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    iget v6, p0, Lcom/twitter/ui/widget/TweetHeaderView;->E:F

    invoke-virtual {v0, v6}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 507
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    iget-object v6, p0, Lcom/twitter/ui/widget/TweetHeaderView;->p:Lcom/twitter/ui/widget/i;

    iget-object v6, v6, Lcom/twitter/ui/widget/i;->c:Landroid/graphics/Typeface;

    invoke-virtual {v0, v6}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 508
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    iget v6, p0, Lcom/twitter/ui/widget/TweetHeaderView;->B:I

    invoke-virtual {v0, v6}, Landroid/text/TextPaint;->setColor(I)V

    .line 509
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 510
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 512
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->d:Landroid/graphics/Rect;

    iget v6, p0, Lcom/twitter/ui/widget/TweetHeaderView;->t:I

    iget-object v7, p0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    invoke-virtual {v7}, Landroid/text/StaticLayout;->getEllipsizedWidth()I

    move-result v7

    add-int/2addr v7, v4

    iget v8, p0, Lcom/twitter/ui/widget/TweetHeaderView;->t:I

    iget-object v9, p0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    .line 513
    invoke-virtual {v9}, Landroid/text/StaticLayout;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    .line 512
    invoke-virtual {v0, v4, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 516
    :cond_2
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/twitter/ui/widget/TweetHeaderView;->p:Lcom/twitter/ui/widget/i;

    iget-object v4, v4, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 519
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    if-eqz v0, :cond_3

    .line 520
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 521
    int-to-float v0, v3

    iget v4, p0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    int-to-float v4, v4

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 522
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    iget v4, p0, Lcom/twitter/ui/widget/TweetHeaderView;->F:F

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 524
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->M:Z

    if-eqz v0, :cond_11

    .line 525
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    iget v4, p0, Lcom/twitter/ui/widget/TweetHeaderView;->D:I

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setColor(I)V

    .line 529
    :goto_4
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 530
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 532
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->d:Landroid/graphics/Rect;

    iget v4, p0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    iget-object v6, p0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    .line 533
    invoke-virtual {v6}, Landroid/text/StaticLayout;->getEllipsizedWidth()I

    move-result v6

    add-int/2addr v6, v3

    iget v7, p0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    iget-object v8, p0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    .line 534
    invoke-virtual {v8}, Landroid/text/StaticLayout;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    .line 532
    invoke-virtual {v0, v3, v4, v6, v7}, Landroid/graphics/Rect;->union(IIII)V

    .line 538
    :cond_3
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    .line 539
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 540
    int-to-float v0, v1

    iget v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->w:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 541
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v3, p0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    .line 542
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 541
    invoke-virtual {v0, v5, v5, v1, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 543
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 544
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 548
    :cond_4
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    if-eqz v0, :cond_5

    .line 549
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 550
    int-to-float v0, v2

    iget v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->v:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 551
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    iget v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->G:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 552
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    iget v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->D:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 553
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 554
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 556
    :cond_5
    return-void

    :cond_6
    move v1, v2

    .line 436
    goto/16 :goto_0

    :cond_7
    move v3, v2

    .line 445
    goto/16 :goto_1

    :cond_8
    move v4, v2

    .line 452
    goto/16 :goto_2

    :cond_9
    move v2, v5

    .line 459
    goto/16 :goto_3

    .line 468
    :cond_a
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    if-eqz v1, :cond_13

    .line 469
    iget-boolean v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->N:Z

    if-nez v1, :cond_c

    .line 470
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getEllipsizedWidth()I

    move-result v1

    iget v3, p0, Lcom/twitter/ui/widget/TweetHeaderView;->i:I

    add-int/2addr v1, v3

    add-int/2addr v1, v5

    move v3, v1

    move v1, v5

    .line 477
    :goto_5
    iget-object v4, p0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    if-eqz v4, :cond_12

    .line 478
    iget-object v4, p0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getEllipsizedWidth()I

    move-result v4

    iget v6, p0, Lcom/twitter/ui/widget/TweetHeaderView;->i:I

    add-int/2addr v4, v6

    add-int/2addr v4, v3

    .line 480
    :goto_6
    iget-boolean v6, p0, Lcom/twitter/ui/widget/TweetHeaderView;->N:Z

    if-eqz v6, :cond_d

    move v6, v1

    .line 481
    :goto_7
    iget-object v7, p0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_b

    .line 482
    iget-boolean v7, p0, Lcom/twitter/ui/widget/TweetHeaderView;->N:Z

    if-nez v7, :cond_e

    .line 483
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget v7, p0, Lcom/twitter/ui/widget/TweetHeaderView;->i:I

    add-int/2addr v1, v7

    add-int/2addr v4, v1

    .line 490
    :cond_b
    :goto_8
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    if-eqz v1, :cond_10

    .line 491
    iget-boolean v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->O:Z

    if-eqz v1, :cond_f

    .line 493
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getEllipsizedWidth()I

    move-result v0

    iget v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->i:I

    add-int/2addr v0, v1

    move v1, v6

    move v2, v4

    move v4, v5

    goto/16 :goto_3

    .line 473
    :cond_c
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getEllipsizedWidth()I

    move-result v1

    add-int/2addr v1, v5

    iget v3, p0, Lcom/twitter/ui/widget/TweetHeaderView;->i:I

    add-int/2addr v1, v3

    move v3, v5

    goto :goto_5

    :cond_d
    move v6, v4

    .line 480
    goto :goto_7

    .line 485
    :cond_e
    iget-object v7, p0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    iget v8, p0, Lcom/twitter/ui/widget/TweetHeaderView;->i:I

    add-int/2addr v7, v8

    add-int/2addr v1, v7

    .line 486
    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    goto :goto_8

    .line 495
    :cond_f
    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getWidth()I

    move-result v1

    sub-int v2, v0, v1

    move v1, v6

    move v4, v5

    goto/16 :goto_3

    :cond_10
    move v1, v6

    move v4, v5

    .line 498
    goto/16 :goto_3

    .line 527
    :cond_11
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    iget v4, p0, Lcom/twitter/ui/widget/TweetHeaderView;->C:I

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setColor(I)V

    goto/16 :goto_4

    :cond_12
    move v4, v3

    goto :goto_6

    :cond_13
    move v1, v5

    move v3, v5

    goto :goto_5
.end method

.method protected onMeasure(II)V
    .locals 20

    .prologue
    .line 266
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v15

    .line 271
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->K:Ljava/lang/String;

    if-eqz v1, :cond_9

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->L:Z

    if-eqz v1, :cond_9

    .line 272
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->G:F

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 273
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->p:Lcom/twitter/ui/widget/i;

    iget-object v2, v2, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 274
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    if-nez v1, :cond_0

    .line 275
    new-instance v1, Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->K:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->K:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    .line 276
    invoke-static {v4, v5}, Lcom/twitter/util/ui/k;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v4

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    .line 279
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getWidth()I

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->i:I

    add-int/2addr v1, v2

    sub-int v3, v15, v1

    .line 280
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->K:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->K:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/ui/widget/TweetHeaderView;->c:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v4, v5, v6}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 281
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->c:Landroid/graphics/Rect;

    invoke-static {v1, v2}, Lcom/twitter/util/ui/h;->a(Landroid/text/Layout;Landroid/graphics/Rect;)I

    move-result v1

    neg-int v1, v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->v:I

    .line 282
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->c:Landroid/graphics/Rect;

    invoke-static {v1}, Lcom/twitter/util/ui/h;->a(Landroid/graphics/Rect;)I

    move-result v2

    .line 283
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->c:Landroid/graphics/Rect;

    invoke-static {v1, v4}, Lcom/twitter/util/ui/h;->b(Landroid/text/Layout;Landroid/graphics/Rect;)I

    move-result v1

    move v13, v1

    move v14, v2

    move v1, v3

    .line 289
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1a

    .line 290
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/ui/widget/TweetHeaderView;->i:I

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    move/from16 v16, v1

    .line 295
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->H:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 296
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->E:F

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 297
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->p:Lcom/twitter/ui/widget/i;

    iget-object v2, v2, Lcom/twitter/ui/widget/i;->c:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 298
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    if-nez v1, :cond_1

    .line 299
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->H:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    invoke-static {v1, v2}, Lcom/twitter/util/ui/k;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v6

    .line 300
    move/from16 v0, v16

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 301
    new-instance v1, Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->H:Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->H:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/ui/widget/TweetHeaderView;->g:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/ui/widget/TweetHeaderView;->h:I

    int-to-float v9, v9

    const/4 v10, 0x0

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-direct/range {v1 .. v12}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    .line 305
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->H:Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->H:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->c:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 306
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getEllipsizedWidth()I

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->i:I

    add-int/2addr v1, v2

    sub-int v16, v16, v1

    .line 307
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->c:Landroid/graphics/Rect;

    invoke-static {v1}, Lcom/twitter/util/ui/h;->a(Landroid/graphics/Rect;)I

    move-result v3

    .line 308
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->c:Landroid/graphics/Rect;

    invoke-static {v1, v2}, Lcom/twitter/util/ui/h;->b(Landroid/text/Layout;Landroid/graphics/Rect;)I

    move-result v2

    .line 309
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->c:Landroid/graphics/Rect;

    invoke-static {v1, v4}, Lcom/twitter/util/ui/h;->a(Landroid/text/Layout;Landroid/graphics/Rect;)I

    move-result v1

    .line 312
    neg-int v4, v1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->t:I

    move/from16 v17, v2

    move/from16 v18, v3

    move/from16 v19, v1

    move/from16 v1, v16

    move/from16 v16, v19

    .line 321
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->N:Z

    if-eqz v2, :cond_2

    move v1, v15

    .line 325
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->I:Ljava/lang/String;

    .line 326
    if-eqz v2, :cond_e

    if-lez v1, :cond_e

    .line 327
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->F:F

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 328
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->p:Lcom/twitter/ui/widget/i;

    iget-object v4, v4, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 330
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    if-nez v3, :cond_3

    .line 331
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    invoke-static {v2, v3}, Lcom/twitter/util/ui/k;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v6

    .line 332
    invoke-static {v6, v1}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 333
    new-instance v1, Landroid/text/StaticLayout;

    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-direct/range {v1 .. v12}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    .line 337
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->e:Landroid/text/TextPaint;

    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->c:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 338
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->c:Landroid/graphics/Rect;

    invoke-static {v1, v2}, Lcom/twitter/util/ui/h;->b(Landroid/text/Layout;Landroid/graphics/Rect;)I

    move-result v3

    .line 339
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->c:Landroid/graphics/Rect;

    invoke-static {v1, v2}, Lcom/twitter/util/ui/h;->a(Landroid/text/Layout;Landroid/graphics/Rect;)I

    move-result v1

    .line 340
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->c:Landroid/graphics/Rect;

    invoke-static {v2}, Lcom/twitter/util/ui/h;->a(Landroid/graphics/Rect;)I

    move-result v2

    .line 341
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->N:Z

    if-eqz v4, :cond_c

    .line 342
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    if-eqz v4, :cond_b

    .line 344
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->t:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    .line 370
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    if-eqz v4, :cond_6

    .line 371
    const/4 v4, 0x0

    .line 372
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    if-eqz v5, :cond_f

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    if-eqz v5, :cond_f

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->N:Z

    if-nez v5, :cond_f

    .line 374
    move/from16 v0, v18

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    sub-int/2addr v4, v14

    .line 381
    :cond_5
    :goto_4
    if-lez v4, :cond_12

    .line 382
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->v:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->v:I

    .line 388
    :cond_6
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_7

    .line 389
    const/4 v4, 0x0

    .line 390
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->w:I

    .line 391
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    if-eqz v5, :cond_13

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    if-eqz v5, :cond_13

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    if-eqz v5, :cond_13

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->N:Z

    if-nez v5, :cond_13

    .line 393
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    add-int/2addr v1, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    .line 394
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    sub-int/2addr v2, v4

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 400
    :goto_6
    if-lez v1, :cond_15

    .line 401
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->w:I

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->w:I

    .line 411
    :cond_7
    :goto_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    if-eqz v1, :cond_8

    if-nez v18, :cond_16

    :cond_8
    const/4 v1, 0x0

    .line 413
    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    if-nez v2, :cond_17

    const/4 v2, 0x0

    .line 415
    :goto_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    if-nez v3, :cond_18

    const/4 v3, 0x0

    .line 417
    :goto_a
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 418
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/ui/widget/TweetHeaderView;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v1}, Lcom/twitter/ui/widget/TweetHeaderView;->setMeasuredDimension(II)V

    .line 419
    return-void

    .line 285
    :cond_9
    const/4 v1, 0x0

    .line 286
    const/4 v2, 0x0

    move v13, v1

    move v14, v2

    move v1, v15

    goto/16 :goto_0

    .line 314
    :cond_a
    const/4 v3, 0x0

    .line 315
    const/4 v2, 0x0

    .line 316
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->t:I

    .line 317
    const/4 v1, 0x0

    move/from16 v17, v2

    move/from16 v18, v3

    move/from16 v19, v1

    move/from16 v1, v16

    move/from16 v16, v19

    goto/16 :goto_2

    .line 347
    :cond_b
    neg-int v4, v1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    goto/16 :goto_3

    .line 352
    :cond_c
    sub-int v4, v18, v2

    .line 353
    neg-int v5, v1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    .line 354
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    if-eqz v5, :cond_4

    .line 355
    if-lez v4, :cond_d

    .line 356
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    goto/16 :goto_3

    .line 358
    :cond_d
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->t:I

    sub-int v4, v5, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->t:I

    goto/16 :goto_3

    .line 363
    :cond_e
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    .line 364
    const/4 v3, 0x0

    .line 365
    const/4 v2, 0x0

    .line 366
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 376
    :cond_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    if-nez v5, :cond_10

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    if-eqz v5, :cond_5

    .line 378
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    if-eqz v4, :cond_11

    move/from16 v4, v18

    :goto_b
    sub-int/2addr v4, v14

    goto/16 :goto_4

    :cond_11
    move v4, v2

    goto :goto_b

    .line 384
    :cond_12
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->t:I

    sub-int/2addr v5, v4

    move-object/from16 v0, p0

    iput v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->t:I

    .line 385
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    sub-int v4, v5, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    goto/16 :goto_5

    .line 395
    :cond_13
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    if-nez v1, :cond_14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    if-eqz v1, :cond_19

    .line 397
    :cond_14
    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->t:I

    add-int v1, v1, v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->y:Landroid/graphics/drawable/Drawable;

    .line 398
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    sub-int v2, v18, v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    goto/16 :goto_6

    .line 403
    :cond_15
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->t:I

    sub-int/2addr v2, v1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->t:I

    .line 404
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    sub-int/2addr v2, v1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    .line 405
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->v:I

    sub-int v1, v2, v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->v:I

    goto/16 :goto_7

    .line 411
    :cond_16
    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/ui/widget/TweetHeaderView;->t:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->s:Landroid/text/StaticLayout;

    .line 412
    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    sub-int v1, v1, v17

    goto/16 :goto_8

    .line 413
    :cond_17
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/ui/widget/TweetHeaderView;->r:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->q:Landroid/text/StaticLayout;

    .line 414
    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v2, v4

    sub-int/2addr v2, v3

    goto/16 :goto_9

    .line 415
    :cond_18
    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/ui/widget/TweetHeaderView;->v:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/ui/widget/TweetHeaderView;->u:Landroid/text/StaticLayout;

    .line 416
    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    sub-int/2addr v3, v13

    goto/16 :goto_a

    :cond_19
    move v1, v4

    goto/16 :goto_6

    :cond_1a
    move/from16 v16, v1

    goto/16 :goto_1
.end method

.method public setOnAuthorClick(Landroid/view/View$OnClickListener;)V
    .locals 8

    .prologue
    .line 221
    iput-object p1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->f:Landroid/view/View$OnClickListener;

    .line 222
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->f:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->x:Lcom/twitter/ui/widget/TouchableView$a;

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/TweetHeaderView;->b(Lcom/twitter/ui/widget/TouchableView$a;)V

    .line 238
    :goto_0
    return-void

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->x:Lcom/twitter/ui/widget/TouchableView$a;

    if-nez v0, :cond_1

    .line 226
    new-instance v0, Lcom/twitter/ui/widget/TouchableView$a;

    const-wide/16 v2, 0x2

    const-wide/16 v4, 0x1

    new-instance v6, Lcom/twitter/ui/widget/TweetHeaderView$1;

    invoke-direct {v6, p0}, Lcom/twitter/ui/widget/TweetHeaderView$1;-><init>(Lcom/twitter/ui/widget/TweetHeaderView;)V

    sget-object v7, Lcom/twitter/ui/widget/TweetHeaderView;->a:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/ui/widget/TouchableView$a;-><init>(Lcom/twitter/ui/widget/TouchableView;JJLjava/lang/Runnable;[I)V

    iput-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->x:Lcom/twitter/ui/widget/TouchableView$a;

    .line 234
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->x:Lcom/twitter/ui/widget/TouchableView$a;

    iget-object v1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TouchableView$a;->a(Landroid/graphics/Rect;)V

    .line 236
    :cond_1
    iget-object v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->x:Lcom/twitter/ui/widget/TouchableView$a;

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/TweetHeaderView;->a(Lcom/twitter/ui/widget/TouchableView$a;)V

    goto :goto_0
.end method

.method public setShowTimestamp(Z)V
    .locals 1

    .prologue
    .line 183
    iget-boolean v0, p0, Lcom/twitter/ui/widget/TweetHeaderView;->L:Z

    if-eq p1, v0, :cond_0

    .line 184
    iput-boolean p1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->L:Z

    .line 185
    invoke-direct {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->e()V

    .line 186
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->requestLayout()V

    .line 187
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->invalidate()V

    .line 189
    :cond_0
    return-void
.end method

.method public setTimestampColor(Landroid/content/res/ColorStateList;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->k:Landroid/content/res/ColorStateList;

    .line 193
    invoke-virtual {p0}, Lcom/twitter/ui/widget/TweetHeaderView;->refreshDrawableState()V

    .line 194
    return-void
.end method

.method public setUseTimestampColorForUsername(Z)V
    .locals 0

    .prologue
    .line 201
    iput-boolean p1, p0, Lcom/twitter/ui/widget/TweetHeaderView;->M:Z

    .line 202
    return-void
.end method
