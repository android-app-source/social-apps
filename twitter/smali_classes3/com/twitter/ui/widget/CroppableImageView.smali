.class public Lcom/twitter/ui/widget/CroppableImageView;
.super Lcom/twitter/ui/widget/MultiTouchImageView;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/ui/widget/CroppableImageView$b;,
        Lcom/twitter/ui/widget/CroppableImageView$c;,
        Lcom/twitter/ui/widget/CroppableImageView$d;,
        Lcom/twitter/ui/widget/CroppableImageView$a;
    }
.end annotation


# instance fields
.field private final e:Landroid/graphics/Bitmap;

.field private final f:Landroid/graphics/Bitmap;

.field private final g:Landroid/graphics/Bitmap;

.field private final h:Landroid/graphics/Bitmap;

.field private final i:Landroid/graphics/Paint;

.field private final j:Landroid/graphics/Paint;

.field private final k:Landroid/graphics/Paint;

.field private final l:Landroid/graphics/RectF;

.field private final m:Landroid/graphics/PointF;

.field private final n:I

.field private final o:I

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Lcom/twitter/ui/widget/CroppableImageView$b;

.field private t:Z

.field private u:I

.field private v:I

.field private w:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/ui/widget/CroppableImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 82
    sget v0, Lckh$b;->croppableImageViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/ui/widget/CroppableImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 83
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/high16 v7, 0x42b40000    # 90.0f

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 86
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/ui/widget/MultiTouchImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->i:Landroid/graphics/Paint;

    .line 61
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->j:Landroid/graphics/Paint;

    .line 62
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->k:Landroid/graphics/Paint;

    .line 63
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->l:Landroid/graphics/RectF;

    .line 64
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->m:Landroid/graphics/PointF;

    .line 68
    iput-boolean v3, p0, Lcom/twitter/ui/widget/CroppableImageView;->p:Z

    .line 88
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 89
    sget-object v1, Lckh$k;->CroppableImageView:[I

    .line 90
    invoke-virtual {p1, p2, v1, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 92
    sget v2, Lckh$k;->CroppableImageView_cropRectPadding:I

    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->n:I

    .line 94
    sget v2, Lckh$k;->CroppableImageView_toolbarMargin:I

    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->o:I

    .line 96
    sget v2, Lckh$k;->CroppableImageView_draggableCorners:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->r:Z

    .line 97
    sget v2, Lckh$k;->CroppableImageView_showGrid:I

    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->q:Z

    .line 99
    iget-object v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->i:Landroid/graphics/Paint;

    .line 100
    sget v3, Lckh$k;->CroppableImageView_cropRectStrokeColor:I

    sget v4, Lckh$c;->white:I

    .line 101
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 100
    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 102
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 103
    sget v3, Lckh$k;->CroppableImageView_cropRectStrokeWidth:I

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 105
    iget-object v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->j:Landroid/graphics/Paint;

    .line 106
    sget v3, Lckh$k;->CroppableImageView_gridColor:I

    invoke-virtual {v1, v3, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 107
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 108
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 110
    iget-object v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->k:Landroid/graphics/Paint;

    sget v3, Lckh$k;->CroppableImageView_cropShadowColor:I

    sget v4, Lckh$c;->dark_transparent_black:I

    .line 111
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 110
    invoke-virtual {v1, v3, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 112
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 114
    iget-boolean v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->r:Z

    if-eqz v0, :cond_0

    .line 116
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 117
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lckh$e;->ic_filters_crop_corner:I

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    .line 119
    invoke-virtual {v0, v7}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 120
    iget-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    invoke-static {v1, v6, v0, v5}, Lcom/twitter/media/util/a;->a(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->f:Landroid/graphics/Bitmap;

    .line 121
    invoke-virtual {v0, v7}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 122
    iget-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    invoke-static {v1, v6, v0, v5}, Lcom/twitter/media/util/a;->a(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->h:Landroid/graphics/Bitmap;

    .line 123
    invoke-virtual {v0, v7}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 124
    iget-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    invoke-static {v1, v6, v0, v5}, Lcom/twitter/media/util/a;->a(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->g:Landroid/graphics/Bitmap;

    .line 125
    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->v:I

    .line 132
    :goto_0
    return-void

    .line 127
    :cond_0
    iput-object v6, p0, Lcom/twitter/ui/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    .line 128
    iput-object v6, p0, Lcom/twitter/ui/widget/CroppableImageView;->f:Landroid/graphics/Bitmap;

    .line 129
    iput-object v6, p0, Lcom/twitter/ui/widget/CroppableImageView;->h:Landroid/graphics/Bitmap;

    .line 130
    iput-object v6, p0, Lcom/twitter/ui/widget/CroppableImageView;->g:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private a(Landroid/graphics/PointF;)I
    .locals 3

    .prologue
    .line 384
    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    .line 385
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, v0, Landroid/graphics/RectF;->top:F

    invoke-static {p1, v1, v2}, Lcom/twitter/util/math/b;->a(Landroid/graphics/PointF;FF)F

    move-result v1

    iget v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->v:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 386
    const/4 v0, 0x1

    .line 394
    :goto_0
    return v0

    .line 387
    :cond_0
    iget v1, v0, Landroid/graphics/RectF;->right:F

    iget v2, v0, Landroid/graphics/RectF;->top:F

    invoke-static {p1, v1, v2}, Lcom/twitter/util/math/b;->a(Landroid/graphics/PointF;FF)F

    move-result v1

    iget v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->v:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 388
    const/4 v0, 0x2

    goto :goto_0

    .line 389
    :cond_1
    iget v1, v0, Landroid/graphics/RectF;->right:F

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    invoke-static {p1, v1, v2}, Lcom/twitter/util/math/b;->a(Landroid/graphics/PointF;FF)F

    move-result v1

    iget v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->v:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 390
    const/4 v0, 0x3

    goto :goto_0

    .line 391
    :cond_2
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-static {p1, v1, v0}, Lcom/twitter/util/math/b;->a(Landroid/graphics/PointF;FF)F

    move-result v0

    iget v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->v:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 392
    const/4 v0, 0x4

    goto :goto_0

    .line 394
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(IFF)V
    .locals 8

    .prologue
    .line 403
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView;->getImageRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 404
    iget-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    .line 405
    iget v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->v:I

    int-to-float v2, v2

    .line 406
    packed-switch p1, :pswitch_data_0

    .line 431
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView;->invalidate()V

    .line 432
    return-void

    .line 408
    :pswitch_0
    iget v3, v1, Landroid/graphics/RectF;->left:F

    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v5, v1, Landroid/graphics/RectF;->left:F

    iget v6, v0, Landroid/graphics/RectF;->left:F

    iget v7, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v7, v2

    invoke-static {p2, v4, v5, v6, v7}, Lcom/twitter/util/math/b;->a(FFFFF)F

    move-result v4

    add-float/2addr v3, v4

    iput v3, v1, Landroid/graphics/RectF;->left:F

    .line 409
    iget v3, v1, Landroid/graphics/RectF;->top:F

    iget v4, v1, Landroid/graphics/RectF;->top:F

    iget v5, v1, Landroid/graphics/RectF;->top:F

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget v6, v1, Landroid/graphics/RectF;->bottom:F

    sub-float v2, v6, v2

    invoke-static {p3, v4, v5, v0, v2}, Lcom/twitter/util/math/b;->a(FFFFF)F

    move-result v0

    add-float/2addr v0, v3

    iput v0, v1, Landroid/graphics/RectF;->top:F

    goto :goto_0

    .line 413
    :pswitch_1
    iget v3, v1, Landroid/graphics/RectF;->right:F

    iget v4, v1, Landroid/graphics/RectF;->right:F

    iget v5, v1, Landroid/graphics/RectF;->right:F

    iget v6, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v6, v2

    iget v7, v0, Landroid/graphics/RectF;->right:F

    invoke-static {p2, v4, v5, v6, v7}, Lcom/twitter/util/math/b;->a(FFFFF)F

    move-result v4

    add-float/2addr v3, v4

    iput v3, v1, Landroid/graphics/RectF;->right:F

    .line 414
    iget v3, v1, Landroid/graphics/RectF;->top:F

    iget v4, v1, Landroid/graphics/RectF;->top:F

    iget v5, v1, Landroid/graphics/RectF;->top:F

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget v6, v1, Landroid/graphics/RectF;->bottom:F

    sub-float v2, v6, v2

    invoke-static {p3, v4, v5, v0, v2}, Lcom/twitter/util/math/b;->a(FFFFF)F

    move-result v0

    add-float/2addr v0, v3

    iput v0, v1, Landroid/graphics/RectF;->top:F

    goto :goto_0

    .line 418
    :pswitch_2
    iget v3, v1, Landroid/graphics/RectF;->right:F

    iget v4, v1, Landroid/graphics/RectF;->right:F

    iget v5, v1, Landroid/graphics/RectF;->right:F

    iget v6, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v6, v2

    iget v7, v0, Landroid/graphics/RectF;->right:F

    invoke-static {p2, v4, v5, v6, v7}, Lcom/twitter/util/math/b;->a(FFFFF)F

    move-result v4

    add-float/2addr v3, v4

    iput v3, v1, Landroid/graphics/RectF;->right:F

    .line 419
    iget v3, v1, Landroid/graphics/RectF;->bottom:F

    iget v4, v1, Landroid/graphics/RectF;->bottom:F

    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    iget v6, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v6

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-static {p3, v4, v5, v2, v0}, Lcom/twitter/util/math/b;->a(FFFFF)F

    move-result v0

    add-float/2addr v0, v3

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0

    .line 423
    :pswitch_3
    iget v3, v1, Landroid/graphics/RectF;->left:F

    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v5, v1, Landroid/graphics/RectF;->left:F

    iget v6, v0, Landroid/graphics/RectF;->left:F

    iget v7, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v7, v2

    invoke-static {p2, v4, v5, v6, v7}, Lcom/twitter/util/math/b;->a(FFFFF)F

    move-result v4

    add-float/2addr v3, v4

    iput v3, v1, Landroid/graphics/RectF;->left:F

    .line 424
    iget v3, v1, Landroid/graphics/RectF;->bottom:F

    iget v4, v1, Landroid/graphics/RectF;->bottom:F

    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    iget v6, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v6

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-static {p3, v4, v5, v2, v0}, Lcom/twitter/util/math/b;->a(FFFFF)F

    move-result v0

    add-float/2addr v0, v3

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 406
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/ui/widget/CroppableImageView;Z)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/twitter/ui/widget/CroppableImageView;->setAnimating(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 14

    .prologue
    const/4 v7, 0x0

    .line 356
    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    .line 357
    invoke-direct {p0}, Lcom/twitter/ui/widget/CroppableImageView;->getPaddedViewRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 359
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    .line 360
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    .line 361
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    .line 362
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    .line 363
    sub-float v10, v3, v2

    .line 364
    sub-float v11, v5, v4

    .line 365
    const/4 v6, 0x1

    invoke-static {v0, v1, v6}, Lcom/twitter/util/math/b;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;Z)F

    move-result v6

    .line 367
    cmpl-float v0, v10, v7

    if-nez v0, :cond_0

    cmpl-float v0, v11, v7

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, v6, v0

    if-eqz v0, :cond_1

    .line 368
    :cond_0
    if-eqz p1, :cond_2

    .line 369
    new-instance v0, Lcom/twitter/ui/widget/CroppableImageView$d;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/ui/widget/CroppableImageView$d;-><init>(Lcom/twitter/ui/widget/CroppableImageView;FFFFF)V

    .line 371
    invoke-virtual {v0}, Lcom/twitter/ui/widget/CroppableImageView$d;->a()V

    .line 377
    :cond_1
    :goto_0
    return-void

    .line 373
    :cond_2
    const/4 v13, 0x0

    move-object v7, p0

    move v8, v2

    move v9, v4

    move v12, v6

    invoke-virtual/range {v7 .. v13}, Lcom/twitter/ui/widget/CroppableImageView;->a(FFFFFI)Z

    .line 374
    invoke-virtual {p0, v10, v11, v6}, Lcom/twitter/ui/widget/CroppableImageView;->a(FFF)V

    goto :goto_0
.end method

.method private getPaddedViewRect()Landroid/graphics/RectF;
    .locals 3

    .prologue
    .line 331
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->b:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 332
    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->o:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 333
    iget v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->n:I

    int-to-float v1, v1

    iget v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->n:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 334
    return-object v0
.end method

.method private setAnimating(Z)V
    .locals 0

    .prologue
    .line 435
    iput-boolean p1, p0, Lcom/twitter/ui/widget/CroppableImageView;->t:Z

    .line 436
    return-void
.end method


# virtual methods
.method public a(IZ)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 223
    iget-boolean v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->t:Z

    if-eqz v0, :cond_0

    .line 234
    :goto_0
    return-void

    .line 226
    :cond_0
    iget v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->d:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->d:I

    .line 227
    if-nez p2, :cond_1

    .line 228
    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    move v4, v3

    move v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/ui/widget/CroppableImageView;->a(FFFFFI)Z

    .line 229
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView;->g()V

    goto :goto_0

    .line 231
    :cond_1
    new-instance v0, Lcom/twitter/ui/widget/CroppableImageView$c;

    invoke-direct {v0, p0, p1}, Lcom/twitter/ui/widget/CroppableImageView$c;-><init>(Lcom/twitter/ui/widget/CroppableImageView;I)V

    .line 232
    invoke-virtual {v0}, Lcom/twitter/ui/widget/CroppableImageView$c;->a()V

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->r:Z

    return v0
.end method

.method protected c()V
    .locals 1

    .prologue
    .line 339
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/ui/widget/CroppableImageView;->a(Z)V

    .line 340
    return-void
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->w:I

    if-eqz v0, :cond_0

    .line 345
    iget v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->w:I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/CroppableImageView;->setRotation(I)V

    .line 346
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->w:I

    .line 348
    :cond_0
    invoke-super {p0}, Lcom/twitter/ui/widget/MultiTouchImageView;->d()V

    .line 349
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const v12, 0x3f2aaaab

    const v11, 0x3eaaaaab

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 285
    invoke-super {p0, p1}, Lcom/twitter/ui/widget/MultiTouchImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 287
    iget-boolean v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->p:Z

    if-nez v0, :cond_1

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    .line 292
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    .line 293
    iget-object v6, p0, Lcom/twitter/ui/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    .line 294
    iget-object v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->l:Landroid/graphics/RectF;

    .line 295
    iget-object v3, p0, Lcom/twitter/ui/widget/CroppableImageView;->k:Landroid/graphics/Paint;

    .line 297
    iget v4, v6, Landroid/graphics/RectF;->top:F

    iget v5, v6, Landroid/graphics/RectF;->left:F

    iget v7, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v9, v4, v5, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 298
    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 299
    int-to-float v4, v0

    iget v5, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v2, v9, v9, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 300
    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 301
    iget v4, v6, Landroid/graphics/RectF;->right:F

    iget v5, v6, Landroid/graphics/RectF;->top:F

    int-to-float v7, v0

    iget v8, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v4, v5, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 302
    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 303
    iget v4, v6, Landroid/graphics/RectF;->bottom:F

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {v2, v9, v4, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 304
    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 306
    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 308
    iget-boolean v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->q:Z

    if-eqz v0, :cond_0

    .line 310
    iget v0, v6, Landroid/graphics/RectF;->left:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v1

    mul-float/2addr v1, v11

    add-float/2addr v1, v0

    .line 311
    iget v0, v6, Landroid/graphics/RectF;->left:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float/2addr v2, v12

    add-float v7, v0, v2

    .line 312
    iget v0, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v11

    add-float v8, v0, v2

    .line 313
    iget v0, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v12

    add-float v9, v0, v2

    .line 314
    iget v2, v6, Landroid/graphics/RectF;->top:F

    iget v4, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, Lcom/twitter/ui/widget/CroppableImageView;->j:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 315
    iget v2, v6, Landroid/graphics/RectF;->top:F

    iget v4, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, Lcom/twitter/ui/widget/CroppableImageView;->j:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v7

    move v3, v7

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 316
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v3, v6, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/twitter/ui/widget/CroppableImageView;->j:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v8

    move v4, v8

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 317
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v3, v6, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/twitter/ui/widget/CroppableImageView;->j:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v9

    move v4, v9

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 320
    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 321
    int-to-float v1, v0

    const v2, 0x3e0ba2e9

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    .line 322
    iget-object v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v1

    iget v4, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, v1

    invoke-virtual {p1, v2, v3, v4, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 323
    iget-object v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->f:Landroid/graphics/Bitmap;

    iget v3, v6, Landroid/graphics/RectF;->right:F

    int-to-float v4, v0

    sub-float/2addr v3, v4

    add-float/2addr v3, v1

    iget v4, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, v1

    invoke-virtual {p1, v2, v3, v4, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 324
    iget-object v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->h:Landroid/graphics/Bitmap;

    iget v3, v6, Landroid/graphics/RectF;->right:F

    int-to-float v4, v0

    sub-float/2addr v3, v4

    add-float/2addr v3, v1

    iget v4, v6, Landroid/graphics/RectF;->bottom:F

    int-to-float v5, v0

    sub-float/2addr v4, v5

    add-float/2addr v4, v1

    invoke-virtual {p1, v2, v3, v4, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 325
    iget-object v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->g:Landroid/graphics/Bitmap;

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v1

    iget v4, v6, Landroid/graphics/RectF;->bottom:F

    int-to-float v0, v0

    sub-float v0, v4, v0

    add-float/2addr v0, v1

    invoke-virtual {p1, v2, v3, v0, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 136
    iget-boolean v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->t:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView;->e()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 180
    :goto_0
    return v0

    .line 139
    :cond_1
    iget-boolean v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->r:Z

    if-nez v2, :cond_2

    .line 140
    invoke-super {p0, p1}, Lcom/twitter/ui/widget/MultiTouchImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 142
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    .line 180
    :cond_3
    invoke-super {p0, p1}, Lcom/twitter/ui/widget/MultiTouchImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 144
    :pswitch_0
    iget-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->m:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 145
    iget-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->m:Landroid/graphics/PointF;

    invoke-direct {p0, v1}, Lcom/twitter/ui/widget/CroppableImageView;->a(Landroid/graphics/PointF;)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->u:I

    .line 146
    iget v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->u:I

    if-eqz v1, :cond_3

    goto :goto_0

    .line 152
    :pswitch_1
    iget v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->u:I

    if-eqz v2, :cond_3

    .line 153
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-direct {v2, v3, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 154
    iget v1, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/twitter/ui/widget/CroppableImageView;->b:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/twitter/ui/widget/CroppableImageView;->b:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    .line 155
    invoke-static {v1, v6, v6, v3, v4}, Lcom/twitter/util/math/b;->a(FFFFF)F

    move-result v1

    iget v3, v2, Landroid/graphics/PointF;->y:F

    iget-object v4, p0, Lcom/twitter/ui/widget/CroppableImageView;->b:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/twitter/ui/widget/CroppableImageView;->b:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    .line 156
    invoke-static {v3, v6, v6, v4, v5}, Lcom/twitter/util/math/b;->a(FFFFF)F

    move-result v3

    .line 154
    invoke-virtual {v2, v1, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 157
    iget v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->u:I

    iget v3, v2, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/twitter/ui/widget/CroppableImageView;->m:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    iget v4, v2, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/twitter/ui/widget/CroppableImageView;->m:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v5

    invoke-direct {p0, v1, v3, v4}, Lcom/twitter/ui/widget/CroppableImageView;->a(IFF)V

    .line 159
    iget-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->s:Lcom/twitter/ui/widget/CroppableImageView$b;

    if-eqz v1, :cond_4

    .line 160
    iget-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->s:Lcom/twitter/ui/widget/CroppableImageView$b;

    invoke-interface {v1}, Lcom/twitter/ui/widget/CroppableImageView$b;->a()V

    .line 162
    :cond_4
    iget-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->m:Landroid/graphics/PointF;

    invoke-virtual {v1, v2}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    goto/16 :goto_0

    .line 169
    :pswitch_2
    iget v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->u:I

    if-eqz v2, :cond_3

    .line 170
    iput v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->u:I

    .line 171
    invoke-direct {p0, v0}, Lcom/twitter/ui/widget/CroppableImageView;->a(Z)V

    goto/16 :goto_0

    .line 142
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setCropAspectRatio(F)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 188
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView;->getImageRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 189
    if-eqz v2, :cond_3

    .line 190
    invoke-direct {p0}, Lcom/twitter/ui/widget/CroppableImageView;->getPaddedViewRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    .line 193
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float/2addr v0, v3

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    move v0, v1

    .line 194
    :goto_0
    if-eqz v0, :cond_2

    .line 196
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v3

    div-float/2addr v3, p1

    sub-float/2addr v0, v3

    div-float/2addr v0, v5

    .line 195
    invoke-virtual {v2, v4, v0}, Landroid/graphics/RectF;->inset(FF)V

    .line 202
    :goto_1
    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 203
    invoke-direct {p0, v1}, Lcom/twitter/ui/widget/CroppableImageView;->a(Z)V

    .line 204
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView;->invalidate()V

    .line 213
    :cond_0
    return-void

    .line 193
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 198
    :cond_2
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v3

    mul-float/2addr v3, p1

    sub-float/2addr v0, v3

    div-float/2addr v0, v5

    invoke-virtual {v2, v0, v4}, Landroid/graphics/RectF;->inset(FF)V

    goto :goto_1

    .line 206
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Crop aspect ratio cannot be set until drawable is ready"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 208
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 209
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    throw v0
.end method

.method public setCropListener(Lcom/twitter/ui/widget/CroppableImageView$b;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/twitter/ui/widget/CroppableImageView;->s:Lcom/twitter/ui/widget/CroppableImageView$b;

    .line 256
    return-void
.end method

.method public setDraggableCorners(Z)V
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->r:Z

    if-eq v0, p1, :cond_0

    .line 260
    iput-boolean p1, p0, Lcom/twitter/ui/widget/CroppableImageView;->r:Z

    .line 261
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView;->invalidate()V

    .line 263
    :cond_0
    return-void
.end method

.method public setRotation(I)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 241
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    iput p1, p0, Lcom/twitter/ui/widget/CroppableImageView;->w:I

    .line 252
    :goto_0
    return-void

    .line 245
    :cond_0
    iget v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->d:I

    sub-int v6, p1, v0

    .line 246
    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    move v4, v3

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/ui/widget/CroppableImageView;->a(FFFFFI)Z

    .line 247
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 248
    int-to-float v1, v6

    iget-object v2, p0, Lcom/twitter/ui/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    iget-object v3, p0, Lcom/twitter/ui/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 249
    iget-object v1, p0, Lcom/twitter/ui/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 250
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView;->g()V

    .line 251
    iput p1, p0, Lcom/twitter/ui/widget/CroppableImageView;->d:I

    goto :goto_0
.end method

.method public setShowCrop(Z)V
    .locals 1

    .prologue
    .line 277
    iget-boolean v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->p:Z

    if-eq v0, p1, :cond_0

    .line 278
    iput-boolean p1, p0, Lcom/twitter/ui/widget/CroppableImageView;->p:Z

    .line 279
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView;->invalidate()V

    .line 281
    :cond_0
    return-void
.end method

.method public setShowGrid(Z)V
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/twitter/ui/widget/CroppableImageView;->q:Z

    if-eq v0, p1, :cond_0

    .line 271
    iput-boolean p1, p0, Lcom/twitter/ui/widget/CroppableImageView;->q:Z

    .line 272
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView;->invalidate()V

    .line 274
    :cond_0
    return-void
.end method
