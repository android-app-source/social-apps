.class final Lcom/twitter/ui/widget/Tooltip$d$a;
.super Landroid/animation/AnimatorListenerAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/ui/widget/Tooltip$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/twitter/ui/widget/Tooltip$d;


# direct methods
.method private constructor <init>(Lcom/twitter/ui/widget/Tooltip$d;)V
    .locals 0

    .prologue
    .line 1139
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 1140
    iput-object p1, p0, Lcom/twitter/ui/widget/Tooltip$d$a;->a:Lcom/twitter/ui/widget/Tooltip$d;

    .line 1141
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/ui/widget/Tooltip$d;Lcom/twitter/ui/widget/Tooltip$1;)V
    .locals 0

    .prologue
    .line 1135
    invoke-direct {p0, p1}, Lcom/twitter/ui/widget/Tooltip$d$a;-><init>(Lcom/twitter/ui/widget/Tooltip$d;)V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1151
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d$a;->a:Lcom/twitter/ui/widget/Tooltip$d;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/Tooltip$d;->b(Lcom/twitter/ui/widget/Tooltip$d;Z)Z

    .line 1152
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d$a;->a:Lcom/twitter/ui/widget/Tooltip$d;

    invoke-static {v0}, Lcom/twitter/ui/widget/Tooltip$d;->a(Lcom/twitter/ui/widget/Tooltip$d;)Landroid/animation/Animator;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1153
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d$a;->a:Lcom/twitter/ui/widget/Tooltip$d;

    invoke-static {v0}, Lcom/twitter/ui/widget/Tooltip$d;->b(Lcom/twitter/ui/widget/Tooltip$d;)V

    .line 1157
    :cond_0
    :goto_0
    return-void

    .line 1154
    :cond_1
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d$a;->a:Lcom/twitter/ui/widget/Tooltip$d;

    invoke-static {v0}, Lcom/twitter/ui/widget/Tooltip$d;->c(Lcom/twitter/ui/widget/Tooltip$d;)Landroid/animation/Animator;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1155
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d$a;->a:Lcom/twitter/ui/widget/Tooltip$d;

    invoke-static {v0}, Lcom/twitter/ui/widget/Tooltip$d;->d(Lcom/twitter/ui/widget/Tooltip$d;)V

    goto :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1145
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d$a;->a:Lcom/twitter/ui/widget/Tooltip$d;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/Tooltip$d;->b(Lcom/twitter/ui/widget/Tooltip$d;Z)Z

    .line 1146
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d$a;->a:Lcom/twitter/ui/widget/Tooltip$d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$d;->setVisibility(I)V

    .line 1147
    return-void
.end method
