.class public Lcom/twitter/ui/widget/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/widget/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/ui/widget/a$a;,
        Lcom/twitter/ui/widget/a$b;
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/graphics/drawable/Drawable;

.field private final c:Landroid/graphics/drawable/Drawable;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private i:I

.field private final j:I

.field private final k:F

.field private final l:Ljava/lang/String;

.field private final m:I

.field private final n:I

.field private o:Landroid/animation/Animator;

.field private p:Lcmh;

.field private q:I

.field private r:I

.field private s:F


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;I)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/twitter/ui/widget/a;->s:F

    .line 53
    iput-object p1, p0, Lcom/twitter/ui/widget/a;->a:Landroid/view/View;

    .line 54
    sget-object v0, Lckh$k;->BadgeIndicator:[I

    invoke-virtual {p2, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 55
    sget v1, Lckh$k;->BadgeIndicator_numberBackground:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/a;->d:I

    .line 56
    sget v1, Lckh$k;->BadgeIndicator_numberColor:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/a;->e:I

    .line 57
    sget v1, Lckh$k;->BadgeIndicator_numberTextSize:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/a;->k:F

    .line 58
    sget v1, Lckh$k;->BadgeIndicator_numberMinHeight:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/a;->g:I

    .line 59
    sget v1, Lckh$k;->BadgeIndicator_numberMinWidth:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/a;->f:I

    .line 60
    sget v1, Lckh$k;->BadgeIndicator_indicatorMarginBottom:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/a;->h:I

    .line 62
    sget v1, Lckh$k;->BadgeIndicator_indicatorDrawable:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/ui/widget/a;->b:Landroid/graphics/drawable/Drawable;

    .line 63
    sget v1, Lckh$k;->BadgeIndicator_circleDrawable:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/ui/widget/a;->c:Landroid/graphics/drawable/Drawable;

    .line 65
    invoke-static {p2}, Lcnh;->a(Landroid/content/Context;)I

    move-result v1

    .line 66
    iget-object v2, p0, Lcom/twitter/ui/widget/a;->c:Landroid/graphics/drawable/Drawable;

    invoke-static {v2, v1}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 67
    iget-object v2, p0, Lcom/twitter/ui/widget/a;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v2, v1}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 69
    sget v1, Lckh$k;->BadgeIndicator_circleMarginTop:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/a;->i:I

    .line 70
    sget v1, Lckh$k;->BadgeIndicator_circleMarginRight:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/a;->j:I

    .line 71
    sget v1, Lckh$k;->BadgeIndicator_badgeMode:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/a;->q:I

    .line 72
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 74
    const-string/jumbo v0, "99+"

    iput-object v0, p0, Lcom/twitter/ui/widget/a;->l:Ljava/lang/String;

    .line 75
    const/16 v0, 0x63

    iput v0, p0, Lcom/twitter/ui/widget/a;->m:I

    .line 76
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lckh$d;->modern_badge_indicator_offset:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/a;->n:I

    .line 77
    return-void
.end method

.method private a(FFJLandroid/view/animation/Interpolator;Landroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;
    .locals 3

    .prologue
    .line 271
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 272
    invoke-virtual {v0, p5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 273
    invoke-virtual {v0, p3, p4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 274
    new-instance v1, Lcom/twitter/ui/widget/a$1;

    invoke-direct {v1, p0}, Lcom/twitter/ui/widget/a$1;-><init>(Lcom/twitter/ui/widget/a;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 280
    invoke-virtual {v0, p6}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 281
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/ui/widget/a;Landroid/animation/Animator;)Landroid/animation/Animator;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/twitter/ui/widget/a;->o:Landroid/animation/Animator;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/ui/widget/a;Lcmh;)Lcmh;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/twitter/ui/widget/a;->p:Lcmh;

    return-object p1
.end method

.method private d()V
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->o:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/widget/a;->o:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->o:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 266
    :cond_0
    return-void
.end method


# virtual methods
.method a(F)V
    .locals 1

    .prologue
    .line 103
    iput p1, p0, Lcom/twitter/ui/widget/a;->s:F

    .line 104
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 105
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 80
    iput p1, p0, Lcom/twitter/ui/widget/a;->i:I

    .line 81
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 174
    iget v0, p0, Lcom/twitter/ui/widget/a;->q:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/ui/widget/a;->q:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->p:Lcmh;

    if-eqz v0, :cond_2

    .line 176
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->o:Landroid/animation/Animator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/ui/widget/a;->o:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 177
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->o:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 179
    :cond_1
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->p:Lcmh;

    invoke-virtual {v0}, Lcmh;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 180
    iget v1, p0, Lcom/twitter/ui/widget/a;->s:F

    .line 181
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->save(I)I

    move-result v2

    .line 182
    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v0

    invoke-virtual {p1, v1, v1, v3, v0}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 183
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->p:Lcmh;

    invoke-virtual {v0, p1}, Lcmh;->draw(Landroid/graphics/Canvas;)V

    .line 184
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 203
    :cond_2
    :goto_0
    return-void

    .line 186
    :cond_3
    iget v0, p0, Lcom/twitter/ui/widget/a;->q:I

    if-ne v0, v2, :cond_4

    .line 187
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/ui/widget/a;->r:I

    if-lez v0, :cond_2

    .line 188
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 190
    :cond_4
    iget v0, p0, Lcom/twitter/ui/widget/a;->q:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 191
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/ui/widget/a;->r:I

    if-eqz v0, :cond_2

    .line 192
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->o:Landroid/animation/Animator;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/ui/widget/a;->o:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_5

    .line 193
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->o:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 195
    :cond_5
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 196
    iget v1, p0, Lcom/twitter/ui/widget/a;->s:F

    .line 197
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->save(I)I

    move-result v2

    .line 198
    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v0

    invoke-virtual {p1, v1, v1, v3, v0}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 199
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 200
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method public a(ZIIIILandroid/graphics/Rect;I)V
    .locals 7

    .prologue
    .line 113
    iget v0, p0, Lcom/twitter/ui/widget/a;->q:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/ui/widget/a;->q:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 114
    :cond_0
    iget-object v2, p0, Lcom/twitter/ui/widget/a;->p:Lcmh;

    .line 115
    if-eqz v2, :cond_1

    .line 116
    invoke-virtual {v2}, Lcmh;->getIntrinsicWidth()I

    move-result v0

    iget v1, p0, Lcom/twitter/ui/widget/a;->f:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 117
    invoke-virtual {v2}, Lcmh;->getIntrinsicHeight()I

    move-result v0

    iget v1, p0, Lcom/twitter/ui/widget/a;->g:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 122
    sub-int v0, p4, p2

    sub-int/2addr v0, v3

    .line 123
    if-eqz p6, :cond_2

    .line 125
    iget v1, p6, Landroid/graphics/Rect;->top:I

    iget v5, p0, Lcom/twitter/ui/widget/a;->n:I

    sub-int/2addr v1, v5

    .line 126
    iget v5, p6, Landroid/graphics/Rect;->right:I

    div-int/lit8 v6, v3, 0x2

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/twitter/ui/widget/a;->f:I

    div-int/lit8 v6, v6, 0x4

    sub-int/2addr v0, v6

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 131
    :goto_0
    add-int/2addr v3, v0

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Lcmh;->setBounds(IIII)V

    .line 171
    :cond_1
    :goto_1
    return-void

    .line 128
    :cond_2
    iget v1, p0, Lcom/twitter/ui/widget/a;->i:I

    goto :goto_0

    .line 133
    :cond_3
    iget v0, p0, Lcom/twitter/ui/widget/a;->q:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 134
    iget-object v1, p0, Lcom/twitter/ui/widget/a;->b:Landroid/graphics/drawable/Drawable;

    .line 135
    if-eqz v1, :cond_1

    .line 136
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    .line 137
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 138
    sub-int v0, p4, p2

    invoke-static {v0, v2}, Lcom/twitter/util/ui/k;->a(II)I

    move-result v4

    .line 140
    iget v0, p0, Lcom/twitter/ui/widget/a;->h:I

    const/4 v5, -0x1

    if-eq v0, v5, :cond_4

    .line 141
    sub-int v0, p5, p3

    iget-object v5, p0, Lcom/twitter/ui/widget/a;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v0, v5

    iget v5, p0, Lcom/twitter/ui/widget/a;->h:I

    sub-int/2addr v0, v5

    sub-int/2addr v0, v3

    .line 147
    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v3, v0

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_1

    .line 144
    :cond_4
    sub-int v0, p5, p7

    iget-object v5, p0, Lcom/twitter/ui/widget/a;->a:Landroid/view/View;

    .line 145
    invoke-virtual {v5}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v0, v5

    .line 144
    invoke-static {v0, v3}, Lcom/twitter/util/ui/k;->a(II)I

    move-result v0

    add-int/2addr v0, p7

    goto :goto_2

    .line 151
    :cond_5
    iget v0, p0, Lcom/twitter/ui/widget/a;->q:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 152
    iget-object v2, p0, Lcom/twitter/ui/widget/a;->c:Landroid/graphics/drawable/Drawable;

    .line 153
    if-eqz v2, :cond_1

    .line 154
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 155
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    .line 158
    sub-int v0, p4, p2

    sub-int/2addr v0, v3

    .line 159
    if-eqz p6, :cond_6

    .line 161
    iget v1, p6, Landroid/graphics/Rect;->top:I

    iget v5, p0, Lcom/twitter/ui/widget/a;->i:I

    add-int/2addr v1, v5

    .line 162
    iget v5, p6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v5, v3

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v5, p0, Lcom/twitter/ui/widget/a;->j:I

    sub-int/2addr v0, v5

    .line 168
    :goto_3
    add-int/2addr v3, v0

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_1

    .line 165
    :cond_6
    iget v1, p0, Lcom/twitter/ui/widget/a;->i:I

    .line 166
    iget v5, p0, Lcom/twitter/ui/widget/a;->j:I

    sub-int/2addr v0, v5

    goto :goto_3
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 89
    iget v0, p0, Lcom/twitter/ui/widget/a;->r:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/twitter/ui/widget/a;->q:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->p:Lcmh;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/widget/a;->b:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/widget/a;->c:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/widget/a;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->b:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/twitter/ui/widget/a;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 100
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/ui/widget/a;->o:Landroid/animation/Animator;

    .line 109
    return-void
.end method

.method public setBadgeMode(I)V
    .locals 0

    .prologue
    .line 85
    iput p1, p0, Lcom/twitter/ui/widget/a;->q:I

    .line 86
    return-void
.end method

.method public setBadgeNumber(I)V
    .locals 12

    .prologue
    const/4 v7, 0x3

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 207
    iget v0, p0, Lcom/twitter/ui/widget/a;->r:I

    if-eq v0, p1, :cond_2

    .line 208
    iget v0, p0, Lcom/twitter/ui/widget/a;->q:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/ui/widget/a;->q:I

    if-ne v0, v7, :cond_7

    .line 209
    :cond_0
    iget v0, p0, Lcom/twitter/ui/widget/a;->r:I

    .line 210
    iput p1, p0, Lcom/twitter/ui/widget/a;->r:I

    .line 211
    invoke-virtual {p0}, Lcom/twitter/ui/widget/a;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 212
    invoke-direct {p0}, Lcom/twitter/ui/widget/a;->d()V

    .line 213
    const-wide/16 v4, 0xc8

    new-instance v6, Landroid/view/animation/AnticipateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/AnticipateInterpolator;-><init>()V

    new-instance v7, Lcom/twitter/ui/widget/a$a;

    invoke-direct {v7, p0}, Lcom/twitter/ui/widget/a$a;-><init>(Lcom/twitter/ui/widget/a;)V

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/twitter/ui/widget/a;->a(FFJLandroid/view/animation/Interpolator;Landroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/a;->o:Landroid/animation/Animator;

    .line 241
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 242
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->refreshDrawableState()V

    .line 243
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 260
    :cond_2
    :goto_1
    return-void

    .line 216
    :cond_3
    iget-object v1, p0, Lcom/twitter/ui/widget/a;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 217
    iget-object v4, p0, Lcom/twitter/ui/widget/a;->p:Lcmh;

    if-nez v4, :cond_4

    .line 218
    new-instance v4, Lcmh;

    invoke-direct {v4, v1}, Lcmh;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/twitter/ui/widget/a;->p:Lcmh;

    .line 219
    iget-object v4, p0, Lcom/twitter/ui/widget/a;->p:Lcmh;

    iget v5, p0, Lcom/twitter/ui/widget/a;->e:I

    invoke-virtual {v4, v5}, Lcmh;->a(I)V

    .line 220
    iget-object v4, p0, Lcom/twitter/ui/widget/a;->p:Lcmh;

    iget v5, p0, Lcom/twitter/ui/widget/a;->k:F

    invoke-virtual {v4, v5}, Lcmh;->a(F)V

    .line 221
    iget v4, p0, Lcom/twitter/ui/widget/a;->d:I

    if-eqz v4, :cond_4

    .line 222
    iget-object v4, p0, Lcom/twitter/ui/widget/a;->p:Lcmh;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget v6, p0, Lcom/twitter/ui/widget/a;->d:I

    .line 223
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 222
    invoke-virtual {v4, v5}, Lcmh;->a(Landroid/graphics/drawable/Drawable;)V

    .line 227
    :cond_4
    iget v4, p0, Lcom/twitter/ui/widget/a;->q:I

    if-ne v4, v7, :cond_5

    .line 228
    iget-object v4, p0, Lcom/twitter/ui/widget/a;->p:Lcmh;

    const-string/jumbo v5, "\u2605"

    invoke-virtual {v4, v1, v5}, Lcmh;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 235
    :goto_2
    if-nez v0, :cond_1

    .line 236
    invoke-direct {p0}, Lcom/twitter/ui/widget/a;->d()V

    .line 237
    const-wide/16 v8, 0xfa

    new-instance v10, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v10}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    new-instance v11, Lcom/twitter/ui/widget/a$b;

    invoke-direct {v11, p0}, Lcom/twitter/ui/widget/a$b;-><init>(Lcom/twitter/ui/widget/a;)V

    move-object v5, p0

    move v6, v3

    move v7, v2

    invoke-direct/range {v5 .. v11}, Lcom/twitter/ui/widget/a;->a(FFJLandroid/view/animation/Interpolator;Landroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/a;->o:Landroid/animation/Animator;

    goto :goto_0

    .line 229
    :cond_5
    iget v4, p0, Lcom/twitter/ui/widget/a;->m:I

    if-gt p1, v4, :cond_6

    .line 230
    iget-object v4, p0, Lcom/twitter/ui/widget/a;->p:Lcmh;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lcmh;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 232
    :cond_6
    iget-object v4, p0, Lcom/twitter/ui/widget/a;->p:Lcmh;

    iget-object v5, p0, Lcom/twitter/ui/widget/a;->l:Ljava/lang/String;

    invoke-virtual {v4, v1, v5}, Lcmh;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 244
    :cond_7
    iget v0, p0, Lcom/twitter/ui/widget/a;->q:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_9

    .line 245
    iput p1, p0, Lcom/twitter/ui/widget/a;->r:I

    .line 246
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_8

    .line 247
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/ui/widget/a;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 249
    :cond_8
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto/16 :goto_1

    .line 251
    :cond_9
    iget v0, p0, Lcom/twitter/ui/widget/a;->q:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 252
    iput p1, p0, Lcom/twitter/ui/widget/a;->r:I

    .line 253
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    .line 254
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/ui/widget/a;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 256
    :cond_a
    iget-object v0, p0, Lcom/twitter/ui/widget/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto/16 :goto_1
.end method
