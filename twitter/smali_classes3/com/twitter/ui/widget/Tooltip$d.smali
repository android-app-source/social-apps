.class final Lcom/twitter/ui/widget/Tooltip$d;
.super Landroid/widget/FrameLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/ui/widget/Tooltip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/ui/widget/Tooltip$d$b;,
        Lcom/twitter/ui/widget/Tooltip$d$a;
    }
.end annotation


# static fields
.field private static final a:Z


# instance fields
.field private A:Landroid/animation/Animator;

.field private B:Z

.field private final C:Landroid/app/Activity;

.field private D:Ljava/lang/Runnable;

.field private final b:Landroid/widget/TextView;

.field private final c:I

.field private final d:I

.field private final e:I

.field private f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:J

.field private l:Z

.field private m:Z

.field private final n:Landroid/view/View;

.field private final o:Landroid/view/ViewGroup;

.field private final p:[I

.field private final q:[I

.field private final r:[I

.field private final s:Landroid/graphics/Path;

.field private final t:Landroid/graphics/Paint;

.field private u:Landroid/graphics/RectF;

.field private final v:I

.field private final w:Landroid/view/WindowManager;

.field private x:Landroid/view/animation/Animation;

.field private y:Landroid/view/animation/Animation;

.field private z:Landroid/animation/Animator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 643
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/ui/widget/Tooltip$d;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/app/Activity;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/CharSequence;II)V
    .locals 7

    .prologue
    const/4 v4, -0x2

    const/4 v6, 0x0

    const/4 v1, 0x2

    const/4 v5, 0x0

    .line 741
    invoke-static {p1, p6}, Lcom/twitter/ui/widget/Tooltip$d;->a(Landroid/content/Context;I)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 674
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->p:[I

    .line 675
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->q:[I

    .line 676
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->r:[I

    .line 677
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->s:Landroid/graphics/Path;

    .line 678
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->t:Landroid/graphics/Paint;

    .line 742
    iput-object p1, p0, Lcom/twitter/ui/widget/Tooltip$d;->C:Landroid/app/Activity;

    .line 743
    iput-object p2, p0, Lcom/twitter/ui/widget/Tooltip$d;->n:Landroid/view/View;

    .line 744
    iput-object p3, p0, Lcom/twitter/ui/widget/Tooltip$d;->o:Landroid/view/ViewGroup;

    .line 746
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lckh$k;->TooltipView:[I

    invoke-virtual {v0, p6, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 748
    sget v1, Lckh$k;->TooltipView_arrowWidth:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->h:I

    .line 749
    sget v1, Lckh$k;->TooltipView_arrowHeight:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->g:I

    .line 750
    sget v1, Lckh$k;->TooltipView_xOffset:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->c:I

    .line 751
    sget v1, Lckh$k;->TooltipView_yOffset:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->d:I

    .line 752
    sget v1, Lckh$k;->TooltipView_screenEdgePadding:I

    .line 753
    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->e:I

    .line 754
    sget v1, Lckh$k;->TooltipView_cornerRadius:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->i:I

    .line 755
    sget v1, Lckh$k;->TooltipView_transitionAnimationDelayMs:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->k:J

    .line 757
    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->t:Landroid/graphics/Paint;

    sget v2, Lckh$k;->TooltipView_tooltipColor:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 759
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->b:Landroid/widget/TextView;

    .line 760
    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->b:Landroid/widget/TextView;

    sget v2, Lckh$k;->TooltipView_textAppearance:I

    .line 761
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 760
    invoke-virtual {v1, p1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 762
    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->b:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v2}, Lcom/twitter/ui/widget/Tooltip$d;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 764
    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->b:Landroid/widget/TextView;

    invoke-virtual {v1, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 766
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 768
    iput p5, p0, Lcom/twitter/ui/widget/Tooltip$d;->j:I

    .line 770
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->t:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 774
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip$d;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 775
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getPaddingTop()I

    move-result v1

    iget v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->g:I

    add-int/2addr v1, v2

    .line 776
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getPaddingRight()I

    move-result v2

    .line 777
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getPaddingBottom()I

    move-result v3

    iget v4, p0, Lcom/twitter/ui/widget/Tooltip$d;->g:I

    add-int/2addr v3, v4

    .line 775
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/ui/widget/Tooltip$d;->setPadding(IIII)V

    .line 783
    :goto_0
    invoke-virtual {p0, v5}, Lcom/twitter/ui/widget/Tooltip$d;->setWillNotDraw(Z)V

    .line 785
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->C:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->w:Landroid/view/WindowManager;

    .line 786
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->C:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->v:I

    .line 788
    sget-boolean v0, Lcom/twitter/ui/widget/Tooltip$d;->a:Z

    if-eqz v0, :cond_0

    .line 789
    sget v0, Lckh$a;->tooltip_transition_in:I

    .line 790
    invoke-static {p1, v0}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    .line 791
    invoke-virtual {v0, p0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 792
    new-instance v1, Lcom/twitter/ui/widget/Tooltip$d$a;

    invoke-direct {v1, p0, v6}, Lcom/twitter/ui/widget/Tooltip$d$a;-><init>(Lcom/twitter/ui/widget/Tooltip$d;Lcom/twitter/ui/widget/Tooltip$1;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 793
    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->z:Landroid/animation/Animator;

    .line 795
    sget v0, Lckh$a;->tooltip_transition_out:I

    .line 796
    invoke-static {p1, v0}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    .line 797
    invoke-virtual {v0, p0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 798
    new-instance v1, Lcom/twitter/ui/widget/Tooltip$d$a;

    invoke-direct {v1, p0, v6}, Lcom/twitter/ui/widget/Tooltip$d$a;-><init>(Lcom/twitter/ui/widget/Tooltip$d;Lcom/twitter/ui/widget/Tooltip$1;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 799
    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->A:Landroid/animation/Animator;

    .line 801
    :cond_0
    return-void

    .line 779
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->g:I

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getPaddingTop()I

    move-result v1

    .line 780
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getPaddingRight()I

    move-result v2

    iget v3, p0, Lcom/twitter/ui/widget/Tooltip$d;->g:I

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getPaddingBottom()I

    move-result v3

    .line 779
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/ui/widget/Tooltip$d;->setPadding(IIII)V

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/app/Activity;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/CharSequence;IILcom/twitter/ui/widget/Tooltip$1;)V
    .locals 0

    .prologue
    .line 639
    invoke-direct/range {p0 .. p6}, Lcom/twitter/ui/widget/Tooltip$d;-><init>(Landroid/app/Activity;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/CharSequence;II)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/ui/widget/Tooltip$d;)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->z:Landroid/animation/Animator;

    return-object v0
.end method

.method private static a(Landroid/content/Context;I)Landroid/content/Context;
    .locals 4

    .prologue
    .line 728
    if-eqz p1, :cond_0

    .line 729
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-direct {v0, p0, p1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 733
    :goto_0
    return-object v0

    .line 731
    :cond_0
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 732
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget v2, Lckh$b;->tooltipStyle:I

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 733
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/ui/widget/Tooltip$d;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
    .locals 0

    .prologue
    .line 639
    iput-object p1, p0, Lcom/twitter/ui/widget/Tooltip$d;->x:Landroid/view/animation/Animation;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/ui/widget/Tooltip$d;Z)V
    .locals 0

    .prologue
    .line 639
    invoke-direct {p0, p1}, Lcom/twitter/ui/widget/Tooltip$d;->a(Z)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/ui/widget/Tooltip$d;ZLjava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 639
    invoke-direct {p0, p1, p2}, Lcom/twitter/ui/widget/Tooltip$d;->a(ZLjava/lang/Runnable;)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 804
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 808
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 809
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 808
    invoke-virtual {p0, v0, v1}, Lcom/twitter/ui/widget/Tooltip$d;->measure(II)V

    .line 810
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip$d;->b()V

    .line 812
    if-eqz p1, :cond_0

    .line 817
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->setVisibility(I)V

    .line 818
    new-instance v0, Lcom/twitter/ui/widget/Tooltip$d$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/ui/widget/Tooltip$d$b;-><init>(Lcom/twitter/ui/widget/Tooltip$d;Lcom/twitter/ui/widget/Tooltip$1;)V

    iget-wide v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->k:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/ui/widget/Tooltip$d;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 820
    :cond_0
    return-void
.end method

.method private a(ZLjava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 707
    iget-boolean v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->m:Z

    if-nez v0, :cond_0

    .line 708
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 709
    iput-object p2, p0, Lcom/twitter/ui/widget/Tooltip$d;->D:Ljava/lang/Runnable;

    .line 711
    if-eqz p1, :cond_2

    .line 712
    sget-boolean v0, Lcom/twitter/ui/widget/Tooltip$d;->a:Z

    if-eqz v0, :cond_1

    .line 713
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->A:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 722
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->m:Z

    .line 724
    :cond_0
    return-void

    .line 715
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->b(Z)Landroid/view/animation/ScaleAnimation;

    move-result-object v0

    .line 716
    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->y:Landroid/view/animation/Animation;

    .line 717
    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 720
    :cond_2
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method private a([I)V
    .locals 1

    .prologue
    .line 995
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->n:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 996
    return-void
.end method

.method static synthetic a()Z
    .locals 1

    .prologue
    .line 639
    sget-boolean v0, Lcom/twitter/ui/widget/Tooltip$d;->a:Z

    return v0
.end method

.method private b(Z)Landroid/view/animation/ScaleAnimation;
    .locals 9

    .prologue
    .line 943
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->q:[I

    invoke-direct {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->a([I)V

    .line 944
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->p:[I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->getLocationInWindow([I)V

    .line 946
    const/4 v1, 0x0

    .line 947
    const/4 v0, 0x0

    .line 948
    iget v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->j:I

    packed-switch v2, :pswitch_data_0

    move v8, v0

    move v6, v1

    .line 975
    :goto_0
    if-eqz p1, :cond_0

    .line 976
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    int-to-float v6, v6

    const/4 v7, 0x0

    int-to-float v8, v8

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 978
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 984
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lckh$h;->tooltip_transition_duration_ms:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 986
    invoke-virtual {v0, p0}, Landroid/view/animation/ScaleAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 987
    return-object v0

    .line 950
    :pswitch_0
    iget v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->f:I

    .line 951
    const/4 v0, 0x0

    move v8, v0

    move v6, v1

    .line 952
    goto :goto_0

    .line 955
    :pswitch_1
    iget v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->f:I

    .line 956
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getHeight()I

    move-result v0

    move v8, v0

    move v6, v1

    .line 957
    goto :goto_0

    .line 960
    :pswitch_2
    const/4 v1, 0x0

    .line 961
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    move v8, v0

    move v6, v1

    .line 962
    goto :goto_0

    .line 965
    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getWidth()I

    move-result v1

    .line 966
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    move v8, v0

    move v6, v1

    .line 967
    goto :goto_0

    .line 980
    :cond_0
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x0

    int-to-float v6, v6

    const/4 v7, 0x0

    int-to-float v8, v8

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 982
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-direct {v1, v2}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    goto :goto_1

    .line 948
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 823
    iget-boolean v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->B:Z

    if-eqz v0, :cond_0

    .line 939
    :goto_0
    return-void

    .line 827
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->l:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->n:Landroid/view/View;

    .line 828
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    if-nez v0, :cond_1

    .line 831
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->requestLayout()V

    goto :goto_0

    .line 834
    :cond_1
    iput-boolean v8, p0, Lcom/twitter/ui/widget/Tooltip$d;->l:Z

    .line 836
    iget-object v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->o:Landroid/view/ViewGroup;

    .line 837
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->q:[I

    invoke-direct {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->a([I)V

    .line 841
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getMeasuredHeight()I

    move-result v3

    .line 842
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getMeasuredWidth()I

    move-result v4

    .line 843
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->q:[I

    aget v0, v0, v9

    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->n:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v5, v0, v1

    .line 848
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip$d;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 853
    div-int/lit8 v0, v4, 0x2

    sub-int v1, v5, v0

    .line 854
    div-int/lit8 v0, v4, 0x2

    add-int v4, v5, v0

    .line 858
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v0

    iget v6, p0, Lcom/twitter/ui/widget/Tooltip$d;->e:I

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 859
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v6

    .line 860
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v2

    iget v7, p0, Lcom/twitter/ui/widget/Tooltip$d;->e:I

    invoke-static {v2, v7}, Ljava/lang/Math;->max(II)I

    move-result v2

    sub-int v2, v6, v2

    .line 865
    if-ge v1, v0, :cond_4

    .line 871
    :goto_1
    iget v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->c:I

    add-int/2addr v1, v0

    .line 873
    iget v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->j:I

    if-ne v0, v8, :cond_5

    .line 874
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->q:[I

    aget v0, v0, v8

    iget-object v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->n:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    iget v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->d:I

    sub-int/2addr v0, v2

    .line 888
    :goto_2
    iget-object v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->p:[I

    invoke-virtual {p0, v2}, Lcom/twitter/ui/widget/Tooltip$d;->getLocationInWindow([I)V

    .line 889
    iget-object v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->p:[I

    aget v2, v2, v9

    sub-int/2addr v1, v2

    .line 890
    iget-object v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->p:[I

    aget v2, v2, v8

    sub-int/2addr v0, v2

    .line 892
    sget-boolean v2, Lcom/twitter/ui/widget/Tooltip$d;->a:Z

    if-eqz v2, :cond_8

    .line 893
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getX()F

    move-result v2

    int-to-float v1, v1

    add-float/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/twitter/ui/widget/Tooltip$d;->setX(F)V

    .line 894
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getY()F

    move-result v1

    int-to-float v0, v0

    add-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->setY(F)V

    .line 901
    :goto_3
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->p:[I

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->getLocationInWindow([I)V

    .line 902
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->p:[I

    aget v0, v0, v9

    sub-int v0, v5, v0

    .line 903
    iget v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->f:I

    sub-int v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 904
    iput v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->f:I

    .line 905
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->invalidate()V

    .line 908
    :cond_2
    sget-boolean v0, Lcom/twitter/ui/widget/Tooltip$d;->a:Z

    if-eqz v0, :cond_3

    .line 909
    iget v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->j:I

    packed-switch v0, :pswitch_data_0

    .line 937
    :cond_3
    :goto_4
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->r:[I

    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->q:[I

    aget v1, v1, v9

    aput v1, v0, v9

    .line 938
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->r:[I

    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->q:[I

    aget v1, v1, v8

    aput v1, v0, v8

    goto/16 :goto_0

    .line 867
    :cond_4
    if-le v4, v2, :cond_9

    .line 868
    sub-int v0, v4, v2

    sub-int v0, v1, v0

    goto :goto_1

    .line 876
    :cond_5
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->q:[I

    aget v0, v0, v8

    iget v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->d:I

    add-int/2addr v0, v2

    sub-int/2addr v0, v3

    goto :goto_2

    .line 879
    :cond_6
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->q:[I

    aget v0, v0, v8

    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->n:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 880
    div-int/lit8 v1, v3, 0x2

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->d:I

    add-int/2addr v0, v1

    .line 882
    iget v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->j:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_7

    .line 883
    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->q:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->n:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->c:I

    sub-int/2addr v1, v2

    goto/16 :goto_2

    .line 885
    :cond_7
    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->q:[I

    aget v1, v1, v9

    iget v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->c:I

    add-int/2addr v1, v2

    sub-int/2addr v1, v4

    goto/16 :goto_2

    .line 896
    :cond_8
    invoke-virtual {p0, v1}, Lcom/twitter/ui/widget/Tooltip$d;->offsetLeftAndRight(I)V

    .line 897
    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->offsetTopAndBottom(I)V

    goto/16 :goto_3

    .line 911
    :pswitch_0
    iget v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->f:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->setPivotX(F)V

    .line 912
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->setPivotY(F)V

    goto :goto_4

    .line 916
    :pswitch_1
    iget v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->f:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->setPivotX(F)V

    .line 917
    invoke-virtual {p0, v10}, Lcom/twitter/ui/widget/Tooltip$d;->setPivotY(F)V

    goto :goto_4

    .line 921
    :pswitch_2
    invoke-virtual {p0, v10}, Lcom/twitter/ui/widget/Tooltip$d;->setPivotX(F)V

    .line 922
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->setPivotY(F)V

    goto/16 :goto_4

    .line 926
    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->setPivotX(F)V

    .line 927
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->setPivotY(F)V

    goto/16 :goto_4

    :cond_9
    move v0, v1

    goto/16 :goto_1

    .line 909
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic b(Lcom/twitter/ui/widget/Tooltip$d;)V
    .locals 0

    .prologue
    .line 639
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip$d;->b()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/ui/widget/Tooltip$d;Z)Z
    .locals 0

    .prologue
    .line 639
    iput-boolean p1, p0, Lcom/twitter/ui/widget/Tooltip$d;->B:Z

    return p1
.end method

.method static synthetic c(Lcom/twitter/ui/widget/Tooltip$d;)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->A:Landroid/animation/Animator;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/ui/widget/Tooltip$d;Z)Landroid/view/animation/ScaleAnimation;
    .locals 1

    .prologue
    .line 639
    invoke-direct {p0, p1}, Lcom/twitter/ui/widget/Tooltip$d;->b(Z)Landroid/view/animation/ScaleAnimation;

    move-result-object v0

    return-object v0
.end method

.method private c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1005
    iget v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->j:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->j:I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/ui/widget/Tooltip$d;)V
    .locals 0

    .prologue
    .line 639
    invoke-static {p0}, Lcom/twitter/ui/widget/Tooltip$d;->f(Lcom/twitter/ui/widget/Tooltip$d;)V

    return-void
.end method

.method static synthetic e(Lcom/twitter/ui/widget/Tooltip$d;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->x:Landroid/view/animation/Animation;

    return-object v0
.end method

.method private static f(Lcom/twitter/ui/widget/Tooltip$d;)V
    .locals 1

    .prologue
    .line 999
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->D:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1000
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->D:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1002
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1102
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->u:Landroid/graphics/RectF;

    .line 1104
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip$d;->b()V

    .line 1105
    return-void
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->y:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    .line 1115
    invoke-static {p0}, Lcom/twitter/ui/widget/Tooltip$d;->f(Lcom/twitter/ui/widget/Tooltip$d;)V

    .line 1119
    :cond_0
    :goto_0
    return-void

    .line 1116
    :cond_1
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->x:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    .line 1117
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip$d;->b()V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1123
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 1

    .prologue
    .line 1109
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->setVisibility(I)V

    .line 1110
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 1037
    iget-boolean v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->l:Z

    if-nez v0, :cond_0

    .line 1099
    :goto_0
    return-void

    .line 1041
    :cond_0
    iget v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->h:I

    .line 1042
    iget v2, p0, Lcom/twitter/ui/widget/Tooltip$d;->g:I

    .line 1043
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getWidth()I

    move-result v3

    .line 1044
    invoke-virtual {p0}, Lcom/twitter/ui/widget/Tooltip$d;->getHeight()I

    move-result v4

    .line 1045
    iget-object v5, p0, Lcom/twitter/ui/widget/Tooltip$d;->t:Landroid/graphics/Paint;

    .line 1048
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->u:Landroid/graphics/RectF;

    if-nez v0, :cond_2

    .line 1049
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 1050
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip$d;->c()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1051
    int-to-float v6, v2

    int-to-float v7, v3

    sub-int v8, v4, v2

    int-to-float v8, v8

    invoke-virtual {v0, v10, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1055
    :goto_1
    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->u:Landroid/graphics/RectF;

    .line 1060
    :goto_2
    iget v6, p0, Lcom/twitter/ui/widget/Tooltip$d;->i:I

    .line 1061
    int-to-float v7, v6

    int-to-float v6, v6

    invoke-virtual {p1, v0, v7, v6, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 1063
    iget-object v6, p0, Lcom/twitter/ui/widget/Tooltip$d;->s:Landroid/graphics/Path;

    .line 1064
    invoke-virtual {v6}, Landroid/graphics/Path;->rewind()V

    .line 1068
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip$d;->c()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1069
    iget v3, p0, Lcom/twitter/ui/widget/Tooltip$d;->f:I

    .line 1070
    div-int/lit8 v7, v1, 0x2

    sub-int v7, v3, v7

    .line 1071
    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v3

    .line 1072
    iget v8, p0, Lcom/twitter/ui/widget/Tooltip$d;->j:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_3

    .line 1073
    int-to-float v0, v7

    int-to-float v4, v2

    invoke-virtual {v6, v0, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1074
    int-to-float v0, v3

    invoke-virtual {v6, v0, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1075
    int-to-float v0, v1

    int-to-float v1, v2

    invoke-virtual {v6, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1097
    :goto_3
    invoke-virtual {v6}, Landroid/graphics/Path;->close()V

    .line 1098
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->s:Landroid/graphics/Path;

    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 1053
    :cond_1
    int-to-float v6, v2

    sub-int v7, v3, v2

    int-to-float v7, v7

    int-to-float v8, v4

    invoke-virtual {v0, v6, v10, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_1

    .line 1057
    :cond_2
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->u:Landroid/graphics/RectF;

    goto :goto_2

    .line 1077
    :cond_3
    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    .line 1078
    int-to-float v2, v7

    int-to-float v7, v0

    invoke-virtual {v6, v2, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1079
    int-to-float v2, v3

    int-to-float v3, v4

    invoke-virtual {v6, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1080
    int-to-float v1, v1

    int-to-float v0, v0

    invoke-virtual {v6, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_3

    .line 1083
    :cond_4
    sub-int/2addr v4, v1

    div-int/lit8 v4, v4, 0x2

    .line 1084
    add-int v7, v4, v1

    .line 1085
    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v4

    .line 1086
    iget v8, p0, Lcom/twitter/ui/widget/Tooltip$d;->j:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_5

    .line 1087
    int-to-float v0, v2

    int-to-float v3, v4

    invoke-virtual {v6, v0, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1088
    int-to-float v0, v2

    int-to-float v2, v7

    invoke-virtual {v6, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1089
    int-to-float v0, v1

    invoke-virtual {v6, v10, v0}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_3

    .line 1091
    :cond_5
    iget v0, v0, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    .line 1092
    int-to-float v2, v0

    int-to-float v4, v4

    invoke-virtual {v6, v2, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1093
    int-to-float v2, v3

    int-to-float v1, v1

    invoke-virtual {v6, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1094
    int-to-float v0, v0

    int-to-float v1, v7

    invoke-virtual {v6, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_3
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 700
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 701
    iget-boolean v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->l:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    .line 702
    :cond_0
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip$d;->b()V

    .line 704
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    const v2, 0x3f666666    # 0.9f

    .line 1011
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->w:Landroid/view/WindowManager;

    invoke-static {v0}, Lcom/twitter/util/ui/k;->a(Landroid/view/WindowManager;)Landroid/graphics/Point;

    move-result-object v0

    .line 1014
    iget v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->v:I

    packed-switch v1, :pswitch_data_0

    .line 1020
    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 1025
    :goto_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 1026
    if-nez v1, :cond_0

    .line 1027
    const/high16 v1, -0x80000000

    move v3, v1

    move v1, v0

    move v0, v3

    .line 1032
    :goto_1
    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v0, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 1033
    return-void

    .line 1016
    :pswitch_0
    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 1017
    goto :goto_0

    .line 1029
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v3, v1

    move v1, v0

    move v0, v3

    goto :goto_1

    .line 1014
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onPreDraw()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1127
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->q:[I

    invoke-direct {p0, v0}, Lcom/twitter/ui/widget/Tooltip$d;->a([I)V

    .line 1128
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->q:[I

    aget v0, v0, v3

    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->r:[I

    aget v1, v1, v3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d;->q:[I

    aget v0, v0, v2

    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$d;->r:[I

    aget v1, v1, v2

    if-eq v0, v1, :cond_1

    .line 1130
    :cond_0
    invoke-direct {p0}, Lcom/twitter/ui/widget/Tooltip$d;->b()V

    .line 1132
    :cond_1
    return v2
.end method
