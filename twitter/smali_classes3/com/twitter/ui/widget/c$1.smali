.class Lcom/twitter/ui/widget/c$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/ui/widget/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/ui/widget/c;


# direct methods
.method constructor <init>(Lcom/twitter/ui/widget/c;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/twitter/ui/widget/c$1;->a:Lcom/twitter/ui/widget/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/twitter/ui/widget/c$1;->a:Lcom/twitter/ui/widget/c;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/c;->invalidateSelf()V

    .line 115
    return-void
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/twitter/ui/widget/c$1;->a:Lcom/twitter/ui/widget/c;

    invoke-virtual {v0, p2, p3, p4}, Lcom/twitter/ui/widget/c;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 120
    return-void
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/twitter/ui/widget/c$1;->a:Lcom/twitter/ui/widget/c;

    invoke-virtual {v0, p2}, Lcom/twitter/ui/widget/c;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 125
    return-void
.end method
