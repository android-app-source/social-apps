.class Lcom/twitter/ui/widget/CroppableImageView$d;
.super Lcom/twitter/ui/widget/CroppableImageView$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/ui/widget/CroppableImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# instance fields
.field private a:F

.field private b:F

.field private c:F


# direct methods
.method constructor <init>(Lcom/twitter/ui/widget/CroppableImageView;FFFFF)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 498
    invoke-direct {p0, p1}, Lcom/twitter/ui/widget/CroppableImageView$a;-><init>(Lcom/twitter/ui/widget/CroppableImageView;)V

    .line 499
    iput p2, p0, Lcom/twitter/ui/widget/CroppableImageView$d;->a:F

    .line 500
    iput p4, p0, Lcom/twitter/ui/widget/CroppableImageView$d;->b:F

    .line 501
    invoke-virtual {p1}, Lcom/twitter/ui/widget/CroppableImageView;->getActiveRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 502
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    .line 503
    mul-float v1, v0, p6

    .line 504
    iput v0, p0, Lcom/twitter/ui/widget/CroppableImageView$d;->c:F

    .line 506
    const-string/jumbo v2, "x"

    new-array v3, v8, [F

    aput p2, v3, v6

    aput p3, v3, v7

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 507
    const-string/jumbo v3, "y"

    new-array v4, v8, [F

    aput p4, v4, v6

    aput p5, v4, v7

    invoke-static {v3, v4}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 508
    const-string/jumbo v4, "width"

    new-array v5, v8, [F

    aput v0, v5, v6

    aput v1, v5, v7

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 509
    const/4 v1, 0x3

    new-array v1, v1, [Landroid/animation/PropertyValuesHolder;

    aput-object v2, v1, v6

    aput-object v3, v1, v7

    aput-object v0, v1, v8

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 511
    invoke-super {p0, v0}, Lcom/twitter/ui/widget/CroppableImageView$a;->a(Landroid/animation/ValueAnimator;)V

    .line 512
    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 10

    .prologue
    .line 516
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CroppableImageView$d;->b()Lcom/twitter/ui/widget/CroppableImageView;

    move-result-object v0

    .line 517
    if-eqz v0, :cond_0

    .line 518
    const-string/jumbo v1, "x"

    invoke-virtual {p1, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 519
    const-string/jumbo v1, "y"

    invoke-virtual {p1, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 520
    const-string/jumbo v1, "width"

    invoke-virtual {p1, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 522
    iget v1, p0, Lcom/twitter/ui/widget/CroppableImageView$d;->a:F

    sub-float v3, v7, v1

    .line 523
    iget v1, p0, Lcom/twitter/ui/widget/CroppableImageView$d;->b:F

    sub-float v4, v8, v1

    .line 524
    iget v1, p0, Lcom/twitter/ui/widget/CroppableImageView$d;->c:F

    div-float v5, v9, v1

    .line 526
    invoke-virtual {v0}, Lcom/twitter/ui/widget/CroppableImageView;->getActiveRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 527
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/ui/widget/CroppableImageView;->a(FFFFFI)Z

    .line 528
    invoke-virtual {v0, v3, v4, v5}, Lcom/twitter/ui/widget/CroppableImageView;->a(FFF)V

    .line 530
    iput v7, p0, Lcom/twitter/ui/widget/CroppableImageView$d;->a:F

    .line 531
    iput v8, p0, Lcom/twitter/ui/widget/CroppableImageView$d;->b:F

    .line 532
    iput v9, p0, Lcom/twitter/ui/widget/CroppableImageView$d;->c:F

    .line 534
    :cond_0
    return-void
.end method
