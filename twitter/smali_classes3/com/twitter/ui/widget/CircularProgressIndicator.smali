.class public Lcom/twitter/ui/widget/CircularProgressIndicator;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static final a:Landroid/view/animation/Interpolator;


# instance fields
.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/RectF;

.field private final d:F

.field private final e:Landroid/animation/ObjectAnimator;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/twitter/ui/widget/CircularProgressIndicator;->a:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/ui/widget/CircularProgressIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    sget v0, Lckh$c;->deep_gray:I

    invoke-static {p1, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 49
    if-eqz p2, :cond_0

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lckh$k;->CircularProgressIndicator:[I

    const/4 v3, 0x0

    invoke-virtual {v1, p2, v2, p3, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 52
    sget v2, Lckh$k;->CircularProgressIndicator_foregroundColor:I

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 53
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 58
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CircularProgressIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lckh$d;->circular_progress_indicator_stroke_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->d:F

    .line 60
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->b:Landroid/graphics/Paint;

    .line 61
    iget-object v1, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 62
    iget-object v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->b:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 63
    iget-object v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->b:Landroid/graphics/Paint;

    iget v1, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->d:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 64
    iget-object v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 65
    iget-object v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 67
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->c:Landroid/graphics/RectF;

    .line 69
    const-string/jumbo v0, "progress"

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->e:Landroid/animation/ObjectAnimator;

    .line 70
    iget-object v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->e:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 71
    iget-object v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->e:Landroid/animation/ObjectAnimator;

    sget-object v1, Lcom/twitter/ui/widget/CircularProgressIndicator;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 72
    return-void

    .line 69
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->f:I

    if-eqz v0, :cond_0

    .line 117
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->f:I

    .line 118
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CircularProgressIndicator;->invalidate()V

    .line 120
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const v3, 0x461c4000    # 10000.0f

    .line 88
    iget-object v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->e:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 90
    if-gez p1, :cond_0

    int-to-float v0, p1

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_2

    :cond_0
    move v0, v2

    .line 92
    :goto_0
    if-nez v0, :cond_3

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid progress value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". Progress should be between 0 and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " inclusive."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 102
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 90
    goto :goto_0

    .line 98
    :cond_3
    iget v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->f:I

    if-le p1, v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->e:Landroid/animation/ObjectAnimator;

    const/4 v3, 0x2

    new-array v3, v3, [I

    iget v4, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->f:I

    aput v4, v3, v1

    aput p1, v3, v2

    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 100
    iget-object v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->e:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 124
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 126
    iget v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->d:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 127
    iget-object v1, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->c:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/twitter/ui/widget/CircularProgressIndicator;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, v0

    invoke-virtual {p0}, Lcom/twitter/ui/widget/CircularProgressIndicator;->getWidth()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v0

    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 129
    const/high16 v0, 0x43b40000    # 360.0f

    iget v1, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->f:I

    int-to-float v1, v1

    const v2, 0x461c4000    # 10000.0f

    div-float/2addr v1, v2

    mul-float v3, v0, v1

    .line 130
    iget-object v1, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->c:Landroid/graphics/RectF;

    const/high16 v2, 0x43870000    # 270.0f

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 131
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 76
    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    .line 77
    return-void
.end method

.method setProgress(I)V
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->f:I

    if-le p1, v0, :cond_0

    .line 110
    iput p1, p0, Lcom/twitter/ui/widget/CircularProgressIndicator;->f:I

    .line 111
    invoke-virtual {p0}, Lcom/twitter/ui/widget/CircularProgressIndicator;->invalidate()V

    .line 113
    :cond_0
    return-void
.end method
