.class Lcom/twitter/ui/widget/Tooltip$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/ui/widget/Tooltip;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Landroid/os/Bundle;

.field final synthetic d:Lcom/twitter/ui/widget/Tooltip;


# direct methods
.method constructor <init>(Lcom/twitter/ui/widget/Tooltip;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 518
    iput-object p1, p0, Lcom/twitter/ui/widget/Tooltip$2;->d:Lcom/twitter/ui/widget/Tooltip;

    iput p2, p0, Lcom/twitter/ui/widget/Tooltip$2;->a:I

    iput-object p3, p0, Lcom/twitter/ui/widget/Tooltip$2;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/ui/widget/Tooltip$2;->c:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .prologue
    .line 521
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$2;->d:Lcom/twitter/ui/widget/Tooltip;

    invoke-static {v0}, Lcom/twitter/ui/widget/Tooltip;->c(Lcom/twitter/ui/widget/Tooltip;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 522
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 523
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$2;->d:Lcom/twitter/ui/widget/Tooltip;

    iget v1, p0, Lcom/twitter/ui/widget/Tooltip$2;->a:I

    iget-object v2, p0, Lcom/twitter/ui/widget/Tooltip$2;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/ui/widget/Tooltip;->a(Lcom/twitter/ui/widget/Tooltip;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 524
    if-eqz v0, :cond_0

    .line 525
    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$2;->d:Lcom/twitter/ui/widget/Tooltip;

    iget-object v2, p0, Lcom/twitter/ui/widget/Tooltip$2;->c:Landroid/os/Bundle;

    invoke-static {v1, v0, v2}, Lcom/twitter/ui/widget/Tooltip;->a(Lcom/twitter/ui/widget/Tooltip;Landroid/view/View;Landroid/os/Bundle;)V

    .line 533
    :goto_0
    return-void

    .line 527
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$2;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "fragmentTag"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 528
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/twitter/ui/widget/Tooltip$2;->d:Lcom/twitter/ui/widget/Tooltip;

    invoke-static {v3}, Lcom/twitter/ui/widget/Tooltip;->c(Lcom/twitter/ui/widget/Tooltip;)Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " cannot find tooltip target view: id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/twitter/ui/widget/Tooltip$2;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " targetView tag="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/ui/widget/Tooltip$2;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " fragment tag="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
