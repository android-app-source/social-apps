.class final Lcom/twitter/ui/widget/Tooltip$d$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/ui/widget/Tooltip$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/ui/widget/Tooltip$d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/twitter/ui/widget/Tooltip$d;)V
    .locals 1

    .prologue
    .line 1164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d$b;->a:Ljava/lang/ref/WeakReference;

    .line 1166
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/ui/widget/Tooltip$d;Lcom/twitter/ui/widget/Tooltip$1;)V
    .locals 0

    .prologue
    .line 1160
    invoke-direct {p0, p1}, Lcom/twitter/ui/widget/Tooltip$d$b;-><init>(Lcom/twitter/ui/widget/Tooltip$d;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$d$b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/Tooltip$d;

    .line 1171
    if-eqz v0, :cond_0

    .line 1172
    invoke-static {}, Lcom/twitter/ui/widget/Tooltip$d;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1173
    invoke-static {v0}, Lcom/twitter/ui/widget/Tooltip$d;->a(Lcom/twitter/ui/widget/Tooltip$d;)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 1180
    :cond_0
    :goto_0
    return-void

    .line 1175
    :cond_1
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/Tooltip$d;->c(Lcom/twitter/ui/widget/Tooltip$d;Z)Landroid/view/animation/ScaleAnimation;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/Tooltip$d;->a(Lcom/twitter/ui/widget/Tooltip$d;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    .line 1176
    invoke-static {v0}, Lcom/twitter/ui/widget/Tooltip$d;->e(Lcom/twitter/ui/widget/Tooltip$d;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$d;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1177
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$d;->setVisibility(I)V

    goto :goto_0
.end method
