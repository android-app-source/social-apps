.class public Lcom/twitter/ui/widget/Tooltip$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/ui/widget/Tooltip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private b:I

.field private c:I

.field private d:Lcom/twitter/ui/widget/Tooltip$c;

.field private e:Ljava/lang/CharSequence;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:I

.field private i:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    iput-object p1, p0, Lcom/twitter/ui/widget/Tooltip$a;->a:Landroid/content/Context;

    .line 162
    iput p2, p0, Lcom/twitter/ui/widget/Tooltip$a;->f:I

    .line 163
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;ILcom/twitter/ui/widget/Tooltip$1;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Lcom/twitter/ui/widget/Tooltip$a;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    iput-object p1, p0, Lcom/twitter/ui/widget/Tooltip$a;->a:Landroid/content/Context;

    .line 167
    iput-object p2, p0, Lcom/twitter/ui/widget/Tooltip$a;->g:Ljava/lang/String;

    .line 168
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/ui/widget/Tooltip$1;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Lcom/twitter/ui/widget/Tooltip$a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(I)Lcom/twitter/ui/widget/Tooltip$a;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/twitter/ui/widget/Tooltip$a;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/Tooltip$a;->e:Ljava/lang/CharSequence;

    .line 179
    return-object p0
.end method

.method public a(Lcom/twitter/ui/widget/Tooltip$c;)Lcom/twitter/ui/widget/Tooltip$a;
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/twitter/ui/widget/Tooltip$a;->d:Lcom/twitter/ui/widget/Tooltip$c;

    .line 242
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/twitter/ui/widget/Tooltip$a;
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/twitter/ui/widget/Tooltip$a;->e:Ljava/lang/CharSequence;

    .line 191
    return-object p0
.end method

.method public a(Z)Lcom/twitter/ui/widget/Tooltip$a;
    .locals 0

    .prologue
    .line 273
    iput-boolean p1, p0, Lcom/twitter/ui/widget/Tooltip$a;->i:Z

    .line 274
    return-object p0
.end method

.method public a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;
    .locals 1

    .prologue
    .line 291
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Z)Lcom/twitter/ui/widget/Tooltip;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Z)Lcom/twitter/ui/widget/Tooltip;
    .locals 10

    .prologue
    .line 311
    invoke-virtual {p1, p2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 312
    instance-of v1, v0, Lcom/twitter/ui/widget/Tooltip;

    if-eqz v1, :cond_0

    .line 313
    check-cast v0, Lcom/twitter/ui/widget/Tooltip;

    .line 314
    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$a;->d:Lcom/twitter/ui/widget/Tooltip$c;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip;->a(Lcom/twitter/ui/widget/Tooltip$c;)V

    .line 327
    :goto_0
    return-object v0

    .line 316
    :cond_0
    iget v0, p0, Lcom/twitter/ui/widget/Tooltip$a;->f:I

    iget-object v1, p0, Lcom/twitter/ui/widget/Tooltip$a;->g:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/ui/widget/Tooltip$a;->h:I

    iget-object v3, p0, Lcom/twitter/ui/widget/Tooltip$a;->e:Ljava/lang/CharSequence;

    iget v4, p0, Lcom/twitter/ui/widget/Tooltip$a;->b:I

    iget v5, p0, Lcom/twitter/ui/widget/Tooltip$a;->c:I

    iget-object v6, p0, Lcom/twitter/ui/widget/Tooltip$a;->d:Lcom/twitter/ui/widget/Tooltip$c;

    iget-boolean v7, p0, Lcom/twitter/ui/widget/Tooltip$a;->i:Z

    move v8, p3

    move-object v9, p2

    invoke-static/range {v0 .. v9}, Lcom/twitter/ui/widget/Tooltip;->a(ILjava/lang/String;ILjava/lang/CharSequence;IILcom/twitter/ui/widget/Tooltip$c;ZZLjava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    move-result-object v0

    .line 322
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 323
    invoke-virtual {v1, v0, p2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 324
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method public b(I)Lcom/twitter/ui/widget/Tooltip$a;
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 203
    iput p1, p0, Lcom/twitter/ui/widget/Tooltip$a;->c:I

    .line 204
    return-object p0
.end method

.method public c(I)Lcom/twitter/ui/widget/Tooltip$a;
    .locals 2

    .prologue
    .line 220
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 222
    :cond_0
    iput p1, p0, Lcom/twitter/ui/widget/Tooltip$a;->b:I

    .line 227
    return-object p0

    .line 224
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "arrowDirection must be one of the Tooltip.POINTING_* constants"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d(I)Lcom/twitter/ui/widget/Tooltip$a;
    .locals 0

    .prologue
    .line 256
    iput p1, p0, Lcom/twitter/ui/widget/Tooltip$a;->h:I

    .line 257
    return-object p0
.end method
