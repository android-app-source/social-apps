.class Lcom/twitter/ui/widget/c$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/ui/widget/c;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/ui/widget/c$b;

.field final synthetic b:Lcom/twitter/ui/widget/c;


# direct methods
.method constructor <init>(Lcom/twitter/ui/widget/c;Lcom/twitter/ui/widget/c$b;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/twitter/ui/widget/c$3;->b:Lcom/twitter/ui/widget/c;

    iput-object p2, p0, Lcom/twitter/ui/widget/c$3;->a:Lcom/twitter/ui/widget/c$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Lcom/twitter/ui/widget/c$3;->b:Lcom/twitter/ui/widget/c;

    invoke-static {v0}, Lcom/twitter/ui/widget/c;->a(Lcom/twitter/ui/widget/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/twitter/ui/widget/c$3;->a:Lcom/twitter/ui/widget/c$b;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/c$b;->a()V

    .line 354
    iget-object v0, p0, Lcom/twitter/ui/widget/c$3;->a:Lcom/twitter/ui/widget/c$b;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/c$b;->j()V

    .line 355
    iget-object v0, p0, Lcom/twitter/ui/widget/c$3;->a:Lcom/twitter/ui/widget/c$b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/c$b;->a(Z)V

    .line 356
    iget-object v0, p0, Lcom/twitter/ui/widget/c$3;->b:Lcom/twitter/ui/widget/c;

    invoke-static {v0}, Lcom/twitter/ui/widget/c;->c(Lcom/twitter/ui/widget/c;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/ui/widget/c$3;->b:Lcom/twitter/ui/widget/c;

    invoke-static {v1}, Lcom/twitter/ui/widget/c;->b(Lcom/twitter/ui/widget/c;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 358
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 362
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 348
    return-void
.end method
