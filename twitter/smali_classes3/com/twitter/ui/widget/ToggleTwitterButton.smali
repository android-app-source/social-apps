.class public Lcom/twitter/ui/widget/ToggleTwitterButton;
.super Lcom/twitter/ui/widget/TwitterButton;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/ui/widget/ToggleTwitterButton$SavedState;
    }
.end annotation


# instance fields
.field private a:Z

.field private final g:I

.field private final h:I

.field private final i:Z

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Landroid/graphics/Bitmap;

.field private o:Z

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 49
    sget v0, Lckh$b;->buttonStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/ui/widget/TwitterButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput-boolean v2, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->a:Z

    .line 42
    iput-boolean v2, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->p:Z

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v3, Lckh$k;->ToggleTwitterButton:[I

    invoke-virtual {v0, p2, v3, p3, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 57
    sget v0, Lckh$k;->ToggleTwitterButton_styleIdOn:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->g:I

    .line 58
    sget v0, Lckh$k;->ToggleTwitterButton_styleIdOff:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->h:I

    .line 59
    sget v0, Lckh$k;->ToggleTwitterButton_shouldToggleOnClick:I

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->i:Z

    .line 61
    sget v0, Lckh$k;->ToggleTwitterButton_textIdOn:I

    invoke-direct {p0, v3, v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->a(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->j:Ljava/lang/String;

    .line 62
    sget v0, Lckh$k;->ToggleTwitterButton_textIdOff:I

    invoke-direct {p0, v3, v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->a(Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->k:Ljava/lang/String;

    .line 64
    sget v0, Lckh$k;->ToggleTwitterButton_toggleIconCanBeFlipped:I

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->o:Z

    .line 65
    sget v0, Lckh$k;->ToggleTwitterButton_nodpiBaseToggleIconName:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->m:Ljava/lang/String;

    .line 66
    iget-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->m:Ljava/lang/String;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->l:Z

    .line 68
    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->l:Z

    if-eqz v0, :cond_0

    .line 69
    sget v0, Lckh$k;->ToggleTwitterButton_toggleIconSize:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 70
    iget-object v4, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->m:Ljava/lang/String;

    invoke-virtual {p0, v4, v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->a(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->n:Landroid/graphics/Bitmap;

    .line 71
    iget-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->n:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    :goto_1
    iput-boolean v1, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->l:Z

    .line 74
    :cond_0
    sget v0, Lckh$k;->ToggleTwitterButton_initOn:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 76
    if-eqz v1, :cond_4

    iget v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->g:I

    :goto_2
    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setButtonAppearance(I)V

    .line 78
    invoke-direct {p0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->j:Ljava/lang/String;

    :goto_3
    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setText(Ljava/lang/CharSequence;)V

    .line 82
    :cond_1
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 83
    return-void

    :cond_2
    move v0, v2

    .line 66
    goto :goto_0

    :cond_3
    move v1, v2

    .line 71
    goto :goto_1

    .line 76
    :cond_4
    iget v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->h:I

    goto :goto_2

    .line 79
    :cond_5
    iget-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->k:Ljava/lang/String;

    goto :goto_3
.end method

.method private a(Landroid/content/res/TypedArray;I)Ljava/lang/String;
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/StyleableRes;
        .end annotation
    .end param

    .prologue
    .line 87
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->l:Z

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/twitter/ui/widget/TwitterButton;->a()Z

    move-result v0

    goto :goto_0
.end method

.method b()V
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->a:Z

    if-eqz v0, :cond_3

    .line 109
    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->n:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->n:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->n:Landroid/graphics/Bitmap;

    .line 114
    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->p:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->p:Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 116
    :cond_3
    invoke-super {p0}, Lcom/twitter/ui/widget/TwitterButton;->b()V

    goto :goto_0
.end method

.method c()Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->p:Z

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/twitter/ui/widget/TwitterButton;->c()Z

    move-result v0

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setToggledOn(Z)V

    .line 146
    return-void

    .line 145
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->a:Z

    return v0
.end method

.method getIcon()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->n:Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/twitter/ui/widget/TwitterButton;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 160
    check-cast p1, Lcom/twitter/ui/widget/ToggleTwitterButton$SavedState;

    .line 161
    invoke-virtual {p1}, Lcom/twitter/ui/widget/ToggleTwitterButton$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/twitter/ui/widget/TwitterButton;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 162
    iget-boolean v0, p1, Lcom/twitter/ui/widget/ToggleTwitterButton$SavedState;->a:Z

    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setToggledOn(Z)V

    .line 163
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 155
    new-instance v0, Lcom/twitter/ui/widget/ToggleTwitterButton$SavedState;

    invoke-super {p0}, Lcom/twitter/ui/widget/TwitterButton;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->a:Z

    invoke-direct {v0, v1, v2}, Lcom/twitter/ui/widget/ToggleTwitterButton$SavedState;-><init>(Landroid/os/Parcelable;Z)V

    return-object v0
.end method

.method public performClick()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->i:Z

    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {p0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->d()V

    .line 130
    :cond_0
    invoke-super {p0}, Lcom/twitter/ui/widget/TwitterButton;->performClick()Z

    move-result v0

    return v0
.end method

.method public setToggledOn(Z)V
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->a:Z

    if-eq v0, p1, :cond_1

    .line 135
    iput-boolean p1, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->a:Z

    .line 136
    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->a:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->g:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setButtonAppearance(I)V

    .line 137
    invoke-direct {p0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-boolean v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->a:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->j:Ljava/lang/String;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setText(Ljava/lang/CharSequence;)V

    .line 140
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/ui/widget/ToggleTwitterButton;->requestLayout()V

    .line 142
    :cond_1
    return-void

    .line 136
    :cond_2
    iget v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->h:I

    goto :goto_0

    .line 138
    :cond_3
    iget-object v0, p0, Lcom/twitter/ui/widget/ToggleTwitterButton;->k:Ljava/lang/String;

    goto :goto_1
.end method
