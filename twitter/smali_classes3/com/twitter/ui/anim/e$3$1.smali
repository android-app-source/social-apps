.class Lcom/twitter/ui/anim/e$3$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/ui/anim/e$3;->a(Lrx/i;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lrx/f$a;

.field final synthetic b:Lrx/i;

.field final synthetic c:Lcom/twitter/ui/anim/e$3;


# direct methods
.method constructor <init>(Lcom/twitter/ui/anim/e$3;Lrx/f$a;Lrx/i;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/twitter/ui/anim/e$3$1;->c:Lcom/twitter/ui/anim/e$3;

    iput-object p2, p0, Lcom/twitter/ui/anim/e$3$1;->a:Lrx/f$a;

    iput-object p3, p0, Lcom/twitter/ui/anim/e$3$1;->b:Lrx/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/twitter/ui/anim/e$3$1;->c:Lcom/twitter/ui/anim/e$3;

    iget-object v0, v0, Lcom/twitter/ui/anim/e$3;->b:Lcom/twitter/ui/anim/e;

    invoke-static {v0}, Lcom/twitter/ui/anim/e;->b(Lcom/twitter/ui/anim/e;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/anim/e$3$1;->c:Lcom/twitter/ui/anim/e$3;

    iget-object v0, v0, Lcom/twitter/ui/anim/e$3;->b:Lcom/twitter/ui/anim/e;

    iget-object v0, v0, Lcom/twitter/ui/anim/e;->b:Lcom/twitter/ui/anim/e$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/anim/e$3$1;->c:Lcom/twitter/ui/anim/e$3;

    iget-object v0, v0, Lcom/twitter/ui/anim/e$3;->b:Lcom/twitter/ui/anim/e;

    iget-object v0, v0, Lcom/twitter/ui/anim/e;->c:Ljava/util/List;

    if-nez v0, :cond_1

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/anim/e$3$1;->a:Lrx/f$a;

    invoke-virtual {v0}, Lrx/f$a;->B_()V

    .line 189
    iget-object v0, p0, Lcom/twitter/ui/anim/e$3$1;->c:Lcom/twitter/ui/anim/e$3;

    iget-object v0, v0, Lcom/twitter/ui/anim/e$3;->b:Lcom/twitter/ui/anim/e;

    invoke-virtual {v0}, Lcom/twitter/ui/anim/e;->e()V

    .line 205
    :goto_0
    return-void

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/twitter/ui/anim/e$3$1;->c:Lcom/twitter/ui/anim/e$3;

    iget-object v0, v0, Lcom/twitter/ui/anim/e$3;->b:Lcom/twitter/ui/anim/e;

    invoke-static {v0}, Lcom/twitter/ui/anim/e;->c(Lcom/twitter/ui/anim/e;)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/ui/anim/e$3$1;->c:Lcom/twitter/ui/anim/e$3;

    iget-object v1, v1, Lcom/twitter/ui/anim/e$3;->b:Lcom/twitter/ui/anim/e;

    iget-object v1, v1, Lcom/twitter/ui/anim/e;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/ui/anim/e$3$1;->c:Lcom/twitter/ui/anim/e$3;

    iget-object v0, v0, Lcom/twitter/ui/anim/e$3;->b:Lcom/twitter/ui/anim/e;

    invoke-static {v0}, Lcom/twitter/ui/anim/e;->d(Lcom/twitter/ui/anim/e;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 194
    :try_start_0
    iget-object v0, p0, Lcom/twitter/ui/anim/e$3$1;->c:Lcom/twitter/ui/anim/e$3;

    iget-object v0, v0, Lcom/twitter/ui/anim/e$3;->b:Lcom/twitter/ui/anim/e;

    iget-object v0, v0, Lcom/twitter/ui/anim/e;->b:Lcom/twitter/ui/anim/e$d;

    iget-object v1, p0, Lcom/twitter/ui/anim/e$3$1;->c:Lcom/twitter/ui/anim/e$3;

    iget-object v1, v1, Lcom/twitter/ui/anim/e$3;->b:Lcom/twitter/ui/anim/e;

    invoke-static {v1}, Lcom/twitter/ui/anim/e;->c(Lcom/twitter/ui/anim/e;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/anim/e$d;->a(I)Lcom/twitter/ui/anim/e$b;

    move-result-object v0

    .line 195
    iget-object v1, p0, Lcom/twitter/ui/anim/e$3$1;->c:Lcom/twitter/ui/anim/e$3;

    iget-object v1, v1, Lcom/twitter/ui/anim/e$3;->b:Lcom/twitter/ui/anim/e;

    invoke-static {v1}, Lcom/twitter/ui/anim/e;->e(Lcom/twitter/ui/anim/e;)I

    .line 196
    iget-object v1, p0, Lcom/twitter/ui/anim/e$3$1;->b:Lrx/i;

    invoke-virtual {v1, v0}, Lrx/i;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 197
    :catch_0
    move-exception v0

    .line 198
    iget-object v1, p0, Lcom/twitter/ui/anim/e$3$1;->b:Lrx/i;

    invoke-virtual {v1, v0}, Lrx/i;->a(Ljava/lang/Throwable;)V

    .line 199
    iget-object v0, p0, Lcom/twitter/ui/anim/e$3$1;->a:Lrx/f$a;

    invoke-virtual {v0}, Lrx/f$a;->B_()V

    goto :goto_0

    .line 202
    :cond_2
    iget-object v0, p0, Lcom/twitter/ui/anim/e$3$1;->b:Lrx/i;

    invoke-virtual {v0}, Lrx/i;->by_()V

    .line 203
    iget-object v0, p0, Lcom/twitter/ui/anim/e$3$1;->a:Lrx/f$a;

    invoke-virtual {v0}, Lrx/f$a;->B_()V

    goto :goto_0
.end method
