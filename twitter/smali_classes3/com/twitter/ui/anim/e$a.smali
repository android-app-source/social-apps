.class public Lcom/twitter/ui/anim/e$a;
.super Lcom/twitter/ui/anim/e$d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/ui/anim/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "a"
.end annotation


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 454
    invoke-direct {p0, p1}, Lcom/twitter/ui/anim/e$d;-><init>(Landroid/content/res/Resources;)V

    .line 455
    iput-object p2, p0, Lcom/twitter/ui/anim/e$a;->b:Ljava/util/List;

    .line 456
    return-void
.end method

.method private a(Landroid/graphics/BitmapFactory$Options;)V
    .locals 1

    .prologue
    .line 466
    iget v0, p1, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    if-nez v0, :cond_0

    .line 468
    const/16 v0, 0x1e0

    iput v0, p1, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 470
    :cond_0
    iget v0, p1, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    if-nez v0, :cond_1

    .line 471
    iget-object v0, p0, Lcom/twitter/ui/anim/e$a;->a:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, p1, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 473
    :cond_1
    return-void
.end method


# virtual methods
.method protected a(Landroid/graphics/BitmapFactory$Options;I)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 461
    invoke-direct {p0, p1}, Lcom/twitter/ui/anim/e$a;->a(Landroid/graphics/BitmapFactory$Options;)V

    .line 462
    iget-object v0, p0, Lcom/twitter/ui/anim/e$a;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
