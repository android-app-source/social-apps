.class public Lcom/twitter/ui/anim/AnimatableParams;
.super Landroid/view/WindowManager$LayoutParams;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/ui/anim/AnimatableParams$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/graphics/PointF;

.field private final b:Landroid/graphics/PointF;

.field private final c:Landroid/graphics/PointF;

.field private final d:Landroid/graphics/PointF;

.field private final e:Landroid/graphics/PointF;

.field private final f:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/twitter/ui/anim/AnimatableParams$a;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/graphics/PointF;

.field private final h:Lcom/twitter/ui/anim/k;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 29
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->a:Landroid/graphics/PointF;

    .line 31
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->b:Landroid/graphics/PointF;

    .line 33
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->c:Landroid/graphics/PointF;

    .line 35
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->d:Landroid/graphics/PointF;

    .line 37
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->e:Landroid/graphics/PointF;

    .line 39
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->f:Ljava/util/Queue;

    .line 41
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->g:Landroid/graphics/PointF;

    .line 43
    new-instance v0, Lcom/twitter/ui/anim/m;

    invoke-direct {v0}, Lcom/twitter/ui/anim/m;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->h:Lcom/twitter/ui/anim/k;

    .line 46
    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->a:Landroid/graphics/PointF;

    return-object v0
.end method

.method public a(Landroid/graphics/PointF;)V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->b:Landroid/graphics/PointF;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 103
    return-void
.end method

.method public a(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->c:Landroid/graphics/PointF;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 143
    iget-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->e:Landroid/graphics/PointF;

    iget v1, p2, Landroid/graphics/PointF;->x:F

    iget v2, p2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 144
    return-void
.end method

.method public b()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->b:Landroid/graphics/PointF;

    return-object v0
.end method

.method public b(Landroid/graphics/PointF;)V
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->a:Landroid/graphics/PointF;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 124
    return-void
.end method

.method public b(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 5

    .prologue
    .line 220
    iget-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/anim/AnimatableParams$a;

    .line 221
    invoke-static {v0}, Lcom/twitter/ui/anim/AnimatableParams$a;->a(Lcom/twitter/ui/anim/AnimatableParams$a;)Landroid/graphics/PointF;

    move-result-object v2

    iget v3, p2, Landroid/graphics/PointF;->x:F

    iget v4, p2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/PointF;->equals(FF)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 222
    invoke-static {v0}, Lcom/twitter/ui/anim/AnimatableParams$a;->a(Lcom/twitter/ui/anim/AnimatableParams$a;)Landroid/graphics/PointF;

    move-result-object v2

    iget v3, p1, Landroid/graphics/PointF;->x:F

    iget v4, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 224
    iget-object v2, p0, Lcom/twitter/ui/anim/AnimatableParams;->f:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v2

    if-ne v0, v2, :cond_0

    .line 225
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v2, p2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v2

    .line 226
    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    .line 227
    new-instance v3, Landroid/graphics/PointF;

    invoke-virtual {p0}, Lcom/twitter/ui/anim/AnimatableParams;->b()Landroid/graphics/PointF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v4

    invoke-virtual {p0}, Lcom/twitter/ui/anim/AnimatableParams;->b()Landroid/graphics/PointF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    add-float/2addr v2, v4

    invoke-direct {v3, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p0, v3}, Lcom/twitter/ui/anim/AnimatableParams;->a(Landroid/graphics/PointF;)V

    goto :goto_0

    .line 233
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/ui/anim/AnimatableParams;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 234
    invoke-virtual {p0, p1}, Lcom/twitter/ui/anim/AnimatableParams;->d(Landroid/graphics/PointF;)Lcom/twitter/ui/anim/AnimatableParams$a;

    .line 236
    :cond_2
    invoke-virtual {p0, p1}, Lcom/twitter/ui/anim/AnimatableParams;->b(Landroid/graphics/PointF;)V

    .line 237
    return-void
.end method

.method public c()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->d:Landroid/graphics/PointF;

    return-object v0
.end method

.method public c(Landroid/graphics/PointF;)V
    .locals 1

    .prologue
    .line 150
    iget v0, p1, Landroid/graphics/PointF;->x:F

    float-to-int v0, v0

    iput v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->x:I

    .line 151
    iget v0, p1, Landroid/graphics/PointF;->y:F

    float-to-int v0, v0

    iput v0, p0, Lcom/twitter/ui/anim/AnimatableParams;->y:I

    .line 152
    return-void
.end method

.method public d(Landroid/graphics/PointF;)Lcom/twitter/ui/anim/AnimatableParams$a;
    .locals 2

    .prologue
    .line 197
    new-instance v0, Lcom/twitter/ui/anim/AnimatableParams$a;

    iget-object v1, p0, Lcom/twitter/ui/anim/AnimatableParams;->h:Lcom/twitter/ui/anim/k;

    invoke-direct {v0, v1}, Lcom/twitter/ui/anim/AnimatableParams$a;-><init>(Lcom/twitter/ui/anim/k;)V

    .line 198
    invoke-virtual {v0, p1}, Lcom/twitter/ui/anim/AnimatableParams$a;->a(Landroid/graphics/PointF;)V

    .line 199
    iget-object v1, p0, Lcom/twitter/ui/anim/AnimatableParams;->f:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/twitter/ui/anim/AnimatableParams;->h:Lcom/twitter/ui/anim/k;

    invoke-interface {v1}, Lcom/twitter/ui/anim/k;->a()V

    .line 201
    invoke-virtual {p0}, Lcom/twitter/ui/anim/AnimatableParams;->d()V

    .line 203
    :cond_0
    iget-object v1, p0, Lcom/twitter/ui/anim/AnimatableParams;->f:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 204
    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/twitter/ui/anim/AnimatableParams;->b()Landroid/graphics/PointF;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/ui/anim/AnimatableParams;->c()Landroid/graphics/PointF;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/ui/anim/AnimatableParams;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    .line 132
    return-void
.end method

.method public e()Z
    .locals 3

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/twitter/ui/anim/AnimatableParams;->a()Landroid/graphics/PointF;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/ui/anim/AnimatableParams;->b()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {p0}, Lcom/twitter/ui/anim/AnimatableParams;->b()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->equals(FF)Z

    move-result v0

    return v0
.end method
