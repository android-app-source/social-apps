.class public abstract Lcom/twitter/ui/anim/e$d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/ui/anim/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "d"
.end annotation


# instance fields
.field protected final a:Landroid/content/res/Resources;

.field private b:Lcom/twitter/util/math/Size;

.field private final c:Lcom/twitter/util/collection/l$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/l$b",
            "<",
            "Landroid/graphics/BitmapFactory$Options;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    new-instance v0, Lcom/twitter/util/collection/l$b;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/twitter/util/collection/l$b;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/ui/anim/e$d;->c:Lcom/twitter/util/collection/l$b;

    .line 332
    iput-object p1, p0, Lcom/twitter/ui/anim/e$d;->a:Landroid/content/res/Resources;

    .line 333
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/graphics/BitmapFactory$Options;I)Landroid/graphics/Bitmap;
.end method

.method public a(I)Lcom/twitter/ui/anim/e$b;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 374
    iget-object v3, p0, Lcom/twitter/ui/anim/e$d;->c:Lcom/twitter/util/collection/l$b;

    monitor-enter v3

    .line 378
    :try_start_0
    iget-object v0, p0, Lcom/twitter/ui/anim/e$d;->c:Lcom/twitter/util/collection/l$b;

    invoke-virtual {v0}, Lcom/twitter/util/collection/l$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/BitmapFactory$Options;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    if-nez v0, :cond_0

    .line 383
    :try_start_1
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 385
    :goto_0
    :try_start_2
    invoke-virtual {p0, v1, p1}, Lcom/twitter/ui/anim/e$d;->a(Landroid/graphics/BitmapFactory$Options;I)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 391
    :goto_1
    :try_start_3
    new-instance v0, Lcom/twitter/ui/anim/e$b;

    invoke-direct {v0, p1, v1, v2}, Lcom/twitter/ui/anim/e$b;-><init>(ILandroid/graphics/BitmapFactory$Options;Landroid/graphics/Bitmap;)V

    monitor-exit v3

    return-object v0

    .line 386
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 387
    :goto_2
    invoke-static {v0}, Lcpg;->a(Ljava/lang/OutOfMemoryError;)V

    goto :goto_1

    .line 392
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 388
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 389
    :goto_3
    :try_start_4
    const-class v4, Lcom/twitter/ui/anim/e;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "Error decoding resource"

    invoke-static {v4, v5, v0}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 388
    :catch_2
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_3

    :catch_3
    move-exception v0

    goto :goto_3

    .line 386
    :catch_4
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_2

    :catch_5
    move-exception v0

    goto :goto_2

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 340
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/ui/anim/e$d;->c()Lcom/twitter/util/math/Size;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/anim/e$d;->b:Lcom/twitter/util/math/Size;

    .line 341
    iget-object v0, p0, Lcom/twitter/ui/anim/e$d;->b:Lcom/twitter/util/math/Size;

    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 342
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 343
    iget-object v1, p0, Lcom/twitter/ui/anim/e$d;->b:Lcom/twitter/util/math/Size;

    .line 344
    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->a()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/ui/anim/e$d;->b:Lcom/twitter/util/math/Size;

    invoke-virtual {v2}, Lcom/twitter/util/math/Size;->b()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 345
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 347
    iput-object v1, v2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 348
    const/4 v1, 0x1

    iput-boolean v1, v2, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 349
    const/4 v1, 0x1

    iput v1, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 350
    iget-object v1, p0, Lcom/twitter/ui/anim/e$d;->c:Lcom/twitter/util/collection/l$b;

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/l$b;->a(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 342
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 353
    :catch_0
    move-exception v0

    .line 354
    invoke-static {v0}, Lcpg;->a(Ljava/lang/OutOfMemoryError;)V

    .line 360
    :cond_0
    :goto_1
    return-void

    .line 357
    :catch_1
    move-exception v0

    .line 358
    const-class v1, Lcom/twitter/ui/anim/e;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Error initializing FrameDecoder"

    invoke-static {v1, v2, v0}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/ui/anim/e$b;)V
    .locals 3

    .prologue
    .line 398
    iget-object v1, p0, Lcom/twitter/ui/anim/e$d;->c:Lcom/twitter/util/collection/l$b;

    monitor-enter v1

    .line 401
    :try_start_0
    iget-object v0, p0, Lcom/twitter/ui/anim/e$d;->c:Lcom/twitter/util/collection/l$b;

    invoke-virtual {p1}, Lcom/twitter/ui/anim/e$b;->b()Landroid/graphics/BitmapFactory$Options;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/util/collection/l$b;->a(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    :goto_0
    :try_start_1
    monitor-exit v1

    .line 406
    return-void

    .line 405
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 402
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public b()Lcom/twitter/util/math/Size;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/twitter/ui/anim/e$d;->b:Lcom/twitter/util/math/Size;

    return-object v0
.end method

.method protected c()Lcom/twitter/util/math/Size;
    .locals 4

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    .line 410
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 411
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 412
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/ui/anim/e$d;->a(Landroid/graphics/BitmapFactory$Options;I)Landroid/graphics/Bitmap;

    .line 413
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    if-eqz v1, :cond_0

    .line 415
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 416
    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v2, v2

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    float-to-int v2, v2

    .line 417
    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v0, v0

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    float-to-int v0, v0

    .line 418
    invoke-static {v2, v0}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v0

    .line 420
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/twitter/util/math/Size;->a(Landroid/graphics/BitmapFactory$Options;)Lcom/twitter/util/math/Size;

    move-result-object v0

    goto :goto_0
.end method
