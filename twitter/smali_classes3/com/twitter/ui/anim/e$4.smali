.class Lcom/twitter/ui/anim/e$4;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/ui/anim/e;->d()Lrx/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lcom/twitter/ui/anim/e$b;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/twitter/ui/anim/e$b;

.field final synthetic b:Lcom/twitter/ui/anim/e;


# direct methods
.method constructor <init>(Lcom/twitter/ui/anim/e;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/twitter/ui/anim/e$4;->b:Lcom/twitter/ui/anim/e;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/ui/anim/e$4;)V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/twitter/ui/anim/e$4;->e()V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/twitter/ui/anim/e$4;->a:Lcom/twitter/ui/anim/e$b;

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/twitter/ui/anim/e$4;->b:Lcom/twitter/ui/anim/e;

    iget-object v0, v0, Lcom/twitter/ui/anim/e;->b:Lcom/twitter/ui/anim/e$d;

    iget-object v1, p0, Lcom/twitter/ui/anim/e$4;->a:Lcom/twitter/ui/anim/e$b;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/anim/e$d;->a(Lcom/twitter/ui/anim/e$b;)V

    .line 254
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/ui/anim/e$4;->a:Lcom/twitter/ui/anim/e$b;

    .line 256
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/ui/anim/e$b;)V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/twitter/ui/anim/e$4;->b:Lcom/twitter/ui/anim/e;

    invoke-static {v0}, Lcom/twitter/ui/anim/e;->d(Lcom/twitter/ui/anim/e;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/ui/anim/e$4$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/ui/anim/e$4$1;-><init>(Lcom/twitter/ui/anim/e$4;Lcom/twitter/ui/anim/e$b;)V

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 249
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 214
    check-cast p1, Lcom/twitter/ui/anim/e$b;

    invoke-virtual {p0, p1}, Lcom/twitter/ui/anim/e$4;->a(Lcom/twitter/ui/anim/e$b;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 226
    invoke-super {p0, p1}, Lcqw;->a(Ljava/lang/Throwable;)V

    .line 227
    iget-object v0, p0, Lcom/twitter/ui/anim/e$4;->b:Lcom/twitter/ui/anim/e;

    invoke-static {v0}, Lcom/twitter/ui/anim/e;->f(Lcom/twitter/ui/anim/e;)V

    .line 228
    invoke-direct {p0}, Lcom/twitter/ui/anim/e$4;->e()V

    .line 229
    return-void
.end method

.method public by_()V
    .locals 1

    .prologue
    .line 219
    invoke-super {p0}, Lcqw;->by_()V

    .line 220
    iget-object v0, p0, Lcom/twitter/ui/anim/e$4;->b:Lcom/twitter/ui/anim/e;

    invoke-static {v0}, Lcom/twitter/ui/anim/e;->f(Lcom/twitter/ui/anim/e;)V

    .line 221
    invoke-direct {p0}, Lcom/twitter/ui/anim/e$4;->e()V

    .line 222
    return-void
.end method
