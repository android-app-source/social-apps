.class public Lcom/twitter/ui/anim/e;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/ui/anim/e$b;,
        Lcom/twitter/ui/anim/e$a;,
        Lcom/twitter/ui/anim/e$e;,
        Lcom/twitter/ui/anim/e$d;,
        Lcom/twitter/ui/anim/e$c;
    }
.end annotation


# instance fields
.field a:I
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field b:Lcom/twitter/ui/anim/e$d;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final d:Landroid/view/View;

.field private final e:Ljava/lang/Runnable;

.field private final f:Ljava/lang/Runnable;

.field private g:Lcom/twitter/ui/anim/e$c;

.field private h:I

.field private i:I

.field private j:Z

.field private k:Z

.field private l:Lrx/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/i",
            "<",
            "Lcom/twitter/ui/anim/e$b;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lrx/f;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/16 v0, 0x3c

    iput v0, p0, Lcom/twitter/ui/anim/e;->h:I

    .line 68
    iput-object p1, p0, Lcom/twitter/ui/anim/e;->d:Landroid/view/View;

    .line 69
    new-instance v0, Lcom/twitter/ui/anim/e$1;

    invoke-direct {v0, p0}, Lcom/twitter/ui/anim/e$1;-><init>(Lcom/twitter/ui/anim/e;)V

    iput-object v0, p0, Lcom/twitter/ui/anim/e;->e:Ljava/lang/Runnable;

    .line 77
    new-instance v0, Lcom/twitter/ui/anim/e$2;

    invoke-direct {v0, p0}, Lcom/twitter/ui/anim/e$2;-><init>(Lcom/twitter/ui/anim/e;)V

    iput-object v0, p0, Lcom/twitter/ui/anim/e;->f:Ljava/lang/Runnable;

    .line 85
    return-void
.end method

.method static synthetic a(Lcom/twitter/ui/anim/e;)Lcom/twitter/ui/anim/e$c;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->g:Lcom/twitter/ui/anim/e$c;

    return-object v0
.end method

.method private a(Lrx/f;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/f;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/ui/anim/e$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    new-instance v0, Lcom/twitter/ui/anim/e$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/ui/anim/e$3;-><init>(Lcom/twitter/ui/anim/e;Lrx/f;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/ui/anim/e;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/twitter/ui/anim/e;->j:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/ui/anim/e;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/twitter/ui/anim/e;->i:I

    return v0
.end method

.method static synthetic d(Lcom/twitter/ui/anim/e;)Landroid/view/View;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->d:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/ui/anim/e;)I
    .locals 2

    .prologue
    .line 43
    iget v0, p0, Lcom/twitter/ui/anim/e;->i:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/twitter/ui/anim/e;->i:I

    return v0
.end method

.method static synthetic f(Lcom/twitter/ui/anim/e;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/twitter/ui/anim/e;->h()V

    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 285
    iput-boolean v0, p0, Lcom/twitter/ui/anim/e;->j:Z

    .line 286
    iput-boolean v0, p0, Lcom/twitter/ui/anim/e;->k:Z

    .line 287
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->l:Lrx/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/anim/e;->l:Lrx/i;

    invoke-virtual {v0}, Lrx/i;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->l:Lrx/i;

    invoke-virtual {v0}, Lrx/i;->B_()V

    .line 289
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/ui/anim/e;->l:Lrx/i;

    .line 291
    :cond_0
    return-void
.end method

.method private declared-synchronized h()V
    .locals 2

    .prologue
    .line 297
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/twitter/ui/anim/e;->g()V

    .line 298
    invoke-static {}, Lcom/twitter/util/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    :goto_0
    monitor-exit p0

    return-void

    .line 301
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/ui/anim/e;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 297
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a(Lcom/twitter/ui/anim/e$c;)Lcom/twitter/ui/anim/e;
    .locals 1

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/twitter/ui/anim/e;->g:Lcom/twitter/ui/anim/e$c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    monitor-exit p0

    return-object p0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/util/List;)Lcom/twitter/ui/anim/e;
    .locals 2
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/twitter/ui/anim/e;"
        }
    .end annotation

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/twitter/ui/anim/e;->c:Ljava/util/List;

    .line 108
    new-instance v0, Lcom/twitter/ui/anim/e$e;

    iget-object v1, p0, Lcom/twitter/ui/anim/e;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/ui/anim/e$e;-><init>(Landroid/content/res/Resources;Ljava/util/List;)V

    iput-object v0, p0, Lcom/twitter/ui/anim/e;->b:Lcom/twitter/ui/anim/e$d;

    .line 109
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->b:Lcom/twitter/ui/anim/e$d;

    invoke-virtual {v0}, Lcom/twitter/ui/anim/e$d;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    monitor-exit p0

    return-object p0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .locals 4

    .prologue
    .line 137
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/anim/e;->b:Lcom/twitter/ui/anim/e$d;

    if-nez v0, :cond_1

    .line 138
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "No frames have been set to this animation."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 140
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 161
    :goto_0
    monitor-exit p0

    return-void

    .line 143
    :cond_2
    :try_start_2
    iget-boolean v0, p0, Lcom/twitter/ui/anim/e;->k:Z

    if-eqz v0, :cond_3

    .line 144
    invoke-virtual {p0}, Lcom/twitter/ui/anim/e;->e()V

    goto :goto_0

    .line 147
    :cond_3
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iget v2, p0, Lcom/twitter/ui/anim/e;->h:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/twitter/ui/anim/e;->a:I

    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/ui/anim/e;->j:Z

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/ui/anim/e;->k:Z

    .line 150
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/ui/anim/e;->i:I

    .line 151
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->g:Lcom/twitter/ui/anim/e$c;

    if-eqz v0, :cond_4

    .line 152
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->g:Lcom/twitter/ui/anim/e$c;

    invoke-interface {v0}, Lcom/twitter/ui/anim/e$c;->a()V

    .line 154
    :cond_4
    invoke-virtual {p0}, Lcom/twitter/ui/anim/e;->c()Lrx/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/anim/e;->m:Lrx/f;

    .line 155
    invoke-virtual {p0}, Lcom/twitter/ui/anim/e;->d()Lrx/i;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/anim/e;->l:Lrx/i;

    .line 157
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->m:Lrx/f;

    invoke-direct {p0, v0}, Lcom/twitter/ui/anim/e;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/ui/anim/e;->m:Lrx/f;

    .line 158
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/ui/anim/e;->m:Lrx/f;

    .line 159
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/ui/anim/e;->l:Lrx/i;

    .line 160
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized b(Ljava/util/List;)Lcom/twitter/ui/anim/e;
    .locals 2
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/twitter/ui/anim/e;"
        }
    .end annotation

    .prologue
    .line 116
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/twitter/ui/anim/e;->c:Ljava/util/List;

    .line 117
    new-instance v0, Lcom/twitter/ui/anim/e$a;

    iget-object v1, p0, Lcom/twitter/ui/anim/e;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/ui/anim/e$a;-><init>(Landroid/content/res/Resources;Ljava/util/List;)V

    iput-object v0, p0, Lcom/twitter/ui/anim/e;->b:Lcom/twitter/ui/anim/e$d;

    .line 118
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->b:Lcom/twitter/ui/anim/e$d;

    invoke-virtual {v0}, Lcom/twitter/ui/anim/e$d;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    monitor-exit p0

    return-object p0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Lcom/twitter/util/math/Size;
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->b:Lcom/twitter/ui/anim/e$d;

    if-nez v0, :cond_0

    .line 166
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "FrameDecoder not initialized! Call setFrames first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->b:Lcom/twitter/ui/anim/e$d;

    invoke-virtual {v0}, Lcom/twitter/ui/anim/e$d;->b()Lcom/twitter/util/math/Size;

    move-result-object v0

    return-object v0
.end method

.method c()Lrx/f;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 174
    invoke-static {}, Lcws;->b()Lrx/f;

    move-result-object v0

    return-object v0
.end method

.method d()Lrx/i;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/i",
            "<",
            "Lcom/twitter/ui/anim/e$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 214
    new-instance v0, Lcom/twitter/ui/anim/e$4;

    invoke-direct {v0, p0}, Lcom/twitter/ui/anim/e$4;-><init>(Lcom/twitter/ui/anim/e;)V

    return-object v0
.end method

.method public declared-synchronized e()V
    .locals 2

    .prologue
    .line 264
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/twitter/ui/anim/e;->g()V

    .line 265
    invoke-static {}, Lcom/twitter/util/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->f:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270
    :goto_0
    monitor-exit p0

    return-void

    .line 268
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/twitter/ui/anim/e;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/ui/anim/e;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 264
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()V
    .locals 1

    .prologue
    .line 276
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/ui/anim/e;->k:Z

    if-eqz v0, :cond_0

    .line 277
    invoke-direct {p0}, Lcom/twitter/ui/anim/e;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282
    :goto_0
    monitor-exit p0

    return-void

    .line 280
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/twitter/ui/anim/e;->g()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
