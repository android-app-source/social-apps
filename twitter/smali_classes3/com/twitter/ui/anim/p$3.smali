.class Lcom/twitter/ui/anim/p$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/anim/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/ui/anim/p;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/ui/anim/p;


# direct methods
.method constructor <init>(Lcom/twitter/ui/anim/p;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/twitter/ui/anim/p$3;->a:Lcom/twitter/ui/anim/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 79
    iget-object v0, p0, Lcom/twitter/ui/anim/p$3;->a:Lcom/twitter/ui/anim/p;

    invoke-static {v0, v1}, Lcom/twitter/ui/anim/p;->a(Lcom/twitter/ui/anim/p;Z)Z

    .line 80
    iget-object v0, p0, Lcom/twitter/ui/anim/p$3;->a:Lcom/twitter/ui/anim/p;

    invoke-static {v0}, Lcom/twitter/ui/anim/p;->c(Lcom/twitter/ui/anim/p;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/twitter/ui/anim/p$3;->a:Lcom/twitter/ui/anim/p;

    invoke-static {v0}, Lcom/twitter/ui/anim/p;->d(Lcom/twitter/ui/anim/p;)Lcom/twitter/ui/anim/p$a;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 82
    iget-object v0, p0, Lcom/twitter/ui/anim/p$3;->a:Lcom/twitter/ui/anim/p;

    invoke-static {v0}, Lcom/twitter/ui/anim/p;->d(Lcom/twitter/ui/anim/p;)Lcom/twitter/ui/anim/p$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/ui/anim/p$a;->a()V

    .line 84
    iget-object v0, p0, Lcom/twitter/ui/anim/p$3;->a:Lcom/twitter/ui/anim/p;

    invoke-static {v0}, Lcom/twitter/ui/anim/p;->e(Lcom/twitter/ui/anim/p;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/twitter/ui/anim/p$3;->a:Lcom/twitter/ui/anim/p;

    invoke-virtual {v0}, Lcom/twitter/ui/anim/p;->b()V

    .line 86
    iget-object v0, p0, Lcom/twitter/ui/anim/p$3;->a:Lcom/twitter/ui/anim/p;

    invoke-static {v0, v1}, Lcom/twitter/ui/anim/p;->b(Lcom/twitter/ui/anim/p;Z)Z

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/twitter/ui/anim/p$3;->a:Lcom/twitter/ui/anim/p;

    invoke-static {v0}, Lcom/twitter/ui/anim/p;->g(Lcom/twitter/ui/anim/p;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/ui/anim/p$3;->a:Lcom/twitter/ui/anim/p;

    invoke-static {v1}, Lcom/twitter/ui/anim/p;->f(Lcom/twitter/ui/anim/p;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 92
    :cond_2
    iget-object v0, p0, Lcom/twitter/ui/anim/p$3;->a:Lcom/twitter/ui/anim/p;

    invoke-virtual {v0}, Lcom/twitter/ui/anim/p;->b()V

    goto :goto_0
.end method
