.class public Lcom/twitter/ui/anim/e$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/ui/anim/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "b"
.end annotation


# instance fields
.field private final a:I

.field private final b:Landroid/graphics/BitmapFactory$Options;

.field private final c:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(ILandroid/graphics/BitmapFactory$Options;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482
    iput p1, p0, Lcom/twitter/ui/anim/e$b;->a:I

    .line 483
    iput-object p2, p0, Lcom/twitter/ui/anim/e$b;->b:Landroid/graphics/BitmapFactory$Options;

    .line 484
    iput-object p3, p0, Lcom/twitter/ui/anim/e$b;->c:Landroid/graphics/Bitmap;

    .line 485
    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/twitter/ui/anim/e$b;->c:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public b()Landroid/graphics/BitmapFactory$Options;
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/twitter/ui/anim/e$b;->b:Landroid/graphics/BitmapFactory$Options;

    return-object v0
.end method
