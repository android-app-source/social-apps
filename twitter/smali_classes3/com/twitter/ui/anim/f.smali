.class public Lcom/twitter/ui/anim/f;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/ui/anim/f$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/ui/anim/f$a;


# instance fields
.field private final b:F

.field private final c:F

.field private final d:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/twitter/ui/anim/f$a;

    invoke-direct {v0}, Lcom/twitter/ui/anim/f$a;-><init>()V

    sput-object v0, Lcom/twitter/ui/anim/f;->a:Lcom/twitter/ui/anim/f$a;

    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 29
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Friction cannot zero or negative or the inertial system will not converge."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 32
    :cond_0
    iput p1, p0, Lcom/twitter/ui/anim/f;->b:F

    .line 33
    iput p2, p0, Lcom/twitter/ui/anim/f;->c:F

    .line 34
    iput p3, p0, Lcom/twitter/ui/anim/f;->d:F

    .line 35
    return-void
.end method


# virtual methods
.method public a()F
    .locals 3

    .prologue
    .line 49
    iget v0, p0, Lcom/twitter/ui/anim/f;->d:F

    iget v1, p0, Lcom/twitter/ui/anim/f;->c:F

    iget v2, p0, Lcom/twitter/ui/anim/f;->b:F

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public a(F)F
    .locals 8

    .prologue
    .line 38
    iget v0, p0, Lcom/twitter/ui/anim/f;->d:F

    float-to-double v0, v0

    iget v2, p0, Lcom/twitter/ui/anim/f;->c:F

    iget v3, p0, Lcom/twitter/ui/anim/f;->b:F

    div-float/2addr v2, v3

    float-to-double v2, v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iget v6, p0, Lcom/twitter/ui/anim/f;->b:F

    neg-float v6, v6

    mul-float/2addr v6, p1

    const v7, 0x3a83126f    # 0.001f

    mul-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method public b(F)F
    .locals 4

    .prologue
    .line 42
    iget v0, p0, Lcom/twitter/ui/anim/f;->c:F

    float-to-double v0, v0

    iget v2, p0, Lcom/twitter/ui/anim/f;->b:F

    neg-float v2, v2

    mul-float/2addr v2, p1

    const v3, 0x3a83126f    # 0.001f

    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method
