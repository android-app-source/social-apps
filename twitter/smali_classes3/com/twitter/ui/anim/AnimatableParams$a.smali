.class public Lcom/twitter/ui/anim/AnimatableParams$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/ui/anim/AnimatableParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# static fields
.field public static final a:Ljava/lang/Runnable;


# instance fields
.field private final b:Landroid/graphics/PointF;

.field private c:Lcom/twitter/ui/anim/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 257
    new-instance v0, Lcom/twitter/ui/anim/AnimatableParams$a$1;

    invoke-direct {v0}, Lcom/twitter/ui/anim/AnimatableParams$a$1;-><init>()V

    sput-object v0, Lcom/twitter/ui/anim/AnimatableParams$a;->a:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 267
    sget-object v0, Lcom/twitter/ui/anim/k;->a:Lcom/twitter/ui/anim/k;

    invoke-direct {p0, v0}, Lcom/twitter/ui/anim/AnimatableParams$a;-><init>(Lcom/twitter/ui/anim/k;)V

    .line 268
    return-void
.end method

.method public constructor <init>(Lcom/twitter/ui/anim/k;)V
    .locals 1

    .prologue
    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 262
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams$a;->b:Landroid/graphics/PointF;

    .line 271
    iput-object p1, p0, Lcom/twitter/ui/anim/AnimatableParams$a;->c:Lcom/twitter/ui/anim/k;

    .line 272
    return-void
.end method

.method static synthetic a(Lcom/twitter/ui/anim/AnimatableParams$a;)Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams$a;->b:Landroid/graphics/PointF;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/graphics/PointF;)V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/twitter/ui/anim/AnimatableParams$a;->b:Landroid/graphics/PointF;

    invoke-virtual {v0, p1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 276
    return-void
.end method
