.class public Lcom/twitter/ui/socialproof/SocialBylineView;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static final a:Landroid/text/TextPaint;


# instance fields
.field private final b:I

.field private final c:I

.field private final d:Landroid/content/res/ColorStateList;

.field private final e:Lcom/twitter/ui/widget/i;

.field private f:I

.field private g:I

.field private h:Ljava/lang/CharSequence;

.field private i:F

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:Landroid/text/StaticLayout;

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    sput-object v0, Lcom/twitter/ui/socialproof/SocialBylineView;->a:Landroid/text/TextPaint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/ui/socialproof/SocialBylineView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 44
    sget v0, Lcjo$a;->socialBylineViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/ui/socialproof/SocialBylineView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    sget-object v0, Lcjo$h;->SocialBylineView:[I

    .line 51
    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 52
    sget v1, Lcjo$h;->SocialBylineView_minIconWidth:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->f:I

    .line 53
    sget v1, Lcjo$h;->SocialBylineView_iconMargin:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->b:I

    .line 54
    sget v1, Lcjo$h;->SocialBylineView_socialContextPadding:I

    .line 55
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->c:I

    .line 56
    sget v1, Lcjo$h;->SocialBylineView_labelSize:I

    invoke-static {}, Lcni;->b()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->i:F

    .line 57
    sget v1, Lcjo$h;->SocialBylineView_labelColor:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->d:Landroid/content/res/ColorStateList;

    .line 58
    iget-object v1, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->d:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_0

    .line 59
    iget-object v1, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->d:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getDrawableState()[I

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->g:I

    .line 61
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 62
    invoke-static {p1}, Lcom/twitter/ui/widget/i;->a(Landroid/content/Context;)Lcom/twitter/ui/widget/i;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->e:Lcom/twitter/ui/widget/i;

    .line 63
    return-void
.end method

.method private static a(Ljava/lang/String;Landroid/graphics/Paint;)I
    .locals 3

    .prologue
    .line 235
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 236
    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, p0, v1, v2, v0}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 237
    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 3

    .prologue
    .line 71
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 73
    iget-object v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->d:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->d:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->d:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getDrawableState()[I

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 75
    iget v1, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->g:I

    if-eq v1, v0, :cond_0

    .line 76
    iput v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->g:I

    .line 77
    invoke-virtual {p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->invalidate()V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 84
    :cond_1
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 184
    sget-object v4, Lcom/twitter/ui/socialproof/SocialBylineView;->a:Landroid/text/TextPaint;

    .line 185
    iget-object v3, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->j:Landroid/graphics/drawable/Drawable;

    .line 186
    iget-object v5, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->k:Landroid/text/StaticLayout;

    .line 188
    if-eqz v3, :cond_1

    move v0, v1

    .line 191
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 192
    const/4 v6, 0x0

    iget v7, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->c:I

    int-to-float v7, v7

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    .line 193
    if-eqz v0, :cond_2

    .line 194
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 195
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 196
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v3

    .line 201
    :goto_1
    if-eqz v5, :cond_0

    .line 203
    iget-boolean v6, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->l:Z

    if-eqz v6, :cond_4

    .line 204
    if-eqz v0, :cond_3

    .line 205
    iget v0, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->b:I

    sub-int/2addr v0, v2

    .line 217
    :goto_2
    iget-object v2, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->k:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v2

    if-le v2, v1, :cond_6

    .line 218
    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getPaddingTop()I

    move-result v1

    .line 219
    invoke-virtual {v5}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/twitter/ui/socialproof/SocialBylineView;->a(Ljava/lang/String;Landroid/graphics/Paint;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    .line 218
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 226
    :goto_3
    iget-object v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->e:Lcom/twitter/ui/widget/i;

    iget-object v0, v0, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    invoke-virtual {v4, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 227
    iget v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->i:F

    invoke-virtual {v4, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 228
    iget v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->g:I

    invoke-virtual {v4, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 229
    invoke-virtual {v5, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 230
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 232
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 188
    goto :goto_0

    .line 198
    :cond_2
    const/4 v3, 0x0

    move-object v8, v3

    move v3, v2

    move-object v2, v8

    .line 199
    goto :goto_1

    .line 207
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getWidth()I

    move-result v0

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->b:I

    sub-int/2addr v0, v2

    goto :goto_2

    .line 210
    :cond_4
    if-eqz v0, :cond_5

    .line 211
    iget v0, v2, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->b:I

    add-int/2addr v0, v2

    goto :goto_2

    .line 213
    :cond_5
    iget v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->f:I

    iget v2, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->b:I

    add-int/2addr v0, v2

    goto :goto_2

    .line 221
    :cond_6
    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    .line 222
    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 223
    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getPaddingTop()I

    move-result v3

    sub-int v1, v2, v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v3

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_3
.end method

.method protected onMeasure(II)V
    .locals 18

    .prologue
    .line 89
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    .line 90
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 93
    sget-object v3, Lcom/twitter/ui/socialproof/SocialBylineView;->a:Landroid/text/TextPaint;

    .line 94
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/ui/socialproof/SocialBylineView;->j:Landroid/graphics/drawable/Drawable;

    .line 95
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/ui/socialproof/SocialBylineView;->h:Ljava/lang/CharSequence;

    .line 96
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getPaddingLeft()I

    move-result v16

    .line 97
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getPaddingTop()I

    move-result v11

    .line 98
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getPaddingRight()I

    move-result v17

    .line 103
    if-eqz v15, :cond_1

    .line 104
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/ui/socialproof/SocialBylineView;->f:I

    invoke-virtual {v15}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 105
    invoke-virtual {v15}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    move v13, v4

    move v14, v5

    .line 112
    :goto_0
    sparse-switch v6, :sswitch_data_0

    .line 126
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getSuggestedMinimumWidth()I

    move-result v1

    .line 127
    sub-int v4, v1, v16

    sub-int v4, v4, v17

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/ui/socialproof/SocialBylineView;->b:I

    sub-int/2addr v4, v5

    sub-int/2addr v4, v14

    move v12, v1

    .line 132
    :goto_1
    if-eqz v2, :cond_3

    .line 133
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/socialproof/SocialBylineView;->e:Lcom/twitter/ui/widget/i;

    iget-object v1, v1, Lcom/twitter/ui/widget/i;->a:Landroid/graphics/Typeface;

    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 134
    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/ui/socialproof/SocialBylineView;->i:F

    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 135
    if-nez v6, :cond_2

    .line 136
    new-instance v1, Landroid/text/StaticLayout;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 142
    :goto_2
    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    .line 148
    :goto_3
    invoke-static {v2, v13}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 150
    if-eqz v15, :cond_0

    .line 151
    invoke-virtual {v15}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 154
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/ui/socialproof/SocialBylineView;->l:Z

    if-eqz v3, :cond_4

    .line 155
    sub-int v3, v12, v17

    sub-int/2addr v3, v14

    .line 161
    :goto_4
    if-le v2, v13, :cond_5

    .line 163
    sub-int/2addr v2, v13

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v11

    .line 167
    :goto_5
    add-int/2addr v5, v3

    add-int v6, v2, v13

    invoke-virtual {v15, v3, v2, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 170
    :cond_0
    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/ui/socialproof/SocialBylineView;->k:Landroid/text/StaticLayout;

    .line 173
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    if-ne v1, v2, :cond_6

    .line 174
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 179
    :goto_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v1}, Lcom/twitter/ui/socialproof/SocialBylineView;->setMeasuredDimension(II)V

    .line 180
    return-void

    .line 107
    :cond_1
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/ui/socialproof/SocialBylineView;->f:I

    .line 108
    const/4 v4, 0x0

    move v13, v4

    move v14, v5

    goto :goto_0

    .line 115
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/socialproof/SocialBylineView;->h:Ljava/lang/CharSequence;

    invoke-static {v1, v3}, Lcom/twitter/util/ui/k;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v4

    .line 116
    add-int v1, v16, v17

    add-int/2addr v1, v14

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/ui/socialproof/SocialBylineView;->b:I

    add-int/2addr v1, v5

    add-int/2addr v1, v4

    move v12, v1

    .line 118
    goto :goto_1

    .line 122
    :sswitch_1
    sub-int v4, v1, v16

    sub-int v4, v4, v17

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/ui/socialproof/SocialBylineView;->b:I

    sub-int/2addr v4, v5

    sub-int/2addr v4, v14

    move v12, v1

    .line 123
    goto :goto_1

    .line 138
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/ui/socialproof/SocialBylineView;->h:Ljava/lang/CharSequence;

    invoke-static {v1, v3}, Lcom/twitter/util/ui/k;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v1

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 139
    new-instance v1, Landroid/text/StaticLayout;

    const/4 v3, 0x0

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    sget-object v5, Lcom/twitter/ui/socialproof/SocialBylineView;->a:Landroid/text/TextPaint;

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v1 .. v10}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    goto :goto_2

    .line 144
    :cond_3
    const/4 v2, 0x0

    .line 145
    const/4 v1, 0x0

    goto :goto_3

    .line 157
    :cond_4
    add-int v3, v16, v14

    sub-int/2addr v3, v5

    goto :goto_4

    :cond_5
    move v2, v11

    .line 165
    goto :goto_5

    .line 176
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getSuggestedMinimumHeight()I

    move-result v1

    add-int v2, v4, v11

    .line 177
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    .line 176
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_6

    .line 112
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method

.method public setIcon(I)V
    .locals 1

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/ui/socialproof/SocialBylineView;->setIconDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 263
    return-void
.end method

.method public setIconDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->j:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_1

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    iput-object p1, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->j:Landroid/graphics/drawable/Drawable;

    .line 270
    if-eqz p1, :cond_0

    .line 271
    invoke-virtual {p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->requestLayout()V

    goto :goto_0
.end method

.method public setLabel(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 241
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->h:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->h:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 246
    :cond_1
    :goto_0
    return-void

    .line 244
    :cond_2
    iput-object p1, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->h:Ljava/lang/CharSequence;

    .line 245
    invoke-virtual {p0}, Lcom/twitter/ui/socialproof/SocialBylineView;->requestLayout()V

    goto :goto_0
.end method

.method public setLabelSize(F)V
    .locals 0

    .prologue
    .line 254
    iput p1, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->i:F

    .line 255
    return-void
.end method

.method public setMinIconWidth(I)V
    .locals 0

    .prologue
    .line 258
    iput p1, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->f:I

    .line 259
    return-void
.end method

.method public setRenderRTL(Z)V
    .locals 0

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/twitter/ui/socialproof/SocialBylineView;->l:Z

    .line 67
    return-void
.end method
