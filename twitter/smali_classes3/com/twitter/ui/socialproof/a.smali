.class public Lcom/twitter/ui/socialproof/a;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(I)I
    .locals 1

    .prologue
    .line 15
    packed-switch p0, :pswitch_data_0

    .line 86
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 26
    :pswitch_1
    sget v0, Lcjo$d;->vector_person_activity:I

    goto :goto_0

    .line 35
    :pswitch_2
    sget v0, Lcjo$d;->vector_retweet_activity:I

    goto :goto_0

    .line 41
    :pswitch_3
    sget v0, Lcjo$d;->vector_conversation_activity:I

    goto :goto_0

    .line 45
    :pswitch_4
    invoke-static {}, Lcmj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    sget v0, Lcjo$d;->vector_reply_comment_activity:I

    goto :goto_0

    .line 48
    :cond_0
    sget v0, Lcjo$d;->vector_reply_activity:I

    goto :goto_0

    .line 58
    :pswitch_5
    sget v0, Lcjo$d;->vector_heart_activity:I

    goto :goto_0

    .line 62
    :pswitch_6
    sget v0, Lcjo$d;->vector_megaphone_activity:I

    goto :goto_0

    .line 65
    :pswitch_7
    sget v0, Lcjo$d;->vector_twitter_activity:I

    goto :goto_0

    .line 68
    :pswitch_8
    sget v0, Lcjo$d;->vector_location_activity:I

    goto :goto_0

    .line 71
    :pswitch_9
    sget v0, Lcjo$d;->vector_camera_flash_activity:I

    goto :goto_0

    .line 74
    :pswitch_a
    sget v0, Lcjo$d;->vector_fire_activity:I

    goto :goto_0

    .line 77
    :pswitch_b
    sget v0, Lcjo$d;->vector_collections_activity:I

    goto :goto_0

    .line 80
    :pswitch_c
    sget v0, Lcjo$d;->vector_pin_activity:I

    goto :goto_0

    .line 83
    :pswitch_d
    sget v0, Lcjo$d;->vector_lightning_activity:I

    goto :goto_0

    .line 15
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_2
        :pswitch_5
        :pswitch_5
        :pswitch_2
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_8
        :pswitch_9
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_b
        :pswitch_1
        :pswitch_5
        :pswitch_7
        :pswitch_a
        :pswitch_0
        :pswitch_1
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;III)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x3

    const/4 v3, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 94
    packed-switch p1, :pswitch_data_0

    .line 234
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 96
    :pswitch_1
    sget v0, Lcjo$g;->social_both_follow:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 99
    :pswitch_2
    sget v0, Lcjo$g;->social_both_followed_by:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 103
    :pswitch_3
    if-eqz p3, :cond_1

    .line 104
    if-lez p6, :cond_0

    sget v0, Lcjo$f;->social_follow_and_follow_and_others:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v5

    aput-object p3, v1, v4

    .line 106
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    .line 105
    invoke-virtual {p0, v0, p6, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    sget v0, Lcjo$g;->social_follow_and_follow_with_two_users:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v5

    aput-object p3, v1, v4

    .line 107
    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 110
    :cond_1
    sget v0, Lcjo$g;->social_follow_and_follow:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 113
    :pswitch_4
    sget v0, Lcjo$g;->social_follower_of_follower:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 116
    :pswitch_5
    sget v0, Lcjo$g;->tweets_retweeted:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 119
    :pswitch_6
    sget v0, Lcjo$g;->social_follower_and_retweets:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 122
    :pswitch_7
    sget v0, Lcjo$g;->social_follow_and_reply:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 125
    :pswitch_8
    sget v0, Lcjo$g;->social_follower_and_reply:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 128
    :pswitch_9
    sget v0, Lcjo$g;->social_follow_and_like:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 131
    :pswitch_a
    sget v0, Lcjo$g;->social_follower_and_like:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 134
    :pswitch_b
    sget v0, Lcjo$g;->social_reply_to_follow:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 137
    :pswitch_c
    sget v0, Lcjo$g;->social_reply_to_follower:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 140
    :pswitch_d
    sget v0, Lcjo$g;->tweets_retweeted:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 143
    :pswitch_e
    sget v0, Lcjo$f;->social_like_and_retweets_count:I

    new-array v1, v3, [Ljava/lang/Object;

    int-to-long v2, p4

    .line 144
    invoke-static {p0, v2, v3}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    int-to-long v2, p5

    invoke-static {p0, v2, v3}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 143
    invoke-virtual {p0, v0, p5, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 147
    :pswitch_f
    if-ne p5, v4, :cond_2

    .line 148
    sget v0, Lcjo$g;->social_retweet_and_like_count:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 150
    :cond_2
    sget v0, Lcjo$f;->social_retweet_and_likes_count:I

    new-array v1, v3, [Ljava/lang/Object;

    int-to-long v2, p5

    .line 151
    invoke-static {p0, v2, v3}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    int-to-long v2, p4

    invoke-static {p0, v2, v3}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 150
    invoke-virtual {p0, v0, p4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 155
    :pswitch_10
    if-eqz p3, :cond_3

    .line 156
    sget v0, Lcjo$g;->social_like_with_two_user:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v5

    aput-object p3, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 158
    :cond_3
    sget v0, Lcjo$g;->social_like_with_user:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 162
    :pswitch_11
    sget v0, Lcjo$f;->social_like_count_with_user:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v5

    .line 163
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 162
    invoke-virtual {p0, v0, p4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 166
    :pswitch_12
    sget v0, Lcjo$f;->social_like_count:I

    new-array v1, v4, [Ljava/lang/Object;

    int-to-long v2, p4

    .line 167
    invoke-static {p0, v2, v3}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 166
    invoke-virtual {p0, v0, p4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 170
    :pswitch_13
    sget v0, Lcjo$g;->social_retweet_with_user:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 173
    :pswitch_14
    sget v0, Lcjo$f;->social_retweet_count:I

    new-array v1, v4, [Ljava/lang/Object;

    int-to-long v2, p5

    .line 174
    invoke-static {p0, v2, v3}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 173
    invoke-virtual {p0, v0, p5, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 177
    :pswitch_15
    sget v0, Lcjo$g;->social_top_news:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 180
    :pswitch_16
    sget v0, Lcjo$g;->top_tweet:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 183
    :pswitch_17
    sget v0, Lcjo$g;->social_trending_topic:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 186
    :pswitch_18
    if-nez p6, :cond_5

    .line 187
    if-nez p3, :cond_4

    .line 188
    sget v0, Lcjo$g;->social_conversation_tweet:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 190
    :cond_4
    sget v0, Lcjo$g;->social_conversation_tweet_two:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v5

    aput-object p3, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 193
    :cond_5
    if-nez p3, :cond_6

    .line 194
    sget v0, Lcjo$f;->in_reply_to_name_and_count:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v5

    .line 195
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const-string/jumbo v2, " "

    aput-object v2, v1, v3

    .line 194
    invoke-virtual {p0, v0, p6, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 197
    :cond_6
    sget v0, Lcjo$f;->social_proof_in_reply_multiple_names_and_count:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v5

    aput-object p3, v1, v4

    .line 199
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    .line 197
    invoke-virtual {p0, v0, p6, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 203
    :pswitch_19
    sget v0, Lcjo$g;->social_context_mutual_follow:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 206
    :pswitch_1a
    sget v0, Lcjo$g;->social_following:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 209
    :pswitch_1b
    sget v0, Lcjo$g;->highlight_context_nearby:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 212
    :pswitch_1c
    sget v0, Lcjo$g;->highlight_context_popular:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 215
    :pswitch_1d
    sget v0, Lcjo$g;->social_who_to_follow:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 218
    :pswitch_1e
    sget v0, Lcjo$g;->social_promoted_trend:I

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_1f
    move-object v0, p2

    .line 225
    goto/16 :goto_0

    .line 228
    :pswitch_20
    sget v0, Lcjo$g;->pinned_tweet:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 231
    :pswitch_21
    sget v0, Lcjo$g;->social_you_retweeted:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_17
        :pswitch_16
        :pswitch_0
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_1f
        :pswitch_1f
        :pswitch_1f
        :pswitch_1f
        :pswitch_1e
        :pswitch_0
        :pswitch_3
        :pswitch_20
        :pswitch_0
        :pswitch_1f
        :pswitch_21
    .end packed-switch
.end method

.method public static b(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;III)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 242
    sparse-switch p1, :sswitch_data_0

    .line 287
    invoke-static/range {p0 .. p6}, Lcom/twitter/ui/socialproof/a;->a(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 244
    :sswitch_0
    sget v0, Lcjo$g;->social_follow_and_follow_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 248
    :sswitch_1
    sget v0, Lcjo$g;->tweets_retweeted_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 251
    :sswitch_2
    sget v0, Lcjo$g;->social_follower_and_retweets_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 255
    :sswitch_3
    sget v0, Lcjo$g;->social_follow_and_reply_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 259
    :sswitch_4
    sget v0, Lcjo$g;->social_follower_and_reply_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 263
    :sswitch_5
    sget v0, Lcjo$g;->social_follow_and_like_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 266
    :sswitch_6
    sget v0, Lcjo$g;->social_follower_and_like_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 269
    :sswitch_7
    sget v0, Lcjo$g;->tweets_retweeted_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 272
    :sswitch_8
    if-eqz p3, :cond_0

    .line 273
    sget v0, Lcjo$g;->social_like_with_two_user_accessibility_description:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    aput-object p3, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 276
    :cond_0
    sget v0, Lcjo$g;->social_like_with_user_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 280
    :sswitch_9
    sget v0, Lcjo$f;->social_like_count_with_user_accessibility_description:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    .line 281
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    .line 280
    invoke-virtual {p0, v0, p4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 284
    :sswitch_a
    sget v0, Lcjo$g;->social_retweet_with_user_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 242
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x5 -> :sswitch_1
        0x6 -> :sswitch_2
        0x7 -> :sswitch_3
        0x8 -> :sswitch_4
        0x9 -> :sswitch_5
        0xa -> :sswitch_6
        0xd -> :sswitch_7
        0x10 -> :sswitch_8
        0x12 -> :sswitch_a
        0x21 -> :sswitch_9
    .end sparse-switch
.end method
