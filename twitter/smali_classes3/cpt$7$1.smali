.class Lcpt$7$1;
.super Lcpn;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcpt$7;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcpn",
        "<TOUT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcpt$7;

.field private final b:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+TIN;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+TOUT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcpt$7;)V
    .locals 1

    .prologue
    .line 154
    iput-object p1, p0, Lcpt$7$1;->a:Lcpt$7;

    invoke-direct {p0}, Lcpn;-><init>()V

    .line 155
    iget-object v0, p0, Lcpt$7$1;->a:Lcpt$7;

    iget-object v0, v0, Lcpt$7;->a:Ljava/lang/Iterable;

    .line 156
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcpt$7$1;->b:Ljava/util/Iterator;

    .line 157
    iget-object v0, p0, Lcpt$7$1;->b:Ljava/util/Iterator;

    .line 158
    invoke-direct {p0, v0}, Lcpt$7$1;->a(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcpt$7$1;->c:Ljava/util/Iterator;

    .line 157
    return-void
.end method

.method private a(Ljava/util/Iterator;)Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+TIN;>;)",
            "Ljava/util/Iterator",
            "<+TOUT;>;"
        }
    .end annotation

    .prologue
    .line 178
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lcpt$7$1;->a:Lcpt$7;

    iget-object v0, v0, Lcpt$7;->b:Lcpp;

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcpp;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 180
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 182
    :goto_0
    return-object v0

    .line 180
    :cond_0
    invoke-static {}, Lcpr;->c()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0

    .line 182
    :cond_1
    invoke-static {}, Lcpr;->c()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TOUT;"
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lcpt$7$1;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 162
    :goto_0
    iget-object v0, p0, Lcpt$7$1;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcpt$7$1;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcpt$7$1;->b:Ljava/util/Iterator;

    invoke-direct {p0, v0}, Lcpt$7$1;->a(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcpt$7$1;->c:Ljava/util/Iterator;

    goto :goto_0

    .line 166
    :cond_0
    iget-object v0, p0, Lcpt$7$1;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method
