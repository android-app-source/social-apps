.class Lcyb$1;
.super Ljava/lang/Thread;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcyb;->i()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcyb;


# direct methods
.method constructor <init>(Lcyb;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 385
    iput-object p1, p0, Lcyb$1;->b:Lcyb;

    iput p3, p0, Lcyb$1;->a:I

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 389
    :try_start_0
    iget-object v2, p0, Lcyb$1;->b:Lcyb;

    invoke-static {v2}, Lcyb;->a(Lcyb;)Ljava/util/concurrent/Semaphore;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 390
    iget-object v2, p0, Lcyb$1;->b:Lcyb;

    .line 391
    invoke-static {v2}, Lcyb;->b(Lcyb;)Lcyf;

    move-result-object v2

    invoke-interface {v2}, Lcyf;->f()I

    move-result v2

    if-ne v2, v1, :cond_0

    move v2, v1

    .line 392
    :goto_0
    iget-object v3, p0, Lcyb$1;->b:Lcyb;

    invoke-static {v3}, Lcyb;->b(Lcyb;)Lcyf;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lcyf;->e(I)V

    .line 393
    iget-object v3, p0, Lcyb$1;->b:Lcyb;

    invoke-static {v3}, Lcyb;->b(Lcyb;)Lcyf;

    move-result-object v3

    if-eqz v2, :cond_1

    :goto_1
    invoke-interface {v3, v0}, Lcyf;->c(I)V

    .line 395
    iget-object v0, p0, Lcyb$1;->b:Lcyb;

    iget v1, p0, Lcyb$1;->a:I

    invoke-static {v0, v1}, Lcyb;->a(Lcyb;I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398
    iget-object v0, p0, Lcyb$1;->b:Lcyb;

    invoke-static {v0}, Lcyb;->a(Lcyb;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 400
    :goto_2
    return-void

    :cond_0
    move v2, v0

    .line 391
    goto :goto_0

    :cond_1
    move v0, v1

    .line 393
    goto :goto_1

    .line 396
    :catch_0
    move-exception v0

    .line 398
    iget-object v0, p0, Lcyb$1;->b:Lcyb;

    invoke-static {v0}, Lcyb;->a(Lcyb;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcyb$1;->b:Lcyb;

    invoke-static {v1}, Lcyb;->a(Lcyb;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method
