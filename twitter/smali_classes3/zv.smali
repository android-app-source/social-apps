.class public Lzv;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lzu;

.field private final b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;


# direct methods
.method constructor <init>(Lzu;Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lzv;->a:Lzu;

    .line 50
    iput-object p2, p0, Lzv;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 51
    return-void
.end method

.method public static a(J)Lzv;
    .locals 4

    .prologue
    .line 43
    new-instance v0, Lzv;

    invoke-static {}, Lzu;->a()Lzu;

    move-result-object v1

    new-instance v2, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    .line 44
    invoke-virtual {v2, p0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lzv;-><init>(Lzu;Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;)V

    .line 43
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 54
    iget-object v1, p0, Lzv;->a:Lzu;

    iget-object v0, p0, Lzv;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:::show"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method public a(Ljava/lang/Long;)V
    .locals 6

    .prologue
    .line 98
    iget-object v1, p0, Lzv;->a:Lzu;

    if-nez p1, :cond_0

    iget-object v0, p0, Lzv;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 99
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    :goto_0
    const-string/jumbo v2, "moments:maker:canvas:operations:%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "crop"

    aput-object v5, v3, v4

    .line 98
    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 100
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lzv;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    goto :goto_0
.end method

.method public b()V
    .locals 6

    .prologue
    .line 58
    iget-object v1, p0, Lzv;->a:Lzu;

    iget-object v0, p0, Lzv;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:top_bar:finish_later:%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "click"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public b(Ljava/lang/Long;)V
    .locals 6

    .prologue
    .line 103
    iget-object v1, p0, Lzv;->a:Lzu;

    if-nez p1, :cond_0

    iget-object v0, p0, Lzv;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 104
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    :goto_0
    const-string/jumbo v2, "moments:maker:canvas:operations:%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "uncrop"

    aput-object v5, v3, v4

    .line 103
    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 105
    return-void

    .line 104
    :cond_0
    iget-object v0, p0, Lzv;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    goto :goto_0
.end method

.method public c()V
    .locals 6

    .prologue
    .line 62
    iget-object v1, p0, Lzv;->a:Lzu;

    iget-object v0, p0, Lzv;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:visibility_dialog:public:%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "click"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public d()V
    .locals 6

    .prologue
    .line 70
    iget-object v1, p0, Lzv;->a:Lzu;

    iget-object v0, p0, Lzv;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:visibility_dialog:private:%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "click"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public e()V
    .locals 6

    .prologue
    .line 74
    iget-object v1, p0, Lzv;->a:Lzu;

    iget-object v0, p0, Lzv;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:delete_dialog:delete:%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "click"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method public f()V
    .locals 4

    .prologue
    .line 82
    iget-object v1, p0, Lzv;->a:Lzu;

    iget-object v0, p0, Lzv;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:top_bar:done:click"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    .line 86
    iget-object v1, p0, Lzv;->a:Lzu;

    iget-object v0, p0, Lzv;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:settings:geo:opt_out"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public h()V
    .locals 4

    .prologue
    .line 90
    iget-object v1, p0, Lzv;->a:Lzu;

    iget-object v0, p0, Lzv;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:settings:nsfw:set"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public i()V
    .locals 6

    .prologue
    .line 108
    iget-object v1, p0, Lzv;->a:Lzu;

    iget-object v0, p0, Lzv;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:canvas:operations:%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "change_theme_color"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 109
    return-void
.end method
