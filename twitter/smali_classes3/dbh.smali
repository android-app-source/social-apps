.class public abstract Ldbh;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ltv/periscope/android/ui/broadcast/moderator/d;

.field final c:Ltv/periscope/android/ui/broadcast/moderator/g;

.field final d:Ltv/periscope/android/ui/broadcast/moderator/b;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/d;Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/b;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Ldbh;->a:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Ldbh;->b:Ltv/periscope/android/ui/broadcast/moderator/d;

    .line 30
    iput-object p3, p0, Ldbh;->c:Ltv/periscope/android/ui/broadcast/moderator/g;

    .line 31
    iput-object p4, p0, Ldbh;->d:Ltv/periscope/android/ui/broadcast/moderator/b;

    .line 32
    return-void
.end method

.method private d(Ltv/periscope/model/chat/Message;)Z
    .locals 4

    .prologue
    .line 72
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->B()Ljava/lang/String;

    move-result-object v0

    .line 73
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ldbh;->b:Ltv/periscope/android/ui/broadcast/moderator/d;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "punish-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ltv/periscope/android/ui/broadcast/moderator/d;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Ltv/periscope/model/chat/Message;)Z
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->D:Ltv/periscope/model/chat/MessageType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public a(Ltv/periscope/model/chat/MessageType$VoteType;)V
    .locals 3

    .prologue
    .line 64
    const-string/jumbo v0, "State"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "State: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ldbh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " - userVoted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public abstract a(Ltv/periscope/model/chat/Message;)Z
.end method

.method protected a(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/Message;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 81
    if-nez p2, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v0

    .line 84
    :cond_1
    invoke-virtual {p2}, Ltv/periscope/model/chat/Message;->B()Ljava/lang/String;

    move-result-object v1

    .line 85
    if-eqz v1, :cond_0

    .line 86
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->B()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldbh;->b:Ltv/periscope/android/ui/broadcast/moderator/d;

    .line 87
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ltv/periscope/android/ui/broadcast/moderator/d;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 60
    const-string/jumbo v0, "State"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "State: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ldbh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " - timerExpired"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public abstract b(Ltv/periscope/model/chat/Message;)V
.end method

.method public c()V
    .locals 3

    .prologue
    .line 53
    const-string/jumbo v0, "State"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "State: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ldbh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " - abort"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Ldbh;->b:Ltv/periscope/android/ui/broadcast/moderator/d;

    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/d;->a()V

    .line 57
    return-void
.end method

.method public c(Ltv/periscope/model/chat/Message;)Z
    .locals 3

    .prologue
    .line 39
    invoke-direct {p0, p1}, Ldbh;->e(Ltv/periscope/model/chat/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Ldbh;->d(Ltv/periscope/model/chat/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    iget-object v0, p0, Ldbh;->b:Ltv/periscope/android/ui/broadcast/moderator/d;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "punish-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->B()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/moderator/d;->a(Ljava/lang/String;)V

    .line 41
    new-instance v0, Ltv/periscope/android/ui/broadcast/moderator/a;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->H()Ltv/periscope/model/chat/MessageType$SentenceType;

    move-result-object v1

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->I()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v0, p1, v1, v2}, Ltv/periscope/android/ui/broadcast/moderator/a;-><init>(Ltv/periscope/model/chat/Message;Ltv/periscope/model/chat/MessageType$SentenceType;I)V

    .line 42
    iget-object v1, p0, Ldbh;->d:Ltv/periscope/android/ui/broadcast/moderator/b;

    invoke-virtual {v1, v0}, Ltv/periscope/android/ui/broadcast/moderator/b;->a(Ltv/periscope/android/ui/broadcast/moderator/a;)V

    .line 43
    const/4 v0, 0x1

    .line 45
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 3

    .prologue
    .line 68
    const-string/jumbo v0, "State"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "State: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ldbh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " - userPunished"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    return-void
.end method
