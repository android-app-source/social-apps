.class public Lug;
.super Lant;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;

.field private final b:Lcom/twitter/app/common/base/d;

.field private final c:Lcom/twitter/android/livevideo/a;


# direct methods
.method public constructor <init>(Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;Lank;Lcom/twitter/app/common/base/d;Lcom/twitter/android/livevideo/a;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lant;-><init>(Landroid/app/Activity;Lank;)V

    .line 63
    iput-object p1, p0, Lug;->a:Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;

    .line 64
    iput-object p3, p0, Lug;->b:Lcom/twitter/app/common/base/d;

    .line 65
    iput-object p4, p0, Lug;->c:Lcom/twitter/android/livevideo/a;

    .line 66
    return-void
.end method

.method static a(Landroid/app/Activity;)Lakr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            ")",
            "Lakr",
            "<",
            "Lcom/twitter/android/composer/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    new-instance v0, Lvp;

    invoke-direct {v0, p0}, Lvp;-><init>(Landroid/app/Activity;)V

    return-object v0
.end method

.method static a(Lcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 4

    .prologue
    .line 86
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 87
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "live_video_timeline"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 88
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 89
    return-object v0
.end method

.method static a(Lcom/twitter/android/livevideo/landing/b;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 6

    .prologue
    .line 162
    new-instance v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 163
    iget-wide v0, p0, Lcom/twitter/android/livevideo/landing/b;->a:J

    iput-wide v0, v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 164
    const/16 v0, 0x1c

    iput v0, v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 165
    new-instance v3, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;

    invoke-direct {v3}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;-><init>()V

    iget-wide v0, p0, Lcom/twitter/android/livevideo/landing/b;->a:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/livevideo/landing/b;->a:J

    .line 166
    :goto_0
    invoke-virtual {v3, v0, v1}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    iput-object v0, v2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ar:Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    .line 169
    return-object v2

    .line 165
    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/livevideo/landing/b;Ltt;Lbdh;)Lua;
    .locals 8

    .prologue
    .line 207
    new-instance v1, Lua;

    iget-wide v4, p2, Lcom/twitter/android/livevideo/landing/b;->a:J

    move-object v2, p0

    move-object v3, p1

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, Lua;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLtx;Lbdh;)V

    return-object v1
.end method

.method static a(Lcom/twitter/android/livevideo/landing/d;)Lvm;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/d;->e()Lvn;

    move-result-object v0

    return-object v0
.end method

.method static b(Lcom/twitter/android/livevideo/landing/b;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 4

    .prologue
    .line 176
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    iget-wide v2, p0, Lcom/twitter/android/livevideo/landing/b;->a:J

    .line 177
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v1, "live_video_timeline"

    .line 178
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v1, "highlights"

    .line 179
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v1, ""

    .line 180
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v1, 0x6

    .line 181
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 176
    return-object v0
.end method

.method static b(Landroid/app/Activity;)Lcom/twitter/android/livevideo/landing/b;
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/livevideo/landing/b;->a(Landroid/content/Intent;)Lcom/twitter/android/livevideo/landing/b;

    move-result-object v0

    return-object v0
.end method

.method static b()Lrx/f;
    .locals 1

    .prologue
    .line 96
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v0

    return-object v0
.end method

.method static c(Lcom/twitter/android/livevideo/landing/b;)Lcom/twitter/model/timeline/ap;
    .locals 4

    .prologue
    .line 188
    new-instance v0, Lcom/twitter/model/timeline/ap$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/ap$a;-><init>()V

    iget-wide v2, p0, Lcom/twitter/android/livevideo/landing/b;->a:J

    .line 189
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ap$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/ap$a;

    move-result-object v0

    const/16 v1, 0xd

    .line 190
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ap$a;->a(I)Lcom/twitter/model/timeline/ap$a;

    move-result-object v0

    .line 191
    invoke-virtual {v0}, Lcom/twitter/model/timeline/ap$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ap;

    .line 188
    return-object v0
.end method


# virtual methods
.method a(Lcom/twitter/android/livevideo/landing/b;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lcom/twitter/android/ai;
    .locals 4

    .prologue
    .line 111
    new-instance v0, Lcom/twitter/android/ai$a;

    iget-object v1, p0, Lug;->a:Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;

    .line 112
    invoke-virtual {v1}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/ai$a;-><init>(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    .line 113
    invoke-virtual {v0, v1}, Lcom/twitter/android/ai$a;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ai$a;

    const v1, 0x7f0a05ea

    .line 114
    invoke-virtual {v0, v1}, Lcom/twitter/android/ai$a;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ai$a;

    iget-wide v2, p1, Lcom/twitter/android/livevideo/landing/b;->a:J

    .line 115
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ai$a;->c(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ai$a;

    .line 116
    invoke-virtual {v0, p2}, Lcom/twitter/android/ai$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ai$a;

    .line 118
    invoke-virtual {p1, v0}, Lcom/twitter/android/livevideo/landing/b;->a(Lcom/twitter/android/ai$a;)Lcom/twitter/android/ai;

    move-result-object v0

    return-object v0
.end method

.method a()Lcom/twitter/android/livevideo/a;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lug;->c:Lcom/twitter/android/livevideo/a;

    return-object v0
.end method

.method a(Lcom/twitter/library/client/Session;)Lcom/twitter/android/media/selection/c;
    .locals 9

    .prologue
    .line 125
    new-instance v0, Lcom/twitter/android/media/selection/c;

    iget-object v1, p0, Lug;->a:Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;

    iget-object v2, p0, Lug;->a:Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;

    const-string/jumbo v3, "reply_composition"

    sget-object v4, Lcom/twitter/media/model/MediaType;->h:Ljava/util/EnumSet;

    const/4 v5, 0x1

    sget-object v6, Lcom/twitter/android/composer/ComposerType;->d:Lcom/twitter/android/composer/ComposerType;

    iget-object v8, p0, Lug;->a:Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;

    move-object v7, p1

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/media/selection/c;-><init>(Landroid/content/Context;Lbrp;Ljava/lang/String;Ljava/util/EnumSet;ILcom/twitter/android/composer/ComposerType;Lcom/twitter/library/client/Session;Lcom/twitter/app/common/util/j;)V

    .line 134
    return-object v0
.end method

.method c()Lcom/twitter/app/common/base/d;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lug;->b:Lcom/twitter/app/common/base/d;

    return-object v0
.end method

.method d()Lvo;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lug;->a:Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;

    return-object v0
.end method

.method e()Lcom/twitter/android/livevideo/landing/a$a;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lug;->a:Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;

    return-object v0
.end method

.method f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lug;->a:Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;

    return-object v0
.end method
