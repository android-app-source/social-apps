.class public Lzu;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lzu;
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lzu;

    invoke-direct {v0}, Lzu;-><init>()V

    return-object v0
.end method


# virtual methods
.method public varargs a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 24
    check-cast p3, [Ljava/lang/Object;

    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 25
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-direct {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>([Ljava/lang/String;)V

    .line 26
    if-eqz p1, :cond_0

    .line 27
    invoke-static {p1}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 29
    :cond_0
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 30
    return-void
.end method

.method public varargs a(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 20
    return-void
.end method
