.class public Lyj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lauj;Lauj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;",
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lyj;->a:Lauj;

    .line 23
    iput-object p2, p0, Lyj;->b:Lauj;

    .line 24
    return-void
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lyj;->b:Lauj;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lyj;->a:Lauj;

    .line 33
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->e(Lrx/c;)Lrx/c;

    move-result-object v0

    new-instance v1, Lyj$1;

    invoke-direct {v1, p0}, Lyj$1;-><init>(Lyj;)V

    .line 34
    invoke-virtual {v0, v1}, Lrx/c;->k(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 32
    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lyj;->a:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 45
    iget-object v0, p0, Lyj;->b:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 46
    return-void
.end method
