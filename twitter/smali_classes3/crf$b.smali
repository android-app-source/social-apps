.class Lcrf$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;
.implements Lrx/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcrf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Lrx/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/i",
            "<-",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/view/View;

.field private final c:Z


# direct methods
.method constructor <init>(Landroid/view/View;Lrx/i;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/i",
            "<-",
            "Landroid/view/View;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput-object p1, p0, Lcrf$b;->b:Landroid/view/View;

    .line 121
    iput-object p2, p0, Lcrf$b;->a:Lrx/i;

    .line 122
    iget-object v0, p0, Lcrf$b;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 123
    iput-boolean p3, p0, Lcrf$b;->c:Z

    .line 124
    return-void
.end method


# virtual methods
.method public B_()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcrf$b;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcrf$b;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcrf$b;->b:Landroid/view/View;

    .line 138
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcrf$b;->b:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPreDraw()Z
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcrf$b;->a:Lrx/i;

    iget-object v1, p0, Lcrf$b;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lrx/i;->a(Ljava/lang/Object;)V

    .line 129
    iget-boolean v0, p0, Lcrf$b;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
