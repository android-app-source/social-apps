.class public Lnc;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lna;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lna",
        "<",
        "Ljava/lang/String;",
        "Lcom/twitter/library/api/TwitterLocation;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lnb;


# direct methods
.method public constructor <init>(Lnb;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lnc;->a:Lnb;

    .line 22
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lnc;->a:Lnb;

    invoke-virtual {v0}, Lnb;->b()V

    .line 62
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lna$a;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lnc;->a(Ljava/lang/String;Lna$a;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lna$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lna$a",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/api/TwitterLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lnc;->a:Lnb;

    invoke-virtual {v0}, Lnb;->a()Z

    move-result v0

    .line 28
    if-eqz v0, :cond_0

    .line 29
    invoke-virtual {p0, p1, p2}, Lnc;->b(Ljava/lang/String;Lna$a;)V

    .line 42
    :goto_0
    return-void

    .line 31
    :cond_0
    new-instance v0, Lcbl;

    iget-object v1, p0, Lnc;->a:Lnb;

    .line 32
    invoke-virtual {v1, p1}, Lnb;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    .line 35
    invoke-virtual {v0}, Lcbi;->be_()I

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 36
    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 37
    invoke-virtual {p0, p1, p2}, Lnc;->b(Ljava/lang/String;Lna$a;)V

    goto :goto_0

    .line 39
    :cond_1
    invoke-interface {p2, p1, v0}, Lna$a;->a(Ljava/lang/Object;Lcbi;)V

    goto :goto_0
.end method

.method b(Ljava/lang/String;Lna$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lna$a",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/api/TwitterLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lnc;->a:Lnb;

    new-instance v1, Lnc$1;

    invoke-direct {v1, p0, p1, p2}, Lnc$1;-><init>(Lnc;Ljava/lang/String;Lna$a;)V

    invoke-virtual {v0, v1}, Lnb;->a(Lnb$a;)V

    .line 57
    return-void
.end method
