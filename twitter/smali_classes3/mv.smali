.class public Lmv;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lna;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lna",
        "<",
        "Lnk;",
        "Lcom/twitter/android/provider/e;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lmw;

.field private final b:Lmy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lnd;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Lcom/twitter/android/client/w;->g()I

    move-result v0

    .line 25
    new-instance v1, Lmw;

    invoke-direct {v1, p1, v0, p2}, Lmw;-><init>(Landroid/content/Context;ILnd;)V

    iput-object v1, p0, Lmv;->a:Lmw;

    .line 27
    new-instance v1, Lmy;

    invoke-direct {v1, p1, v0, p2}, Lmy;-><init>(Landroid/content/Context;ILnd;)V

    iput-object v1, p0, Lmv;->b:Lmy;

    .line 29
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lmv;->a:Lmw;

    invoke-virtual {v0}, Lmw;->a()V

    .line 60
    iget-object v0, p0, Lmv;->b:Lmy;

    invoke-virtual {v0}, Lmy;->a()V

    .line 61
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lna$a;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lnk;

    invoke-virtual {p0, p1, p2}, Lmv;->a(Lnk;Lna$a;)V

    return-void
.end method

.method public a(Lnk;Lna$a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnk;",
            "Lna$a",
            "<",
            "Lnk;",
            "Lcom/twitter/android/provider/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    iget v0, p1, Lnk;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 35
    iget-object v0, p0, Lmv;->a:Lmw;

    iget-object v1, p1, Lnk;->a:Ljava/lang/String;

    new-instance v2, Lmv$1;

    invoke-direct {v2, p0, p2, p1}, Lmv$1;-><init>(Lmv;Lna$a;Lnk;)V

    invoke-virtual {v0, v1, v2}, Lmw;->a(Ljava/lang/Object;Lna$a;)V

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    iget v0, p1, Lnk;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 43
    iget-object v0, p1, Lnk;->a:Ljava/lang/String;

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 44
    iget-object v0, p0, Lmv;->b:Lmy;

    iget-object v1, p1, Lnk;->a:Ljava/lang/String;

    new-instance v2, Lmv$2;

    invoke-direct {v2, p0, p2, p1}, Lmv$2;-><init>(Lmv;Lna$a;Lnk;)V

    invoke-virtual {v0, v1, v2}, Lmy;->a(Ljava/lang/Object;Lna$a;)V

    goto :goto_0

    .line 52
    :cond_2
    invoke-static {}, Lcbi;->f()Lcbi;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Lna$a;->a(Ljava/lang/Object;Lcbi;)V

    goto :goto_0
.end method
