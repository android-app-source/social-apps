.class Lcpt$9$1;
.super Lcpn;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcpt$9;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcpn",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcpt$9;

.field private final b:Lcpu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpu",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private final c:Lcpu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpu",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcpt$9;)V
    .locals 2

    .prologue
    .line 396
    iput-object p1, p0, Lcpt$9$1;->a:Lcpt$9;

    invoke-direct {p0}, Lcpn;-><init>()V

    .line 397
    new-instance v0, Lcpu;

    iget-object v1, p0, Lcpt$9$1;->a:Lcpt$9;

    iget-object v1, v1, Lcpt$9;->a:Ljava/lang/Iterable;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1}, Lcpu;-><init>(Ljava/util/Iterator;)V

    iput-object v0, p0, Lcpt$9$1;->b:Lcpu;

    .line 398
    new-instance v0, Lcpu;

    iget-object v1, p0, Lcpt$9$1;->a:Lcpt$9;

    iget-object v1, v1, Lcpt$9;->b:Ljava/lang/Iterable;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1}, Lcpu;-><init>(Ljava/util/Iterator;)V

    iput-object v0, p0, Lcpt$9$1;->c:Lcpu;

    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 408
    iget-object v0, p0, Lcpt$9$1;->c:Lcpu;

    invoke-virtual {v0}, Lcpu;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 409
    iget-object v0, p0, Lcpt$9$1;->b:Lcpu;

    invoke-virtual {v0}, Lcpu;->next()Ljava/lang/Object;

    move-result-object v0

    .line 415
    :goto_0
    return-object v0

    .line 410
    :cond_0
    iget-object v0, p0, Lcpt$9$1;->b:Lcpu;

    invoke-virtual {v0}, Lcpu;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 411
    iget-object v0, p0, Lcpt$9$1;->c:Lcpu;

    invoke-virtual {v0}, Lcpu;->next()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 412
    :cond_1
    iget-object v0, p0, Lcpt$9$1;->a:Lcpt$9;

    iget-object v0, v0, Lcpt$9;->c:Ljava/util/Comparator;

    iget-object v1, p0, Lcpt$9$1;->b:Lcpu;

    invoke-virtual {v1}, Lcpu;->c()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcpt$9$1;->c:Lcpu;

    invoke-virtual {v2}, Lcpu;->c()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_2

    .line 413
    iget-object v0, p0, Lcpt$9$1;->b:Lcpu;

    invoke-virtual {v0}, Lcpu;->next()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 415
    :cond_2
    iget-object v0, p0, Lcpt$9$1;->c:Lcpu;

    invoke-virtual {v0}, Lcpu;->next()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcpt$9$1;->b:Lcpu;

    invoke-virtual {v0}, Lcpu;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcpt$9$1;->c:Lcpu;

    invoke-virtual {v0}, Lcpu;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
