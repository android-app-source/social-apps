.class public Lsw;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcbi;Lcbi;)Lcbi;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lsx;",
            ">;",
            "Lcbi",
            "<",
            "Lsx;",
            ">;)",
            "Lcbi",
            "<",
            "Lsx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    invoke-virtual {p1}, Lcbi;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx;

    .line 157
    iget-wide v2, v0, Lsx;->c:J

    invoke-static {p0, v2, v3}, Lsw;->a(Lcbi;J)Lsx;

    move-result-object v2

    .line 158
    if-eqz v2, :cond_0

    .line 159
    invoke-virtual {v2, v0}, Lsx;->a(Lsx;)V

    goto :goto_0

    .line 162
    :cond_1
    return-object p0
.end method

.method public static a(Ltg;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 105
    new-instance v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v4}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 106
    iput p1, v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 107
    iget-boolean v0, p0, Ltg;->h:Z

    if-eqz v0, :cond_0

    move-wide v0, v2

    :goto_0
    iput-wide v0, v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 108
    iget v0, p0, Ltg;->g:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 109
    iget-object v0, p0, Ltg;->a:Ljava/lang/String;

    iput-object v0, v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "custom_interest"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v6, p0, Ltg;->c:J

    cmp-long v0, v6, v2

    if-ltz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, ",ITS_parent="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ltg;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 116
    :goto_2
    return-object v4

    .line 107
    :cond_0
    const-wide/16 v0, 0x1

    goto :goto_0

    .line 110
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_1

    .line 113
    :cond_2
    iget-wide v0, p0, Ltg;->b:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ITS_parent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v6, p0, Ltg;->c:J

    cmp-long v0, v6, v2

    if-ltz v0, :cond_3

    iget-wide v2, p0, Ltg;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    goto :goto_2

    :cond_3
    const-string/jumbo v0, ""

    goto :goto_3
.end method

.method public static a(Lsx;)Lcom/twitter/util/collection/q;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsx;",
            ")",
            "Lcom/twitter/util/collection/q",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    new-instance v0, Lcom/twitter/util/collection/q;

    iget-object v1, p0, Lsx;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lsx;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p0, Lsx;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/util/collection/q;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltg;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .line 122
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v3

    .line 123
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 124
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltg;

    .line 125
    if-eqz v0, :cond_0

    .line 126
    invoke-static {v0, v1}, Lsw;->a(Ltg;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 123
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 129
    :cond_1
    invoke-virtual {v3}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static a(Ljava/util/List;Lsx;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lsx;",
            ">(",
            "Ljava/util/List",
            "<TT;>;TT;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 178
    invoke-static {p1}, Lsw;->a(Lsx;)Lcom/twitter/util/collection/q;

    move-result-object v2

    .line 179
    const/4 v1, 0x0

    .line 180
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx;

    .line 181
    invoke-static {v0}, Lsw;->a(Lsx;)Lcom/twitter/util/collection/q;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/q;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    const/4 v0, 0x1

    .line 187
    :goto_0
    if-eqz v0, :cond_1

    .line 190
    :goto_1
    return-object p0

    :cond_1
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move-object p0, v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static a()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Lsx;",
            "Lcom/twitter/util/collection/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    new-instance v0, Lsw$1;

    invoke-direct {v0}, Lsw$1;-><init>()V

    return-object v0
.end method

.method public static a(Lcbi;J)Lsx;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lsx;",
            ">;J)",
            "Lsx;"
        }
    .end annotation

    .prologue
    .line 167
    invoke-virtual {p0}, Lcbi;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx;

    .line 168
    iget-wide v2, v0, Lsx;->b:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, v0, Lsx;->b:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 172
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
