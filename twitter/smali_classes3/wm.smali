.class public Lwm;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbdn;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string/jumbo v0, "0"

    invoke-static {p1, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lwm;->a:Ljava/lang/String;

    .line 29
    iput-object p1, p0, Lwm;->b:Ljava/lang/String;

    .line 30
    iput-boolean p2, p0, Lwm;->c:Z

    .line 31
    return-void
.end method

.method private static b(Lcom/twitter/library/service/d$a;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 71
    const-string/jumbo v0, "include_trends"

    invoke-virtual {p0, v0, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "live_video_in_hero"

    .line 72
    invoke-static {}, Lwk;->b()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "pc"

    .line 73
    invoke-virtual {v0, v1, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "tr_img"

    const-string/jumbo v2, "top"

    .line 74
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "tr_desc"

    .line 75
    invoke-virtual {v0, v1, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "tr_ctx_rel"

    .line 76
    invoke-virtual {v0, v1, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "tr_ctx_cnt"

    .line 77
    invoke-virtual {v0, v1, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "tr_empty_ok"

    .line 78
    invoke-virtual {v0, v1, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "max_trends"

    const-wide/16 v2, 0x5

    .line 79
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 80
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    const-string/jumbo v0, "modern_guide"

    return-object v0
.end method

.method public a(Lcom/twitter/library/service/d$a;)V
    .locals 2

    .prologue
    .line 62
    iget-boolean v0, p0, Lwm;->c:Z

    if-eqz v0, :cond_0

    .line 63
    invoke-static {p1}, Lwm;->b(Lcom/twitter/library/service/d$a;)V

    .line 65
    :cond_0
    iget-object v0, p0, Lwm;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 66
    const-string/jumbo v0, "category_id"

    iget-object v1, p0, Lwm;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 68
    :cond_1
    return-void
.end method

.method public b()Lcom/twitter/network/HttpOperation$RequestMethod;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lwm;->a:Ljava/lang/String;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lwm;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 55
    const/4 v0, 0x0

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
