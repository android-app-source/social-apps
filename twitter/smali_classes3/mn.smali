.class public Lmn;
.super Lmq;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmq",
        "<",
        "Ljava/lang/String;",
        "Lcom/twitter/android/provider/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 28
    const v0, 0x7f04013f

    invoke-direct {p0, p1, v0}, Lmn;-><init>(Landroid/content/Context;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lmq;-><init>(Landroid/content/Context;)V

    .line 33
    iput p2, p0, Lmn;->a:I

    .line 34
    const-string/jumbo v0, "hashflags_in_composer_android_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lmn;->b:Z

    .line 35
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/twitter/android/provider/c;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 40
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lmn;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 23
    check-cast p2, Lcom/twitter/android/provider/c;

    invoke-virtual {p0, p1, p2, p3}, Lmn;->a(Landroid/content/Context;Lcom/twitter/android/provider/c;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/provider/c;)V
    .locals 5

    .prologue
    .line 45
    move-object v0, p1

    check-cast v0, Landroid/widget/TextView;

    .line 46
    iget-boolean v1, p0, Lmn;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p3, Lcom/twitter/android/provider/c;->c:Lcom/twitter/library/view/b;

    if-eqz v1, :cond_0

    .line 47
    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v2, p3, Lcom/twitter/android/provider/c;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 48
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p3, Lcom/twitter/android/provider/c;->c:Lcom/twitter/library/view/b;

    const/4 v4, 0x1

    invoke-static {v2, v1, v3, v0, v4}, Lcom/twitter/library/view/b;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Lcom/twitter/library/view/b;Landroid/view/View;Z)I

    .line 49
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v1, p3, Lcom/twitter/android/provider/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p3, Lcom/twitter/android/provider/c;

    invoke-virtual {p0, p1, p2, p3}, Lmn;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/provider/c;)V

    return-void
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lmn;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/provider/c;

    .line 58
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/twitter/android/provider/c;->b:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method
