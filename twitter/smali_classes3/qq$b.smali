.class final Lqq$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lqx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lqq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lqq;

.field private final b:Lant;

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/av/watchmode/WatchModeLayoutManager;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/client/o$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/av/watchmode/view/b;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/ui/anim/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/av/watchmode/view/m;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laol;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laon;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/av/watchmode/view/r;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lqq;Lant;)V
    .locals 1

    .prologue
    .line 344
    iput-object p1, p0, Lqq$b;->a:Lqq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345
    invoke-static {p2}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lant;

    iput-object v0, p0, Lqq$b;->b:Lant;

    .line 346
    invoke-direct {p0}, Lqq$b;->c()V

    .line 347
    return-void
.end method

.method synthetic constructor <init>(Lqq;Lant;Lqq$1;)V
    .locals 0

    .prologue
    .line 313
    invoke-direct {p0, p1, p2}, Lqq$b;-><init>(Lqq;Lant;)V

    return-void
.end method

.method private c()V
    .locals 8

    .prologue
    .line 352
    iget-object v0, p0, Lqq$b;->b:Lant;

    .line 354
    invoke-static {v0}, Lanu;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 353
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq$b;->c:Lcta;

    .line 356
    iget-object v0, p0, Lqq$b;->c:Lcta;

    .line 358
    invoke-static {v0}, Lanx;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 357
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq$b;->d:Lcta;

    .line 363
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lqq$b;->c:Lcta;

    .line 362
    invoke-static {v0, v1}, Lcom/twitter/android/av/watchmode/a;->a(Lcsd;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 361
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq$b;->e:Lcta;

    .line 365
    iget-object v0, p0, Lqq$b;->e:Lcta;

    .line 367
    invoke-static {v0}, Lqz;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 366
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq$b;->f:Lcta;

    .line 370
    iget-object v0, p0, Lqq$b;->c:Lcta;

    .line 372
    invoke-static {v0}, Lcom/twitter/android/av/watchmode/view/c;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 371
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq$b;->g:Lcta;

    .line 374
    iget-object v0, p0, Lqq$b;->a:Lqq;

    .line 377
    invoke-static {v0}, Lqq;->a(Lqq;)Lcta;

    move-result-object v0

    iget-object v1, p0, Lqq$b;->g:Lcta;

    iget-object v2, p0, Lqq$b;->e:Lcta;

    .line 376
    invoke-static {v0, v1, v2}, Lra;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 375
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq$b;->h:Lcta;

    .line 384
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lqq$b;->h:Lcta;

    iget-object v2, p0, Lqq$b;->c:Lcta;

    .line 383
    invoke-static {v0, v1, v2}, Lcom/twitter/android/av/watchmode/view/n;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 382
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq$b;->i:Lcta;

    .line 388
    iget-object v0, p0, Lqq$b;->c:Lcta;

    .line 389
    invoke-static {v0}, Laom;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq$b;->j:Lcta;

    .line 391
    iget-object v0, p0, Lqq$b;->j:Lcta;

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq$b;->k:Lcta;

    .line 396
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lqq$b;->d:Lcta;

    iget-object v2, p0, Lqq$b;->f:Lcta;

    iget-object v3, p0, Lqq$b;->a:Lqq;

    .line 399
    invoke-static {v3}, Lqq;->b(Lqq;)Lcta;

    move-result-object v3

    iget-object v4, p0, Lqq$b;->a:Lqq;

    .line 400
    invoke-static {v4}, Lqq;->a(Lqq;)Lcta;

    move-result-object v4

    iget-object v5, p0, Lqq$b;->a:Lqq;

    .line 401
    invoke-static {v5}, Lqq;->c(Lqq;)Lcta;

    move-result-object v5

    iget-object v6, p0, Lqq$b;->i:Lcta;

    iget-object v7, p0, Lqq$b;->k:Lcta;

    .line 395
    invoke-static/range {v0 .. v7}, Lcom/twitter/android/av/watchmode/view/s;->a(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 394
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq$b;->l:Lcta;

    .line 405
    iget-object v0, p0, Lqq$b;->l:Lcta;

    .line 406
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq$b;->m:Lcta;

    .line 408
    const/4 v0, 0x3

    const/4 v1, 0x0

    .line 409
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lqq$b;->a:Lqq;

    .line 410
    invoke-static {v1}, Lqq;->e(Lqq;)Lcta;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lqq$b;->a:Lqq;

    .line 411
    invoke-static {v1}, Lqq;->d(Lqq;)Lcta;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lqq$b;->m:Lcta;

    .line 412
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 413
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lqq$b;->n:Lcta;

    .line 415
    iget-object v0, p0, Lqq$b;->l:Lcta;

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lqq$b;->o:Lcta;

    .line 416
    return-void
.end method


# virtual methods
.method public a()Laog;
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lqq$b;->o:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laog;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 420
    iget-object v0, p0, Lqq$b;->n:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
