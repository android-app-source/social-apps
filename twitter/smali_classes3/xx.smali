.class public Lxx;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laun;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laun",
        "<",
        "Ljava/util/Collection",
        "<",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Long;",
        "Lcep;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/database/lru/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/database/lru/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lxx;->a:Lcom/twitter/database/lru/l;

    .line 29
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Lrx/g;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lxx;->a(Ljava/util/Collection;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Collection;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/g",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lxx;->a:Lcom/twitter/database/lru/l;

    invoke-interface {v0, p1}, Lcom/twitter/database/lru/l;->a(Ljava/lang/Iterable;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Map;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;)",
            "Lrx/g",
            "<",
            "Lcqx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lxx;->a:Lcom/twitter/database/lru/l;

    invoke-interface {v0, p1}, Lcom/twitter/database/lru/l;->a(Ljava/util/Map;)Lrx/g;

    move-result-object v0

    return-object v0
.end method
