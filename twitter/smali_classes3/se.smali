.class public Lse;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/twitter/android/composer/mediarail/view/MediaRailView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/twitter/android/composer/mediarail/view/MediaRailView$a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

.field private final b:Lsd;

.field private final c:Landroid/support/v4/app/LoaderManager;

.field private final d:I

.field private final e:I

.field private f:Lse$a;

.field private g:Z

.field private h:I

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/composer/mediarail/view/MediaRailView;Landroid/support/v4/app/LoaderManager;I)V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lsd;

    invoke-direct {v0, p1}, Lsd;-><init>(Lcom/twitter/android/composer/mediarail/view/MediaRailView;)V

    invoke-direct {p0, p1, v0, p2, p3}, Lse;-><init>(Lcom/twitter/android/composer/mediarail/view/MediaRailView;Lsd;Landroid/support/v4/app/LoaderManager;I)V

    .line 71
    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/composer/mediarail/view/MediaRailView;Lsd;Landroid/support/v4/app/LoaderManager;I)V
    .locals 6

    .prologue
    .line 66
    const/16 v5, 0x14

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lse;-><init>(Lcom/twitter/android/composer/mediarail/view/MediaRailView;Lsd;Landroid/support/v4/app/LoaderManager;II)V

    .line 67
    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/composer/mediarail/view/MediaRailView;Lsd;Landroid/support/v4/app/LoaderManager;II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-boolean v1, p0, Lse;->g:Z

    .line 50
    const/16 v0, 0x14

    iput v0, p0, Lse;->h:I

    .line 51
    iput-boolean v1, p0, Lse;->i:Z

    .line 55
    iput-object p1, p0, Lse;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    .line 56
    iget-object v0, p0, Lse;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->setOnMediaRailItemClickedListener(Lcom/twitter/android/composer/mediarail/view/MediaRailView$a;)V

    .line 57
    iput-object p2, p0, Lse;->b:Lsd;

    .line 58
    iput-object p3, p0, Lse;->c:Landroid/support/v4/app/LoaderManager;

    .line 59
    iput p4, p0, Lse;->d:I

    .line 60
    iput p5, p0, Lse;->h:I

    .line 61
    invoke-virtual {p1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0308

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lse;->e:I

    .line 62
    return-void
.end method


# virtual methods
.method public a(ILcom/twitter/android/composer/mediarail/view/d;Lsg;)V
    .locals 3

    .prologue
    .line 189
    iget-object v0, p0, Lse;->f:Lse$a;

    if-eqz v0, :cond_0

    .line 190
    instance-of v0, p3, Lsf;

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lse;->f:Lse$a;

    check-cast p3, Lsf;

    invoke-virtual {p3}, Lsf;->c()I

    move-result v1

    invoke-interface {v0, v1}, Lse$a;->a(I)V

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    instance-of v0, p3, Lsh;

    if-eqz v0, :cond_0

    .line 193
    check-cast p2, Lcom/twitter/android/composer/mediarail/view/c;

    invoke-virtual {p2}, Lcom/twitter/android/composer/mediarail/view/c;->a()Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_0

    .line 195
    iget-object v1, p0, Lse;->f:Lse$a;

    check-cast p3, Lsh;

    invoke-virtual {p3}, Lsh;->a()Lcom/twitter/media/model/b;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lse$a;->a(Lcom/twitter/media/model/b;Lcom/twitter/model/media/EditableMedia;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 219
    if-nez p1, :cond_0

    .line 228
    :goto_0
    return-void

    .line 222
    :cond_0
    const-string/jumbo v0, "VisibleState"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 223
    if-eqz v0, :cond_1

    .line 224
    iget-object v0, p0, Lse;->b:Lsd;

    invoke-virtual {v0}, Lsd;->d()V

    goto :goto_0

    .line 226
    :cond_1
    iget-object v0, p0, Lse;->b:Lsd;

    invoke-virtual {v0}, Lsd;->c()V

    goto :goto_0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 167
    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-virtual {p0}, Lse;->h()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 168
    iget-object v0, p0, Lse;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    invoke-virtual {v0, p2}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->setItems(Landroid/database/Cursor;)V

    .line 169
    invoke-virtual {p0}, Lse;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    invoke-virtual {p0}, Lse;->e()V

    .line 173
    :cond_0
    invoke-virtual {p0}, Lse;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lse;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->setVisibility(I)V

    .line 179
    :cond_1
    :goto_0
    return-void

    .line 177
    :cond_2
    invoke-virtual {p0}, Lse;->m()V

    goto :goto_0
.end method

.method public a(Lse$a;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lse;->f:Lse$a;

    .line 100
    return-void
.end method

.method protected a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 241
    invoke-static {p1}, Lcom/twitter/android/util/z;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 242
    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v1, p0, Lse;->e:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lse;->b:Lsd;

    invoke-virtual {v0}, Lsd;->f()V

    .line 133
    return-void
.end method

.method public c()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 208
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 209
    const-string/jumbo v1, "VisibleState"

    iget-object v2, p0, Lse;->b:Lsd;

    invoke-virtual {v2}, Lsd;->a()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 210
    return-object v0
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lse;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    invoke-virtual {v0}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lse;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lse;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 271
    invoke-virtual {p0}, Lse;->p()V

    .line 272
    return-void
.end method

.method public f()Lcom/twitter/android/composer/mediarail/view/MediaRailView;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lse;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    return-object v0
.end method

.method protected g()Z
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lse;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    invoke-virtual {v0}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lse;->b:Lsd;

    invoke-virtual {v0}, Lsd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected h()I
    .locals 1

    .prologue
    .line 95
    const/16 v0, 0xa

    return v0
.end method

.method public i()V
    .locals 3

    .prologue
    .line 103
    iget-boolean v0, p0, Lse;->i:Z

    if-eqz v0, :cond_0

    .line 104
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "loadMediaRailItems should only be called once. If you want to update items call refresh() instead"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lse;->i:Z

    .line 110
    invoke-virtual {p0}, Lse;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 111
    iget-object v0, p0, Lse;->c:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lse;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 112
    invoke-virtual {p0}, Lse;->o()Z

    move-result v0

    if-nez v0, :cond_1

    .line 113
    invoke-virtual {p0}, Lse;->e()V

    .line 120
    :cond_1
    :goto_0
    return-void

    .line 118
    :cond_2
    invoke-virtual {p0}, Lse;->m()V

    goto :goto_0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lse;->b:Lsd;

    invoke-virtual {v0}, Lsd;->e()V

    .line 140
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lse;->b:Lsd;

    invoke-virtual {v0}, Lsd;->c()V

    .line 147
    return-void
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lse;->b:Lsd;

    invoke-virtual {v0}, Lsd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lse;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected m()V
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lse;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->setVisibility(I)V

    .line 251
    return-void
.end method

.method protected n()Z
    .locals 5

    .prologue
    .line 254
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    iget-object v1, p0, Lse;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    invoke-virtual {v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected o()Z
    .locals 1

    .prologue
    .line 258
    iget-boolean v0, p0, Lse;->g:Z

    return v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 162
    new-instance v0, Lcom/twitter/media/util/n;

    iget-object v1, p0, Lse;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    invoke-virtual {v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lse;->h:I

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/twitter/media/util/n;-><init>(Landroid/content/Context;ZZI)V

    return-object v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lse;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lse;->a:Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->setItems(Landroid/database/Cursor;)V

    .line 184
    return-void
.end method

.method protected p()V
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x1

    iput-boolean v0, p0, Lse;->g:Z

    .line 263
    return-void
.end method
