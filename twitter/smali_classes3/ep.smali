.class public final Lep;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lep$a;,
        Lep$b;
    }
.end annotation


# instance fields
.field protected final a:Lep;

.field protected final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lep$b;",
            ">;"
        }
    .end annotation
.end field

.field protected c:Z

.field protected final d:Z

.field protected e:I

.field protected f:I

.field protected g:I

.field protected h:[I

.field protected i:[Ler;

.field protected j:[Lep$a;

.field protected k:I

.field protected l:I

.field protected m:Ljava/util/BitSet;

.field private final n:I

.field private transient o:Z

.field private p:Z

.field private q:Z

.field private r:Z


# direct methods
.method private constructor <init>(IZIZ)V
    .locals 2

    .prologue
    const/16 v0, 0x10

    .line 268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269
    const/4 v1, 0x0

    iput-object v1, p0, Lep;->a:Lep;

    .line 270
    iput p3, p0, Lep;->n:I

    .line 271
    iput-boolean p2, p0, Lep;->c:Z

    .line 272
    iput-boolean p4, p0, Lep;->d:Z

    .line 274
    if-ge p1, v0, :cond_1

    move p1, v0

    .line 288
    :cond_0
    :goto_0
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p0, p1}, Lep;->f(I)Lep$b;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lep;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 289
    return-void

    .line 280
    :cond_1
    add-int/lit8 v1, p1, -0x1

    and-int/2addr v1, p1

    if-eqz v1, :cond_0

    .line 282
    :goto_1
    if-ge v0, p1, :cond_2

    .line 283
    add-int/2addr v0, v0

    goto :goto_1

    :cond_2
    move p1, v0

    .line 285
    goto :goto_0
.end method

.method private constructor <init>(Lep;ZIZLep$b;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297
    iput-object p1, p0, Lep;->a:Lep;

    .line 298
    iput p3, p0, Lep;->n:I

    .line 299
    iput-boolean p2, p0, Lep;->c:Z

    .line 300
    iput-boolean p4, p0, Lep;->d:Z

    .line 301
    const/4 v0, 0x0

    iput-object v0, p0, Lep;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 304
    iget v0, p5, Lep$b;->a:I

    iput v0, p0, Lep;->e:I

    .line 305
    iget v0, p5, Lep$b;->b:I

    iput v0, p0, Lep;->g:I

    .line 306
    iget-object v0, p5, Lep$b;->c:[I

    iput-object v0, p0, Lep;->h:[I

    .line 307
    iget-object v0, p5, Lep$b;->d:[Ler;

    iput-object v0, p0, Lep;->i:[Ler;

    .line 308
    iget-object v0, p5, Lep$b;->e:[Lep$a;

    iput-object v0, p0, Lep;->j:[Lep$a;

    .line 309
    iget v0, p5, Lep$b;->f:I

    iput v0, p0, Lep;->k:I

    .line 310
    iget v0, p5, Lep$b;->g:I

    iput v0, p0, Lep;->l:I

    .line 311
    iget v0, p5, Lep$b;->h:I

    iput v0, p0, Lep;->f:I

    .line 314
    const/4 v0, 0x0

    iput-boolean v0, p0, Lep;->o:Z

    .line 315
    iput-boolean v1, p0, Lep;->p:Z

    .line 316
    iput-boolean v1, p0, Lep;->q:Z

    .line 317
    iput-boolean v1, p0, Lep;->r:Z

    .line 318
    return-void
.end method

.method public static a()Lep;
    .locals 4

    .prologue
    .line 350
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 352
    long-to-int v2, v0

    const/16 v3, 0x20

    ushr-long/2addr v0, v3

    long-to-int v0, v0

    add-int/2addr v0, v2

    or-int/lit8 v0, v0, 0x1

    .line 353
    invoke-static {v0}, Lep;->a(I)Lep;

    move-result-object v0

    return-object v0
.end method

.method protected static a(I)Lep;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 361
    new-instance v0, Lep;

    const/16 v1, 0x40

    invoke-direct {v0, v1, v2, p0, v2}, Lep;-><init>(IZIZ)V

    return-object v0
.end method

.method private static a(ILjava/lang/String;[II)Ler;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1115
    const/4 v0, 0x4

    if-ge p3, v0, :cond_0

    .line 1116
    packed-switch p3, :pswitch_data_0

    .line 1126
    :cond_0
    invoke-static {p1, p0, p2, p3}, Lev;->a(Ljava/lang/String;I[II)Lev;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1118
    :pswitch_0
    new-instance v0, Les;

    aget v1, p2, v1

    invoke-direct {v0, p1, p0, v1}, Les;-><init>(Ljava/lang/String;II)V

    goto :goto_0

    .line 1120
    :pswitch_1
    new-instance v0, Let;

    aget v1, p2, v1

    aget v2, p2, v2

    invoke-direct {v0, p1, p0, v1, v2}, Let;-><init>(Ljava/lang/String;III)V

    goto :goto_0

    .line 1122
    :pswitch_2
    new-instance v0, Leu;

    aget v3, p2, v1

    aget v4, p2, v2

    const/4 v1, 0x2

    aget v5, p2, v1

    move-object v1, p1

    move v2, p0

    invoke-direct/range {v0 .. v5}, Leu;-><init>(Ljava/lang/String;IIII)V

    goto :goto_0

    .line 1116
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(ILep$a;)V
    .locals 2

    .prologue
    .line 904
    iget-object v0, p0, Lep;->m:Ljava/util/BitSet;

    if-nez v0, :cond_0

    .line 905
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lep;->m:Ljava/util/BitSet;

    .line 906
    iget-object v0, p0, Lep;->m:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->set(I)V

    .line 920
    :goto_0
    iget-object v0, p0, Lep;->j:[Lep$a;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 921
    iget v0, p0, Lep;->e:I

    iget v1, p2, Lep$a;->d:I

    sub-int/2addr v0, v1

    iput v0, p0, Lep;->e:I

    .line 923
    const/4 v0, -0x1

    iput v0, p0, Lep;->f:I

    .line 924
    return-void

    .line 908
    :cond_0
    iget-object v0, p0, Lep;->m:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 910
    iget-boolean v0, p0, Lep;->d:Z

    if-eqz v0, :cond_1

    .line 911
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lep;->e(I)V

    .line 914
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lep;->c:Z

    goto :goto_0

    .line 916
    :cond_2
    iget-object v0, p0, Lep;->m:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->set(I)V

    goto :goto_0
.end method

.method private a(ILer;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 821
    iget-boolean v0, p0, Lep;->p:Z

    if-eqz v0, :cond_0

    .line 822
    invoke-direct {p0}, Lep;->h()V

    .line 825
    :cond_0
    iget-boolean v0, p0, Lep;->o:Z

    if-eqz v0, :cond_1

    .line 826
    invoke-direct {p0}, Lep;->e()V

    .line 829
    :cond_1
    iget v0, p0, Lep;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lep;->e:I

    .line 834
    iget v0, p0, Lep;->g:I

    and-int v1, p1, v0

    .line 835
    iget-object v0, p0, Lep;->i:[Ler;

    aget-object v0, v0, v1

    if-nez v0, :cond_4

    .line 836
    iget-object v0, p0, Lep;->h:[I

    shl-int/lit8 v2, p1, 0x8

    aput v2, v0, v1

    .line 837
    iget-boolean v0, p0, Lep;->q:Z

    if-eqz v0, :cond_2

    .line 838
    invoke-direct {p0}, Lep;->j()V

    .line 840
    :cond_2
    iget-object v0, p0, Lep;->i:[Ler;

    aput-object p2, v0, v1

    .line 887
    :goto_0
    iget-object v0, p0, Lep;->h:[I

    array-length v0, v0

    .line 888
    iget v1, p0, Lep;->e:I

    shr-int/lit8 v2, v0, 0x1

    if-le v1, v2, :cond_3

    .line 889
    shr-int/lit8 v1, v0, 0x2

    .line 893
    iget v2, p0, Lep;->e:I

    sub-int/2addr v0, v1

    if-le v2, v0, :cond_a

    .line 894
    iput-boolean v5, p0, Lep;->o:Z

    .line 900
    :cond_3
    :goto_1
    return-void

    .line 845
    :cond_4
    iget-boolean v0, p0, Lep;->r:Z

    if-eqz v0, :cond_5

    .line 846
    invoke-direct {p0}, Lep;->i()V

    .line 848
    :cond_5
    iget v0, p0, Lep;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lep;->k:I

    .line 849
    iget-object v0, p0, Lep;->h:[I

    aget v2, v0, v1

    .line 850
    and-int/lit16 v0, v2, 0xff

    .line 851
    if-nez v0, :cond_8

    .line 852
    iget v0, p0, Lep;->l:I

    const/16 v3, 0xfe

    if-gt v0, v3, :cond_7

    .line 853
    iget v0, p0, Lep;->l:I

    .line 854
    iget v3, p0, Lep;->l:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lep;->l:I

    .line 856
    iget-object v3, p0, Lep;->j:[Lep$a;

    array-length v3, v3

    if-lt v0, v3, :cond_6

    .line 857
    invoke-direct {p0}, Lep;->k()V

    .line 863
    :cond_6
    :goto_2
    iget-object v3, p0, Lep;->h:[I

    and-int/lit16 v2, v2, -0x100

    add-int/lit8 v4, v0, 0x1

    or-int/2addr v2, v4

    aput v2, v3, v1

    .line 869
    :goto_3
    new-instance v1, Lep$a;

    iget-object v2, p0, Lep;->j:[Lep$a;

    aget-object v2, v2, v0

    invoke-direct {v1, p2, v2}, Lep$a;-><init>(Ler;Lep$a;)V

    .line 870
    iget v2, v1, Lep$a;->d:I

    .line 871
    const/16 v3, 0x64

    if-le v2, v3, :cond_9

    .line 875
    invoke-direct {p0, v0, v1}, Lep;->a(ILep$a;)V

    goto :goto_0

    .line 860
    :cond_7
    invoke-direct {p0}, Lep;->g()I

    move-result v0

    goto :goto_2

    .line 865
    :cond_8
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 877
    :cond_9
    iget-object v2, p0, Lep;->j:[Lep$a;

    aput-object v1, v2, v0

    .line 879
    iget v0, v1, Lep$a;->d:I

    iget v1, p0, Lep;->f:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lep;->f:I

    goto :goto_0

    .line 895
    :cond_a
    iget v0, p0, Lep;->k:I

    if-lt v0, v1, :cond_3

    .line 896
    iput-boolean v5, p0, Lep;->o:Z

    goto :goto_1
.end method

.method private a(Lep$b;)V
    .locals 3

    .prologue
    .line 406
    iget v1, p1, Lep$b;->a:I

    .line 407
    iget-object v0, p0, Lep;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lep$b;

    .line 413
    iget v2, v0, Lep$b;->a:I

    if-ne v1, v2, :cond_0

    .line 432
    :goto_0
    return-void

    .line 423
    :cond_0
    const/16 v2, 0x1770

    if-le v1, v2, :cond_1

    .line 429
    const/16 v1, 0x40

    invoke-direct {p0, v1}, Lep;->f(I)Lep$b;

    move-result-object p1

    .line 431
    :cond_1
    iget-object v1, p0, Lep;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static d()Ler;
    .locals 1

    .prologue
    .line 493
    invoke-static {}, Les;->b()Les;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 928
    iput-boolean v1, p0, Lep;->o:Z

    .line 930
    iput-boolean v1, p0, Lep;->q:Z

    .line 936
    iget-object v0, p0, Lep;->h:[I

    .line 937
    array-length v3, v0

    .line 938
    add-int v0, v3, v3

    .line 943
    const/high16 v2, 0x10000

    if-le v0, v2, :cond_1

    .line 944
    invoke-direct {p0}, Lep;->f()V

    .line 1024
    :cond_0
    :goto_0
    return-void

    .line 948
    :cond_1
    new-array v2, v0, [I

    iput-object v2, p0, Lep;->h:[I

    .line 949
    add-int/lit8 v2, v0, -0x1

    iput v2, p0, Lep;->g:I

    .line 950
    iget-object v4, p0, Lep;->i:[Ler;

    .line 951
    new-array v0, v0, [Ler;

    iput-object v0, p0, Lep;->i:[Ler;

    move v2, v1

    move v0, v1

    .line 953
    :goto_1
    if-ge v2, v3, :cond_3

    .line 954
    aget-object v5, v4, v2

    .line 955
    if-eqz v5, :cond_2

    .line 956
    add-int/lit8 v0, v0, 0x1

    .line 957
    invoke-virtual {v5}, Ler;->hashCode()I

    move-result v6

    .line 958
    iget v7, p0, Lep;->g:I

    and-int/2addr v7, v6

    .line 959
    iget-object v8, p0, Lep;->i:[Ler;

    aput-object v5, v8, v7

    .line 960
    iget-object v5, p0, Lep;->h:[I

    shl-int/lit8 v6, v6, 0x8

    aput v6, v5, v7

    .line 953
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 968
    :cond_3
    iget v5, p0, Lep;->l:I

    .line 969
    if-nez v5, :cond_4

    .line 970
    iput v1, p0, Lep;->f:I

    goto :goto_0

    .line 974
    :cond_4
    iput v1, p0, Lep;->k:I

    .line 975
    iput v1, p0, Lep;->l:I

    .line 976
    iput-boolean v1, p0, Lep;->r:Z

    .line 980
    iget-object v6, p0, Lep;->j:[Lep$a;

    .line 981
    array-length v2, v6

    new-array v2, v2, [Lep$a;

    iput-object v2, p0, Lep;->j:[Lep$a;

    move v4, v1

    move v2, v0

    .line 982
    :goto_2
    if-ge v4, v5, :cond_a

    .line 983
    aget-object v0, v6, v4

    move-object v12, v0

    move v0, v2

    move-object v2, v12

    :goto_3
    if-eqz v2, :cond_9

    .line 984
    add-int/lit8 v3, v0, 0x1

    .line 985
    iget-object v7, v2, Lep$a;->a:Ler;

    .line 986
    invoke-virtual {v7}, Ler;->hashCode()I

    move-result v0

    .line 987
    iget v8, p0, Lep;->g:I

    and-int/2addr v8, v0

    .line 988
    iget-object v9, p0, Lep;->h:[I

    aget v9, v9, v8

    .line 989
    iget-object v10, p0, Lep;->i:[Ler;

    aget-object v10, v10, v8

    if-nez v10, :cond_5

    .line 990
    iget-object v9, p0, Lep;->h:[I

    shl-int/lit8 v0, v0, 0x8

    aput v0, v9, v8

    .line 991
    iget-object v0, p0, Lep;->i:[Ler;

    aput-object v7, v0, v8

    move v0, v1

    .line 983
    :goto_4
    iget-object v1, v2, Lep$a;->b:Lep$a;

    move-object v2, v1

    move v1, v0

    move v0, v3

    goto :goto_3

    .line 993
    :cond_5
    iget v0, p0, Lep;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lep;->k:I

    .line 994
    and-int/lit16 v0, v9, 0xff

    .line 995
    if-nez v0, :cond_8

    .line 996
    iget v0, p0, Lep;->l:I

    const/16 v10, 0xfe

    if-gt v0, v10, :cond_7

    .line 997
    iget v0, p0, Lep;->l:I

    .line 998
    iget v10, p0, Lep;->l:I

    add-int/lit8 v10, v10, 0x1

    iput v10, p0, Lep;->l:I

    .line 1000
    iget-object v10, p0, Lep;->j:[Lep$a;

    array-length v10, v10

    if-lt v0, v10, :cond_6

    .line 1001
    invoke-direct {p0}, Lep;->k()V

    .line 1007
    :cond_6
    :goto_5
    iget-object v10, p0, Lep;->h:[I

    and-int/lit16 v9, v9, -0x100

    add-int/lit8 v11, v0, 0x1

    or-int/2addr v9, v11

    aput v9, v10, v8

    .line 1012
    :goto_6
    new-instance v8, Lep$a;

    iget-object v9, p0, Lep;->j:[Lep$a;

    aget-object v9, v9, v0

    invoke-direct {v8, v7, v9}, Lep$a;-><init>(Ler;Lep$a;)V

    .line 1013
    iget-object v7, p0, Lep;->j:[Lep$a;

    aput-object v8, v7, v0

    .line 1014
    iget v0, v8, Lep$a;->d:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_4

    .line 1004
    :cond_7
    invoke-direct {p0}, Lep;->g()I

    move-result v0

    goto :goto_5

    .line 1009
    :cond_8
    add-int/lit8 v0, v0, -0x1

    goto :goto_6

    .line 982
    :cond_9
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    goto :goto_2

    .line 1019
    :cond_a
    iput v1, p0, Lep;->f:I

    .line 1021
    iget v0, p0, Lep;->e:I

    if-eq v2, v0, :cond_0

    .line 1022
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Internal error: count after rehash "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "; should be "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lep;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private f(I)Lep$b;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 325
    new-instance v0, Lep$b;

    add-int/lit8 v2, p1, -0x1

    new-array v3, p1, [I

    new-array v4, p1, [Ler;

    const/4 v5, 0x0

    move v6, v1

    move v7, v1

    move v8, v1

    invoke-direct/range {v0 .. v8}, Lep$b;-><init>(II[I[Ler;[Lep$a;III)V

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1031
    iput v1, p0, Lep;->e:I

    .line 1032
    iput v1, p0, Lep;->f:I

    .line 1033
    iget-object v0, p0, Lep;->h:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 1034
    iget-object v0, p0, Lep;->i:[Ler;

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1035
    iget-object v0, p0, Lep;->j:[Lep$a;

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1036
    iput v1, p0, Lep;->k:I

    .line 1037
    iput v1, p0, Lep;->l:I

    .line 1038
    return-void
.end method

.method private g()I
    .locals 6

    .prologue
    .line 1046
    iget-object v4, p0, Lep;->j:[Lep$a;

    .line 1047
    const v3, 0x7fffffff

    .line 1048
    const/4 v0, -0x1

    .line 1050
    const/4 v1, 0x0

    iget v5, p0, Lep;->l:I

    :goto_0
    if-ge v1, v5, :cond_2

    .line 1051
    aget-object v2, v4, v1

    .line 1053
    if-nez v2, :cond_1

    .line 1065
    :cond_0
    :goto_1
    return v1

    .line 1056
    :cond_1
    iget v2, v2, Lep$a;->d:I

    .line 1057
    if-ge v2, v3, :cond_3

    .line 1058
    const/4 v0, 0x1

    if-eq v2, v0, :cond_0

    move v0, v1

    .line 1050
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    goto :goto_0

    :cond_2
    move v1, v0

    .line 1065
    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2
.end method

.method private h()V
    .locals 2

    .prologue
    .line 1075
    iget-object v0, p0, Lep;->h:[I

    .line 1076
    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lep;->h:[I

    .line 1077
    const/4 v0, 0x0

    iput-boolean v0, p0, Lep;->p:Z

    .line 1078
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1081
    iget-object v0, p0, Lep;->j:[Lep$a;

    .line 1082
    if-nez v0, :cond_0

    .line 1083
    const/16 v0, 0x20

    new-array v0, v0, [Lep$a;

    iput-object v0, p0, Lep;->j:[Lep$a;

    .line 1087
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lep;->r:Z

    .line 1088
    return-void

    .line 1085
    :cond_0
    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lep$a;

    iput-object v0, p0, Lep;->j:[Lep$a;

    goto :goto_0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1091
    iget-object v0, p0, Lep;->i:[Ler;

    .line 1092
    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ler;

    iput-object v0, p0, Lep;->i:[Ler;

    .line 1093
    const/4 v0, 0x0

    iput-boolean v0, p0, Lep;->q:Z

    .line 1094
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1097
    iget-object v0, p0, Lep;->j:[Lep$a;

    .line 1098
    array-length v1, v0

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lep$a;

    iput-object v0, p0, Lep;->j:[Lep$a;

    .line 1099
    return-void
.end method


# virtual methods
.method public a(II)Ler;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 562
    if-nez p2, :cond_0

    invoke-virtual {p0, p1}, Lep;->d(I)I

    move-result v0

    .line 563
    :goto_0
    iget v2, p0, Lep;->g:I

    and-int/2addr v2, v0

    .line 564
    iget-object v3, p0, Lep;->h:[I

    aget v3, v3, v2

    .line 569
    shr-int/lit8 v4, v3, 0x8

    xor-int/2addr v4, v0

    shl-int/lit8 v4, v4, 0x8

    if-nez v4, :cond_2

    .line 571
    iget-object v4, p0, Lep;->i:[Ler;

    aget-object v2, v4, v2

    .line 572
    if-nez v2, :cond_1

    move-object v0, v1

    .line 591
    :goto_1
    return-object v0

    .line 562
    :cond_0
    invoke-virtual {p0, p1, p2}, Lep;->b(II)I

    move-result v0

    goto :goto_0

    .line 575
    :cond_1
    invoke-virtual {v2, p1, p2}, Ler;->a(II)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v0, v2

    .line 576
    goto :goto_1

    .line 578
    :cond_2
    if-nez v3, :cond_3

    move-object v0, v1

    .line 579
    goto :goto_1

    .line 582
    :cond_3
    and-int/lit16 v2, v3, 0xff

    .line 583
    if-lez v2, :cond_4

    .line 584
    add-int/lit8 v2, v2, -0x1

    .line 585
    iget-object v3, p0, Lep;->j:[Lep$a;

    aget-object v2, v3, v2

    .line 586
    if-eqz v2, :cond_4

    .line 587
    invoke-virtual {v2, v0, p1, p2}, Lep$a;->a(III)Ler;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 591
    goto :goto_1
.end method

.method public a(Ljava/lang/String;[II)Ler;
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 659
    iget-boolean v0, p0, Lep;->c:Z

    if-eqz v0, :cond_0

    .line 660
    sget-object v0, Lcom/fasterxml/jackson/core/util/InternCache;->a:Lcom/fasterxml/jackson/core/util/InternCache;

    invoke-virtual {v0, p1}, Lcom/fasterxml/jackson/core/util/InternCache;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 663
    :cond_0
    const/4 v0, 0x3

    if-ge p3, v0, :cond_2

    .line 664
    if-ne p3, v2, :cond_1

    aget v0, p2, v1

    invoke-virtual {p0, v0}, Lep;->d(I)I

    move-result v0

    .line 668
    :goto_0
    invoke-static {v0, p1, p2, p3}, Lep;->a(ILjava/lang/String;[II)Ler;

    move-result-object v1

    .line 669
    invoke-direct {p0, v0, v1}, Lep;->a(ILer;)V

    .line 670
    return-object v1

    .line 664
    :cond_1
    aget v0, p2, v1

    aget v1, p2, v2

    invoke-virtual {p0, v0, v1}, Lep;->b(II)I

    move-result v0

    goto :goto_0

    .line 666
    :cond_2
    invoke-virtual {p0, p2, p3}, Lep;->b([II)I

    move-result v0

    goto :goto_0
.end method

.method public a([II)Ler;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 613
    const/4 v2, 0x3

    if-ge p2, v2, :cond_2

    .line 614
    aget v1, p1, v0

    const/4 v2, 0x2

    if-ge p2, v2, :cond_1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lep;->a(II)Ler;

    move-result-object v0

    .line 637
    :cond_0
    :goto_1
    return-object v0

    .line 614
    :cond_1
    const/4 v0, 0x1

    aget v0, p1, v0

    goto :goto_0

    .line 616
    :cond_2
    invoke-virtual {p0, p1, p2}, Lep;->b([II)I

    move-result v2

    .line 618
    iget v0, p0, Lep;->g:I

    and-int/2addr v0, v2

    .line 619
    iget-object v3, p0, Lep;->h:[I

    aget v3, v3, v0

    .line 620
    shr-int/lit8 v4, v3, 0x8

    xor-int/2addr v4, v2

    shl-int/lit8 v4, v4, 0x8

    if-nez v4, :cond_4

    .line 621
    iget-object v4, p0, Lep;->i:[Ler;

    aget-object v0, v4, v0

    .line 622
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Ler;->a([II)Z

    move-result v4

    if-nez v4, :cond_0

    .line 629
    :cond_3
    and-int/lit16 v0, v3, 0xff

    .line 630
    if-lez v0, :cond_5

    .line 631
    add-int/lit8 v0, v0, -0x1

    .line 632
    iget-object v3, p0, Lep;->j:[Lep$a;

    aget-object v0, v3, v0

    .line 633
    if-eqz v0, :cond_5

    .line 634
    invoke-virtual {v0, v2, p1, p2}, Lep$a;->a(I[II)Ler;

    move-result-object v0

    goto :goto_1

    .line 626
    :cond_4
    if-nez v3, :cond_3

    move-object v0, v1

    .line 627
    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 637
    goto :goto_1
.end method

.method public b(II)I
    .locals 2

    .prologue
    .line 706
    .line 707
    ushr-int/lit8 v0, p1, 0xf

    xor-int/2addr v0, p1

    .line 708
    mul-int/lit8 v1, p2, 0x21

    add-int/2addr v0, v1

    .line 709
    iget v1, p0, Lep;->n:I

    xor-int/2addr v0, v1

    .line 710
    ushr-int/lit8 v1, v0, 0x7

    add-int/2addr v0, v1

    .line 711
    return v0
.end method

.method public b([II)I
    .locals 3

    .prologue
    const/4 v0, 0x3

    .line 717
    if-ge p2, v0, :cond_0

    .line 718
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 726
    :cond_0
    const/4 v1, 0x0

    aget v1, p1, v1

    iget v2, p0, Lep;->n:I

    xor-int/2addr v1, v2

    .line 727
    ushr-int/lit8 v2, v1, 0x9

    add-int/2addr v1, v2

    .line 728
    mul-int/lit8 v1, v1, 0x21

    .line 729
    const/4 v2, 0x1

    aget v2, p1, v2

    add-int/2addr v1, v2

    .line 730
    const v2, 0x1003f

    mul-int/2addr v1, v2

    .line 731
    ushr-int/lit8 v2, v1, 0xf

    add-int/2addr v1, v2

    .line 732
    const/4 v2, 0x2

    aget v2, p1, v2

    xor-int/2addr v1, v2

    .line 733
    ushr-int/lit8 v2, v1, 0x11

    add-int/2addr v1, v2

    .line 735
    :goto_0
    if-ge v0, p2, :cond_1

    .line 736
    mul-int/lit8 v1, v1, 0x1f

    aget v2, p1, v0

    xor-int/2addr v1, v2

    .line 738
    ushr-int/lit8 v2, v1, 0x3

    add-int/2addr v1, v2

    .line 739
    shl-int/lit8 v2, v1, 0x7

    xor-int/2addr v1, v2

    .line 735
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 742
    :cond_1
    ushr-int/lit8 v0, v1, 0xf

    add-int/2addr v0, v1

    .line 743
    shl-int/lit8 v1, v0, 0x9

    xor-int/2addr v0, v1

    .line 744
    return v0
.end method

.method public b(I)Lep;
    .locals 6

    .prologue
    .line 369
    new-instance v0, Lep;

    sget-object v1, Lcom/fasterxml/jackson/core/JsonFactory$Feature;->a:Lcom/fasterxml/jackson/core/JsonFactory$Feature;

    invoke-virtual {v1, p1}, Lcom/fasterxml/jackson/core/JsonFactory$Feature;->a(I)Z

    move-result v2

    iget v3, p0, Lep;->n:I

    sget-object v1, Lcom/fasterxml/jackson/core/JsonFactory$Feature;->c:Lcom/fasterxml/jackson/core/JsonFactory$Feature;

    invoke-virtual {v1, p1}, Lcom/fasterxml/jackson/core/JsonFactory$Feature;->a(I)Z

    move-result v4

    iget-object v1, p0, Lep;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lep$b;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lep;-><init>(Lep;ZIZLep$b;)V

    return-object v0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 393
    iget-object v0, p0, Lep;->a:Lep;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lep;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lep;->a:Lep;

    new-instance v1, Lep$b;

    invoke-direct {v1, p0}, Lep$b;-><init>(Lep;)V

    invoke-direct {v0, v1}, Lep;->a(Lep$b;)V

    .line 398
    iput-boolean v2, p0, Lep;->p:Z

    .line 399
    iput-boolean v2, p0, Lep;->q:Z

    .line 400
    iput-boolean v2, p0, Lep;->r:Z

    .line 402
    :cond_0
    return-void
.end method

.method public c(I)Ler;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 513
    invoke-virtual {p0, p1}, Lep;->d(I)I

    move-result v2

    .line 514
    iget v1, p0, Lep;->g:I

    and-int/2addr v1, v2

    .line 515
    iget-object v3, p0, Lep;->h:[I

    aget v3, v3, v1

    .line 520
    shr-int/lit8 v4, v3, 0x8

    xor-int/2addr v4, v2

    shl-int/lit8 v4, v4, 0x8

    if-nez v4, :cond_2

    .line 522
    iget-object v4, p0, Lep;->i:[Ler;

    aget-object v1, v4, v1

    .line 523
    if-nez v1, :cond_1

    .line 542
    :cond_0
    :goto_0
    return-object v0

    .line 526
    :cond_1
    invoke-virtual {v1, p1}, Ler;->a(I)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v0, v1

    .line 527
    goto :goto_0

    .line 529
    :cond_2
    if-eqz v3, :cond_0

    .line 533
    :cond_3
    and-int/lit16 v1, v3, 0xff

    .line 534
    if-lez v1, :cond_0

    .line 535
    add-int/lit8 v1, v1, -0x1

    .line 536
    iget-object v3, p0, Lep;->j:[Lep$a;

    aget-object v1, v3, v1

    .line 537
    if-eqz v1, :cond_0

    .line 538
    const/4 v0, 0x0

    invoke-virtual {v1, v2, p1, v0}, Lep$a;->a(III)Ler;

    move-result-object v0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 459
    iget-boolean v0, p0, Lep;->p:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(I)I
    .locals 2

    .prologue
    .line 695
    iget v0, p0, Lep;->n:I

    xor-int/2addr v0, p1

    .line 696
    ushr-int/lit8 v1, v0, 0xf

    add-int/2addr v0, v1

    .line 697
    ushr-int/lit8 v1, v0, 0x9

    xor-int/2addr v0, v1

    .line 698
    return v0
.end method

.method protected e(I)V
    .locals 3

    .prologue
    .line 1140
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Longest collision chain in symbol table (of size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lep;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") now exceeds maximum, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- suspect a DoS attack based on hash collisions"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
