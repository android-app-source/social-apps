.class public final Lud;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lux;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lud$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ltz;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/livevideo/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lud;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lud;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lud$a;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    sget-boolean v0, Lud;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 45
    :cond_0
    invoke-direct {p0, p1}, Lud;->a(Lud$a;)V

    .line 46
    return-void
.end method

.method synthetic constructor <init>(Lud$a;Lud$1;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lud;-><init>(Lud$a;)V

    return-void
.end method

.method public static a()Lud$a;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lud$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lud$a;-><init>(Lud$1;)V

    return-object v0
.end method

.method private a(Lud$a;)V
    .locals 4

    .prologue
    .line 55
    new-instance v0, Lud$1;

    invoke-direct {v0, p0, p1}, Lud$1;-><init>(Lud;Lud$a;)V

    iput-object v0, p0, Lud;->b:Lcta;

    .line 68
    new-instance v0, Lud$2;

    invoke-direct {v0, p0, p1}, Lud$2;-><init>(Lud;Lud$a;)V

    iput-object v0, p0, Lud;->c:Lcta;

    .line 84
    invoke-static {p1}, Lud$a;->b(Lud$a;)Luy;

    move-result-object v0

    .line 83
    invoke-static {v0}, Lva;->a(Luy;)Ldagger/internal/c;

    move-result-object v0

    .line 82
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lud;->d:Lcta;

    .line 89
    invoke-static {p1}, Lud$a;->b(Lud$a;)Luy;

    move-result-object v0

    .line 88
    invoke-static {v0}, Luz;->a(Luy;)Ldagger/internal/c;

    move-result-object v0

    .line 87
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lud;->e:Lcta;

    .line 91
    iget-object v0, p0, Lud;->b:Lcta;

    iget-object v1, p0, Lud;->c:Lcta;

    iget-object v2, p0, Lud;->d:Lcta;

    iget-object v3, p0, Lud;->e:Lcta;

    .line 93
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/livevideo/b;->a(Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 92
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lud;->f:Lcta;

    .line 99
    iget-object v0, p0, Lud;->f:Lcta;

    .line 100
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lud;->g:Lcta;

    .line 102
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 103
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lud;->g:Lcta;

    .line 104
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lud;->h:Lcta;

    .line 107
    iget-object v0, p0, Lud;->c:Lcta;

    .line 109
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 108
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lud;->i:Lcta;

    .line 110
    return-void
.end method


# virtual methods
.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lud;->h:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lud;->i:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public d()Lcom/twitter/android/livevideo/a;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lud;->f:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/livevideo/a;

    return-object v0
.end method
